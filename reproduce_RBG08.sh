#!/bin/bash

## HACKS:
## Define these environment variables if you wish:
## RGB08_N_THREADS=number of threads for faster compiling of GSL

if [ "x${RGB08_N_THREADS}" = "x" ]; then
    N_THREADS=1
else
    N_THREADS=${RGB08_N_THREADS}
fi

# Download data files:
if [ -r wmap_ilc_5yr_v3.fits ]; then
    s=$(sha512sum wmap_ilc_5yr_v3.fits|awk '{print $1}')
    if [ "X$s" = "Xc88f959cb5f37cf99fb910732992115b1b51968c4b7db75d5b03c96573c1a847b8124ed3a444cd20f45d9a6e46c9dc8f93d6372d3f03e3f9eea341073547b275" ]; then
        printf "wmap_ilc_5yr_v3.fits is OK\n"
    else
        mv -v wmap_ilc_5yr_v3.fits wmap_ilc_5yr_v3.fits.bak
        printf "Downloading WMAP-5yr data file as per footnote 1.\n"
        wget https://lambda.gsfc.nasa.gov/data/map/dr3/dfp/wmap_ilc_5yr_v3.fits
    fi
else
    printf "Downloading WMAP-5yr data file as per footnote 1.\n"
    wget https://lambda.gsfc.nasa.gov/data/map/dr3/dfp/wmap_ilc_5yr_v3.fits
fi

if [ -r wiener5yr_map.fits ]; then
    s=$(sha512sum wiener5yr_map.fits|awk '{print $1}')
    if [ "X$s" = "Xa1070dd78004365ffb7f0edf70cedaa1c8024b1fe9234aec7aab4eec936a765dda862c6fc0e8f9b25f37016788c96c1a555da239d88da7665f593b18ac6aed03" ]; then
        printf "wiener5yr_map.fits is OK\n"
    else
        mv -v wiener5yr_map.fits wiener5yr_map.fits.bak
        printf "Downloading WMAP-5yr data file as per footnote 2.\n"
        wget http://space.mit.edu/home/tegmark/wmap/wiener5yr_map.fits
    fi
else
    printf "Downloading WMAP-5yr data file as per footnote 2.\n"
    wget http://space.mit.edu/home/tegmark/wmap/wiener5yr_map.fits
fi

if [ -r wmap_kp2_r9_mask_3yr_v2.fits ]; then
    s=$(sha512sum wmap_kp2_r9_mask_3yr_v2.fits|awk '{print $1}')
    if [ "X$s" = "X918176c1f3b11f2520eeffbf79383ab484727440506aedacba7892c5a73a5dd2b93ec5f4153b320f0e5563b470b50c0cdb98f6f40c97d7c5d7557446c2be4d4f" ]; then
        printf "wmap_kp2_r9_mask_3yr_v2.fits is OK\n"
    else
        mv -v wmap_kp2_r9_mask_3yr_v2.fits wmap_kp2_r9_mask_3yr_v2.fits.bak
        printf "Downloading WMAP-5yr data file as per footnote 3.\n"
        wget http://lambda.gsfc.nasa.gov/data/map/dr2/ancillary/wmap_kp2_r9_mask_3yr_v2.fits
    fi
else
    printf "Downloading WMAP-5yr data file as per footnote 3.\n"
    wget http://lambda.gsfc.nasa.gov/data/map/dr2/ancillary/wmap_kp2_r9_mask_3yr_v2.fits
fi

printf "\n"

printf "A patched version of circles-0.3.2.1 software, as in footnote 7\n"
printf "of the original paper RBG08, is provided here\n"
printf "and will be configure and compiled.\n"

#Alternative:
#
#printf "Downloading circles-0.3.2.1 software as per footnote 7.\n"
#wget http://cosmo.torun.pl/GPLdownload/dodec/circles-0.3.2.1.tar.gz
#
# extract the software
#tar -xv -f circles-0.3.2.1.tar.gz
#cd circles-0.3.2.1

printf "This reproducibility setup is optimised for Debian derivative systems.\n"
printf "On other systems you will have to install standard software in other ways.\n"

HAVE_CFITSIO_DEV=$(dpkg -l |grep libcfitsio-dev)
HAVE_CFITSIO5=$(dpkg -l |grep libcfitsio5)

printf "Checking: \n"
printf "libcfitsio-dev: ${HAVE_CFITSIO_DEV}\n"
printf "libcfitsio5: ${HAVE_CFITSIO5}\n"
printf "If one or both of these are missing, then you might have to do:\n\n"
printf "sudo aptitude install libcfitsio-dev libcfitsio5\n\n"
printf "(Press Ctrl-C within 5 seconds if you wish to cancel this script.)\n"

sleep 5

#Configure and compile

## old gsl
if [ -r gsl-1.10.tar.gz ]; then
    s=$(sha512sum gsl-1.10.tar.gz|awk '{print $1}')
    if [ "X$s" = "X11ebd2f58cd367d020f0432f15b473dd911033b1f06d1fbfd762eaf870ffef04fe26fba6a9cabd34543a479ee3cc60fde0be5c23288641483d445bc4b52edaff" ]; then
        printf "gsl-1.10.tar.gz is OK\n"
    else
	printf "Downloading GSL-1.10\n"
        wget ftp://ftp.gnu.org/gnu/gsl/gsl-1.10.tar.gz
    fi
else
    printf "Downloading GSL-1.10\n"
    wget ftp://ftp.gnu.org/gnu/gsl/gsl-1.10.tar.gz
fi
tar -x -f gsl-1.10.tar.gz
mkdir -p local/lib local/include local/bin
PREFIX=$(/bin/pwd)/local
cd gsl-1.10
if !([ -r ./Makefile ] && [ -r ./cblas/Makefile ] ); then
    ./configure --prefix=${PREFIX}
fi
make -j${N_THREADS}
make install
cd ..


## newer cosmdist
COSMDISTTARGZ=cosmdist-0.3.2-d17caed.tar.gz
COSMDISTDIR=broukema-cosmdist-d17caed98cc6
if [ -r ${COSMDISTTARGZ} ]; then
    s=$(sha512sum ${COSMDISTTARGZ} |awk '{print $1}')
    if [ "X$s" = "Xbbe55a9e6f9e7a4b62e5d6bd32a64511d76328e43e65a2d422dc03c5003f7ea666942cdc7df24993e7e84ecc9fccbe85c18799e421e4bbc4199e7dee5e9d0cbc" ]; then
        printf "cosmdist.master.tar.gz is OK\n"
    else
	printf "Downloading cosmdist\n"
        wget -O ${COSMDISTTARGZ} https://bitbucket.org/broukema/cosmdist/get/master.tar.gz
    fi
else
	printf "Downloading cosmdist\n"
        wget -O ${COSMDISTTARGZ} https://bitbucket.org/broukema/cosmdist/get/master.tar.gz
fi
tar -x -f ${COSMDISTTARGZ}
mkdir -p local/lib local/include local/bin
PREFIX=$(/bin/pwd)/local
cd ${COSMDISTDIR}
if !([ -r ./Makefile ] ); then
    ./configure --prefix=${PREFIX}
fi
make
make install
cd ..



## circles-0.3.2.1
cd circles-0.3.2.1
LDFLAGS="-L${PREFIX}/lib ${LDFLAGS}"
CFLAGS="-I${PREFIX}/include ${CFLAGS}"
FFLAGS="-fcheck=bounds -Wall"

printf "LDFLAGS=${LDFLAGS}\n"
printf "CFLAGS=${CFLAGS}\n"
printf "FFLAGS=${FFLAGS}\n"

## If all the Makefile's exist, then assume that ./configure has been run.
## To force running ./configure again, either remove one of the Makefiles, or
## run ./configure by hand.
if !([ -r ./Makefile ] && [ -r ./astromisc/Makefile ] && \
	 [ -r ./isolat/Makefile ] && [ -r ./lib/Makefile ]); then
    ./configure LDFLAGS="${LDFLAGS}" CFLAGS="${CFLAGS}" FFLAGS="${FFLAGS}"
fi
make
#Return to the top directory
cd ..

#Analyse data


#What commands?
LD_LIBRARY_PATH=$(pwd)/local/lib ./circles-0.3.2.1/circles --spatial-correlation --use-unsmoothed --cmb_file_raw=wmap_ilc_5yr_v3.fits --mask --cmb_file_mask=wmap_kp2_r9_mask_3yr_v2.fits --verbose
