Git repository for reproducing the cosmic topology paper RBG08.

(C) 2019,2020 Boud Roukema GPL-3+ (version 3 or any later version at your choosing)

This repository is GPLv3+ licensed. The intention is to provide the 'circles'
source code, updated for reproducibility, together with a reproducibility
script.

The source code for the paper discussing the reproducibility of RBG08
[1] is available in a git repository [2] and will be submitted for
peer-review for publication in ReScience C [3].

See COPYING for details of the GPL-3 licence.

[1] RBG08:
https://arxiv.org/abs/0807.4260;
https://ui.adsabs.harvard.edu/abs/2008A%26A...492..657R

[2] https://github.com/broukema/RBG08_rep

[3] https://rescience.github.io
