/* 
   astromisc - library with miscellaneous GPL tools for astronomy research

   Copyright (C) 2004 Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

#include <termios.h>
#include <grp.h>
#include <pwd.h>
*/

#include <stdio.h>
#include <sys/types.h>
#include <argp.h>
#include "system.h"

#ifdef HAVE_ARGP_H
#  include <argp.h>
#endif  /* HAVE_ARGP_H */


#define EXIT_FAILURE 1

#if ENABLE_NLS
# include <libintl.h>
# define _(Text) gettext (Text)
#else
# define textdomain(Domain)
# define _(Text) Text
#endif
#define N_(Text) Text

#include "lib/astromisc.h"

char *xmalloc ();
char *xrealloc ();
char *xstrdup ();

#ifdef HAVE_ARGP_H
static error_t parse_opt (int key, char *arg, struct argp_state *state);
static void show_version (FILE *stream, struct argp_state *state);
#else
  static void show_version (FILE *stream);
#endif


/* argp option keys */
enum {DUMMY_KEY=129
      ,NOWARN_KEY
};

/* Option flags and variables.  These are initialized in parse_opt.  */

int want_verbose;		/* --verbose */
int want_no_warn;		/* --no-warn */
int want_faces;		        /* --faces */

#ifdef HAVE_ARGP_H
static struct argp_option options[] =
{
  { "verbose",     'v',           NULL,            0,
    N_("Print more information"), 0 },
  { "faces",     'f',           NULL,            0,
    N_("get face centres from input gallong, gallat, theta"), 0 },
  { "no-warn",     NOWARN_KEY,    NULL,            0,
    N_("Disable warnings"), 0 },
  { NULL, 0, NULL, 0, NULL, 0 }
};

/* The argp functions examine these global variables.  */
const char *argp_program_bug_address = "<boud at astro.uni.torun.pl>";
void (*argp_program_version_hook) (FILE *, struct argp_state *) = show_version;

static struct argp argp =
{
  options, parse_opt, N_("[FILE...]"),
  N_("library with miscellaneous GPL tools for astronomy research"),
  NULL, NULL, NULL
};
#endif  /* HAVE_ARGP_H */

int
main (int argc, char **argv)
{
  textdomain(PACKAGE);
#if HAVE_ARGP_H
  argp_parse(&argp, argc, argv, 0, NULL, NULL);
#else
  printf("Warning: no command line options since a recent glibc is not installed.\n");
#endif  /* HAVE_ARGP_H */

  if(want_faces){
    CVTDOD();  /* read in l, b, theta; output dodec face centres */
    exit(0);
  };

  CHKRAN();  /* call prob.f test subroutine */

  exit(0);

  CHKDOD();  /* call test program unit  testgendodec.f */

  CHKHUN();  /* check "hunt"/bsearch wrapper function  hunt_g() */
  
  CHKSORT(); /* check fortran wrapper to C routine qsort  
		(POSIX, ISO 9899...) */

  CHKFIND(); 
/* check findopt - subroutine to 
 find global min and max in 1D 
 periodic function, with smoothing and "HWHM" output as well */


  /* TODO: do the work */

  exit (0);
}

#ifdef HAVE_ARGP_H
/* Parse a single option.  */
static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  switch (key)
    {
    case ARGP_KEY_INIT:
      /* Set up default values.  */
      want_verbose = 0;
      want_no_warn = 0;
      want_faces = 0;
      break;
    case 'v':			/* --verbose */
      want_verbose = 1;
      break;
    case 'f':			/* --faces */
      want_faces = 1;
      break;
    case NOWARN_KEY:		/* --no-warn */
      want_no_warn = 1;
      break;

    case ARGP_KEY_ARG:		/* [FILE]... */
      /* TODO: Do something with ARG, or remove this case and make
         main give argp_parse a non-NULL fifth argument.  */
      break;

    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}
#endif  /* HAVE_ARGP_H */


/* Show the version number and copyright information.  */
static void
#ifdef HAVE_ARGP_H
show_version (FILE *stream, struct argp_state *state)
#else
     show_version (FILE *stream)
#endif  /* HAVE_ARGP_H */
{
#ifdef HAVE_ARGP_H
  (void) state;
#endif  /* HAVE_ARGP_H */
  /* Print in small parts whose localizations can hopefully be copied
     from other programs.  */
  fputs(PACKAGE" "VERSION"\n", stream);
  fprintf(stream, _("Written by %s.\n\n"), "Boud Roukema");
  fprintf(stream, _("Copyright (C) %s %s\n"), "2004", "Boud Roukema");
  fputs(_("\
This program is free software; you may redistribute it under the terms of\n\
the GNU General Public License.  This program has absolutely no warranty.\n"),
	stream);
}
