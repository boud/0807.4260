C     Copyright (C) 2004 B. Roukema et al. <shape-univ at astro.uni.torun.pl>
C     
C     This program is free software; you can redistribute it and/or modify
C     it under the terms of the GNU General Public License as published by
C     the Free Software Foundation; either version 2 of the License, or
C     (at your option) any later version.
C     
C     This program is distributed in the hope that it will be useful,
C     but WITHOUT ANY WARRANTY; without even the implied warranty of
C     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C     GNU General Public License for more details.
C     
C     You should have received a copy of the GNU General Public License
C     along with this program; if not, write to the Free Software
C     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
C  
C     See also http://www.gnu.org/licenses/gpl.html

      subroutine chkdod

      parameter(pi=3.14159265358979,pion5=pi/5.0,pi_180=pi/180.0)

      parameter(nface=12)
      real*4  xdod(nface),ydod(nface),zdod(nface)
c     -- orthogonal unit vectors to generate circle
      real*4  xdodp(nface,2),ydodp(nface,2),zdodp(nface,2) ! perpendiculars

c     --- test angles between face centres ---

c      call gendodec(0.0,90.0, 0.0, xdod,ydod,zdod, ! OK :)
c     _     xdodp,ydodp,zdodp)
      call gendodec(-70.0,33.0, 0.0, xdod,ydod,zdod,
     _     xdodp,ydodp,zdodp)
      print*,' '
      print*,'have called gendodec'
      print*,' '
      do ii=1,12
         print*,'vertex: ',ii,xdod(ii),ydod(ii),zdod(ii)
      enddo
      print*,' '

      do ii=1,11
         do jj=ii+1,12
            if(jj-ii.eq.1)write(6,'(a,i3,i3,6f6.2,a)')'===',ii,jj, 
     _           xdod(ii),ydod(ii),zdod(ii),
     _           xdod(jj),ydod(jj),zdod(jj),' ==='

            aa= angle(xdod(ii),ydod(ii),zdod(ii),
     _           xdod(jj),ydod(jj),zdod(jj)) /pi_180
            write(6,'(a,i3,i3,f8.2)')
     _           'ii,jj,aa=',ii,jj,aa
         enddo
      enddo


      print*,' '
      print*,' '
      print*,'orthonormality test'
      do ii=1,12
         x01= xdod(ii)*xdodp(ii,1)+ydod(ii)*ydodp(ii,1)
     _        + zdod(ii)*zdodp(ii,1)
         x02= xdod(ii)*xdodp(ii,2)+ydod(ii)*ydodp(ii,2)
     _        + zdod(ii)*zdodp(ii,2)


         x21= xdodp(ii,2)*xdodp(ii,1)+ydodp(ii,2)*ydodp(ii,1)
     _        + zdodp(ii,2)*zdodp(ii,1)

         x00= xdod(ii)*xdod(ii)+ydod(ii)*ydod(ii)+zdod(ii)*zdod(ii)
         x11= xdodp(ii,1)*xdodp(ii,1)+ydodp(ii,1)*ydodp(ii,1)
     _        +zdodp(ii,1)*zdodp(ii,1)
         x22= xdodp(ii,2)*xdodp(ii,2)+ydodp(ii,2)*ydodp(ii,2)
     _        +zdodp(ii,2)*zdodp(ii,2)

         write(6,'(a,i2,6f7.2)')'ii,x01,x02,x21,x00,x11,x22=',
     _        ii,x01,x02,x21,x00,x11,x22
      enddo


      print*,'There is also a face-to-face angle test but it is not'
      print*,'yet documented.'
      return


      print*,' '
      print*,' '
      print*,'face-to-face angle (alpha) test'

      th= 25.0 * pi_180 ! 25.0 degrees
      cth=cos(th)
      sth=sin(th)

      do ii=1,12
         print*,'ii=',ii
         do ialph=1,5
            alph=real(ialph)*20.0 *pi_180 ! units of 20 degrees
            calph= cos(alph)
            salph= sin(alph)

            v1x= cth* xdod(ii) 
     _           + sth*(calph* xdodp(ii,1) +salph*xdodp(ii,2))
            v1y= cth* ydod(ii) 
     _           + sth*(calph* ydodp(ii,1) +salph*ydodp(ii,2))
            v1z= cth* zdod(ii) 
     _           + sth*(calph* zdodp(ii,1) +salph*zdodp(ii,2))

c     --- face-to-face should be offset by pi if all goes as planned ---
            calph= cos(alph+pi+0.1)
            salph= sin(alph+pi+0.1)

            jj=mod(ii+6-1,12)+1

            v2x= cth* xdod(jj) 
     _           + sth*(calph* xdodp(jj,1) +salph*xdodp(jj,2))
            v2y= cth* ydod(jj) 
     _           + sth*(calph* ydodp(jj,1) +salph*ydodp(jj,2))
            v2z= cth* zdod(jj) 
     _           + sth*(calph* zdodp(jj,1) +salph*zdodp(jj,2))

            dx=v2x-v1x
            dy=v2y-v1y
            dz=v2z-v1z
            write(6,'(a,4f7.2)')'parallel vectors: ',alph,dx,dy,dz
         enddo
      enddo

      return
      end


      subroutine cvtdod ! convert (gllong,b,theta) to dodec face centres

      parameter(nface=12)
      real*4  xdod(nface),ydod(nface),zdod(nface)
c     -- orthogonal unit vectors to generate circle
      real*4  xdodp(nface,2),ydodp(nface,2),zdodp(nface,2) ! perpendiculars

c      write(6,'(a,$)')'Enter l,b,theta: '

      do while(1+1.eq.2)
         read(5,*,end=777,err=777)glong,glat,theta ! no error warnings

         call gendodec(glong,glat,theta, xdod,ydod,zdod,
     _        xdodp,ydodp,zdodp)
         
c     ---- i=1,1 
c         do i=1, 12 !6               !
         do i=1,12 !6               !
            call xyz_rda(xdod(i),ydod(i),zdod(i),rr,dd,aa)
            aa = aa*15.0        ! hours to degrees
            print*,aa,dd
         enddo
      enddo

 777  continue

      return
      end

