C     Copyright (C) 2004 B. Roukema et al. <shape-univ at astro.uni.torun.pl>
C     
C     This program is free software; you can redistribute it and/or modify
C     it under the terms of the GNU General Public License as published by
C     the Free Software Foundation; either version 2 of the License, or
C     (at your option) any later version.
C     
C     This program is distributed in the hope that it will be useful,
C     but WITHOUT ANY WARRANTY; without even the implied warranty of
C     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C     GNU General Public License for more details.
C     
C     You should have received a copy of the GNU General Public License
C     along with this program; if not, write to the Free Software
C     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
C  
C     See also http://www.gnu.org/licenses/gpl.html

c     Generate a dodecahedron.
c     
c     Given an input position on the sphere 
c     and a rotation in the range from 0 degrees to 72 degrees, 
c     GENerate 12 output positions (normally including the initial
c     position as one of these) corresponding to the 12 face centres of
c     a regular DODECahedron, in cartesian coordinates.

c     See 
c     http://mathworld.wolfram.com/Dodecahedron.html 
c     http://mathworld.wolfram.com/Icosahedron.html 
c     for more on the 12 dodecahedral faces, whose centres are the
c     12 icosahedral vertices.

      subroutine gendodec(gall,galb, theta, ! INPUTS
     _     xdod,ydod,zdod, ! OUTPUTS
     _     xdodp,ydodp,zdodp)
C     INPUTS:
      real*4  gall,galb         ! input position
      real*4  theta             ! in degrees from 0 to 72 degrees

C     OUTPUTS:
      parameter(nface=12)
c     -- centre of each face --
      real*4  xdod(nface),ydod(nface),zdod(nface)
c     -- orthogonal unit vectors to generate circle
      real*4  xdodp(nface,2),ydodp(nface,2),zdodp(nface,2) ! perpendiculars

C     INTERNAL:
      real*4     cheight        ! height in Z dirn of circle of face centres
      real*4     cradius        ! radius of circle of face centres
      real*4     tpion5          ! 72 degrees
      real*4     x1(nface),y1(nface),z1(nface)
      real*4     x1p(nface,2),y1p(nface,2),z1p(nface,2)

      parameter(pi=3.14159265358979,
     _     tpion5=2.0*pi/5.0,pi_180=pi/180.0)

      integer*4  ifirst

      data  ifirst/0/
      save  ifirst, cheight,cradius, x1,y1,z1,x1p,y1p,z1p


      if(ifirst.ne.12345)then
c         ifirst=12345
         
         cheight= 1.0/sqrt(5.0)
         cradius= 2.0*cheight
c     -- "north pole" and "south pole" --
c         do ii=1,12,11
         do ii=1,7,6
            x1(ii)=0.0
            y1(ii)=0.0

            z1p(ii,1)= 0.0
            z1p(ii,2)= 0.0


            if(ii.eq.1)then
               z1(ii)=1.0
               x1p(ii,1)=1.0
               y1p(ii,1)=0.0
               x1p(ii,2)=0.0
               y1p(ii,2)=1.0
            else
               z1(ii)=-1.0
               x1p(ii,1)=-1.0
               y1p(ii,1)=0.0
               x1p(ii,2)=0.0
               y1p(ii,2)=-1.0
            endif
         enddo
      endif !       if(ifirst.ne.12345)then


c     -- upper and lower intermediate faces --
      do index=2,11
         if(index.le.6)then
            ii=index
         else
c     --- (7,8,9,10,11 -> 11,12,8,9,10) ---
            ii= mod(index-7+4-1, 5) +8
         endif

         if(ifirst.ne.12345)then
            if(index.le.6)then
               z1(ii)= cheight
               z1p(ii,1)= -cradius
            else
               z1(ii)= -cheight
               z1p(ii,1)= cradius
            endif
            
            z1p(ii,2)=0.0
         endif

c     ---- following depends on theta, so needs to be recalculated eac
c     time ----

c     --- find angle in x-y plane
         if(index.le.6)then
            aa= real(index-2)*tpion5 + theta*pi_180
         else
            aa= (real(index-7)+0.5)*tpion5 + theta*pi_180
         endif
c     print*,'ii,aa=',ii,aa
         x1(ii)=cradius*cos(aa)
         y1(ii)=cradius*sin(aa)
c     print*,'x1(ii),y1(ii)=',x1(ii),y1(ii)

c     --- same x-y position
         x1p(ii,1)=cheight*cos(aa)
         y1p(ii,1)=cheight*sin(aa)
c     --  same x-y position + pi/2
         x1p(ii,2)= -sin(aa)
         y1p(ii,2)= cos(aa)
         
      enddo

      if(ifirst.ne.12345)then
         ifirst=12345
      endif



c      do ii=1,12
c         xdod(ii)=x1(ii)
c         ydod(ii)=y1(ii)
c         zdod(ii)=z1(ii)
c      enddo 
c      return !! test only !!
         

      do ii=1,12
c         print*,'gendodec: x1(ii)...=',x1(ii),y1(ii),z1(ii)
         call nprotcart(gall,galb, x1(ii),y1(ii),z1(ii),
     _        xdod(ii),ydod(ii),zdod(ii))

         do jj=1,2
            call nprotcart(gall,galb, 
     _           x1p(ii,jj),y1p(ii,jj),z1p(ii,jj),
     _           xdodp(ii,jj),ydodp(ii,jj),zdodp(ii,jj))
         enddo

c         print*,'gendodec: xdod(ii)...=',xdod(ii),ydod(ii),zdod(ii)
      enddo
      
      return
      end

