c      program testang4D
      subroutine testang4D
      
      logical*4  dodub
      real*8     x1d,y1d,z1d, x2d,y2d,z2d
      real*8     w1d,w2d, chid,thetad,phid, dthetad,  dd, rd

      real*8     angle4D8,sinhd,coshd,sinhinvd
      external   angle4D8,sinhd,coshd,sinhinvd

      data w1,x1,y1,z1, w2,x2,y2,z2/ 
c     _     0.0, 1.0,0.0,0.0, 
     _     0.0, -30.0,-24.6,110.0,
     _     0.0, -20.0,-37.0,23.0/

      dodub=.true. ! .false. ! 

         x1d=dble(x1)
         y1d=dble(y1)
         z1d=dble(z1)
         w1d= sqrt(1.0d0 + x1d*x1d + y1d*y1d + z1d*z1d)

         x2d=dble(x2)
         y2d=dble(y2)
         z2d=dble(z2)
         w2d= sqrt(1.0d0 + x2d*x2d + y2d*y2d + z2d*z2d)

c     --- test 3-d euclidean distance --
      print*,'Euclidean test'
      k=0
c      aa= angle4D(k, dodub,w1,x1,y1,z1, w2,x2,y2,z2)      
      aa= real( angle4D8(k, w1d,x1d,y1d,z1d, w2d,x2d,y2d,z2d) )

      print*,'aa=',aa
      xx= x1-x2
      yy= y1-y2
      zz= z1-z2
      dd= sqrt(xx*xx+yy*yy+zz*zz)
      print*,'aa,dd=',aa,dd

c     --- hyperbolic test ---
      k=-1
      print*,'== hyperbolic test'
      
      print*,'radial'
      dd= sqrt(x1d*x1d+y1d*y1d+z1d*z1d)
c      r=sinh(2.0*log(dd + sqrt(dd*dd+1.0))) !log(dd + sqrt(dd*dd+1))) = arcsinh
      rd=sinhd(2.0*sinhinvd(dd)) /dd
      print*,'rd=',rd

      x2d=rd*x1d
      y2d=rd*y1d
      z2d=rd*z1d
      w1d=sqrt(1.0d0 + dd*dd)
      w2d=sqrt(1.0d0 + x2d*x2d+y2d*y2d+z2d*z2d)
      print*,'w1d,w2d=',w1d,w2d

c      aa= angle4D(k,dodub, w1,x1,y1,z1, w2,x2,y2,z2) 
      aa= real( angle4D8(k, w1d,x1d,y1d,z1d, w2d,x2d,y2d,z2d) )

      print*,'aa,sinhinvd(dd)=',aa,sinhinvd(dd)

      print*,'== tangential'
      thetad=2.345
      phid=-5.26
      chid=3.0

C     ---- WARNING funny convention in theta (not astronomers' convention) ---
      w1d= coshd(chid)
      x1d= sinhd(chid)*sin(thetad)*sin(phid)
      y1d= sinhd(chid)*sin(thetad)*cos(phid)
      z1d= sinhd(chid)*cos(thetad)

      dthetad=0.003   
      thetad=thetad+ dthetad
      w2d=w1d
      x2d= sinhd(chid)*sin(thetad)*sin(phid)
      y2d= sinhd(chid)*sin(thetad)*cos(phid)
      z2d= sinhd(chid)*cos(thetad)
      
c      aa= angle4D(k,dodub, w1,x1,y1,z1, w2,x2,y2,z2) 
      aa= real( angle4D8(k, w1d,x1d,y1d,z1d, w2d,x2d,y2d,z2d) )
      print*,'aa,sinh(chid)*dthetad=',aa,sinh(chid)*dthetad

      end

      real*4  function sinh(x)
      sinh= 0.5*(exp(x)-exp(-x))
      return
      end

      real*4  function sinhinv(x)
      sinhinv= log(x + sqrt(x*x+1.0))
      return
      end

      real*4  function cosh(x)
      cosh= 0.5*(exp(x)+exp(-x))
      return
      end

      real*8  function sinhd(x)
      real*8  x
      sinhd= 0.5d0*(exp(x)-exp(-x))
      return
      end

      real*8  function sinhinvd(x)
      real*8  x
      sinhinvd= log(x + sqrt(x*x+1.0))
      return
      end

      real*8  function coshd(x)
      real*8  x
      coshd= 0.5d0*(exp(x)+exp(-x))
      return
      end


c      real*4     function angle4D(k, dodub, w1,x1,y1,z1, w2,x2,y2,z2)
      real*8  function angle4D8(k, w1d,x1d,y1d,z1d, w2d,x2d,y2d,z2d)

C     !!! WARNING !! MUST be declared in calling routines !!

c     ==== calculates "angle" between these two vectors at 
C     the origin (0,0,0,0) in  R^4 ====
C
C     INPUTS:
      integer*4  k  ! only the sign of k is used, not its amplitude !
C     k > 0 positive curvature
C     k = 0 flat space 
C     k < 0 negative curvature, where  w  is the odd variable out
C
      logical*4  r4,dodub          ! internally use real*8 ? .true./.false.

c      real*4     x1,y1,z1, x2,y2,z2
c      real*4     w1,w2

      real*8     x1d,y1d,z1d, x2d,y2d,z2d
      real*8     w1d,w2d

C     INTERNAL:
c      real*4  signk

      real*8     r1sqd,r2sqd,r1d,r2d,xxd,yyd,zzd
      real*8     dotd_loc,costhd,costh2_1d

      real*8  signkd

      real*8  tolerd !,toler_cos

c      parameter(toler=1e-4,tolerd=1d-6) !,toler_cos=1d-8)
      parameter(toler=1e-4,tolerd=1d-8) ! tolerd=1d-8, 28/04/00

      logical*4  test

      data r4,dodub/.false.,.true./
      data test/.false./ !.true./

      if(r4.and.dodub)then
         w1d=dble(w1)
         x1d=dble(x1)
         y1d=dble(y1)
         z1d=dble(z1)

         w2d=dble(w2)
         x2d=dble(x2)
         y2d=dble(y2)
         z2d=dble(z2)
      endif


      if(.not.(dodub))then
         if(k.gt.0)then
            signk=1.0
         elseif(k.lt.0)then
            signk=-1.0
         else
c     signk=0.0
            xx= x1-x2
            yy= y1-y2
            zz= z1-z2
            angle4D= sqrt(xx*xx+yy*yy+zz*zz)
            return
         endif
      else
         if(k.gt.0)then
            signkd=1.0
         elseif(k.lt.0)then
            signkd=-1.0
         else
c     signk=0.0
            xxd= x1d-x2d
            yyd= y1d-y2d
            zzd= z1d-z2d
            if(r4)then
               angle4D= real( sqrt(xxd*xxd+yyd*yyd+zzd*zzd) )
            else
               angle4D8= sqrt(xxd*xxd+yyd*yyd+zzd*zzd) 
            endif
            return
         endif
      endif

      if(.not.(dodub))then
         r1sq= signk* w1*w1 + x1*x1+y1*y1+z1*z1
         r2sq= signk* w2*w2 + x2*x2+y2*y2+z2*z2
      else
         r1sqd= signkd* w1d*w1d + x1d*x1d+y1d*y1d+z1d*z1d
         r2sqd= signkd* w2d*w2d + x2d*x2d+y2d*y2d+z2d*z2d
      endif

      if(k.lt.0)then
         if(.not.(dodub))then
            if(abs(r1sq+1.0).gt.toler)then
               print*,'angle4D WARNING: not on H^3 '
               print*,'k,w1,x1,y1,z1,r1sq=',k,w1,x1,y1,z1,r1sq

               if(abs(r1sq).lt.toler)then
                  print*,'angle4D WARNING: on null light cone'
                  print*,'k,w1,x1,y1,z1,r1sq=',k,w1,x1,y1,z1,r1sq
                  angle4D=-9e9
                  return
               endif
               if(r1sq.gt.toler)then
                  r1=sqrt(r1sq)
               endif
            else
               r1=sqrt(-r1sq)
            endif

            if(abs(r2sq+1.0).gt.toler)then
               print*,'angle4D WARNING: not on H^3 '
               print*,'k,w2,x2,y2,z2,r2sq=',k,w2,x2,y2,z2,r2sq

               if(abs(r2sq).lt.toler)then
                  print*,'angle4D WARNING: on null light cone'
                  print*,'k,w2,x2,y2,z2,r2sq=',k,w2,x2,y2,z2,r2sq
                  angle4D=-9e9
                  return
               endif
               if(r2sq.gt.toler)then
                  r2=sqrt(r2sq)
               endif
            else
               r2=sqrt(-r2sq)
            endif
         else                   ! dodub=.true.
            if(abs(r1sqd+1.0d0).gt.tolerd)then
               print*,'angle4D WARNING: not on H^3 '
               print*,'k,w1,x1,y1,z1,r1sqd=',k,w1,x1,y1,z1,r1sqd

               if(abs(r1sqd).lt.tolerd)then
                  print*,'angle4D WARNING: on null light cone'
                  print*,'k,w1,x1,y1,z1,r1sqd=',k,w1,x1,y1,z1,r1sqd
                  angle4D8=-9d9
                  return
               endif
               if(r1sqd.gt.tolerd)then
                  r1d=sqrt(r1sqd)
               endif
            else
               r1d=sqrt(-r1sqd)
            endif

            if(abs(r2sqd+1.0d0).gt.tolerd)then
               print*,'angle4D WARNING: not on H^3 '
               print*,'k,w2,x2,y2,z2,r2sqd=',k,w2,x2,y2,z2,r2sqd

               if(abs(r2sqd).lt.tolerd)then
                  print*,'angle4D WARNING: on null light cone'
                  print*,'k,w2,x2,y2,z2,r2sqd=',k,w2,x2,y2,z2,r2sqd
                  angle4D8=-9d9
                  return
               endif
               if(r2sqd.gt.tolerd)then
                  r2d=sqrt(r2sqd)
               endif
            else
               r2d=sqrt(-r2sqd)
            endif
         endif

      elseif(k.ge.0)then
         if(.not.(dodub))then
            if(abs(r1sq).lt.toler)then
               print*,'angle4D WARNING: too close to origin'
               print*,'k,w1,x1,y1,z1,r1sq=',k,w1,x1,y1,z1,r1sq
            endif
            r1=sqrt(r1sq)

            if(abs(r2sq).lt.toler)then
               print*,'angle4D WARNING: too close to origin'
               print*,'k,w2,x2,y2,z2,r2sq=',k,w2,x2,y2,z2,r2sq
            endif
            r2=sqrt(r2sq)
         else
            if(abs(r1sqd).lt.tolerd)then
               print*,'angle4D WARNING: too close to origin'
               print*,'k,w1,x1,y1,z1,r1sqd=',k,w1,x1,y1,z1,r1sqd
            endif
            r1d=sqrt(r1sqd)

            if(abs(r2sqd).lt.tolerd)then
               print*,'angle4D WARNING: too close to origin'
               print*,'k,w2,x2,y2,z2,r2sqd=',k,w2,x2,y2,z2,r2sqd
            endif
            r2d=sqrt(r2sqd)
         endif
      endif

      if(.not.(dodub))then
         dot= signk* w1*w2 + x1*x2+y1*y2+z1*z2 ! dot product
         costh = dot/r1/r2

         if(test)print*,'signk,dot,r1,r2=',signk,dot,r1,r2
      else
         dotd_loc= signkd* w1d*w2d + x1d*x2d+y1d*y2d+z1d*z2d ! dot product
         costhd = dotd_loc/r1d/r2d

         if(test)print*,'signk,dot,r1,r2=',signkd,dotd_loc,r1d,r2d
      endif

c      if(k.eq.0)then
c         angle4D = costh   !! experimental
c
c      else

      if(.not.(dodub))then
         if(k.gt.0)then
            if(costh.lt.-1.0)costh=-1.0
            if(costh.gt.1.0)costh=1.0

            angle4D= acos(costh)

         elseif(k.lt.0)then
            costh2_1= costh*costh -1.0
            if(costh2_1.lt.toler)print*,
     _           'angle4D WARNING costh2_1=',costh2_1
            xx= costh + sqrt(costh2_1) 
            if(xx.gt.toler)print*,'angle4D WARNING xx=',xx
            angle4D= - log( - xx )
         endif
      else
         if(k.gt.0)then
            if(costhd.lt.-1.0d0)costhd=-1.0d0
            if(costhd.gt.1.0d0)costhd=1.0d0

            if(r4)then
               angle4D= real( acos(costhd) )
            else
               angle4D8= acos(costhd)
            endif

         elseif(k.lt.0)then
            costh2_1d= costhd*costhd -1.0d0
            if(costh2_1d.lt.tolerd)then
               print*,
     _           'angle4D WARNING costh2_1d=',costh2_1d
               print*,'k,w1d,x1d,y1d,z1d, w2d,x2d,y2d,z2d='
               print*,k,w1d,x1d,y1d,z1d, w2d,x2d,y2d,z2d
               print*,'r1sqd,r2sqd,r1d,r2d='
               print*,r1sqd,r2sqd,r1d,r2d
               print*,'dotd_loc,costhd=',dotd_loc,costhd
c               angle4D8 = -99.9 !! test only !!
c               return           !! test only !!
            endif
            xxd= costhd + sqrt(costh2_1d) 
            if(xxd.gt.tolerd)then
               print*,'angle4D WARNING xxd=',xxd
               print*,'costhd,costh2_1d=',costhd,costh2_1d
            endif


            if(r4)then            
               angle4D= real( - log( - xxd ) )
            else
               angle4D8= - log( - xxd ) 
            endif
         endif
      endif

      return

      end






