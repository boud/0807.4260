C     Copyright (C) 2003 B. Roukema et al. <shape-univ at astro.uni.torun.pl>
C     
C     This program is free software; you can redistribute it and/or modify
C     it under the terms of the GNU General Public License as published by
C     the Free Software Foundation; either version 2 of the License, or
C     (at your option) any later version.
C     
C     This program is distributed in the hope that it will be useful,
C     but WITHOUT ANY WARRANTY; without even the implied warranty of
C     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C     GNU General Public License for more details.
C     
C     You should have received a copy of the GNU General Public License
C     along with this program; if not, write to the Free Software
C     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
C  
C     See also http://www.gnu.org/licenses/gpl.html

c       ==== this file holds some basic, first principles, not necessarily
c       computationally efficient matrix operations =====
c     maybe you should try the BLAS library.

c        program testmat_8
        subroutine testmat_8
c       ==== tests matrix routines ====
        parameter(nmat=3)
        real*8   mat(nmat,nmat),inv(nmat,nmat),test(nmat,nmat)
        real*8   det_8,cof_8

        mat(1,1)=1.0
        mat(1,2)=-4.0
        mat(2,2)=2.0
        mat(1,3)=3.4
        mat(2,3)=9.0
        mat(3,1)=1.0
        mat(3,3)=2.0
c        mat(3,4)=7.3
c        mat(4,1)=-2.3
c        mat(4,3)=6.7
c        mat(4,4)=1.2

        print*,'matrix = '
        call printmat_8(mat,nmat,nmat)
        lev=0
c        print*,'det_8= ',det_8(mat,nmat)
        print*,'end of first det_8 calcn'

        call invert_8(mat,nmat,inv)
        print*,'inverse = '
        call printmat_8(inv,nmat,nmat)

        call mult_8(mat,inv,test,nmat,nmat,nmat,nmat)
        print*,' '
        print*,' product of the two is: '
        call printmat_8(test,nmat,nmat)
        end


        subroutine printmat_8(mat,m,n)
c       ============================
        integer*4  m,n
        real*8     mat(n,m)

        do i=1,m
            write(6,'(10g9.2)')(mat(j,i),j=1,n)
        enddo
        return
        end

        real*8  function det_8(mat,n)
c       ==========================
        integer*4       n
        parameter(nmat=3)
        real*8      mat(nmat,nmat)
c        automatic j
        real*8      cof_8

        lev=lev+1
c        print*,'Got to level ',lev
        if(n.eq.1)then
            det_8=dble(mat(1,1))
c           ---- test line ---
c            print*,'det_8 of 1x1 is: ',mat(1,1)
        else
            det_8=0.0d0
            do j=1,n
                det_8=det_8+mat(j,1)*cof_8(mat,n,1,j)
            enddo
        endif

        lev=lev-1
        return
        end


        real*8  function cof_8(mat,n,i,j)
c       ================================
        integer*4  n,i,j
        parameter(nmat=3)
        real*8  mat(nmat,nmat)
c        automatic ia,ja
c        automatic adj
        real*8 adj(nmat,nmat)
        real*8 det_8

c       ==== calculate cofactor A_ij= (-1)^{i+j} * det_8 M_ij ====
        do ia=1,i-1
            do ja=1,j-1
                adj(ja,ia)=mat(ja,ia)
            enddo
            do ja=j,n-1
                adj(ja,ia)=mat(ja+1,ia)
            enddo
        enddo
        do ia=i,n-1
            do ja=1,j-1
                adj(ja,ia)=mat(ja,ia+1)
            enddo
            do ja=j,n-1
                adj(ja,ia)=mat(ja+1,ia+1)
            enddo
        enddo
        if(2*((i+j)/2).eq.i+j)then
            cof_8=det_8(adj,n-1)
        else
            cof_8=-1.0*det_8(adj,n-1)
        endif

        return
        end


        subroutine invert_8(mat,n,inv)
c       ============================
        integer*4       n
        parameter(nmat=3)
        real*8    mat(nmat,nmat),inv(nmat,nmat), x
        real*8    det_8,cof_8

        x=det_8(mat,n)
        if(x.eq.0.0)then
           print*,'ERROR: determinant zero. Null matrix output.'
        else
           if(n.eq.1)then
              inv(1,1)=1.0/mat(1,1)
           else
              do i=1,n
                 do j=1,n
                    inv(j,i)=cof_8(mat,n,j,i)/x
c     ----   transpose of adjugate matrix for testing only ----
c     -             inv(j,i)=cof_8(mat,n,i,j)
c     -----
                 enddo
              enddo
           endif
        endif

        return
        end



        subroutine mult_8(a,b,c,ma,na,mb,nb)
c       ==== multiplies two matrices with ma, mb rows, na, nb columns;
c       with rows being the second fortran index and cols the first ====

        integer*4       ma,na,mb,nb
        real*8  a(na,ma),b(nb,mb),c(nb,ma)

c        print*,'mult: a,b'
c        call printmat(a,ma,na)
c        print*,'---------'
c        call printmat(b,mb,nb)
c        print*,'-------- '
        if(na.ne.mb)stop "matrix sizes don't match!!!!"

        do i=1,ma
            do j=1,nb
                c(j,i)=0.0
                do index=1,na
                    c(j,i)=c(j,i)+a(index,i)*b(j,index)
                enddo
            enddo
        enddo
c        print*,'mult: c'
c        call printmat(c,nb,ma)
c        print*,'------------end of mult:'

        return
        end
