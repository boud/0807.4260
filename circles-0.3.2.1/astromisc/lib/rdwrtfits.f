C Copyright (C) 2003 Boud Roukema et al (shape-univ at astro.uni.torun.pl)
C 
C This program is free software; you can redistribute it and/or modify
C it under the terms of the GNU General Public License as published by
C the Free Software Foundation; either version 2 of the License, or
C (at your option) any later version.
C 
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C 
C You should have received a copy of the GNU General Public License
C along with this program; if not, write to the Free Software
C Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
C 
C See also http://www.gnu.org/licenses/gpl.html

C     Warning for wrtdisk: declare values() as, say, 2880 bytes
C     more than strictly necessary to avoid memory overwrite.

c     !!!! WARNING: these assume SIGNED integers; problem for > maxint/2

        subroutine rddisk(flnm,nhblocks,header,naxis1,naxis2,
     _                                              nvalues,values)
c       - read header and data in   flnm , returning the header and
c       values to the calling routine


c        integer*4           maxnvalues
c        parameter (maxnvalues=2156736)

        character*100        flnm
        character*80        header(108)
        real*4              values(*)
        byte                byteblock(2880), swbyte
        integer*2           int2block(1440)
        integer*4           int4block(720)
        real*4              real4block(720) ! added July 1998  nbitpix=-32
        character*80        stringblock(36)
        logical*1           eohead
        integer*4           nvalues,naxis1,naxis2

        equivalence (byteblock(1),int2block(1))
        equivalence (byteblock(1),int4block(1))
        equivalence (byteblock(1),real4block(1))
        equivalence (byteblock(1),stringblock(1)(1:1))

        open(unit=23,file=flnm,access='direct',form='unformatted',
     _       status='old',
     _       recl=2880,err=199)
c23456

c       ---- reader up to 10 header blocks looking for 'END   ' statement
c       to signify end of header and report error otherwise;
c       evaluate  nhblocks.

        eohead=.false.
        nhblocks=0
        naxis1=0
c                naxis2 set to 1 in case of 1d images
        naxis2=1
        bscale=1.0
        bzero=0.0
        nbitpix=16

        do 110 while ((nhblocks.lt.20).and.(.not.eohead))
            nhblocks=nhblocks +1
            read(23,rec=nhblocks) byteblock
            if(nhblocks.eq.3)then
               print*,'rddisk Warning: no more header blocks '//
     _              'will be saved.'
               print*,'header(108) would need to be resized.'
               print*,'Reading header block #: ',nhblocks
            endif

            do 120 i=1,36
               if(nhblocks.le.3)then
                  header((nhblocks-1)*36 +i)(1:80)=stringblock(i)(1:80)
               endif
c                                           12345678
                if (stringblock(i)(1:8).eq.'NAXIS1  ') then
                    naxis1=nint( head_val(stringblock(i)(9:80)))
                    end if
                if (stringblock(i)(1:8).eq.'NAXIS2  ') then
                    naxis2=nint(head_val(stringblock(i)(9:80)))
                    end if
                if (stringblock(i)(1:8).eq.'BSCALE  ') then
                    bscale= head_val(stringblock(i)(9:80))
                    end if
                if (stringblock(i)(1:8).eq.'BZERO   ') then
                    bzero=head_val(stringblock(i)(9:80))
                    end if
                if (stringblock(i)(1:8).eq.'BITPIX  ') then
                    nbitpix=nint(head_val(stringblock(i)(9:80)))
                    end if

                if (stringblock(i)(1:8).eq.'END     ') then
c                                           12345678
                    eohead=.true.
                    end if
120         continue
110     continue

        if(.not.eohead)write(6,*)'Warning:End of header not found.'

        if((nbitpix.ne.16).and.(nbitpix.ne.32)
     _       .and.(nbitpix.ne.8).and.(nbitpix.ne.-32)) then
            print*,'Invalid value of BITPIX: ',nbitpix
            print*,'Will try reading with BITPIX=16'
            nbitpix=16
        end if

c       ---- define  nvalues as  naxis1*naxis2  and check this is
c       allowed for;  read in  nvalues  values.----------
        print*,'nhblocks= ',nhblocks

        nvalues=naxis1*naxis2
        if (nvalues.eq.0) write(6,*)'Warning: naxis1*naxis2=0.'

        temp=(nvalues/2880.0) *abs(nbitpix/8.0) ! change July 1998 BFR (abs)
        if(float(int(temp)).eq.temp)then
            ndatablocks=int(temp)
        else
            ndatablocks=int(temp)+1
        endif
        print*,'ndatablocks= ',ndatablocks

        do 140 i=1,ndatablocks
            read(23,rec=nhblocks+i) byteblock
            do 150 j=1, 2880/abs(nbitpix/8)
               if (nbitpix.eq.16) then
c                   --- Include following byte swap for VMS -----
c                    swbyte=byteblock(2*j-1)
c                    byteblock(2*j-1)=byteblock(2*j)
c                    byteblock(2*j)=swbyte
                  values((i-1)*1440+j)=bscale* int2block(j) +bzero
               else if(nbitpix.eq.32) then
c                    --- Include following byte swap for VMS -----
c                    swbyte=byteblock(4*j-3)
c                    byteblock(4*j-3)=byteblock(4*j)
c                    byteblock(4*j)=swbyte
c                    swbyte=byteblock(4*j-2)
c                    byteblock(4*j-2)=byteblock(4*j-1)
c                    byteblock(4*j-1)=swbyte
                  values((i-1)*720+j)=bscale* int4block(j) +bzero
               elseif(nbitpix.eq.8)then
c     ---- CHANGE for reading UNsigned int*8 ----
                  ivalue= byteblock(j) 
                  if(ivalue.lt.0)ivalue=ivalue+256
c     ---- CHANGE for reading UNsigned int*8 ----
                  values((i-1)*2880+j)=bscale* real(ivalue) +bzero
               elseif(nbitpix.eq.-32)then
                  values((i-1)*720+j)=bscale* real4block(j) +bzero
               else
                  print*,'You should have been '//
     _                 'stopped before getting here.'
               end if
c                                                                       |<<<

150         continue
140     continue
        print*,'values(117)=',values(117)
        close(23)
        print*,'Finished subroutine rddisk'
        return

199     write(6,*)'Error on reading file.'
        end
c               of subroutine rddisk





        subroutine mkheader(nhblocks,nbitpix,naxis1,naxis2,bscale,
     _       bzero,header,nvalues,values,logscal, valscal )
c       ==== makes header: nbitpix, naxis1, naxis2, nvalues and values
c       are the only arguments not set by this subroutine.==============

        external head_str

        character*80    header(108)
        real*4          values(*)
        real*4          valscal(*)
        character       logscal
        real*8          valmin,valmax,intspan
        character*10    intstr
        character*20    realstr

             print*,'mkheaderbegin:values(117)=',values(117)
             print*,'logscal= ',logscal
        nhblocks=1
        do 7 i=1,36
            do 8 j=1,80
                header(i)(j:j)=' '
8           continue
7       continue

c                        123456789012345678901234567890123
        header(1)(1:33)='SIMPLE  =                    T  /'
       print*,nbitpix
        call head_str(1.0*nbitpix,'I',10,intstr)
       print*,nbitpix
        header(2)(1:33)='BITPIX  =           '//intstr//'  /'
        header(3)(1:33)='NAXIS   =                    2  /'
        call head_str(1.0*naxis1,'I',10,intstr)
        header(4)(1:33)='NAXIS1  =           '//intstr//'  /'
        call head_str(1.0*naxis2,'I',10,intstr)
        header(5)(1:33)='NAXIS2  =           '//intstr//'  /'

        if(logscal.eq.'T')then
            print*,'Got just past logscal test'
            valmin=9.0e20
            valmax=-9.0e20
            do 101 i=1,nvalues
                if(values(i).le.valmin) valmin=values(i)
                if(values(i).ge.valmax) valmax=values(i)
101         continue
              print*,'valmin,valmax',valmin,valmax


           print*,nbitpix
            if(nbitpix.eq.16)then
                intspan=65530.0
            else if (nbitpix.eq.32)then
c     intspan=4294967200.0
                intspan=4294960000.0 ! modified 14/10/98 due to roundoff
            elseif(nbitpix.eq.8)then
                intspan=254.0
            else
                print*,'nbitpix not presently in code - will set to 16'
                nbitpix=16
                intspan=65530.0
            end if
           print*,intspan

            bscale=(valmax-valmin)/intspan
            bzero= 0.5*(valmin+valmax)

            do 110 i=1,nvalues
                valscal(i)=(values(i)-bzero) /bscale
           if(i.eq.117)print*,'valscal(117)=',valscal(117)
110         continue
        else
            bscale=1.0
            bzero=0.0
        end if

        call head_str(bscale,'R',20,realstr)
        header(6)(1:33)='BSCALE  = '//realstr//'  /'
        call head_str(bzero,'R',20,realstr)
        header(7)(1:33)='BZERO   = '//realstr//'  /'
        header(8)(1:33)='END                              '
c                        123456789012345678901234567890123
        return
        end

c     !!!! WARNING: these assume SIGNED integers; problem for > maxint/2

c     !!!! Warning: for wrtdisk, values _must_ have an array size
c     which is a multiple of 2880. The padded values will be zeroed
c     by this routine if nvalues.lt.<array size of  values>.

        subroutine wrtdisk(flnm,nhblocks,header,nbitpix,nvalues,values)
c       - writes data in header blocks and in array  values  of int*'s
c       to disk file   flnm  in  standard FITS format.


        character*100        flnm
        character*80        header(108)
        real*4              values(*)
        byte                byteblock(2880),swbyte
        integer*2           int2block(1440)
        integer*4           int4block(720)

        equivalence (byteblock(1),int2block(1))
        equivalence (byteblock(1),int4block(1))

                print*,'wrtdisk begin: values(117)=',values(117)



c       ----- Check that header and  values  dimensions allowed for---
        if (nhblocks.gt.3) then
            write(6,*) 'nhblocks  given to subroutine wrtdisk is more'
            write(6,*) 'than presently allowed for '
        end if


        open(unit=17,file=flnm,access='direct',form='unformatted',
c23456
     _  recl=2880,err=99)
c     _  recl=2880/4,err=99) !! for DEC !!

c       ---------write out header blocks--------------
        do 13 j=1,nhblocks
            do 12 i=1,2880
                iinheader=i-(j-1)*2880
c                ia=2880*(j-1)+ int(iinheader/80.0)+1 
                ia=2880*(j-1)+ int((iinheader-1)/80.0)+1 ! new 12.01.2004
                ib=iinheader - (ia-1)*80
c     debugged 12.01.2004 - error picked up by -ffortran-bounds-check 
c                if(ia.lt.99999)then
c                   print*,'rdwrtfits: ',
c     _                  'j,i,iinheader,ia,ib=',
c     _                  j,i,iinheader,ia,ib
c                endif
                byteblock(iinheader)=ichar(header(ia)(ib:ib))
c                  -------(ichar is unnecessary, but works, in Sun Fortran)
12          continue
        write(17,rec=j) byteblock
13      continue

c       ------ round up number of blocks and add one
        temp=(nvalues/2880.0) *(nbitpix/8.0)
        if(float(int(temp)).eq.temp)then
            ndatablocks=int(temp)
        else
            ndatablocks=int(temp)+1
        endif
        print*,'ndatablocks= ',ndatablocks

c       ---------zero extra values to fill last block if needed---
        if (nvalues.lt.((ndatablocks)*2880/(nbitpix/8))) then
            do 20 i=nvalues+1,ndatablocks*2880/(nbitpix/8)
                values(i)=0.0
20          continue
        end if

c       ---- check BITPIX -------------------------------------
        if((nbitpix.ne.16).and.(nbitpix.ne.32)
     _       .and.(nbitpix.ne.8)) then
            print*,'Invalid value of BITPIX: ',nbitpix
            print*,'Will try writing with BITPIX=16'
            nbitpix=16
        end if


c       ----------write out data blocks  values(i)--------------
        do 15 j=1,ndatablocks
            do 10 i=1, 2880/(nbitpix/8)

                if (nbitpix.eq.16) then
                    int2block(i)=nint(values((j-1)*1440+i))
c                   --- Include following byte swap for VMS -----
                    swbyte=byteblock(2*i-1)
                    byteblock(2*i-1)=byteblock(2*i)
                    byteblock(2*i)=swbyte

                else if(nbitpix.eq.32) then
                     int4block(i)=nint(values((j-1)*720+i))
c                    --- Include following byte swap for VMS -----
c                    swbyte=byteblock(4*i-3)
c                    byteblock(4*i-3)=byteblock(4*i)
c                    byteblock(4*i)=swbyte
c                    swbyte=byteblock(4*i-2)
c                    byteblock(4*i-2)=byteblock(4*i-1)
c                    byteblock(4*i-1)=swbyte
                elseif(nbitpix.eq.8)then
                   byteblock(i)= nint(values((j-1)*2880+i))
                else
              print*,'You should have been stopped before getting here.'
                end if
c                                                                       |<<<

10          continue
            write(17,rec=nhblocks+j) byteblock
15      continue

        close(17)
        return

99      write(6,*) 'Error on opening file.'
        end
c               of subroutine wrtdisk
