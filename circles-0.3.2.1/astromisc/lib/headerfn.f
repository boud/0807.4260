C Copyright (C) 2003 Boud Roukema et al (shape-univ at astro.uni.torun.pl)
C 
C This program is free software; you can redistribute it and/or modify
C it under the terms of the GNU General Public License as published by
C the Free Software Foundation; either version 2 of the License, or
C (at your option) any later version.
C 
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C 
C You should have received a copy of the GNU General Public License
C along with this program; if not, write to the Free Software
C Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
C 
C See also http://www.gnu.org/licenses/gpl.html

        real*4 function head_val(string)
c       --- reads an integer or floating point number from the 72 character
c       string which should commence with an '=' bar spaces
c       NB: value  is REAL*4. -----------------------------------------

        character*72    string
        character       x

        i=1
        x=string(i:i)
        do 161 while ((x.ne.'=').and.(i.lt.72))
            i=i+1
            x=string(i:i)
161     continue
        i=i+1
c        open(string(i:72))
                print*,string
                print*,'about to read value...'
        read(string(i:72),*)head_val
                print*,'value obtained = ',head_val
        return

134     format(g23.0)
        end


        subroutine head_str(value,type,length,string)
c       -----------   This is really a FUNCTION which
c       reads an integer or floating point number from the 72 character
c       string which should commence with an '=' bar spaces
c       and puts the results in   string  of type  character*(*).
c       NB: value  is REAL*4. -----------------------------------------

        real*4          value
        character       type
        integer         length
        character*(*)   string

c        open(string)
        if((type.eq.'I').and.(length.eq.10)) then
            write(string,234)nint(value)
        else if ((type.eq.'R').and.(length.eq.20))then
            write(string,236)value
        else
            print*,'Call to head_str has invalid parameters.'
            stop
        end if

        print*,'__:',string,':__'

234     format(i10)
235     format(a10)
236     format(g20.10)
237     format(a20)
        return
        end
