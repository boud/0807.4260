/*
  astromisc - library with miscellaneous GPL tools for astronomy research

   Copyright (C) 2005 Boud Roukema
     
     This program is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation; either version 2 of the License, or
     (at your option) any later version.
     
     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.
     
     You should have received a copy of the GNU General Public License
     along with this program; if not, write to the Free Software
     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  
     See also http://www.gnu.org/licenses/gpl.html
*/

#include <stdlib.h>
#include "astromisc.h"

int compar_float(const float *x1, const float *x2)
{
  int ix;
  float x;  /* added version astromisc-0.1.7 */ 
  
  /*  printf("*x1 = %f  *x2 = %f\n",*x1,*x2); */
  x= *x1-*x2;
  if(x > 0.0)
    ix= 1;
  else if (x < 0.0)
    ix= -1;
  else
    ix= 0;

  return ix;
};
      

/*  does a sort on real*4 (float) - WARNING: input array is modified! */
void R4SORT( void *array, size_t *nmemb
	     )
{
  size_t nbytes=4;

  int   ii;
  float xx[3];
  xx[0]=6.0;
  xx[1]=-8.8;
  xx[2]=-17.4;

  /*  printf("r4sort: *nmemb= %d\n",*nmemb); */

  ii=compar_float(&xx[0],&xx[2]);
  /*  printf("ii=%d\n",ii);  */

  qsort( array, *nmemb, nbytes, (void *) compar_float );  /* -> compiler warning :( */

};

  




