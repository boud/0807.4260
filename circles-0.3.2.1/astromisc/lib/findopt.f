C   astromisc - library with miscellaneous GPL tools for astronomy research
C
C   Copyright (C) 2004 Boud Roukema
C
C   This program is free software; you can redistribute it and/or modify
C   it under the terms of the GNU General Public License as published by
C   the Free Software Foundation; either version 2, or (at your option)
C   any later version.
C
C   This program is distributed in the hope that it will be useful,
C   but WITHOUT ANY WARRANTY; without even the implied warranty of
C   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C   GNU General Public License for more details.
C
C   You should have received a copy of the GNU General Public License
C   along with this program; if not, write to the Free Software Foundation,
C   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

c     --- test routine - subroutine itself is below ---
      subroutine chkfind()

      parameter(pi=3.1415926536,pi_180=pi/180.0)
      parameter(maxn=50)
      real*4   x(maxn),y(maxn)
      
      n=maxn
      ampnoise=0.02
      amp2= 0.01
      amp3= 0.01
      do ipower=1,5
         print*,' ' 
         print*,'Testing findopt: y= sin (x) + noise, power=',ipower

         dx = 360/real(n)
         do i=1,n
            x(i)= (real(i)-0.5)* dx
            y(i)= sin( x(i)*pi_180 -85.0)

c     --- make the max and min narrower by power   ipower ---
            if(y(i).gt.0.0)then
               sign= 1.0
            else
               sign= -1.0
            endif
            do j=2,ipower
               y(i)= y(i)*y(i)
            enddo
            if(ipower.ge.2)y(i)= sign*y(i)

            if(2*(i/2).eq.i)then
               y(i)=y(i) + ampnoise
            else
               y(i)=y(i) - ampnoise
            endif

c     ---- add a bit more noise ----
            y(i)=y(i) + amp2* sin(x(i)* 3000.0)
            y(i)=y(i) + amp3* sin(x(i)* 7000.0)

         enddo

         ismoo= 6
         modif_y= 0
         iperiodic= 1
         call findopt(n,y,ismoo, modif_y,iperiodic, 
     _        imin,imax,ihwmin,ihwmax)
         
         print*,'imin,imax,ihwmin,ihwmax=',imin,imax,ihwmin,ihwmax
         print*,'x(imin),... imax,dx*ihwmin,ihwmax = ',
     _        x(imin),x(imax),dx*ihwmin,dx*ihwmax

      enddo

      return
      end



c     === findopt: find optima - global min and max 
c     - in 1D periodic function
C     - with smoothing 
c     OUTPUTS:
c     - positions of max/min are output: imin,imax
c     - "HWHM" (half-width-half-max/min), defined using half-max/min
c     _  to be the relative to median value 
c     ===

      subroutine findopt(n,y,ismoo, modif_y, iperiodic,
     _     imin,imax,ihwmin,ihwmax)
C     ==== finds local minimum and maxima after smoothing an array ====
      parameter(maxn=3000,mxsmoo=20,mxwork=100) ! WARNING: hard-wired

C     INPUTS:
      integer*4   n
      integer*4   ismoo  ! smoothing
      integer*4   iperiodic ! 1 means periodic, otherwise non-periodic
      integer*4   modif_y ! 1 means y will be modified, otherwise no modif

C     INPUTS which may be MODIFIED:
c     -- y() will be modified if  (modif_y.eq.1), otherwise will not 
c     be modified --
      real*4      y(*) ! size must not be greater than maxn

C     OUTPUTS:
      integer*4   imin,imax,ihwmin,ihwmax
C     !! y is replaced by ysmoo if ( modif_y .eq. .true. ) !!

C     INTERNAL:
      real*4      ycopy(-mxwork+1:maxn+mxwork)
      real*4      ysmoo(maxn),x(maxn)
      parameter(pi=3.14159265359)

      integer*4   nsig ! how many sigma to do smoothing

      logical*4   test

c      data iperiodic /1/
      data nsig /2/

      data test/.true./ !.false./ !

      if(n.gt.maxn)print*,'findopt ERROR: n,maxn=',n,maxn
      if(ismoo.gt.mxsmoo)print*,'findopt ERROR: ismoo,mxsmoo=',
     _     ismoo,mxsmoo


      if(ismoo.ge.1)then
         fact=1.0/sqrt(2.0*pi) /real(ismoo)
      endif


c     ---- find median ----
      do i=1,n
         ycopy(i)=y(i)
      enddo
      call r4sort(ycopy,n)
      if((n/2)*2.eq.n)then
         dmedian= 0.5* (ycopy(n/2) + ycopy(n/2+1))
      else
         dmedian= ycopy(n/2)
      endif

c     ---- reset ycopy to original order of  y  ----
      do i=1,n
         ycopy(i)=y(i)
      enddo

      if(iperiodic.eq.1)then
         nextend= nsig*ismoo
         if(nextend.gt.mxwork)then
            print*,'findopt: WARNING: array limits in smoothing',
     _           ' over periodic boundary have problem ',
     _           'nextend,mxwork=',nextend,mxwork
         endif
         do i=-nextend+ 1,0
            ycopy(i)=y( mod(i+10*n -1,n) +1 )
         enddo
         do i=n+1,n+nextend
            ycopy(i)=y( mod(i+10*n -1,n) +1 )
         enddo
      endif
      
      ymin=9e9
      ymax=-9e9
      if(test)print*,'ismoo=',ismoo

      sm2= real(ismoo*ismoo)
      do i=1,n
         if(ismoo.ge.1)then
            ysmoo(i)=0.0
            do j=max(-nextend+1,i-2*ismoo),
     _           min(i+nsig*ismoo,n+nextend) ! -> nsig sigma
               dd= real(j-i)
               xx= exp(-0.5*dd*dd/sm2)
               ysmoo(i)=ysmoo(i)+ xx*ycopy(j)   

c     if(test)print*,'j,dd=',j,dd
c     ysmoo(i)=ysmoo(i)+y(i)
c     fact=1.0/real(2*ismoo+1)
               
            enddo

            ysmoo(i)=ysmoo(i)*fact  
         else
            ysmoo(i)= ycopy(i)
         endif
         ymin=min(ymin,y(i))
         ymax=max(ymax,y(i))

      enddo


C      if(test)then
C
C         ymax=min(ymax,600.0) !
C         do i=1,n
C            x(i)=real(i)
C         enddo
C
C         call pgbegin(0,'?',1,1)
C         call pgvsize(1.0,6.0,1.0,6.0)
C         call pgscf(2)
C         call pgslw(3)
C         call pgwindow(0.0,real(n), ymin,ymax)
C         call pgbox('bcnts',0.,0,'bcnts',0.,0)
C         call pgsls(2)
C         call pgline(n,x,y)
C         call pgsls(1)
C         call pgline(n,x,ysmoo)
C         call pgsls(1)
C         call pglabel('i','y, ysmoo',' ')
C      endif



c     --------------------------------------------------------------

c     -- find global min, max, median --
      dmin=ysmoo(1)
      dmax=ysmoo(1)
      imin=1
      imax=1
      do i=1,n
         if(ysmoo(i).lt.dmin)then
            dmin=ysmoo(i)
            imin=i
         endif
         if(ysmoo(i).gt.dmax)then
            dmax=ysmoo(i)
            imax=i
         endif
      enddo

      hwhmax= 0.5*(dmax +dmedian)
      hwhmin= 0.5*(dmin +dmedian)

c     ---- hwhmin left
      if(iperiodic.eq.1)then
         ilimit= imin-n/2
      else
         ilimit= 1
      endif

      i=imin
      j= i
      do while(i.gt.ilimit.and.ysmoo(j).lt.hwhmin)
         j= mod( i-1, n )+1    
         i=i-1
      enddo
      ihwmn1 = imin-i

c     ---- hwhmin right
      if(iperiodic.eq.1)then
         ilimit= imin+n/2
      else
         ilimit= n
      endif

      i=imin
      j= i
      do while(i.lt.ilimit.and.ysmoo(j).lt.hwhmin)
         j= mod( i-1, n )+1    
         i=i+1
      enddo
      ihwmn2 = i-imin

      ihwmin=(ihwmn1+ihwmn2) /2


c     ---- hwhmax left
      if(iperiodic.eq.1)then
         ilimit= imax-n/2
      else
         ilimit= 1
      endif

      i=imax
      j= i
      do while(i.gt.ilimit.and.ysmoo(j).gt.hwhmax)
         j= mod( i-1, n )+1    
         i=i-1
      enddo
      ihwmn1 = imax-i

c     ---- hwhmax right
      if(iperiodic.eq.1)then
         ilimit= imax+n/2
      else
         ilimit= n
      endif

      i=imax
      j= i
      do while(i.lt.ilimit.and.ysmoo(j).gt.hwhmax)
         j= mod( i-1, n )+1    
         i=i+1
      enddo
      ihwmn2 = i-imax

      ihwmax=(ihwmn1+ihwmn2) /2

      

c     --- replace the input y array by the smoothed array if  modif_y.eq.1  ---
      if(modif_y.eq.1)then
         do i=1,n
            y(i)=ysmoo(i) ! warning: y is modified !
         enddo
      endif

c     --------------------------------------------------------------


C      if(test)then
C         call pgsch(3.0)
C         call pgslw(6)
C         call pgpoint(1,x(imin1),ysmoo(imin1),22)
C         call pgpoint(1,x(imax2),ysmoo(imax2),22)
C         call pgpoint(1,x(imax3),ysmoo(imax3),4)
C         call pgpoint(1,x(imaxfin),ysmoo(imaxfin),14) ! 22)
C
C         call pgsch(0.9)
C         call pgslw(1)
C         call pgptxt(x(imin1),ysmoo(imin1)+0.05*(ymax-ymin),
C     _        0.0,0.0,'imin1')
C         call pgptxt(x(imax2),ysmoo(imin1)+0.05*(ymax-ymin),
C     _        0.0,0.0,'imax2')
C         call pgptxt(x(imax3),ysmoo(imin1)+0.05*(ymax-ymin),
C     _        0.0,0.0,'imax3')
C         call pgptxt(x(imaxfin),ysmoo(imin1)+0.05*(ymax-ymin),
C     _        0.0,0.0,
C     _        'imaxfin')
C
C         call pgend
C      endif


      return
      end



