C   astromisc - library with miscellaneous GPL tools for astronomy research
C
C   Copyright (C) 2004 Boud Roukema
C
C   This program is free software; you can redistribute it and/or modify
C   it under the terms of the GNU General Public License as published by
C   the Free Software Foundation; either version 2, or (at your option)
C   any later version.
C
C   This program is distributed in the hope that it will be useful,
C   but WITHOUT ANY WARRANTY; without even the implied warranty of
C   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C   GNU General Public License for more details.
C
C   You should have received a copy of the GNU General Public License
C   along with this program; if not, write to the Free Software Foundation,
C   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

c     ==== subroutines to convert between coordinate systems ====
c     B.Roukema Sep 1995


c      program testcoco
      subroutine testcoco

      call init_coco

      do i=1,10
         read(5,*)raj,decj
c         read(5,*)ra,dec
c         read(5,*)glong,galb

         call j2000_b1950(raj,decj,ra,dec)
         print*,'raj,decj,ra,dec='
         print*,raj,decj
         print*,ra,dec

         call b1950_gal(ra,dec,glong,galb)
         print*,glong,galb
         call gal_b1950(glong,galb,ra,dec)
         print*,ra,dec
         print*,' '

         call b1950_j2000(ra,dec,raj,decj)
         print*,'ra,dec,raj,decj='
         print*,ra,dec
         print*,raj,decj


         call b1950_gal(ra,dec,glong,galb)
         print*,glong,galb



      enddo
      end

      subroutine init_coco      ! for initialising coco
c     ---- North Galactic Pole and Galactic Centre ----
      parameter(pi=3.1415926536)
      real*4  xngp,yngp,zngp,xgc,ygc,zgc
      real*4  xncp,yncp,zncp,xcc,ycc,zcc
      common  /gcoords/ xngp,yngp,zngp,xgc,ygc,zgc,
     _     xncp,yncp,zncp,xcc,ycc,zcc

      ra_ngp=12.817  !12.0
      dec_ngp=27.400 !30.0
      ra_gc= 17.707390  !18.0
      dec_gc= -28.916792 !-30.0

      ra_ngp= ra_ngp *pi/12.0
      dec_ngp= dec_ngp *pi/180.0
      ra_gc= ra_gc *pi/12.0
      dec_gc= dec_gc *pi/180.0

      xngp= cos(ra_ngp)*cos(dec_ngp)
      yngp= sin(ra_ngp)*cos(dec_ngp)
      zngp= sin(dec_ngp)
      xgc= cos(ra_gc)*cos(dec_gc)
      ygc= sin(ra_gc)*cos(dec_gc)
      zgc= sin(dec_gc)

c     ---- celestial North pole (ncp) and autumnal equinox (cc) (~23 March)
      call b1950_gal(0.0,90.0,gl_ncp,gb_ncp)
      call b1950_gal(0.0,0.0,gl_cc,gb_cc)

      gl_ncp= gl_ncp *pi/180.0
      gb_ncp= gb_ncp *pi/180.0
      gl_cc= gl_cc *pi/180.0
      gb_cc= gb_cc *pi/180.0

      xncp= cos(gl_ncp)*cos(gb_ncp)
      yncp= sin(gl_ncp)*cos(gb_ncp)
      zncp= sin(gb_ncp)
      xcc= cos(gl_cc)*cos(gb_cc)
      ycc= sin(gl_cc)*cos(gb_cc)
      zcc= sin(gb_cc)

      return
      end

      subroutine b1950_gal(ra,dec,glong,galb)
c     --- b1950 in  hours, degrees;  gal in  deg(0,360), deg (-90,90) ---
      real*4  xngp,yngp,zngp,xgc,ygc,zgc
      real*4  xncp,yncp,zncp,xcc,ycc,zcc
      common  /gcoords/ xngp,yngp,zngp,xgc,ygc,zgc,
     _     xncp,yncp,zncp,xcc,ycc,zcc
      integer first
      save    first

      parameter(pi=3.1415926536)
      data  first/0/

      if(first.eq.0)then
         first=1234
         call init_coco
      endif

      rar=ra *pi/12.0           ! pi= 12 hours !
      decr= dec*pi/180.0
      x= cos(rar)*cos(decr)
      y= sin(rar)*cos(decr)
      z= sin(decr)

      to_ngp = angle(x,y,z,xngp,yngp,zngp)  ! angle between  source S and NGP
      galb= pi/2.0 - to_ngp

c     -- calculate component perp. to NGP; call this vector V = S - S.NGP
      dot= x*xngp +y*yngp +z*zngp
      xx= x- dot*xngp
      yy= y- dot*yngp
      zz= z- dot*zngp

      if(xx*yy*zz.ne.0.0)then
         to_gc= angle(xx,yy,zz,xgc,ygc,zgc)
      elseif(dot.gt.0.0)then
         galb=90.0
         glong= 99.99           ! any is equivalent
         return
      elseif(dot.lt.0.0)then
         galb=-90.0
         glong= 99.99
         return
      endif
c     -- to know if  glong < 12h or > 12h calculate  ( GC x V ) . NGP
      xcr= ygc*zz - zgc*yy
      ycr= zgc*xx - xgc*zz
      zcr= xgc*yy - ygc*xx

      dot = xcr*xngp + ycr*yngp + zcr*zngp

      if(dot.ge.0.0)then
         glong= to_gc
      elseif(dot.lt.0.0)then
         glong= 2.0*pi - to_gc
      endif

      galb= galb*180.0/pi
      glong= glong*180.0/pi
      if(glong.ge.360.0)glong=glong-360.0
      return
      end


      subroutine gal_b1950(glong,galb,ra,dec)
c     --- b1950 in  hours, degrees;  gal in  deg(0,360), deg (-90,90) ---
      real*4  xngp,yngp,zngp,xgc,ygc,zgc
      real*4  xncp,yncp,zncp,xcc,ycc,zcc
      common  /gcoords/ xngp,yngp,zngp,xgc,ygc,zgc,
     _     xncp,yncp,zncp,xcc,ycc,zcc
      integer first
      save    first

      parameter(pi=3.1415926536)
c     follow. param is for near poles: (1 arcsec)^2= 2.35e-11 rad^2
      parameter(tol2=1e-11)
      data  first/0/

      if(first.eq.0)then
         first=1234
         call init_coco
      endif


      glongr=glong *pi/180.0
      galbr= galb*pi/180.0
      x= cos(glongr)*cos(galbr)
      y= sin(glongr)*cos(galbr)
      z= sin(galbr)

      to_ncp = angle(x,y,z,xncp,yncp,zncp)  ! angle between  source S and NCP
      dec = pi/2.0 - to_ncp

c     -- calculate component perp. to NCP; call this vector V = S - S.NCP
      dot= x*xncp +y*yncp +z*zncp
      xx= x- dot*xncp
      yy= y- dot*yncp
      zz= z- dot*zncp

      if((xx*xx+yy*yy+zz*zz).gt.tol2)then
         to_cc= angle(xx,yy,zz,xcc,ycc,zcc)
      elseif(dot.gt.0.0)then
         ra=9.999
         dec= 90.0
         return
      elseif(dot.lt.0.0)then
         ra=9.999
         dec= -90.0
         return
      endif

c     -- to know if  ra < 12h or > 12h calculate  ( CC x V ) . NCP
      xcr= ycc*zz - zcc*yy
      ycr= zcc*xx - xcc*zz
      zcr= xcc*yy - ycc*xx

      dot = xcr*xncp + ycr*yncp + zcr*zncp

      if(dot.ge.0.0)then
         ra = to_cc
      elseif(dot.lt.0.0)then
         ra = 2.0*pi - to_cc
      endif

      dec= dec*180.0/pi
      ra = ra*12.0/pi
      if(ra.ge.24.0)ra=ra-24.0
      return
      end


c     ==== ajoute avril 1998 -> conversion b1950.0 <-> j2000.0,
c     d'apres l'Almanac de 1998 ====

      subroutine b1950_j2000(rab,decb,raj,decj)
c     --- b1950 in  hours, degrees;  j2000 in  hours, degrees;  ---
      real*4     rab,decb       ! inputs
      real*4     raj,decj       ! outputs

c     internal
      real*4    x,y,z
      real*8    r0(3),  r0_a, r1(3), rp(3)
      parameter(pi=3.1415926536)

c     vector  A (p B42 of Almanac 1998)
      real*8    aa(3)

c     matrix M (p B42 of Almanac 1998, spatial part only, no prop. motion etc.)
      real*8    bigm(3,3)

      logical*1  test

      parameter(tol=3e-6) ! tolerance near poles

      data  aa/-1.62557e-6,-0.31919e-6,-0.13843e-6/

      data bigm/0.9999256782, -0.0111820611, -0.0048579477,
     _     0.0111820610, 0.9999374784, -0.0000271765,
     _     0.0048579479, -0.0000271474, 0.9999881997/


      test=.false. !.true.

      rar=rab *pi/12.0           ! pi= 12 hours !
      decr= decb*pi/180.0

      x= cos(rar)*cos(decr)
      y= sin(rar)*cos(decr)
      z= sin(decr)

      r0(1)=dble(x)
      r0(2)=dble(y)
      r0(3)=dble(z)
      if(test)print*,'r0=',r0

      r0_a= r0(1)*aa(1) +r0(2)*aa(2) +r0(3)*aa(3)
      if(test)print*,'r0_a=',r0_a

      do i=1,3
         r1(i)= r0(i) - aa(i) + r0_a* r0(i)
      enddo
      if(test)print*,'r1=',r1

      call mult_8(bigm,r1,rp,3,3,3,1)
      if(test)print*,'rp=',rp

      x=real(rp(1))
      y=real(rp(2))
      z=real(rp(3))

      decj= 180.0/pi* angle(0.0,0.0,-1.0, x,y,z) -90.0

      if(decj.lt.-90.0+tol*180/pi
     _     .or. decj.gt.90.0-tol*180/pi)then
         raj=9.999
      else
         ra= 12.0/pi* angle(1.0,0.0,0.0, x,y,0.0)
         if(y.lt.0.0) ra=24.0 - ra

         raj= ra
      endif

      return
      end


c     ==== ajout� oct 1999 -> conversion b1950.0 <- j2000.0,
c     d'apres l'Almanac de 1998 ====

      subroutine j2000_b1950(raj,decj,rab,decb)
c     --- b1950 in  hours, degrees;  j2000 in  hours, degrees;  ---
      real*4     raj,decj       ! inputs
      real*4     rab,decb       ! outputs

c     internal
      real*4    x,y,z
      real*8    r0(3),  r1(3),r1mod, s1(3),s(3),s_a, r(3),rmod
      parameter(pi=3.1415926536)

c     vector  A (p B42 of Almanac 1998)
      real*8    aa(3)

c     matrix M (p B42 of Almanac 1998, spatial part only, no prop. motion etc.)
      real*8    bigminv(3,3)

      logical*1  test

      parameter(tol=3e-6) ! tolerance near poles

      data  aa/-1.62557e-6,-0.31919e-6,-0.13843e-6/

      data bigminv/0.9999256795, 0.0111814828, 0.0048590039,
     _     -0.0111814828, 0.9999374849, -0.0000271771,
     _     -0.0048590040, -0.0000271557, 0.9999881946/


      test=.false. !.true.

      rar=raj *pi/12.0           ! pi= 12 hours !
      decr= decj*pi/180.0

      x= cos(rar)*cos(decr)
      y= sin(rar)*cos(decr)
      z= sin(decr)

      r0(1)=dble(x)
      r0(2)=dble(y)
      r0(3)=dble(z)
      if(test)print*,'r0=',r0

      call mult_8(bigminv,r0,r1,3,3,3,1)
      r1mod= r1(1)*r1(1)+r1(2)*r1(2)+r1(3)*r1(3)
      if(r1mod.lt.tol)print*,'j2000_b1950 WARNING: r1mod^2=',r1mod
      r1mod=sqrt(r1mod)
      do i=1,3
         s1(i)=r1(i)/r1mod
         s(i)=s1(i)
      enddo

      do k=1,3
         s_a= s(1)*aa(1) +s(2)*aa(2) +s(3)*aa(3)
         do i=1,3
            r(i)= s1(i) + aa(i) - s_a* s(i)
         enddo
         rmod= r(1)*r(1)+r(2)*r(2)+r(3)*r(3)
         if(rmod.lt.tol)print*,'j2000_b1950 WARNING: rmod^2=',rmod
         rmod=sqrt(rmod)
         do i=1,3
            s(i)=r(i)/rmod
         enddo
      enddo

      if(test)print*,'r1=',r1

      x=real(r(1))
      y=real(r(2))
      z=real(r(3))

      decb= 180.0/pi* angle(0.0,0.0,-1.0, x,y,z) -90.0

      if(decb.lt.-90.0+tol*180/pi
     _     .or. decb.gt.90.0-tol*180/pi)then
         rab=9.999
      else
         ra= 12.0/pi* angle(1.0,0.0,0.0, x,y,0.0)
         if(y.lt.0.0) ra=24.0 - ra

         rab= ra
      endif

      return
      end
