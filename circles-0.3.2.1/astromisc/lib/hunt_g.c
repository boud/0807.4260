/*
  astromisc - library with miscellaneous GPL tools for astronomy research

   Copyright (C) 2005 Boud Roukema
     
     This program is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation; either version 2 of the License, or
     (at your option) any later version.
     
     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.
     
     You should have received a copy of the GNU General Public License
     along with this program; if not, write to the Free Software
     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  
     See also http://www.gnu.org/licenses/gpl.html
*/

#include "astromisc.h"

/*  looks for location of  x_input  in ordered array "x_array" 

    WARNING: must be double = real*8    
    (write another version if you want real*4)

    returns indexf as the lower index, using the fortran indexing convention
*/


void HUNT_G( double x_array[], size_t *nmemb, 
	     double *x_input,
	     size_t *indexf   /* output: index in fortran indexing convention */
	     )
{
  static int ifirst = 1;  /* is this the first time calling the routine ? */

  int test = 0 ;
  double xtest = 54.2 ;
  int iang ;  /* for testing only */

  static gsl_interp_accel * accel;

  if(ifirst==1){
    accel = gsl_interp_accel_alloc();
    ifirst= 0; /* subsequent calls will no longer be the first time */
  };


  /* TEST ONLY */

  if(test)
    {
      /*      printf(" x_array, *x_array, **x_array = %d %d \n",x_array, *x_array);
	      printf(" x_array, *x_array, **x_array = %f %f \n",x_array, *x_array); 
	      printf(" *x_array[0], *x_array[1], *x_array[2] = %f %f %f \n", *x_array[0], *x_array[1], *x_array[2] );
*/
      printf(" x_array[0], x_array[1], x_array[2] = %f %f %f \n", x_array[0], x_array[1], x_array[2] ); 
      printf(" nmemb, *nmemb, x_input, *x_input = %d %d %d %f \n",nmemb, *nmemb, x_input, *x_input);
      /* printf("AAA &xtest, xtest = %d %f \n",&xtest,xtest); */

      /*      for (iang=0; iang < *nmemb; iang++)
	      printf("BBB x_array[*nmemb-1] = %f \n", x_array[(*nmemb)-1]);   */
    };

  /* END OF TEST ONLY */


  /*  gsl_interp_accel_find (gsl_interp_accel * A, const
      double X_ARRAY[], size_t SIZE, double X) */


  /* call the gsl function */

  *indexf = gsl_interp_accel_find (accel, x_array, *nmemb, *x_input) + 1;


  if(test)
    {
      printf("hunt_g: *nmemb, *x_input, *indexf= %d %f %d\n", *nmemb, *x_input, *indexf);
    };

}

