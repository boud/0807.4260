C   astromisc - library with miscellaneous GPL tools for astronomy research
C
C   Copyright (C) 2004 Boud Roukema
C
C   This program is free software; you can redistribute it and/or modify
C   it under the terms of the GNU General Public License as published by
C   the Free Software Foundation; either version 2, or (at your option)
C   any later version.
C
C   This program is distributed in the hope that it will be useful,
C   but WITHOUT ANY WARRANTY; without even the implied warranty of
C   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C   GNU General Public License for more details.
C
C   You should have received a copy of the GNU General Public License
C   along with this program; if not, write to the Free Software Foundation,
C   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

c       ==== This file contains routines to convert an array of values  x  into 
c       an array of binned values plus an array of counts for each bin,
c       given a minimum and maximum value of the input array and a number of bins
c       desired. The minimum and maximum binned value are output. ====

c       NB: Binning is linear for both mkbins and mkblog, but in the latter
c       the output values (bins(*),bmin,bmax) are logarithms in base 10.

c       22/6/92: Versions of both of these which also find the mean
c       value of some variable  y  have been added. They are called
c       xybins  and  xyblog.  
c       26/8/92: xyblog  tested ok via  testbins.f 

c       

        subroutine mkbins(n,xmin,xmax,x,nbins,xb,bmin,bmax,bins,ioob)
c         Input parameters:
        integer*4       n
        real*4          xmin,xmax,x(*)
        integer*4       nbins, ioob
c         Output parameters:
        real*4          xb(*),bmin,bmax,bins(*)

c         local variables:


        del=(xmax-xmin)/real(nbins)
        do ibin=1,nbins
            xb(ibin)=xmin +(real(ibin)-0.5) *del
            bins(ibin)=0.0
        enddo

c        ioob=0
        do i=1,n
            if((x(i).lt.xmin).or.(x(i).gt.xmax))then
               ioob=ioob+1
               if(ioob.le.20)then
                print*,'bins.f: point out of bounds. (',i,'th in array)'
               elseif(ioob.eq.21)then
                  print*,'bins.f: and more out of bounds ...'
               endif
            else
                ibin=int((x(i)-xmin)/del)+1
                bins(ibin)=bins(ibin)+1.0
            endif
        enddo

        bmin=real(n)
        bmax=0.0
        do ibin=1,nbins
            if(bins(ibin).lt.bmin)bmin=bins(ibin)
            if(bins(ibin).gt.bmax)bmax=bins(ibin)
        enddo

        return
        end

c       ==============================================================

        subroutine mkblog(n,xmin,xmax,x,nbins,xb,bmin,bmax,bins)
c         Input parameters:
        integer*4       n
        real*4          xmin,xmax,x(*)
        integer*4       nbins
c         Output parameters:
        real*4          xb(*),bmin,bmax,bins(*)

c         local variables:
        real*4          del
        integer*4       ibin,i,npos


        del=(xmax-xmin)/real(nbins)
        do ibin=1,nbins
            xb(ibin)=xmin +(real(ibin)-0.5) *del
            bins(ibin)=0.0
        enddo

        ioob=0
        do i=1,n
            if((x(i).lt.xmin).or.(x(i).gt.xmax))then
               ioob=ioob+1
               if(ioob.le.20)then
                print*,'bins.f: point out of bounds. (',i,'th in array)'
               elseif(ioob.eq.21)then
                  print*,'bins.f: and more out of bounds ...'
               endif
            else
                ibin=int((x(i)-xmin)/del)+1
c                print*,'i,x(i),del,ibin=',i,x(i),del,ibin
                bins(ibin)=bins(ibin)+1.0
            endif
        enddo

        bmin=1.0e20
        bmax=-1.0e20
        npos=0
        do ibin=1,nbins
            if(bins(ibin).le.0.0)then
                bins(ibin)=-99.9
            else
                npos=npos+1
                bins(ibin)=log10(bins(ibin))
                if(bins(ibin).lt.bmin)bmin=bins(ibin)
                if(bins(ibin).gt.bmax)bmax=bins(ibin)
            endif
        enddo
        if(npos.eq.0)then
            print*,'bins.f: no bins have any points in them'
            bmin=-99.9
            bmax=-99.9
        endif

        return
        end


        subroutine xybins(n,xmin,xmax,x,y,nbins,xb,bmin,bmax,bins,
     _       ymin,ymax,ym)
c         Input parameters:
        integer*4       n
        real*4          xmin,xmax,x(*),y(*)
        integer*4       nbins
c         Output parameters:
        real*4          xb(*),bmin,bmax,bins(*)
        real*4          ymin,ymax,ym(*)
        
c         local variables:


        del=(xmax-xmin)/real(nbins)
        do ibin=1,nbins
            xb(ibin)=xmin +(real(ibin)-0.5) *del
            bins(ibin)=0.0
            ym(ibin)=0.0
        enddo

        ioob=0
        do i=1,n
            if((x(i).lt.xmin).or.(x(i).gt.xmax))then
                ioob=ioob+1
               if(ioob.le.20)then
                print*,'bins.f: point out of bounds. (',i,'th in array)'
               elseif(ioob.eq.21)then
                  print*,'bins.f: and more out of bounds ...'
               endif
            else
                ibin=int((x(i)-xmin)/del)+1
                bins(ibin)=bins(ibin)+1.0
                ym(ibin)=ym(ibin)+y(i)
            endif
        enddo

        bmin=real(n)
        bmax=0.0
        ymin=99.9e19
        ymax=-99e19

        do ibin=1,nbins
            if(bins(ibin).lt.bmin)bmin=bins(ibin)
            if(bins(ibin).gt.bmax)bmax=bins(ibin)
            if(bins(ibin).gt.0.0)then
               ym(ibin)=ym(ibin)/bins(ibin)
            else
               ym(ibin)=-99.9
               print*,'Warning: nothing in bin ',ibin,'.'
               print*,'ym(',ibin,') set to -99.9 .'
            endif

            if(ym(ibin).lt.ymin)ymin=ym(ibin)
            if(ym(ibin).gt.ymax)ymax=ym(ibin)
        enddo

        return
        end

c       =============================================================

        subroutine xyblog(n,xmin,xmax,x,y,nbins,xb,bmin,bmax,bins,
     _       ymin,ymax,ym)

c       ---- NB: if any value  y(.)=-99.9 it will not be binned ----

c         Input parameters:
        integer*4       n
        real*4          xmin,xmax,x(*),y(*)
        integer*4       nbins
c         Output parameters:
        real*4          xb(*),bmin,bmax,bins(*)
        real*4          ymin,ymax,ym(*)

c         local variables:
        real*4          del
        integer*4       ibin,i,npos


        del=(xmax-xmin)/real(nbins)
        do ibin=1,nbins
            xb(ibin)=xmin +(real(ibin)-0.5) *del
            bins(ibin)=0.0
            ym(ibin)=0.0
        enddo

        ioob=0
        do i=1,n
            if((x(i).lt.xmin).or.(x(i).gt.xmax))then
               ioob=ioob+1
               if(ioob.le.20)then
                print*,'bins.f: point out of bounds. (',i,'th in array)'
               elseif(ioob.eq.21)then
                  print*,'bins.f: and more out of bounds ...'
               endif
            else
                ibin=int((x(i)-xmin)/del)+1
c                print*,'i,x(i),del,ibin=',i,x(i),del,ibin

                bins(ibin)=bins(ibin)+1.0
                if(y(i).ne.-99.9)then
                   ym(ibin)=ym(ibin)+y(i)
                elseif(bins(ibin).gt.1.0)then
                   ym(ibin)=ym(ibin)*bins(ibin)/(bins(ibin)-1.0)
                elseif(bins(ibin).eq.1.0)then
                   ym(ibin)=0.0
                endif
c                print*,'ym(ibin)=',ym(ibin)
             endif
        enddo

        bmin=1.0e20
        bmax=-1.0e20
        ymin=1e20
        ymax=-1e20

        npos=0
        do ibin=1,nbins
            if(bins(ibin).le.0.0)then
                bins(ibin)=-99.9
                ym(ibin)=-99.9
            else
                npos=npos+1

                if(ym(ibin).le.0.0)then
                   ym(ibin)=-99.9
                else
                   ym(ibin)=log10(ym(ibin)/bins(ibin))
                   if(ym(ibin).lt.ymin)ymin=ym(ibin)
                   if(ym(ibin).gt.ymax)ymax=ym(ibin)
                endif

                bins(ibin)=log10(bins(ibin))
                if(bins(ibin).lt.bmin)bmin=bins(ibin)
                if(bins(ibin).gt.bmax)bmax=bins(ibin)

            endif
        enddo
        if(npos.eq.0)then
            print*,'bins.f: no bins have any points in them'
            bmin=-99.9
            bmax=-99.9
            ymin=-99.9
            ymax=-99.9
        endif

        return
        end

