C     Copyright (C) 2004 B. Roukema et al. <shape-univ at astro.uni.torun.pl>
C     
C     This program is free software; you can redistribute it and/or modify
C     it under the terms of the GNU General Public License as published by
C     the Free Software Foundation; either version 2 of the License, or
C     (at your option) any later version.
C     
C     This program is distributed in the hope that it will be useful,
C     but WITHOUT ANY WARRANTY; without even the implied warranty of
C     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C     GNU General Public License for more details.
C     
C     You should have received a copy of the GNU General Public License
C     along with this program; if not, write to the Free Software
C     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
C  
C     See also http://www.gnu.org/licenses/gpl.html

c     --- test routine - functions tested are below ---
      subroutine chkran()
      parameter(maxn=1000)
      real*4 x(maxn)
      integer*4 ix(maxn)
      real*4 xb(maxn),bins(maxn)

      print*,'chkran: Test 0: print a few rnd(0) calls'
      print*,'chkran: rnd(0)=',rnd(0),'  rnd(0)=',rnd(0),
     _     '  rnd(0)=',rnd(0)

      print*,'Test 1: uniform distribution - 1..10'
      do i=1,50
         x(i)= rnd(0)*10.0
c         print*,'i,x(i)=',i,x(i)
      enddo
c      print*,'before: '
c      print*,(x(i),i=1,50)
      call r4sort(x,50)
c      print*,'after: '
c      print*,(x(i),i=1,50)
      print*,'--'
      do i=1,50
         ix(i)= int(x(i))
c         print*,'i,ix(i)=',i,ix(i)
      enddo
      write(6,'(25i3)')(ix(i),i=1,25)
      write(6,'(25i3)')(ix(i),i=26,50)
      print*,' '

      print*,'Test 2: gaussian distribution (0,+-1)'
      do i=1,maxn
         x(i)= gauss0(0.0,1.0)
      enddo

      xmin=-3.0
      xmax= 3.0
      nbins=20
      ioob=15
      call mkbins(maxn,xmin,xmax,x,nbins,
     _     xb,bmin,bmax,bins,ioob)
      print*,'-3sigma to +3sigma:'
      write(6,'(20i4)')(int(bins(i)),i=1,nbins)


      print*,'Test 3: 2-sphere distribution'
      do i=1,45221
         xxx=rnd(4)
      enddo
      do i=1,100
         call spher2(phi,theta)
         write(6,'(a,2f7.1)')'spher2: ',phi,theta
      enddo
      print*,' '

      
      print*,'Warning: the above are only VERY elementary tests.'
      print*,'You have been warned!'

      return
      end



  
        real*4  function gauss(mean,sigma,seed)
c       chooses a number from a gaussian probability distribution
c       given randomising function  rnd(seed)

        integer*4 seed
        real*4    mean,sigma,p

        sigmax=4

        z=1.0
        p=0.0
        do while(z.gt.p)
           x=rnd(seed)
           y=mean*1.0 + 2.0*(x-0.5)*sigma*sigmax
           z=rnd(seed)
           p=exp(-(y-mean)*(y-mean)/2.0/sigma/sigma)
        enddo
        gauss=y
        return
        end

        real*4  function gauss0(mean,sigma)
c       chooses a number from a gaussian probability distribution
c       based on randt()  which can be flushed if desired.

c     INPUTS:
        real*4    mean,sigma

c     LOCAL:
        real*4    p
        sigmax=4

        z=1.0
        p=0.0
        do while(z.gt.p)
c           x= randt()
           x= rnd(17)           ! dummy seed
           y=mean*1.0 + 2.0*(x-0.5)*sigma*sigmax
c           z=randt()
           z=rnd(17)            ! dummy seed
           p=exp(-(y-mean)*(y-mean)/2.0/sigma/sigma)
        enddo
        gauss0=y
        return
        end


        function ipoisson(mu,seed)
c       chooses a number from a Poisson probability distribution
c       given randomising function  rnd(seed)

        integer*4 ipoisson,seed
        real*4    mu,sigma,p
        sigmax=4

c       ==== a gaussian approximation suffices and is easier for
c                              large values of  mu:
        if (mu.ge.20) then
            ipoisson=nint(gauss(mu,sqrt(mu*1.0),seed))
            return
            end if

c       ==== a genuine Poisson distribution ========
        sigma=sqrt(mu*1.0)
        z=1.0
        p=0.0
        do 71 while(z.gt.p)
            x=rnd(seed)
            i_y=int(x *( mu+ sigmax*sigma))
            z=rnd(seed)
c           ---- assign  p= mu^i_y exp(-mu) / i_y!
            p=exp(-1.0*mu)
            do 75 i=1,i_y
               p=p*mu /(float(i))
75         continue
        if (p.gt.1.0) write(6,*)'ERROR: Poisson prob. = ',p
71      continue

        ipoisson=i_y
        return
        end


        subroutine spher2(phi,theta)
c       chooses a number from a uniform probability distribution
c       on the 2-sphere (S^2)

C     OUPUTS:
        real*4    phi           ! 0 to 360 degrees
        real*4    theta         ! -90 to +90 degrees

C     LOCAL:
        real*4    p
        parameter(pi_180 = 3.14159265359/180.0)
        parameter(p180 = 180.0/3.14159265359)
        parameter(pi = 3.14159265359 )
        z=1.0
        p=0.0
        do while(z.gt.p)
           x= rnd(17)           ! dummy seed
           thetar= (x-0.5)* pi  ! theta in radians
           z=rnd(17)            ! dummy seed
           p=cos(thetar)
        enddo
        theta = thetar *p180
        phi = rnd(17)* 360.0

        return
        end



c     --- a WRAPPER function for other random generators (GSL etc) ---
      real*4 function rnd(seed)
c     --- as of astromisc-0.1.1 seed is *not* used
c     Use the environmental variable  GSL_RNG_SEED  e.g. 
c        prompt>> GSL_RNG_SEED=9876 astromisc
c     
      integer*4 seed  


C     INTERNAL:
      integer   ifirst
      save      ifirst

      real*8    ran8

      if(ifirst.ne.12345)then
         ifirst=12345
         call ran_op()
      endif

      ran8= rangsl()
      rnd=real(ran8)

      return
      end



c     randt() can be any random number generator
c     A standard source for a GPL random number generator is 
c     http://random.mat.sbg.ac.at/ftp/pub/software/gen/
c     or
c     http://statistik.wu-wien.ac.at/prng/
c     
c     A variation on version 3.0 of prng is  prng-3.0-f77wrap
c     which includes a simple  randt()  fortran wrapper.
c
c     Or just use GSL (GNU Scientific Library).
