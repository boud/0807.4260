/* 
   astromisc - library with miscellaneous GPL tools for astronomy research

   Copyright (C) 2004 Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
*/

#include "astromisc.h"

/* global variables within this file */
const gsl_rng_type * T;
gsl_rng * r;


/* open the random number generator */
/* from gsl-ref.info: 
   environment variables `GSL_RNG_TYPE' and `GSL_RNG_SEED' are read if set */
void RAN_OP(void)
{
  gsl_rng_env_setup();
  T= gsl_rng_default;
  r= gsl_rng_alloc(T);
};

/* call the random number generator */
double RANGSL(void)
{
  double x;
  x= gsl_rng_uniform(r);
  return x;
};


/* close the random number generator */
void RAN_CL(void)
{
  gsl_rng_free(r);
};



