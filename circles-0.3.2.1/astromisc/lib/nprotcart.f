C     Copyright (C) 2004 B. Roukema et al. <shape-univ at astro.uni.torun.pl>
C     
C     This program is free software; you can redistribute it and/or modify
C     it under the terms of the GNU General Public License as published by
C     the Free Software Foundation; either version 2 of the License, or
C     (at your option) any later version.
C     
C     This program is distributed in the hope that it will be useful,
C     but WITHOUT ANY WARRANTY; without even the implied warranty of
C     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C     GNU General Public License for more details.
C     
C     You should have received a copy of the GNU General Public License
C     along with this program; if not, write to the Free Software
C     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
C     
C     See also http://www.gnu.org/licenses/gpl.html


C     rotate from (gall,galb) to the North Pole
c     see also  nprotate.f


      subroutine nprotcart(gall,galb, 
     _     cart1,cart2,cart3, 
     _     carto1,carto2,carto3)


C     INPUTS:
      real*4  gall,galb         ! definition of rotation in degrees
      real*4  cart1,cart2,cart3 ! input position in cartesian coords

C     OUTPUT:
      real*4  carto1,carto2,carto3 ! output position in cart coords

C     LOCAL:
      real*4  gbold,glold
      real*4  rotZ(3,3),rotX(3,3),rotZX(3,3) ! for rotations
      real*4  rotZXZ(3,3)

      real*4  cart(3)           ! input position in cartesian coords
      real*4  cartout(3)           ! output position in cartesian

      data gbold,glold /-9e9,9e9/

      save   gbold,glold, rotZX,rotZXZ
      
      parameter(pi=3.1415926535898,pi_180=pi/180.0)
      parameter(tol=0.01)

      cart(1)=cart1
      cart(2)=cart2
      cart(3)=cart3
c      print*,'nprotcart: cart=',cart

c     --- is this a new rotation or the previous one? ---
      if(abs(gbold-galb).gt.tol.or.abs(glold-gall).gt.tol)then
         gbold=galb
         glold=gall

c     ---- rotation around Z-axis, so that gall,galb is in X-Z plane ----
c         phi= -gall*pi_180
         phi= +gall*pi_180  ! version 0.1.8 

         rotZ(1,1)=cos(phi)
         rotZ(1,2)=sin(phi)
         rotZ(1,3)=0.0
         rotZ(2,1)=-sin(phi)
         rotZ(2,2)=cos(phi)
         rotZ(2,3)=0.0
         rotZ(3,1)=0.0
         rotZ(3,2)=0.0
         rotZ(3,3)=1.0

c         print*,'rotZ:'
c         call printmat(rotZ,3,3)

c     ----rotation around Y-axis ----
         phi= pi/2.0 - galb*pi_180

         rotX(1,1)=cos(phi)
         rotX(1,2)=0.0
         rotX(1,3)=sin(phi)
         rotX(3,1)=-sin(phi)
         rotX(3,2)=0.0
         rotX(3,3)=cos(phi)
         rotX(2,1)=0.0
         rotX(2,2)=1.0
         rotX(2,3)=0.0

         call mult(rotZ,rotX,rotZX,3,3,3,3)

c         print*,'rotZX:'
c         call printmat(rotZX,3,3)

c     ---- rotate back around Z-axis ----
c         phi= +gall*pi_180
         phi= -gall*pi_180   ! version 0.1.8

         rotZ(1,1)=cos(phi)
         rotZ(1,2)=sin(phi)
         rotZ(1,3)=0.0
         rotZ(2,1)=-sin(phi)
         rotZ(2,2)=cos(phi)
         rotZ(2,3)=0.0
         rotZ(3,1)=0.0
         rotZ(3,2)=0.0
         rotZ(3,3)=1.0


         call mult(rotZX,rotZ,rotZXZ,3,3,3,3)

c         print*,'rotZXZ:'
c         call printmat(rotZXZ,3,3)
      endif

      call mult(cart,rotZXZ,cartout,1,3,3,3)

      carto1=cartout(1)
      carto2=cartout(2)
      carto3=cartout(3)

      return
      end

