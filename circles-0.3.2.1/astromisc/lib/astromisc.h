/* 
   astromisc - library with miscellaneous GPL tools for astronomy research

   Copyright (C) 2004 Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
*/

#include <stdio.h>
#include <math.h>
#include <gsl/gsl_integration.h>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_interp.h>
#include <gsl/gsl_spline.h>

#include <gsl/gsl_rng.h>

#include <stddef.h>
#include <errno.h>
#include <string.h>

#include "config.h"

#define ASTROMISC_WANT_DEBUG 0

/* 
   USER PARAMETERS - BEGIN
*/


/*
   USER PARAMETERS - END
   You probably should not change parameters below unless you're really
   sure you know what you're doing.
*/


/* check generation of  dodecahedral positions on sphere */

#define CHKDOD F77_FUNC(chkdod,CHKDOD)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
void CHKDOD( void	   
	     );


/* check generation of  dodecahedral positions on sphere */

#define CVTDOD F77_FUNC(cvtdod,CVTDOD)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
void CVTDOD( void	   
	     );


/* fortran wrapper to gsl_rng_env_setup */

#define RAN_OP F77_FUNC_(ran_op,RAN_OP)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
void RAN_OP( void	   
	     );


/* fortran wrapper to gsl_rng */
#define RANGSL F77_FUNC(rangsl,RANGSL)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
double RANGSL( void	   
	     );


#define RAN_CL F77_FUNC_(ran_cl,RAN_CL)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
void RAN_CL( void	   
	     );

/* check sorting of real*4 array */
#define CHKSORT F77_FUNC(chksort,CHKSORT)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
void CHKSORT( 
	     );

/* check finding local optima (max or min) */
#define CHKFIND F77_FUNC(chkfind,CHKFIND)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
void CHKFIND( 
	     );

/* check pseudorandom number generation functions */
#define CHKRAN F77_FUNC(chkran,CHKRAN)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
void CHKRAN( 
	     );



/* sort real*4 array */
#define R4SORT F77_FUNC(r4sort,R4SORT)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
void R4SORT( void *array, size_t *nmemb
	     );

int compar_float(const float *x1, const float *x2);


/* hunt_g (gsl) for index in  array */
#define HUNT_G F77_FUNC_(hunt_g,HUNT_G)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
void HUNT_G( double x_array[], size_t *nmemb, 
	     double *x_input,
	     size_t *index   /* output */
	     );

/* check hunt_g */
#define CHKHUN F77_FUNC(chkhun,CHKHUN)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
void CHKHUN( void
	     );



