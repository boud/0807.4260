C     Copyright (C) 2003 B. Roukema et al. <shape-univ at astro.uni.torun.pl>
C     
C     This program is free software; you can redistribute it and/or modify
C     it under the terms of the GNU General Public License as published by
C     the Free Software Foundation; either version 2 of the License, or
C     (at your option) any later version.
C     
C     This program is distributed in the hope that it will be useful,
C     but WITHOUT ANY WARRANTY; without even the implied warranty of
C     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C     GNU General Public License for more details.
C     
C     You should have received a copy of the GNU General Public License
C     along with this program; if not, write to the Free Software
C     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
C  
C     See also http://www.gnu.org/licenses/gpl.html


c       ==== this file holds some basic, first principles, not necessarily
c       computationally efficient matrix operations =====
c     Maybe you should try the BLAS library.
c        program testmat_8
        subroutine testmat_8
c       ==== tests matrix routines ====
        parameter(nmat=4)
        real*8   mat(nmat,nmat),inv(nmat,nmat),test(nmat,nmat)
        real*8   det4_8,cof4_8
        real*8   det3_8,cof3_8
        real*8   det2_8,cof2_8
        real*8   det1_8,cof1_8

        mat(1,1)=1.0
        mat(1,2)= -4.0
        mat(2,2)=2.0
        mat(1,3)=3.4
        mat(2,3)=9.0
        mat(3,1)=1.0
        mat(3,3)=2.0
        mat(3,4)=7.3
        mat(4,1)=-2.3
        mat(4,3)=6.7
        mat(4,4)=1.0

        print*,'matrix = '
        call printmat_8(mat,nmat,nmat)
        lev=0
        print*,'det4_8= ',det4_8(mat)
        print*,'end of first det4_8 calcn'

        call invert_8(mat,nmat,inv)
        print*,'inverse = '
        call printmat_8(inv,nmat,nmat)

        call mult_8(mat,inv,test,nmat,nmat,nmat,nmat)
        print*,' '
        print*,' product of the two is: '
        call printmat_8(test,nmat,nmat)
        end


        subroutine printmat_8(mat,m,n)
c       ============================
        integer*4  m,n
        real*8     mat(n,m)

        do i=1,m
            write(6,'(10g9.2)')(mat(j,i),j=1,n)
        enddo
        return
        end

        real*8  function det4_8(mat)
c       ==========================
        parameter(nmat=4,n=nmat)
        real*8      mat(nmat,nmat)
c        automatic j
        real*8      cof4_8

        det4_8=0.0d0
        do j=1,n
           det4_8=det4_8+mat(j,1)*cof4_8(mat,1,j)
        enddo

        return
        end

        real*8  function det3_8(mat)
c       ==========================
        parameter(nmat=3,n=nmat)
        real*8      mat(nmat,nmat)
c        automatic j
        real*8      cof3_8

        det3_8=0.0d0
        do j=1,n
           det3_8=det3_8+mat(j,1)*cof3_8(mat,1,j)
        enddo

        return
        end

        real*8  function det2_8(mat)
c       ==========================
        parameter(nmat=2,n=nmat)
        real*8      mat(nmat,nmat)
c        automatic j
        real*8      cof2_8

c        det2_8=0.0d0
c        do j=1,n
c           det2_8=det2_8+mat(j,1)*cof2_8(mat,1,j)
c        enddo

        det2_8= mat(1,1)*mat(2,2) - mat(1,2)*mat(2,1) ! faster calcn

        return
        end

        real*8  function det1_8(mat)
c       ==========================
        parameter(nmat=1,n=nmat)
        real*8      mat(nmat,nmat)
c        automatic j
        real*8      cof1_8

        det1_8=mat(1,1)

        return
        end

        real*8  function cof4_8(mat,i,j)
c       ================================
        integer*4  i,j
        parameter(nmat=4,n=nmat)
        real*8  mat(nmat,nmat)
c        automatic ia,ja
c        automatic adj
        real*8 adj(nmat-1,nmat-1)
        real*8 det3_8

c       ==== calculate cofactor A_ij= (-1)^{i+j} * det_8 M_ij ====
        do ia=1,i-1
            do ja=1,j-1
                adj(ja,ia)=mat(ja,ia)
            enddo
            do ja=j,n-1
                adj(ja,ia)=mat(ja+1,ia)
            enddo
        enddo
        do ia=i,n-1
            do ja=1,j-1
                adj(ja,ia)=mat(ja,ia+1)
            enddo
            do ja=j,n-1
                adj(ja,ia)=mat(ja+1,ia+1)
            enddo
        enddo
        if(2*((i+j)/2).eq.i+j)then
            cof4_8=det3_8(adj)
        else
            cof4_8=-1.0*det3_8(adj)
        endif

        return
        end

        real*8  function cof3_8(mat,i,j)
c       ================================
        integer*4  i,j
        parameter(nmat=3,n=nmat)
        real*8  mat(nmat,nmat)
c        automatic ia,ja
c        automatic adj
        real*8 adj(nmat-1,nmat-1)
        real*8 det2_8

c       ==== calculate cofactor A_ij= (-1)^{i+j} * det_8 M_ij ====
        do ia=1,i-1
            do ja=1,j-1
                adj(ja,ia)=mat(ja,ia)
            enddo
            do ja=j,n-1
                adj(ja,ia)=mat(ja+1,ia)
            enddo
        enddo
        do ia=i,n-1
            do ja=1,j-1
                adj(ja,ia)=mat(ja,ia+1)
            enddo
            do ja=j,n-1
                adj(ja,ia)=mat(ja+1,ia+1)
            enddo
        enddo
        if(2*((i+j)/2).eq.i+j)then
            cof3_8=det2_8(adj)
        else
            cof3_8=-1.0*det2_8(adj)
        endif

        return
        end

        real*8  function cof2_8(mat,i,j)
c       ================================
        integer*4  i,j
        parameter(nmat=2,n=nmat)
        real*8  mat(nmat,nmat)
c        automatic ia,ja
c        automatic adj
        real*8 adj(nmat-1,nmat-1)

c       ==== calculate cofactor A_ij= (-1)^{i+j} * det_8 M_ij ====
        do ia=1,i-1
            do ja=1,j-1
                adj(ja,ia)=mat(ja,ia)
            enddo
            do ja=j,n-1
                adj(ja,ia)=mat(ja+1,ia)
            enddo
        enddo
        do ia=i,n-1
            do ja=1,j-1
                adj(ja,ia)=mat(ja,ia+1)
            enddo
            do ja=j,n-1
                adj(ja,ia)=mat(ja+1,ia+1)
            enddo
        enddo
        
        cof2_8=adj(1,1)
        return
        end



        subroutine invert_8(mat,n,inv)
c       ============================
        parameter(nmat=4)
        real*8    mat(nmat,nmat),inv(nmat,nmat), x
        real*8    det4_8,cof4_8
        real*8    det3_8,cof3_8
        real*8    det2_8,cof2_8

        if(n.gt.nmat)print*,'invert_8 ERROR: n,nmat=',n,nmat

        if(n.eq.4)then
           x=det4_8(mat)
        elseif(n.eq.3)then
           x=det3_8(mat)
        elseif(n.eq.2)then
           x=det2_8(mat)
        else
           x=mat(1,1)
        endif
        if(x.eq.0.0)then
           print*,'ERROR: determinant zero. Null matrix output.'
        else
           if(n.eq.1)then
              inv(1,1)=1.0/mat(1,1)
           else
              do i=1,n
                 do j=1,n
                    if(n.eq.4)then
                       inv(j,i)=cof4_8(mat,j,i)/x
                    elseif(n.eq.3)then
                       inv(j,i)=cof3_8(mat,j,i)/x
                    elseif(n.eq.2)then
                       inv(j,i)=cof2_8(mat,j,i)/x
                    endif

c     ----   transpose of adjugate matrix for testing only ----
c     -             inv(j,i)=cof4_8(mat,i,j)
c     -----
                 enddo
              enddo
           endif
        endif

        return
        end


      subroutine tran_8(mat,n,tran)
c     ============================
      parameter(nmat=4)
      real*8    mat(nmat,nmat),tran(nmat,nmat)
      do i=1,4
         do j=1,4
            tran(j,i)=mat(i,j)
         enddo
      enddo
      return
      end

         


        subroutine mult_8(a,b,c,ma,na,mb,nb)
c       ==== multiplies two matrices with ma, mb rows, na, nb columns;
c       with rows being the second fortran index and cols the first ====

        integer*4       ma,na,mb,nb
        real*8  a(na,ma),b(nb,mb),c(nb,ma)

c        print*,'mult: a,b'
c        call printmat(a,ma,na)
c        print*,'---------'
c        call printmat(b,mb,nb)
c        print*,'-------- '
        if(na.ne.mb)stop "matrix sizes don't match!!!!"

        do i=1,ma
            do j=1,nb
                c(j,i)=0.0
                do index=1,na
                    c(j,i)=c(j,i)+a(index,i)*b(j,index)
                enddo
            enddo
        enddo
c        print*,'mult: c'
c        call printmat(c,nb,ma)
c        print*,'------------end of mult:'

        return
        end
