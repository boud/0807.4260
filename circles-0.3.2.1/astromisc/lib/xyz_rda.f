C     Copyright (C) 2004 B. Roukema et al. <shape-univ at astro.uni.torun.pl>
C     
C     This program is free software; you can redistribute it and/or modify
C     it under the terms of the GNU General Public License as published by
C     the Free Software Foundation; either version 2 of the License, or
C     (at your option) any later version.
C     
C     This program is distributed in the hope that it will be useful,
C     but WITHOUT ANY WARRANTY; without even the implied warranty of
C     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C     GNU General Public License for more details.
C     
C     You should have received a copy of the GNU General Public License
C     along with this program; if not, write to the Free Software
C     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
C  
C     See also http://www.gnu.org/licenses/gpl.html

      subroutine  xyz_rda(x,y,z,r,del,alph)
      real*4   x,y,z            !inputs (units imply units of  r )
      real*4   r,del,alph       !outputs (del in deg; alph in hours)

      parameter(pi_180= 3.14159265359/180.0) ! conversion deg -> rad
      parameter(pi180h= pi_180*15.0)       ! conversion hours -> rad

c     --- inverses for speed ---
      parameter(p_180= 1.0/(3.14159265359/180.0)) ! conversion deg -> rad
      parameter(p180h= 1.0/(pi_180*15.0))       ! conversion hours -> rad
      
      parameter(tol=1e-5)

      rr=x*x+y*y+z*z
      if(rr.eq.0.0)then
         r=0.0
         del=99.9
         alph=99.9
      else
         r= sqrt(rr)
         
c         del= asin(z/r) /pi_180
         del= asin(z/r) *p_180  ! 03.04.2003 

c         print*,'xyz_rda START debug: z,r =',z,r
         cos2th=1.0-z*z/(r*r)
c         print*,'xyz_rda debug: cos2th=',cos2th
         if(cos2th.lt.tol)then
            alph=9.99           !  arbitrary r.a. since close to NP or SP
         else
            cosalph= x /r /sqrt(cos2th)
            if(cosalph.gt.1.0-tol)then
               cosalph=1.0-tol
               if(cosalph.gt.1.0+10*tol)then
                  print*,'xyz_rda WARNING: x,y,z,cosalph=',
     _                 x,y,z,cosalph
               endif
            elseif(cosalph.lt.-1.0+tol)then
               cosalph=-1.0+tol
               if(cosalph.lt.-1.0-10*tol)then
                  print*,'xyz_rda WARNING: x,y,z,cosalph=',
     _                 x,y,z,cosalph
               endif
            endif
c            print*,'xyz_rda debug: cosalph=',cosalph
c            alph= acos(cosalph) /pi180h
            alph= acos(cosalph) *p180h  ! 03.04.2003
c            print*,'xyz_rda debug: alph=',alph
            if(y.lt.0)then
               alph= 24.0 - alph
            endif
         endif
      endif
      return
      end

      function ixm(a)
      b=abs(a)
      ixm= int( (b-int(b))*60.0 )
      return
      end

      function xs(a)
      b=abs(a)*60.0
      xs= (b-int(b))*60.0
      return
      end



      subroutine  xyz_8rda(x,y,z,r,del,alph)
      implicit real*8(a-h,o-z)
      real*8   x,y,z            !inputs (units imply units of  r )
      real*8   r,del,alph       !outputs (del in deg; alph in hours)

      parameter(pi_180= 3.14159265359/180.0) ! conversion deg -> rad
      parameter(pi180h= pi_180*15.0)       ! conversion hours -> rad
      
      print*,'xyz_rda8: WARNING: this version rounding unstable'

      rr=x*x+y*y+z*z
      if(rr.eq.0.0)then
         r=0.0
         del=99.9
         alph=99.9
      else
         r= sqrt(rr)
         
         del= asin(z/r) /pi_180
         
         alph= acos( x /r /sqrt(1.0-z*z/r/r) ) /pi180h
         if(y.lt.0)then
            alph= 24.0 - alph
         endif
      endif
      return
      end


C     ---- WARNING: this is an old version for checking only: 
C     use it only if you are doing some testing and know what you are doing ---

      subroutine  xyz_notoler(x,y,z,r,del,alph)
      implicit real*4(a-h,o-z)
      real*4   x,y,z            !inputs (units imply units of  r )
      real*4   r,del,alph       !outputs (del in deg; alph in hours)

      parameter(pi_180= 3.14159265359/180.0) ! conversion deg -> rad
      parameter(pi180h= pi_180*15.0)       ! conversion hours -> rad
      
      rr=x*x+y*y+z*z
      if(rr.eq.0.0)then
         r=0.0
         del=99.9
         alph=99.9
      else
         r= sqrt(rr)
         
         del= asin(z/r) /pi_180
         
         alph= acos( x /r /sqrt(1.0-z*z/r/r) ) /pi180h
         if(y.lt.0)then
            alph= 24.0 - alph
         endif
      endif
      return
      end

