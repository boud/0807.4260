C   astromisc - library with miscellaneous GPL tools for astronomy research
C
C   Copyright (C) 2005 Boud Roukema
C     
C     This program is free software; you can redistribute it and/or modify
C     it under the terms of the GNU General Public License as published by
C     the Free Software Foundation; either version 2 of the License, or
C     (at your option) any later version.
C     
C     This program is distributed in the hope that it will be useful,
C     but WITHOUT ANY WARRANTY; without even the implied warranty of
C     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C     GNU General Public License for more details.
C     
C     You should have received a copy of the GNU General Public License
C     along with this program; if not, write to the Free Software
C     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
C  
C     See also http://www.gnu.org/licenses/gpl.html

      subroutine chkhun()
      
      real*4  x(10)
      real*8  x8(10), xx8
c      real*4  x(50)
      
      data x /6.5,-5.0, 45.0, 3.3, 3.0, -99.0,-17.0, 
     _     3.9,3.8, 3.7/

c      data x/
c     _     9.99741745, 1.62909877, 2.82617807, 9.47201061, 2.31656528,
c     $     4.84973621,9.57476997, 7.44305372, 5.4004364, 7.39952993,
c     $     7.59943771, 6.58636618,3.15637612, 8.04403019, 5.19672108,
c     $     1.68572426, 4.75529718, 3.92313981,2.21667695, 2.1319046,
c     $     0.303352058, 3.33539248, 1.9414885, 9.43716812,5.79931688,
c     $     8.98304844, 6.65563965, 4.98610306, 5.60628223, 1.82284641
c     $     ,2.96525526, 1.17408931, 0.629176617, 6.48125601, 7.2541852,
c     $     6.37131166,7.13885069, 0.995762408, 6.99267197, 1.07812476,
c     $     1.29242754, 5.02403021,2.07779908, 2.88910294, 0.831747949,
c     $     1.28124225, 5.47371387, 0.823196054,2.92141438, 8.91623688 /

      print*,'before r4sort, x=',x
      call r4sort(x,10)
c      call r4sort(x,50)
      print*,'after r4sort, x=',x

      do i=1,10
         x8(i)=x(i)  ! convert to real*8
      enddo


      do j=1,12
         xx8= -6.0 + real(j)*1.2
         print*,' '
c         print*,'chkhunt_g.f: x8(1)=',x8(1), '  xx8=',xx8
         imax= 10
         call hunt_g(x8,imax, xx8, index)
         print*,'chkhunt_g.f: index=',index,
     _        ' x(max(1,index)), xx8, x(min(10,index+1))='
         print*,'chkhunt_g.f::', 
     _        x(max(1,index)), xx8, x(min(10,index+1))
      enddo

      return
      end

      
