C     angle - angle between two vectors in 3D cartesian coordinates
C     Copyright (C) 2003 B. Roukema <shape-univ at astro.uni.torun.pl>
C     
C     This program is free software; you can redistribute it and/or modify
C     it under the terms of the GNU General Public License as published by
C     the Free Software Foundation; either version 2 of the License, or
C     (at your option) any later version.
C     
C     This program is distributed in the hope that it will be useful,
C     but WITHOUT ANY WARRANTY; without even the implied warranty of
C     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C     GNU General Public License for more details.
C     
C     You should have received a copy of the GNU General Public License
C     along with this program; if not, write to the Free Software
C     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
C  
C     See also http://www.gnu.org/licenses/gpl.html

      real*4     function angle(x1,y1,z1,x2,y2,z2) ! in radians
c     ==== calculates angle between these two vectors [origin (0,0,0) ] ==== 
      real*4     x1,y1,z1, x2,y2,z2

      r1sq=x1*x1+y1*y1+z1*z1
      r1=sqrt(r1sq)
      if(r1.eq.0.0)print*,'angle ERROR: x1,y1,z1=',x1,y1,z1

      r2sq=x2*x2+y2*y2+z2*z2
      r2=sqrt(r2sq)
      if(r2.eq.0.0)print*,'angle ERROR: x2,y2,z2=',x2,y2,z2

      dot= x1*x2+y1*y2+z1*z2    ! dot product
      costh = dot/r1/r2
      if(costh.lt.-1.0)costh=-1.0
      if(costh.gt.1.0)costh=1.0

      angle= acos(costh)
      return
      end


      real*8     function angle8(x1,y1,z1,x2,y2,z2) ! in radians
c     ==== calculates angle between these two vectors [origin (0,0,0) ] ==== 
      real*8     x1,y1,z1, x2,y2,z2
      real*8     r1sq,r1,r2sq,r2, dot, costh

      r1sq=x1*x1+y1*y1+z1*z1
      r1=sqrt(r1sq)
      if(r1.eq.0.0d0)print*,'angle ERROR: x1,y1,z1=',x1,y1,z1

      r2sq=x2*x2+y2*y2+z2*z2
      r2=sqrt(r2sq)
      if(r2.eq.0.0d0)print*,'angle ERROR: x2,y2,z2=',x2,y2,z2

      dot= x1*x2+y1*y2+z1*z2    ! dot product
      costh = dot/r1/r2
      if(costh.lt.-1.0d0)costh=-1.0d0
      if(costh.gt.1.0d0)costh=1.0d0

      angle8= acos(costh)
      return
      end


