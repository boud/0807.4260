/* 
   cosmdist - standard FLRW cosmological distance functions

   Copyright (C) 2004 Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

*/


#include "cosmdist.h"

struct cosm_param_chain_element *p_chain ;  /*  local to this file */


/*  
    get proper motion instance as a function 
    of redshift, or inversely (for the domain up to (pi/2)R_C)
*/

/* proper motion distance (Weinberg 1972) */

double cosm_get_pm_dist( double H_0, double omm, double omlam, double w_0, 
			 double redshift 
			 )
{
  /* put local cosm parameters into a structure */
  struct cosm_struct_local_params  p_input;

  /*  struct cosm_param_chain_element *p_chain ; */
  /* int want_debug = COSM_WANT_DEBUG ; */    /* local */
  int i_cosm_check_initialisation; 
  double  zz, dd;   
  double  aa; /* scale factor input */

  /* statement section */
  p_input.H_0= H_0;
  p_input.omm= omm;
  p_input.omlam= omlam;
  p_input.w_0= w_0;


  if(redshift < -COSM_TOLERANCE)
    {
      errno= EDOM;   /* negative redshifts are in the future */
      zz= 0.0;
    }
  else
    {
      zz= redshift;
    };

  aa= 1.0/(1.0+ zz);

  i_cosm_check_initialisation = 
    cosm_check_initialisation( p_input, 
			       &p_chain
			       );
  

  dd= gsl_spline_eval( p_chain->spline_object_comov, 
		       aa, p_chain->spline_accel_comov );

  
  if ( (p_chain->params).k_curv == -1 )
    {
      dd= (p_chain->params).R_C * sinh( dd/((p_chain->params).R_C) );
    }
  else if ( (p_chain->params).k_curv == 1 )
    {
      dd= (p_chain->params).R_C * sin( dd/((p_chain->params).R_C) );
    };
  
  return dd;

}


/* proper motion distance (Weinberg 1972) */

double cosm_get_pm_dist_inv( 
			    double H_0, double omm, 
			    double omlam, double w_0, 
			    double distance
			    )
{
  /* put local cosm parameters into a structure */
  struct cosm_struct_local_params  p_input;

  /*  struct cosm_param_chain_element *p_chain ; */
  int want_debug = (0 || COSM_WANT_DEBUG );     /* local */
  int i_cosm_check_initialisation; 
  double  dd;   
  double  aa, zz; /* scale factor, redshift output */



  /* statement section */
  p_input.H_0= H_0;
  p_input.omm= omm;
  p_input.omlam= omlam;
  p_input.w_0= w_0;


  if(want_debug)
    {
      printf("cosm_get_pm_dist_inv:\n");
      printf(" distance= %.4f\n",distance );
    };

  if(distance < -COSM_TOLERANCE)
    {
      errno= EDOM; /* negative redshifts are in the future */ 
      dd= 0.0;
    }
  else
    {
      dd= distance;
    };


  i_cosm_check_initialisation = 
    cosm_check_initialisation( p_input, 
			       &p_chain
			       );


  if(distance > (p_chain->params).pm_max)
    {
      errno = EDOM;  /* warning that distance is too big */
      dd= (1-COSM_TOLERANCE) * (p_chain->params).pm_max;
    };


  
  /* 
     First convert the proper motion distance to a comoving distance
     by doing either an inverse  sinh  or an inverse  sin  for 
     curved cases and nothing for the flat case.
  */


  if(want_debug)
    {
      printf("cosm_get_pm_dist_inv:\n");
      printf(" (p_chain->params).k_curv = %d\n",(p_chain->params).k_curv );
    };

  if(want_debug)
    {
      printf("Based on the p_chain = %d  address we have:\n",(int)p_chain);
      printf("(p_chain->params).H_0 = %.2f \n",(p_chain->params).H_0);
    };

  if( (p_chain->params).k_curv == -1)  /* hyperbolic case */
    {
      if(want_debug)
	{
	  printf("cosm_get_pm_dist_inv:  hyperbolic case \n");
	};

      dd= distance/ (p_chain->params).R_C;
      dd= asinh(dd) *(p_chain->params).R_C;   /* asinh from math.h */

    }
  else if( (p_chain->params).k_curv == 1)  /* spherical case */
    {
      if(want_debug)
	{
	  printf("cosm_get_pm_dist_inv:  spherical case \n");
	};
      dd= distance/ (p_chain->params).R_C;

      if(1.0-COSM_EQUATOR_TOLERANCE < dd && dd < 1.0+COSM_EQUATOR_TOLERANCE)
	{
	  dd= M_PI_2 * (p_chain->params).R_C;
	}
      else if( dd > 1.0+COSM_EQUATOR_TOLERANCE)
	{
	  dd= M_PI_2 * (p_chain->params).R_C;
	  errno= EDOM;  /* This is a bad error: clearly out of domain */
	}
      else
	{
	  dd= asin(dd) *(p_chain->params).R_C;
	};
    };

  aa= gsl_spline_eval( p_chain->spline_object_comov_inv, 
		       dd, p_chain->spline_accel_comov_inv );
    
      
  if(aa < COSM_TOLERANCE)
    {
      zz = 1.0/COSM_TOLERANCE;
    }
  else
    {
      zz= 1.0/aa - 1.0;
    };


  return zz;

}



/* http://arXiv.org/abs/astro-ph/9904172 - Ue-Li Pen fitting formula  */

double cosm_eta_Ue_Li_Pen(double redshift, double omm)
{
  double omm_local;   /* local copy checked not too close to 0.0 or 1.0 */
  double s, scube;
  double zp1, zp1_2, zp1_3, zp1_4;  /* powers of (1+z) */
  double x;

  if(omm < COSM_TOLERANCE)
    {
      errno= EDOM;  /* omm too small */
      omm_local = COSM_TOLERANCE;
    }
  else if (omm > 1.0-COSM_TOLERANCE)
    {
      errno= EDOM;  /* omm too big */
      omm_local = 1.0- COSM_TOLERANCE;
    }
  else
    {
      omm_local = omm;
    };
  

  scube = (1.0-omm_local)/omm_local;

  /* speed up the calculation a bit */
  zp1= 1.0+redshift;
  zp1_2 = zp1*zp1;
  zp1_3 = zp1_2*zp1;
  zp1_4 = zp1_3*zp1;

  s = pow(scube,(1.0/3.0));
  
  x = 2.0* sqrt(scube+1.0) * 
    pow( (
     zp1_4 -0.1540 *s *zp1_3 + 0.4304 *s*s *zp1_2 + 0.19097 *scube*zp1
     + 0.066941 *scube*s
     ), -0.125);

  return x;

}
  


double cosm_get_pm_Ue_Li_Pen( double H_0, double omm, 
			      double redshift 
			      )
{

  double x;

  x = COSM_C_ON_H_0_MPC /(0.01 * H_0) 
    *( cosm_eta_Ue_Li_Pen(0.0,  omm)
      - cosm_eta_Ue_Li_Pen(redshift,  omm) );
  return x;
}

