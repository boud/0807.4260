/* 
   cosmdist - standard FLRW cosmological distance functions

   Copyright (C) 2004 Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

*/

#include <stdio.h>
#include <sys/types.h>

#include <string.h>

#include "system.h"

#ifdef HAVE_ARGP_H
#  include <argp.h>
#endif  /* HAVE_ARGP_H */


#include <stdlib.h>


#define EXIT_FAILURE 1

#if ENABLE_NLS
# include <libintl.h>
# define _(Text) gettext (Text)
#else
# define textdomain(Domain)
# define _(Text) Text
#endif
#define N_(Text) Text

#include <math.h>
#include <gsl/gsl_integration.h>

#include "lib/cosmdist.h" 


char *xmalloc ();
char *xrealloc ();
char *xstrdup ();

#ifdef HAVE_ARGP_H
  static error_t parse_opt (int key, char *arg, struct argp_state *state);
  static void show_version (FILE *stream, struct argp_state *state);
#else
  static void show_version (FILE *stream);
#endif

/* argp option keys */
enum {DUMMY_KEY=129
      ,NOWARN_KEY
};

/* Option flags and variables.  These are initialized in parse_opt.  */

int want_verbose;		/* --verbose */
int want_no_warn;		/* --no-warn */
int want_pm;                    /* -m --proper-motion */
int want_inverse;               /* -z --redshift */
int want_time;                  /* -t --time */
int want_all;                   /* -a --all */
int want_fortran;               /* -f --fortran */
int want_check;                 /* -c --check */
int want_horizon;               /* -i --horizon */
int want_curvature;                   /* -k --curvature */

/* local cosmological parameters */
char *argument_string;
int  nlen_argument_string;
double H_0;
double omm;
double omlam;
double w_0;

char ** endptr;  /*  error handling by   strtod  -  info strtod  */
int  ch;  /* for removing end-of-line after scanf */


#ifdef HAVE_ARGP_H
static struct argp_option options[] =
{
  { "verbose",     'v',           NULL,            0,
    N_("Print more information"), 0 },
  { "all",     'a',           NULL,            0,
    N_("evaluate all 6 functions for default input redshift"), 0 },
  { "check",     'c',           NULL,            0,
    N_("make some Checks that the routines work correctly"), 0 },
  { "proper-motion",     's',           NULL,            0,
    N_("proper motion distance using Sin or Sinh"), 0 },
  { "tangential",     's',           NULL,            0,
    N_("proper motion distance using Sin or Sinh"), 0 },
  { "time",     't',           NULL,            0,
    N_("Time"), 0 },
  { "age",     't',           NULL,            0,
    N_("Time"), 0 },
  { "inverse",     'z',           NULL,            0,
    N_("redshift"), 0 },
  { "redshift",     'z',           NULL,            0,
    N_("redshift"), 0 },
  { "H_0",     'h',           N_("HUBBLE_CONSTANT"),            0,
    N_("Hubble constant in km/s/Mpc"), 0 },
  { "omega_matter",     'm',           N_("OMEGA_MATTER"),            0,
    N_("density of non-relativistic Matter"), 0 },
  { "omega_lambda",     'l',           N_("OMEGA_LAMBDA"),            0,
    N_("Lambda - cosmological or quintessence constant"), 0 },
  { "w_0",     'q',           N_("QUINTESSENCE_PARAMETER_0"),            0,
    N_("first order Quintessence parameter"), 0 },
  { "fortran",     'f',           NULL,            0,
    N_("Fortran wrapper"), 0 },
  { "horizon",     'i',           NULL,            0,
    N_("horIzon distance (comoving)"), 0 },
  { "curvature",     'k',           NULL,            0,
    N_("curvature radius (comoving)"), 0 },
  { "no-warn",     NOWARN_KEY,    NULL,            0,
    N_("disable Warnings"), 0 },
  { NULL, 0, NULL, 0, NULL, 0 }
};

/* The argp functions examine these global variables.  */
const char *argp_program_bug_address = "<boud at astro.uni.torun.pl>";
void (*argp_program_version_hook) (FILE *, struct argp_state *) = show_version;

static struct argp argp =
{
  options, parse_opt, N_(" "),
  N_("standard FLRW cosmological distance functions"),
  NULL, NULL, NULL
};
#endif  /* HAVE_ARGP_H */



int
main (int argc, char **argv)
{

  /*  double H_0, omm, omlam, w_0;  */
  double input_parameter;
  double redshift=-99.9, distance=-99.9, time=-99.9;   /* input value */
  double dist=-99.9, time_output=-99.9, dist2;
  /* double z_single; */
  double red_inv, red_inv2;

  int want_debug = (0 || COSM_WANT_DEBUG);

  /* for testing */
  double z1log = -2.0;
  double z2log = +3.1;
  double dztest;
  double danalyt; /* analytical distance */
  double dnumer, dtest;  /* numerical distance, diff */
  double tanalyt; /* analytical time */
  double tnumer, ttest;  /* numerical time, diff */
  double zinverse, zdiff;  /* inverse redshift, difference  */
  double omm_1 = 0.1 ;
  double omm_2 = 0.9 ;
  double dommtest, omm_test ;

  double xx;  /* very local (few lines only) variable */

  double testtoler = 0.1;  /* Mpc or Gyr */
  int iztest, maxztest = 10000; 
  double testtoler_Ue_Li_Pen_less02 = 0.05;  /* relative error for omm < 0.2 */
  double testtoler_Ue_Li_Pen_more02 = 0.005;  /* relative error for omm > 0.2 */
  int maxztest_Ue_Li_Pen = 10; /* the fitting formula: only few % accuracy*/
  int iommtest, maxommtest = 9;

  textdomain(PACKAGE);

  /* Set up default values.  */
  want_verbose = 0;
  want_no_warn = 0;
  want_pm = 0;
  want_inverse = 0;
  want_time = 0;
  want_all = 0;  
  want_check = 0;  
  want_fortran = 0;
  want_horizon = 0;
  want_curvature = 0;

  H_0 = 70.0;
  omm = 0.3;
  omlam = 0.7;
  w_0 = -1.0;
  
  /*  redshift = 3.0; */

#if HAVE_ARGP_H
  argp_parse(&argp, argc, argv, 0, NULL, NULL);
#else
  printf("Warning: no command line options since a recent glibc is not installed.\n");
#endif  /* HAVE_ARGP_H */


  /* TODO: do the work */

  /*  redshift = 3.0;  */


  /* read parameter from standard input */

  if(want_verbose)
    {
      printf("Enter ");
      if(!want_inverse)
	{
	  printf("redshift: ");
	}
      else if(!want_time)
	{
	  printf("distance in Mpc: ");
	}
      else
	{
	  printf("time in Gyr: ");
	};
    };

  argument_string= xmalloc(40);  /* unlikely to need more than 40 bytes */
  scanf("%40s", argument_string);
  while ( (ch=fgetc(stdin)) != '\n' && ch != EOF )
    {
    };

  if(want_debug)
    {
      printf("string= %s\n",argument_string);
    };
  
  input_parameter= strtod(argument_string,endptr);

  if(errno != 0)
    {
      printf("ERROR: errno= %d  in reading H_0 value\n",errno);
      exit (errno);
    };
  
  if(want_verbose)
    {
    printf("read parameter: %.4f \n",input_parameter);
    };

  if(want_inverse)
    {
      if(!want_time)
	distance= input_parameter;
      else
	time= input_parameter;
    }
  else
    redshift= input_parameter;


  /*  cosm_dist_init();  This is called by the functions if needed */

  /*
    H_0 = 100.0;
    omm = 1.0;
    omlam = 0.0;
  */

  if(want_verbose)
    {
      printf("H_0= %.2f  Omega_matter= %.4f  Omega_lambda= %.4f",
	     H_0,omm,omlam);
      printf(" w_0= %.4f\n",w_0);

      /*
	printf("\n");
	printf("First call to cosm_get_comov_dist: \n"); 
      */
    };


  /* 
     Using cosmdist as a C library
     This comment is referred to by cosmdist.texinfo - please do not
     remove it unless you fix both simultaneously.
  */


  /*  comoving distance and inverse */

  if(want_all || (!want_pm && !want_inverse && !want_time))
    {
      errno=0;
      dist= cosm_get_comov_dist( H_0, omm, omlam, w_0, redshift );
      if(errno != 0)
	  perror("ERROR during cosm_get_comov_dist");

      if(want_verbose)
	printf("comoving distance is %.2f  Mpc \n", dist);
      else
	printf("%.2f  ",dist);
    };
      
  if(want_all || (!want_pm && want_inverse && !want_time))
    {
      if(want_all)
	distance=dist;
      errno=0;
      red_inv= 
	cosm_get_comov_dist_inv( H_0, omm, omlam, w_0, distance );
      if(errno != 0)
	  perror("ERROR during cosm_get_comov_dist_inv");

      if(want_verbose)
	printf("redshift is %.4f  \n", red_inv);
      else
	printf("%.4f  ",red_inv);
    };


  /* proper motion distance and inverse */

  if(want_all || (want_pm && !want_inverse && !want_time))
    {
      errno=0;
      dist= cosm_get_pm_dist( H_0, omm, omlam, w_0, redshift );
      if(errno != 0)
	perror("ERROR during cosm_get_pm_dist");

      if(want_verbose)
	printf("proper motion distance is %.2f  Mpc \n", dist);
      else
	printf("%.2f  ",dist);
    };
      
  if(want_all || (want_pm && want_inverse && !want_time))
    {
      if(want_all)
	distance=dist;
      errno=0;
      red_inv= 
	cosm_get_pm_dist_inv( H_0, omm, omlam, w_0, distance );
      if(errno != 0)
	  perror("ERROR during cosm_get_pm_dist_inv");

      if(want_verbose)
	printf("redshift is %.4f \n", red_inv);
      else
	printf("%.4f  ",red_inv);
    };


  /* cosmological time */

  if(want_all || (!want_inverse && want_time))
    {
      errno=0;
      time_output= 
	cosm_get_comov_time( H_0, omm, omlam, w_0, redshift );
      if(errno != 0)
	  perror("ERROR during cosm_get_comov_time");

      if(want_verbose)
	printf("cosmological time is %.2f Gyr\n", time_output);
      else
	printf("%.2f  ",time_output);
    };
      
  if(want_all || (want_inverse && want_time))
    {
      if(want_all)
	time=time_output;
      errno=0;
      red_inv= 
	cosm_get_comov_time_inv( H_0, omm, omlam, w_0, time );
      if(errno != 0)
	  perror("ERROR during cosm_get_comov_time_inv");

      if(want_verbose)
	printf("redshift is %.4f  \n", red_inv);
      else
	printf("%.4f  ",red_inv);
    };


  /* horizon, curvature radius */
  if(want_all || want_horizon)
    {
      errno=0;
      xx = cosm_get_horizon(H_0, omm, omlam, w_0);
      if(errno != 0)
	  perror("ERROR during cosm_get_horizon");

      if(want_verbose)
	printf("horizon is %.2f  \n", xx);
      else
	printf("%.2f  ",xx);
    };

  if(want_all || want_curvature)
    {
      errno=0;
      xx = cosm_get_R_C(H_0, omm, omlam, w_0);
      if(errno != 0)
	  perror("ERROR during cosm_get_R_C");

      if(want_verbose)
	printf("R_C is %.2f Mpc \n", xx);
      else
	printf("%.2f  ",xx);
    };
      

  /*  following all outputs, add an end-of-line */
  if(!want_verbose)
    printf("\n");



  if(want_check)
    {
      /*  Einstein-de Sitter universe */ 
      if(want_verbose)
	{
	  printf("\nWill compare cosm_get_comov_dist (and _time) ");
	  printf("with analytical formulae\n");
	  printf("for an Einstein-de Sitter universe");
	  printf(" (flat, zero cosmological constant).\n");
	  printf("No news = (probably) good news.\n");
	}

      dztest = (z2log-z1log)/ ((double)maxztest);
      for (iztest = 0; iztest < maxztest; iztest++)
	{
	  redshift = pow( 10.0, z1log + (iztest+0.5)*dztest );
	  danalyt = 2.0* COSM_C_ON_H_0_MPC /(0.01 * H_0) 
	    * (1.0 - pow( 1.0+ redshift, -0.5 ));

	  errno=0;
	  dnumer= cosm_get_comov_dist(H_0, 1.0, 0.0, -1.0, redshift );
	  if(errno != 0)
	    perror("ERROR during cosm_get_comov_dist");

	  dtest = danalyt - dnumer;

	  if(fabs(dtest) > testtoler)
	    {	
	      if(want_verbose)
		{
		  printf("Einstein de Sitter test discrepancy: ");
		};
	      printf("redshift = %.2f  danalyt = %.2f  dnumer = %.2f,",
		     redshift, danalyt, dnumer);
	      printf("dtest = %.2f\n", dtest);
		     
	    };

	  /* test inverse */
	  errno=0;
	  zinverse= cosm_get_comov_dist_inv(H_0, 1.0, 0.0, -1.0, dnumer); 
	  if(errno != 0)
	    perror("ERROR during cosm_get_comov_dist_inv");

	  zdiff= zinverse - redshift;
	  if(fabs(zdiff) > testtoler)
	    {	
	      if(want_verbose)
		{
		  printf("Einstein de Sitter inverse test discrepancy");
		  printf(" (dist):\n");
		};
	      printf("redshift = %.2f  zinverse = %.2f  zdiff = %.2f,",
		     redshift, zinverse, zdiff);
	    };
	  

	  /* times in EdS universe */
	  tanalyt =  2.0/(3.0*H_0 * COSM_H_0_INV_GYR) 
	    * pow( 1.0+ redshift, -1.5 );
	  errno=0;
	  tnumer= cosm_get_comov_time(H_0, 1.0, 0.0, -1.0, redshift); 
	  if(errno != 0)
	    perror("ERROR during cosm_get_comov_time");

	  ttest = tanalyt - tnumer;

	  if(fabs(ttest) > testtoler)
	    {	
	      if(want_verbose)
		{
		  printf("Einstein de Sitter test discrepancy (time):\n");
		};
	      printf("redshift = %.2f  tanalyt = %.2f  tnumer = %.2f,",
		     redshift, tanalyt, tnumer);
	      printf("ttest = %.2f\n", ttest);
		     
	    };

	  /* test inverse */
	  errno=0;
	  zinverse= cosm_get_comov_time_inv(H_0, 1.0, 0.0, -1.0, tnumer); 
	  if(errno != 0)
	    perror("ERROR during cosm_get_comov_time_inv");

	  zdiff= zinverse - redshift;
	  if(fabs(zdiff) > testtoler)
	    {	
	      if(want_verbose)
		{
		  printf("Einstein de Sitter inverse test discrepancy: ");
		};
	      printf("redshift = %.2f  zinverse = %.2f  zdiff = %.2f,",
		     redshift, zinverse, zdiff);
	    };
	  

	};   /* for (iztest = 0; iztest < maxztest; iztest++) */



      /* http://arXiv.org/abs/astro-ph/9904172 - Ue-Li Pen fitting formula  */

      if(want_verbose)
	{
	  printf("\nWill compare cosm_get_pm_dist with fitting formula\n");
	  printf("for flat universes");
	  printf(" with a non-zero cosmological constant.\n");
	  printf("No news = (probably) good news.\n");
	}

      dztest = (z2log-z1log)/ ((double)maxztest_Ue_Li_Pen);
      dommtest = (omm_2-omm_1)/ ((double)maxommtest);

      for (iommtest = 0; iommtest < maxommtest; iommtest++)
	{
	  omm_test= omm_1 + (iommtest+0.5)*dommtest ;
	  
	  for (iztest = 0; iztest < maxztest_Ue_Li_Pen; iztest++)
	    {
	      redshift = pow( 10.0, z1log + (iztest+0.5)*dztest );

	      /* errno=0;
		 errno not yet implemented in ue_li_pen since it is only for
		 testing purposes: it is slow and insufficiently accurate
	      */
	      danalyt = cosm_get_pm_Ue_Li_Pen(H_0, omm_test, redshift);
	      /* if(errno != 0)
		perror("ERROR during cosm_get_pm_Ue_Li_Pen"); 
	      */

	      errno=0;
	      dnumer= cosm_get_pm_dist(H_0, omm_test, 1.0-omm_test, -1.0, 
				       redshift );
	      if(errno != 0)
		perror("ERROR during cosm_get_pm_dist");

	      dtest = (dnumer - danalyt)/dnumer;
	      
	      if( ((omm_test < 0.2) && 
		   (fabs(dtest) > testtoler_Ue_Li_Pen_less02))
		  || 
		  ((omm_test >= 0.2) && 
		   (fabs(dtest) > testtoler_Ue_Li_Pen_more02)) )
		{	
		  if(want_verbose)
		    {
		      printf("Ue-Li Pen flat Lambda univ test discrepancy: \n");
		      printf("omm_test = %.2f\n",omm_test);
		    };
		  printf("redshift = %.2f  danalyt = %.2f  dnumer = %.2f, ",
			 redshift, danalyt, dnumer);
		  printf("dtest = %.2f\n", dtest);
		     
		};
	    };   /* for (iztest = 0; iztest < maxztest; iztest++) */
	}; /*    for (iommtest = 0; iommtest < maxommtest; iommtest++)  */
    };


  if(want_fortran)
    {
      if(want_verbose)
	printf("Will try fortran wrapper test...\n");
      
      TESTF( &input_parameter, &H_0, &omm, &omlam, &w_0,
	     &want_all, &want_verbose, &want_pm, &want_inverse, &want_time,
	     &want_horizon, &want_curvature);

      if(want_verbose)
	printf("Have finished fortran wrapper test...\n");
    };



  if(0 && want_debug)
    {
      dist= cosm_get_comov_dist( H_0, omm, omlam, w_0, redshift );
      red_inv= cosm_get_comov_dist_inv( H_0, omm, omlam, w_0, dist );  

      if(want_verbose)
	{
	  printf("\n");
	  printf("Second call to cosm_get_comov_dist: \n");
	  printf("distance to redshift = %.4f is  %.2f  Mpc \n", 
		 redshift, dist);
	  printf("and inverse redshift = %.4f\n", 
		 red_inv);
	  printf("\n");
	  printf("Third call to cosm_get_comov_dist: \n");
	};
      H_0 = 70.0;
      omm = 0.3;
      omlam = 0.7;
      dist= cosm_get_comov_dist( H_0, omm, omlam, w_0, redshift );  
      red_inv= cosm_get_comov_dist_inv( H_0, omm, omlam, w_0, dist );  
      time= cosm_get_comov_time( H_0, omm, omlam, w_0, 0.0 );  

      if(want_verbose)
	{
	  printf("distance to redshift = %.4f is  %.2f  Mpc \n", 
		 redshift, dist);
	  printf("and inverse redshift = %.4f\n", 
		 red_inv);
	  printf("time down to redshift = %.4f is  %.2f  Gyr \n", 
		 0.0, time);
	  printf("\n");
	  printf("Fourth call to cosm_get_comov_dist: \n");
	};

      omlam = 0.0;
      dist= cosm_get_comov_dist( H_0, omm, omlam, w_0, redshift ); 
      red_inv= cosm_get_comov_dist_inv( H_0, omm, omlam, w_0, dist );  
      time= cosm_get_comov_time( H_0, omm, omlam, w_0, 0.0 );  

      dist2= cosm_get_pm_dist( H_0, omm, omlam, w_0, redshift ); 
      red_inv2= cosm_get_pm_dist_inv( H_0, omm, omlam, w_0, dist2 ); 

      if(want_verbose)
	{
	  printf("distance to redshift = %.4f is  %.2f  Mpc \n", 
		 redshift, dist);
	  printf("proper motion distance to redshift = %.4f is  %.2f  Mpc \n",
		 redshift, dist2);
	  printf("and inverse redshift = %.4f\n", 
		 red_inv);
	  printf("and pm inverse redshift = %.4f\n", 
		 red_inv2);
	  printf("time down to redshift = %.4f is  %.2f  Gyr \n", 
		 0.0, time);
	  printf("\n");
	  printf("Fifth call to cosm_get_comov_dist: \n");
	};

      omlam = 0.8;
      dist= cosm_get_comov_dist( H_0, omm, omlam, w_0, redshift ); 
      red_inv= cosm_get_comov_dist_inv( H_0, omm, omlam, w_0, dist );  
      time= cosm_get_comov_time( H_0, omm, omlam, w_0, 0.0 );  

      dist2= cosm_get_pm_dist( H_0, omm, omlam, w_0, redshift ); 
      red_inv2= cosm_get_pm_dist_inv( H_0, omm, omlam, w_0, dist2 ); 

      if(want_verbose)
	{
	  printf("distance to redshift = %.4f is  %.2f  Mpc \n", 
		 redshift, dist);
	  printf("proper motion distance to redshift = %.4f is  %.2f  Mpc \n",
		 redshift, dist2);
	  printf("and inverse redshift = %.4f\n", 
		 red_inv);
	  printf("and pm inverse redshift = %.4f\n", 
		 red_inv2);
	  printf("time down to redshift = %.4f is  %.2f  Gyr \n", 
		 0.0, time);
	  printf("\n");
	};
    };   /* if(want_debug) */

  exit (0);
}

#ifdef HAVE_ARGP_H
/* Parse a single option.  */
static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  switch (key)
    {
    case ARGP_KEY_INIT:
      break;
    case 'v':			/* --verbose */
      want_verbose = 1;
      break;
    case 'a':
      want_all = 1;
      break;
    case 'c':
      want_check = 1;
      break;
    case 's':
      want_pm= 1;
      break;
    case 't':
      want_time= 1;
      break;
    case 'z':
      want_inverse= 1;
      break;
    case 'h':
      argument_string= xstrdup(arg);
      nlen_argument_string= strlen(argument_string);
      errno=0;
      H_0= strtod(argument_string,endptr);
      if(errno != 0)
	{
	  printf("ERROR: errno= %d  in reading H_0 value\n",errno);
	  exit (errno);
	};
      break;
    case 'm':
      argument_string= xstrdup(arg);
      nlen_argument_string= strlen(argument_string);
      errno=0;
      omm= strtod(argument_string,endptr);
      if(errno != 0)
	{
	  printf("ERROR: errno= %d  in reading Omega_matter value\n",errno);
	  exit (errno);
	};
      break;
    case 'l':
      argument_string= xstrdup(arg);
      nlen_argument_string= strlen(argument_string);
      errno=0;
      omlam= strtod(argument_string,endptr);
      if(errno != 0)
	{
	  printf("ERROR: errno= %d  in reading Omega_Lambda value\n",errno);
	  exit (errno);
	};
      break;
    case 'q':
      argument_string= xstrdup(arg);
      nlen_argument_string= strlen(argument_string);
      errno=0;
      w_0= strtod(argument_string,endptr);
      if(errno != 0)
	{
	  printf("ERROR: errno= %d  in reading w_0 value\n",errno);
	  exit (errno);
	};
      break;
    case 'f':
      want_fortran= 1;
      break;
    case 'i':
      want_horizon = 1;
      break;
    case 'k':
      want_curvature = 1;
      break;

    case NOWARN_KEY:		/* --no-warn */
      want_no_warn = 1;
      break;
      
    case ARGP_KEY_ARG:		/* [FILE]... */
      /* TODO: Do something with ARG, or remove this case and make
         main give argp_parse a non-NULL fifth argument.  */
      break;

    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}
#endif  /* HAVE_ARGP_H */


/* Show the version number and copyright information.  */
static void
#ifdef HAVE_ARGP_H
show_version (FILE *stream, struct argp_state *state)
#else
     show_version (FILE *stream)
#endif  /* HAVE_ARGP_H */
{
#ifdef HAVE_ARGP_H
  (void) state;
#endif  /* HAVE_ARGP_H */
  /* Print in small parts whose localizations can hopefully be copied
     from other programs.  */
  fputs(PACKAGE" "VERSION"\n", stream);
  fprintf(stream, _("Written by %s.\n\n"), "Boud Roukema");
  fprintf(stream, _("Copyright (C) %s %s\n"), "2004", "Boud Roukema");
  fputs(_("\
This program is free software; you may redistribute it under the terms of\n\
the GNU General Public License.  This program has absolutely no warranty.\n"),
	stream);
}
