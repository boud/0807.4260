C   cosmdist - standard FLRW cosmological distance functions
C
C   Copyright (C) 2004 Boud Roukema
C
C   This program is free software; you can redistribute it and/or modify
C   it under the terms of the GNU General Public License as published by
C   the Free Software Foundation; either version 2, or (at your option)
C   any later version.
C
C   This program is distributed in the hope that it will be useful,
C   but WITHOUT ANY WARRANTY; without even the implied warranty of
C   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C   GNU General Public License for more details.
C
C   You should have received a copy of the GNU General Public License
C   along with this program; if not, write to the Free Software Foundation,
C   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

      subroutine i2log( i, logic)
      integer    i
      logical*4  logic
      if(i.eq.1)then
         logic=.true.
      else
         logic=.false.
      endif
      return
      end

      


      subroutine testf( par_input, H_0, omm, omlam, w_0,
     _     iw_all,iw_verb,iw_pm,iw_inverse,iw_time,
     _     iw_hor,iw_curv)

      implicit real*8 (a-h,o-z)
      integer  ierrorflag

      logical*4 w_all,w_verb,w_pm,w_inverse,w_time,w_hor,w_curv

      if(iw_verb.gt.0)then
         print*,'testf: You may look at the source code  cosm_f77.f'
         print*,'to see an example of how to call the cosmdist '
         print*,'routines from a fortran program.  Of course, you'
         print*,'also need to link your fortran routine with the'
         print*,' compiled library libcosmdist.a  '
         print*,'You should declare these functions as real*8:'
         print*,'      real*8  dofz,zofd,dpmofz,zofdpm,dpmofz,zofpdm'
         print*,'      real*8  tofz,zoft,rcurv,rhoriz'
         print*,'and make any needed type conversions (to or from'
         print*,'real*4) explicitly.'
         print*,' '

         print*,'par_input, H_0, omm, omlam, w_0,',
     _        'iw_pm,iw_inverse,iw_time=',
     _        par_input, H_0, omm, omlam, w_0,
     _        iw_pm,iw_inverse,iw_time,iw_hor,iw_curv
      endif

      call i2log(iw_all,w_all)
      call i2log(iw_verb,w_verb)
      call i2log(iw_pm,w_pm)
      call i2log(iw_inverse,w_inverse)
      call i2log(iw_time,w_time)
      call i2log(iw_hor,w_hor)
      call i2log(iw_curv,w_curv)

c      x= dofz(H_0,omm,omlam,w_0, par_input)
c      print*,'testf: x=',x

      if(w_inverse)then
         if(.not.(w_time))then
            distance= par_input
         else
            time= par_input
         endif
      else
         redshift= par_input
      endif


c     /*  comoving distance and inverse */

      if(w_all .or. ((.not.(w_pm)) .and. (.not.(w_inverse))
     _     .and. (.not.(w_time))))then
         dist= dofz( H_0, omm, omlam, w_0, redshift)
         if(w_verb)then
            print*,'comoving distance is', dist, 'Mpc'
         else
            write(6,'(f10.2,$)')dist
         endif
      endif
      
      if((w_all) .or. ((.not.(w_pm)) .and. (w_inverse)
     _     .and. (.not.(w_time))))then
         if((w_all)) distance=dist
         red_inv= 
     _        zofd( H_0, omm, omlam, w_0, distance )
         if(w_verb)then
            print*,'redshift is',red_inv
         else
            write(6,'(f9.4,$)')red_inv
         endif
      endif
      
      
c     /* proper motion distance and inverse */

      if((w_all) .or. ((w_pm) .and. (.not.(w_inverse)) 
     _ .and. (.not.(w_time))))then
         dist= dpmofz( H_0, omm, omlam, w_0, redshift )
         if(w_verb)then
            print*,'proper motion distance is',dist, ' Mpc'
         else
            write(6,'(f10.2,$)')dist
         endif
      endif
      
      
      if((w_all) .or. ((w_pm) .and. (w_inverse)
     _     .and. (.not.(w_time))))then
         if((w_all)) distance=dist
         red_inv= 
     _        zofdpm( h_0, omm, omlam, w_0, distance )
         if(w_verb)then
            print*,'redshift is', red_inv
         else
            write(6,'(f9.4,$)')red_inv
         endif
      endif


c     /* cosmological time */

      if((w_all) .or. ((.not.(w_inverse)) .and. (w_time)))then
         time_output= 
     _        tofz( h_0, omm, omlam, w_0, redshift )
         if(w_verb)then
            print*,'cosmological time',time_output
         else
            write(6,'(f10.2,$)')time_output
         endif
      endif
      
      if((w_all) .or. ((w_inverse) .and. (w_time)))then
         if((w_all)) time=time_output

         red_inv= 
     _        zoft( h_0, omm, omlam, w_0, time )
         if(w_verb)then
            print*,'redshift is',red_inv
         else
            write(6,'(f9.4,$)')red_inv
         endif
      endif



c      /* horizon and/or curvature radius */

      if(w_all .or. w_hor)then
         xx= rhoriz( h_0, omm, omlam, w_0)
         if(w_verb)then
            print*,'horizon in Mpc = ',xx
         else
            write(6,'(f10.2,$)')xx
         endif
      endif
      if(w_all .or. w_curv)then
         xx= rcurv( h_0, omm, omlam, w_0)
         if(w_verb)then
            print*,'curvature radius in Mpc = ',xx
         else
            write(6,'(f10.2,$)')xx
         endif
      endif


c     /*  following all outputs, add an end-of-line */
      if((.not.(w_verb))) print*,' '


      return
      end



      
