C     Copyright (C) 2007 B. Roukema et al. <shape-univ at astro.uni.torun.pl>
C     
C     This program is free software; you can redistribute it and/or modify
C     it under the terms of the GNU General Public License as published by
C     the Free Software Foundation; either version 2 of the License, or
C     (at your option) any later version.
C     
C     This program is distributed in the hope that it will be useful,
C     but WITHOUT ANY WARRANTY; without even the implied warranty of
C     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C     GNU General Public License for more details.
C     
C     You should have received a copy of the GNU General Public License
C     along with this program; if not, write to the Free Software
C     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
C  
C     See also http://www.gnu.org/licenses/gpl.html

      subroutine analys(infiles,ninfiles,
     _     a_pars, na_pars)

C     INPUTS:
      character*(*)  infiles     ! file listing input files
      integer        ninfiles    ! number of characters in  indir

c     --- see a_pars below for required format ---
      character*(*)  a_pars     ! string containing parameters
      integer        na_pars    ! number of characters in a_pars

c      integer        iverb,nowarn ! flags: verbose, suppress warnings


C     INTERNAL:
      character*256  infil2 ! safe string version of indir
      parameter(maxf=30) ! max number of files
      character*256  filename(maxf)

      character*256  line

      parameter(maxn=40000,npars=8)
      real*4         params(maxn,npars,maxf)
      integer*4      nsteps(maxf)


c     --- dodec faces ---
      parameter(nfaces=12,nface=nfaces)
      real*4  xdod(nface),ydod(nface),zdod(nface)
c     -- orthogonal unit vectors to generate circle
      real*4  xdodp(nface,2),ydodp(nface,2),zdodp(nface,2) ! perpendiculars

c     means for individual iterations/files
      parameter(mxiter=100)
      real*4         glongi(nfaces,0:mxiter,maxf)
      real*4         glati(nfaces,0:mxiter,maxf)
      real*4         x_lbi(nfaces,0:mxiter,maxf)
      real*4         y_lbi(nfaces,0:mxiter,maxf)
      real*4         z_lbi(nfaces,0:mxiter,maxf)
      integer        ihigh(nfaces,0:mxiter,maxf)
      integer        ihtot(nfaces)  ! total points from all files
      integer        ivalid(nfaces) ! number of valid files for a given face

      real*4         x_lbm(nfaces)
      real*4         y_lbm(nfaces)
      real*4         z_lbm(nfaces)
      real*4         x_lbs(nfaces)
      real*4         y_lbs(nfaces)
      real*4         z_lbs(nfaces)


      real*4         glongm(nfaces)
      real*4         glatm(nfaces)
      real*4         s_deg(nfaces)
      real*4         se_deg(nfaces)  ! standard error in the mean

      parameter(glNULL= -9e9)

      real*4         alpha(maxf) !, phi(maxf)
      real*4         x_phi(maxf)
      real*4         y_phi(maxf)
      integer        ihi_ap(maxf) ! ihigh  for alpha & phi


      logical*4      test,test2,test3
      logical*4      NaNfound   ! have we found a NaN ?
      logical*4      maxexists(nfaces,mxiter,maxf) ! does a maximum
                                            ! exist for this
                                            ! iteration/file?
      logical*4      calibrate ! is this a calibration test ?


      parameter(pi=3.14159265359,pi_180=pi/180,pi_5=pi/5)

      parameter(tol=1e-4)
      parameter(tolvar=1e-8)

      real*4         glong0(nfaces),glat0(nfaces)
      real*4         offset

c     --- estimate by inspection of mercator plot ---
      data  glong0 / 
     _     0.0, 5.0, 50.0, 65.0, 
     _     120.0, 135.0, 180.0, 185.0, 
     _     235.0, 245.0, 300.0, 315.0 /
      data  glat0  /
     _     -60.0, 5.0, 45.0, -20.0,
     _     25.0, -50.0, 60.0, -5.0, 
     _     -45.0, 20.0, -15.0, 50.0 /

      data offset /5.0/

c     --- estimate from (180.0, 67.0, theta = ? )
c      data  glong0 / 
c     _     180.0, 346.0,  75.0, 139.0, 203.0, 266.0,
c     _       0.0, 166.0, 255.0, 319.0,  23.0, 86.0 /
c      data  glat0  /
c     _      67.0,  49.0,  35.0,  10.0,  6.0,  27.0,
c     _     -67.0, -49.0, -35.0, -10.0, -6.0, -27.0 /


c     --- example of offsetting the initial positions by 
c     several degrees ---
c      data  glong0 / 
c     _     190.0, 336.0,  65.0, 129.0, 213.0, 256.0,
c     _       0.0, 166.0, 255.0, 310.0,  20.0, 76.0 /
c      data  glat0  /
c     _      57.0,  39.0,  25.0,  0.0,  0.0,  21.0,
c     _     -58.0, -59.0, -45.0, -2.0, -0.0, -37.0 /


      test=  .false. ! .true. !
      test2=   .false. !.true.              !
      test3=  .true.              ! .false. !
      calibrate= .false. !.true.         ! 

c     ---- Only set the burnin to zero if the burnin is removed EXTERNALLY
c     to this routine ----
      nburnin= 2000  
      pmin= 0.5

c     -- l,b,theta --
c      ball= 45.0                ! degrees
c     _     * pi_180

      ball1= 30.0 *pi_180
      ball2= 20.0 *pi_180
      nshrink= 10

c     -- alpha --
      b_alpha= 9e9 ! 30.0 ! degrees   ! for alpha - quite wide needed
     _     * pi_180      
      alpmin = 10.0 *pi_180

c     -- twist --
      !balltw = ball  ! for twist angle
      twmin = -9e9 ! 0.0       ! twist minimum (use -9e9 to invalidate)
      twmax = 9e9 !5.0   ! twist maximum (use 9e9 to invalidate)

      print*,'pmin,twmin,twmax=',pmin,twmin,twmax

      if(na_pars.gt.0)then
         xx1=-9e9
         xx2=-9e9
         xx3=-9e9
         print*,'Will try reading command line parameters: '
         print*,'a_pars(1:na_pars)= ',a_pars(1:na_pars)
c         read(a_pars(1:na_pars),'(3g)',err=432,end=432)xx1,xx2,xx3
c         read(a_pars(1:na_pars),'(3g)',err=432)xx1,xx2,xx3
         read(a_pars(1:na_pars),*,err=432,end=432)xx1,xx2,xx3
 432     continue
c         print*,'xx?=',xx1,xx2,xx3
         if(-0.1.lt.xx1 .and. xx1.lt.1.1)pmin= xx1
         if(-10.0.lt.xx2 .and. xx2.lt.10.0)twmin= xx2
         if(-10.0.lt.xx3 .and. xx3.lt.10.0)twmax= xx3
         print*,'pmin,twmin,twmax=',pmin,twmin,twmax
      endif

      niter=20 ! 3 !! 3 = !! test only !!

c     ---- calibration e.g. with 0.2.5.15 to check that the MCMC chain
c     can find the optimal point in an artificially created map with 
c     known preferred twist and weak (l,b,theta) dependence ---- 
      if(calibrate)then
         nburnin= 1000 !! 0 = test only !!
         ball= 9e9
         ball1=8e9
         ball2=9e9
c         b_alpha= 9e9
c         balltw= 45.0 
c     _        *pi_180
c         twmin= -9e9
c         twmax= 9e9

         niter=1
      endif


c     ----------------------------------------------------------------
c     -   read in each line starting "mcmc: pars", get list of  8 parameters
c     -  (l,b)_orig, (l,b)_highb, theta, alpha, phase, pmcnew
c     ----------------------------------------------------------------

      infil2=infiles(1:min(ninfiles,256))

      ifil=0
      open(1,file=infil2,access='sequential',status='old')

      do while(1+1.eq.2)
         read(1,'(a)',end=777)line
         ich=1
         do while(ich.le.256.and.line(ich:ich).ne.' ')
            ich=ich+1
         enddo
         ich=ich-1
         ifil=ifil+1
         print*,'File ',ifil,' is:'
         print*,line(1:ich)
         filename(ifil)= line(1:ich)

         open(2,file=filename(ifil),access='sequential',status='old')

         istep=0

         do while(istep.lt.maxn) ! may stop earlier
c            line='____________'   ! dummy line to be sure to have 12 chars
            do i=1,12
               line(i:i)='_'
            enddo
            do i=13,120  ! 0.2.20
               line(i:i)=' '
            enddo
            read(2,'(a)',end=888)line

c            print*,filename(ifil),'hmm',line

            if(line(1:12).eq.'mcmc: pars= ')then


cmcmc: pars=   195.296   56.976 195.296  56.976   36.8  29.82   0.70  0.401
c               ich=71  !0.2.15
               ich=90   !0.2.16
               do while(ich.le.256.and.line(ich:ich).ne.' ')
                  ich=ich+1
               enddo
               ich=ich-1


               NaNfound = .false.
               do jch=13,ich-2
                  if(line(jch:jch+2).eq.'NaN' .or.
     _                 line(jch:jch+2).eq.'nan' .or. 
     _                 line(jch:jch+2).eq.'NAN' .or.
     _                 line(jch:jch+2).eq.'***')  ! not really NaN 
     _                 NaNfound = .true.
               enddo



               if(test)then
                  print*,filename(ifil),':: '
                  print*,line(13:ich)
               endif

               if(.not.(NaNfound))then
                  istep=istep+1
                  if(test)print*,'13,ich=',13,ich
                  if(test)print*,'line(13:ich)=',
     _                 '__',line(13:ich),'__'
                  read(line(13:ich),*)
     _                 (params(istep,ipar,ifil),ipar=1,npars)
c                  read(line(23:ich),*)
c     _                 (params(istep,ipar,ifil),ipar=2,npars)
               endif
               if(test)write(6,'(8g10.4)')
     _              (params(istep,ipar,ifil),ipar=1,npars)

c     -- read in params (istep, ., ifil)
c            else                ! test only
c               print*,filename(ifil),'?? ',line(1:ich)
            endif

         enddo

 888     continue
         close(2)
         nsteps(ifil) = istep

      enddo !       do while(1+1.eq.2)

 777  continue
      close(1)
      nfiles= ifil
      do i=1,nfiles
         print*,'file: ',i,'  nsteps(.) = ',nsteps(i)
      enddo

c     -----------------------------------------------------------------
c     - convert (l,b) to xyz_lb
c     - convert phase to xy_phase
c     
c     - add xyz_lb, alpha, xy_phase to averaging cumulators
c     -----------------------------------------------------------------

      do i=1,nfaces
         glong0(i)=glong0(i) +
     _        rnd(0) * offset*2.0 - offset ! since 0.2.21
         glat0(i)=glat0(i) +
     _        rnd(0) * offset*2.0 - offset ! since 0.2.21
      enddo

      do ifil=1,nfiles
c         alphai(ifil)=0.0

         do i=1,nfaces
            glongi(i,0,ifil)= glong0(i)
            glati(i,0,ifil)= glat0(i)
            x_lbi(i,0,ifil)= cos(glong0(i)*pi_180) * 
     _            cos(glat0(i)*pi_180)  
            y_lbi(i,0,ifil)= sin(glong0(i)*pi_180) * 
     _            cos(glat0(i)*pi_180)  
            z_lbi(i,0,ifil)= sin(glat0(i)*pi_180)  
         enddo

         alpha(ifil)=0.0
         x_phi(ifil)=0.0
         y_phi(ifil)=0.0
         ihi_ap(ifil)=0
         

         do iter=1,niter

            if(iter.le.nshrink)then
               ball=ball1 + real(iter-1)*(ball2-ball1)
     _              /real(nshrink)
            else
               ball=ball2
            endif
         
            do i=1,12
               maxexists(i,iter,ifil)= .false.
               x_lbi(i,iter,ifil)=0.0
               y_lbi(i,iter,ifil)=0.0
               z_lbi(i,iter,ifil)=0.0
               ihigh(i,iter,ifil)=0
            enddo
c            x_phii(iter,ifil)=0.0
c            y_phii(iter,ifil)=0.0


            do istep=nburnin+1,nsteps(ifil)
               if( 
     _              (params(istep,8,ifil).gt.pmin)
     _              )
     _              then
               
                  gl = params(istep,1,ifil)
                  gb = params(istep,2,ifil)
                  th = params(istep,5,ifil)
                  
                  call gendodec(gl,gb,th, xdod,ydod,zdod,
     _                 xdodp,ydodp,zdodp)
                  
                  
                  do i=1,nfaces
                     do idod=1,nfaces
                        if(iter.eq.1 .or. 
     _                       maxexists(i,iter-1,ifil))then
                           a1 = angle(
     _                          xdod(idod),
     _                          ydod(idod),
     _                          zdod(idod),
     _                       x_lbi(i,iter-1,ifil),
     _                       y_lbi(i,iter-1,ifil),
     _                       z_lbi(i,iter-1,ifil) ) ! radians
                        else
                           a1 = 9e9
                        endif

                        if( a1.lt.ball
     _                       )then
                           ihigh(i,iter,ifil)= ihigh(i,iter,ifil) +1

                           x_lbi(i,iter,ifil)= x_lbi(i,iter,ifil) 
     _                          + xdod(idod)
                           y_lbi(i,iter,ifil)= y_lbi(i,iter,ifil)
     _                          + ydod(idod)
                           z_lbi(i,iter,ifil)= z_lbi(i,iter,ifil)
     _                          + zdod(idod)
                           

                           if(iter.eq.niter)then
c     ---                     --- phase  phi  ---
                              ph=  params(istep,7,ifil) 

                              if(ph.gt.twmin.and.ph.lt.twmax)then

                                 ph= ph * 36.0
                                 x_phi(ifil)= x_phi(ifil) + 
     _                                cos(ph *pi_180)
                                 y_phi(ifil)= y_phi(ifil) + 
     _                                sin(ph *pi_180)
                                 
                                 ihi_ap(ifil)= ihi_ap(ifil)+1
c     ---                     --- circle size alpha ---
                                 alpha(ifil)= alpha(ifil) + 
     _                                params(istep,6,ifil)
                              endif

                              if(test3)then
                                 print*,'alpha,ph: ',
     _                                params(istep,6,ifil),ph
                              endif
                           endif

                           if(test2)then
                              print*,
     _                             'test2: i,iter,ifil=',i,iter,ifil,
     _                             ' a1/pi_180=',a1/pi_180,
     _                             ' i,ihigh=',i, ihigh(i,iter,ifil)
                           endif
                     

c                     alphai(ifil)=alphai(ifil) + dalpha
c                     x_phi = x_phi + dx_phi
c                     y_phi = y_phi + dy_phi
                        endif  ! if ( a1.lt.ball ... )
                     enddo !                      do idod=1,nfaces
                  enddo  !  do i=1,nfaces
               endif            !  (params(istep,8,ifil).gt.pmin)
            enddo               !  do istep=nburnin+1,nsteps(ifil)

            do i=1,nfaces
               if(ihigh(i,iter,ifil).ge.1)then
                  x_lbi(i,iter,ifil)= x_lbi(i,iter,ifil)/
     _                 real(ihigh(i,iter,ifil))
                  y_lbi(i,iter,ifil)= y_lbi(i,iter,ifil)/
     _                 real(ihigh(i,iter,ifil))
                  z_lbi(i,iter,ifil)= z_lbi(i,iter,ifil)/
     _                 real(ihigh(i,iter,ifil))

                  xx2= x_lbi(i,iter,ifil)* x_lbi(i,iter,ifil)
                  yy2= y_lbi(i,iter,ifil)* y_lbi(i,iter,ifil)
                  zz2= z_lbi(i,iter,ifil)* z_lbi(i,iter,ifil)
                  if(xx2+yy2+zz2.gt.
     _                 tol*tol)then
                     call xyz_rda(
     _                    x_lbi(i,iter,ifil),
     _                    y_lbi(i,iter,ifil),
     _                    z_lbi(i,iter,ifil), rr,dd,aa)
                     glongi(i,iter,ifil)= aa*15.0
                     glati(i,iter,ifil)= dd
                  else
                     print*,'WARNING: mean (l,b) vec is close to zero:'
                     print*,'x_lb,y_lb,z_lb=',  
     _                    x_lbi(i,iter,ifil),
     _                    y_lbi(i,iter,ifil),
     _                    z_lbi(i,iter,ifil)
                     glongi(i,iter,ifil) = glNULL
                     glati(i,iter,ifil) = glNULL
                  endif         !     if (xx2+yy2+zz2.gt.tol*tol)then

                  maxexists(i,iter,ifil)= .true.
               else
                  glongi(i,iter,ifil) = glNULL
                  glati(i,iter,ifil) = glNULL
               endif            !   if(ihigh(i,iter,ifil).ge.1)then
            enddo               ! do i=1,nfaces

            print*,'ifil,ihigh,glong,glat' !,alpha,phi='
            print*,'means_a: ',iter,ifil,ihigh(i,iter,ifil)
            print*,'means_b: ',
     _           (glongi(i,iter,ifil),glati(i,iter,ifil),i=1,6)
c     _              alphai(ifil),phii(ifil)
            

            if(iter.lt.niter)then
               write(6,
     _              '(a,i2,a,f5.1,a,i3,a,$)'
     _              )
     _              'means: ',iter,' &',pmin,' &',ifil,' &'
            else
               write(6,
     _              '(a,i2,a,f5.1,a,i3,a,$)'
     _              )
     _              'MEANS: ',iter,' &',pmin,' &',ifil,' &'
            endif

            do i=1,6
               write(6,'(f7.1,a,f7.1,a,$)')
     _              glongi(i,iter,ifil),' &',
     _              glati(i,iter,ifil),' &'
            enddo
            print*,' \\\\'

            if(ihi_ap(ifil).ge.1)then
               alpha(ifil)= alpha(ifil)/real(ihi_ap(ifil))
               x_phi(ifil)= x_phi(ifil)/real(ihi_ap(ifil))
               y_phi(ifil)= y_phi(ifil)/real(ihi_ap(ifil))

               ph = atan2(y_phi(ifil),x_phi(ifil)) /pi_180
               print*,'ifil, alpha(ifil),ph=',
     _              ifil, alpha(ifil),ph

            endif

               
         enddo                  !      do iter=1,niter

      enddo                     ! do ifil=1,nfiles


      if(nfiles.ge.1)then
         print*,'Will estimate average between files: '



         do i=1,nfaces
            x_lbm(i)=0.0
            y_lbm(i)=0.0
            z_lbm(i)=0.0
            x_lbs(i)=0.0
            y_lbs(i)=0.0
            z_lbs(i)=0.0
            ihtot(i)=0          ! sum of ihigh's 
            ivalid(i)=0         ! valid files in which points are found
         enddo
         alpham=0.0
         alphas=0.0

         x_phim=0.0
         y_phim=0.0
         x_phis=0.0
         y_phis=0.0

         ival_ap=0
         do ifil=1,nfiles      
            do i=1,nfaces
               if(ihigh(i,niter,ifil).ge.1)then
                  ihtot(i)= ihtot(i) + ihigh(i,niter,ifil)
                  ivalid(i)= ivalid(i) + 1
                  x_lbm(i)= x_lbm(i) + x_lbi(i,niter,ifil) 
                  y_lbm(i)= y_lbm(i) + y_lbi(i,niter,ifil) 
                  z_lbm(i)= z_lbm(i) + z_lbi(i,niter,ifil) 
               endif
               if(ihigh(i,niter,ifil).ge.2)then
                  x_lbs(i)= x_lbs(i) + x_lbi(i,niter,ifil) *
     _                 x_lbi(i,niter,ifil) 
                  y_lbs(i)= y_lbs(i) + y_lbi(i,niter,ifil) *
     _                 y_lbi(i,niter,ifil) 
                  z_lbs(i)= z_lbs(i) + z_lbi(i,niter,ifil) *
     _                 z_lbi(i,niter,ifil) 
               endif
            enddo
            if(ihi_ap(ifil).ge.1)then
               ival_ap= ival_ap +1
               alpham= alpham+ alpha(ifil)
               x_phim= x_phim+ x_phi(ifil)
               y_phim= y_phim+ y_phi(ifil)
            endif
            if(ihi_ap(ifil).ge.2)then
               alphas= alphas+ alpha(ifil) *alpha(ifil)
               x_phis= x_phis+ x_phi(ifil) *x_phi(ifil)
               y_phis= y_phis+ y_phi(ifil) *y_phi(ifil)
            endif
         enddo ! do ifil=1,nfiles      



         do i=1,nfaces
            s_deg(i) = glNULL
            if(ivalid(i).ge.1)then
               x_lbm(i)=x_lbm(i)/real(ivalid(i))
               y_lbm(i)=y_lbm(i)/real(ivalid(i))
               z_lbm(i)=z_lbm(i)/real(ivalid(i))
               
               if(x_lbm(i)*x_lbm(i) + y_lbm(i)*y_lbm(i) + 
     _              z_lbm(i)*z_lbm(i).gt.
     _              tol*tol)then
                  call xyz_rda(x_lbm(i),y_lbm(i),z_lbm(i), rr,dd,aa)
                  glongm(i)= aa*15.0
                  glatm(i)= dd
               else
                  glongm(i)= -9.9e9
                  glatm(i)= -9.9e9
               endif
            endif

            if(ivalid(i).ge.2)then
               x_lbs(i) = x_lbs(i)/real(ivalid(i))  ! mean square
               y_lbs(i) = y_lbs(i)/real(ivalid(i))
               z_lbs(i) = z_lbs(i)/real(ivalid(i))
               
c     ---  < ( x - <x> )^2 > = <x^2> - <x>^2   ---
               ss= x_lbs(i) + y_lbs(i) + z_lbs(i) 
     _              - x_lbm(i)* x_lbm(i)
     _              - y_lbm(i)* y_lbm(i)
     _              - z_lbm(i)* z_lbm(i)
               if(ss.gt.tol*tol)then
                  s_rad=sqrt(ss) ! radians
                  s_deg(i)= s_rad /pi_180
                  se_deg(i)= s_deg(i) / 
     _                 sqrt(real(ivalid(i)-1))
cccc     _                 / sqrt(2.0)  ! 2dim to 1dim
               endif
            endif  ! if(ivalid(i).ge.2)then
         enddo  !          do i=1,nfaces


         if(ival_ap.ge.1)then
            alpham= alpham/real(ival_ap)
            x_phim= x_phim/real(ival_ap)
            y_phim= y_phim/real(ival_ap)

            if( x_phim*x_phim + y_phim*y_phim .gt.
     _              tol*tol)then
               phim= atan2(y_phim,x_phim) /pi_180
            else
               phim= -9e9
            endif

            if(ival_ap.ge.2)then
               alphas= alphas/real(ival_ap) - alpham*alpham
               if(alphas.gt.tol*tol)then
                  alphas = sqrt(alphas)
                  alphse= alphas/sqrt(real(ival_ap))
               else
                  alphas = -9e9
               endif

               x_phis= x_phis/real(ival_ap) 
               y_phis= y_phis/real(ival_ap) 

               ss= x_phis + y_phis 
     _              - x_phim*x_phim - y_phim*y_phim
               if(ss.gt.tol*tol)then
                  phi_s = sqrt(ss)  /pi_180
                  phi_se= phi_s/sqrt(real(ival_ap))
               else
                  phi_se = -9e9
               endif

            endif  ! if(ival_ap.ge.2)then

            n=0
            do ifil=1,nfiles
               n=n+ ihi_ap(ifil)
            enddo

            print*,'alpha, phi= ',n,alpham,alphse,phim,phi_se


         i=1  ! tmp only
            if(1+1.eq.3.and.ivalid(i).ge.2)then  ! standard errors in the mean
c               alphas=sqrt(max(tolvar, 
c     _              alphas/real(ivalid)-alpha*alpha))/
c     _              sqrt(real(ivalid)-1)
c               phis=sqrt(max(tolvar, 
c     _              phis/real(ivalid)-phi*phi))/
c     _              sqrt(real(ivalid)-1)
            else
               alphas= -9.9e9
               phis= -9.9e9
            endif
         endif                  ! if(ival_ap.ge.1)then

         do i=1,12
            write(6,
     _           '(a,f5.1,a,i3,a,i3,a,i6,a,$)'
     _           )
     _           'MEANS:: ',pmin,' &',i,' &',ivalid(i),' &',
     _           ihtot(i),' &'
            write(6,'(f7.1,a,f7.1,a,f7.1,$)')
     _           glongm(i),' &',glatm(i),' &',se_deg(i)

            print*,' \\\\'
         enddo

c     _        alpha,' &',phi,
               

c     TODO: calculate these correctly
c         print*,'std-err-m:: ',ihtot,glongs,glats,
c     _        alphas,phis
         print*,' '

      endif                     ! if(nfiles.gt.1)then


      return
      end

