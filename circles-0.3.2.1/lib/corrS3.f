C     Copyright (C) 2005 B. Roukema et al. <shape-univ at astro.uni.torun.pl>
C     
C     This program is free software; you can redistribute it and/or modify
C     it under the terms of the GNU General Public License as published by
C     the Free Software Foundation; either version 2 of the License, or
C     (at your option) any later version.
C     
C     This program is distributed in the hope that it will be useful,
C     but WITHOUT ANY WARRANTY; without even the implied warranty of
C     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C     GNU General Public License for more details.
C     
C     You should have received a copy of the GNU General Public License
C     along with this program; if not, write to the Free Software
C     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
C  
C     See also http://www.gnu.org/licenses/gpl.html


C     == calculate 3D 2-point spatial correlation function assuming PDS \in S^3 ===
      subroutine corrS3(indir,nindir, ang_in,
     _     iverb,nowarn,
     _     galcut,bcut,GCmin,
     _     ncc,cc1,cc2,H_0,drSLS,nsig,
     _     iimax0,jjmax0,maxtheta0, imax,jmax,
     _     nthc,glatc,glongc,wmodc,rSLSc,dthMpc,
     _     tt1c,tt2c,s1c,s2c,
     _     glpix1,gbpix1,glpix2,gbpix2,
     _     temp,errtemp,gallong,gallat,
     _     twist,               ! input
     _     oneaxis,
     _     pmcnew)  !output

c
c     Input data (from file):


C     INPUTS:
      character*(*)  indir     ! directory with input files
      integer        nindir    ! number of characters in  indir
      real*4         ang_in     ! angular radius of circles (degrees)
      integer        iverb,nowarn ! flags: verbose, suppress warnings
      logical*4      galcut    ! apply Galactic cut?
      real*4         bcut,GCmin ! galactic cut latitude, ang from GCentre


      include 'ncopies.dec'
      integer*4    nthc(-iimax:iimax,0:jjmax)
      real*4    glatc(2,-iimax:iimax,0:jjmax), ! 2 centres
     _     glongc(2,-iimax:iimax,0:jjmax), ! 2 centres
     _     wmodc(-iimax:iimax,0:jjmax), ! dtheta * rSLS
     _     rSLSc,
     _     dthMpc(-iimax:iimax,0:jjmax) ! dtheta * rSLS

c     ! warning only evaluated if  (iv.gt.0.or.jv.gt.0)

      real*4    tt1c(maxtheta, -iimax:iimax,0:jjmax)
      real*4    tt2c(maxtheta, -iimax:iimax,0:jjmax)
      real*4    s1c(maxtheta, -iimax:iimax,0:jjmax)
      real*4    s2c(maxtheta, -iimax:iimax,0:jjmax)

c     ---- 24.02.2004 ----
c     -- store galactic long, lat at each pixel for double-checking --
      real*4    glpix1(maxtheta, -iimax:iimax,0:jjmax)
      real*4    gbpix1(maxtheta, -iimax:iimax,0:jjmax)
      real*4    glpix2(maxtheta, -iimax:iimax,0:jjmax)
      real*4    gbpix2(maxtheta, -iimax:iimax,0:jjmax)


      real*4   temp(*),errtemp(*) !   Temperature and uncert.
      real*4   gallong(*),gallat(*) ! galactic
      real*4   twist  ! integer or floating multiple of +36.0 degrees = pi/5.0 rad

      logical*4  oneaxis        ! do only one axis instead of 6 ?  since 0.2.19
      integer  iv2_plot ! default = 0 ; otherwise just plot this axis

C     OUTPUTS:
      real*4   pmcnew   ! likelihood of the new point in parameter space


C     EXTERNAL:
      real*8          angle4D8
      external        angle4D8

      external        wmaplin

      real*8          det3x3 ! internal to this file, external to this routine
      external        det3x3

C     COMMON:
      logical*4 measur,noise    
      common /noisecomm/ measur,noise

C     INTERNAL:
      character*256  indir2 ! safe string version of indir

      real*8     alpha  ! local "copy" of ang_in, recalculated
      real*8     alph_d  ! local "copy" of ang_in, recalculated (degrees)
      logical*4  flucOK  ! are there fluctuations (not excluded by Galaxy) ?

      integer*4  imax,jmax

      real*8    pi,pi_180
      parameter(pi=3.141592659,pi_180=pi/180.0)


      logical*4  dododec 
      integer*4  iv1,iv2

      character ch
      character*3 ch3
      character*4 ch4
      character*5 ch5

C     S^3 embedded in R^4: matching vectors and their generator
      real*8   flucA(4,4), flucB(4,4)  ! "fluctuations" A and B 
      real*8   flucAc(4,4), flucBc(4,4)  ! copies of "originals"
      real*8   Xcopy(4,4) ! temporary copy of a matrix (can be overwritten)
      real*8   flucAi(4,4)  ! flucA inverse
      real*8   svec(4),vecMs(4) ! 4th, linearly indep pair of matching vectors
      real*8   tvec(4),vecMt(4) ! 4th, linearly indep pair of matching vectors
      real*8   uvec(4),vecMu(4) ! 4th, linearly indep pair of matching vectors
      real*8   vvec(4),vecMv(4) ! 4th, linearly indep pair of matching vectors
c      real*8   stu(4)  ! cross product of svec, tvec, uvec 
      real*8   s_Ms,t_Ms ! dot products:  s.Ms, t.Ms
      real*8   u_s,u_t ! temporary dot products:  u.s, u.t
      real*8   v_s,v_t,v_u ! temporary dot products: ...
      real*8   tmp_norm,xx8, yy8 ! temporary, local working variables
      real*8   tol_norm,tol_sqrt
      parameter(tol_norm = 0.2, tol_sqrt=1e-8)

c     - TODO:  corrS3.f : check if integer*4 or integer  ipiv(4) causes an
c     - error; IMHO integer*8 should be conservatively OK since we don't actually
c     - want to use ipiv, we just want to give dgesv the memory positions it wants.
      integer*8  ipiv(4)

      real*8   generator(4,4,-iimax:iimax,0:jjmax)  ! A^-1 B
      real*8   geninv(4,4,-iimax:iimax,0:jjmax)  ! (A^-1 B)^-1
      real*8   aa8, cosaa8
      integer*4  kcurv
      data kcurv /1/
c      logical*4  dodub
c      data dodub /.true./

      parameter(tolcos= 1e-4)
      real*8   angSLS,cosrSLS,sinrSLS
      real*8   ang_norm  ! converts S^3 distance to some sort of S^2 units
      real*8   ss8, cc8 ! temporary copies of sin(.) or cos(.)
      real*8   gl1_8,gb1_8,gl2_8,gb2_8
      real*8   tola8a,tola8b,diff8
      parameter(tola8a= 1e-2,tola8b=1e-4)

c     --- random sky positions
      parameter(mxsky= 100000)
      parameter(tolsqrt= 1e-4)
      real*8   gl_8(mxsky),gb_8(mxsky)
      real*4   gl_4(mxsky),gb_4(mxsky)
      real*8   pix(4,mxsky) ! 4D vectors of random sky positions
      real*4   t_4(mxsky) ! wmaplin interpolated value at random positions
      real*8   pix_B2(4)         ! temporary work vector - output from dgemm
      real*8   rr8,delta8,ascen8 ! temp conversion back to 3D sky directions

c     --- is sky position credible as being in an annulus? (since 0.3.0) ---
      logical  maybe,may_ann(iimax,2,mxsky) ! maybe annulus
      logical*4 do_annulus
      real*8   alpmin,alpmax  ! min,max alphas to potentially be in an annulus
      real*8   cen_x(iimax),cen_y(iimax),cen_z(iimax) ! from glongc etc
      parameter(tolalp = 1e-4) ! numerical minimum alpha for e.g. fact_g

c     ---- correlation functions
      parameter(mxbin=1000)
      real*4   dang             ! angular interval (radians)
c     --- WARNING: "degrees" here are a unit of spatial length based on
c     the fact that 1 radian is also a length unit; they should NOT be
c     called degrees to the casual user without sufficient clarification ----
      real*4   dang_d           ! angular interval ("degrees")

      real*4   ang(mxbin)  ! mean angle in a bin ("degrees", converted by ang_norm)
      real*4   dhGpc(mxbin) ! distance in hinvGpc, assumes rSLSc in hinvMpc

c     mean dT/T dT/T correlation in bin: 1 = simply connected, 2 = PDS
      real*4   corr(mxbin,2)  
      integer*4  nvals(mxbin,2)
c     --- correlation in one big bin (e.g. 10-50 "degrees")
      real*4   co1040(2)  
      integer*4  nv1040(2)
      parameter(corrNULL = -99.9) ! TODO: better would be e.g. -9e9 ?
      parameter(tolcor= 1e-2)
      parameter(gtone= 1e-2) ! linear weight to rel corr > 1


      logical*4  handOK ! is the handedness (LH vs RH screw motion) OK ?
      real*8    tolhanded
      parameter(tolhanded= 1e-5) ! tolerance for checking handedness
      real*8   gencopy(4,4) ! temporary copy of first generator
      real*8   diff8c   ! temporary copy of first worst difference of gen a - b

      integer*4  icount ! internal counter
      save       icount
      data       icount /0/

      include 'nxint.dec'  ! 0.1.70 - for tNULL, tNULLUp 


      logical*4  doppler
      logical*4  hwired  ! is the twist hardwired ?
      logical*4  noplot  ! turn off the plots ?
      logical*4  rmincut ! cut off cross-correlation above r_minus ?

      logical*4  test,test2,test3,test5


      data doppler /.false. / !.true. /!
      data hwired /.false. / !.true. /!
      data noplot /.true. /!false. / !
      data iv2_plot /0/ ! default
c      data iv2_plot /6/ 

c     ---- effectively shift both correlations by subtracting a zero point ? ----
      logical*4   dozero
      real*4      czero
      data   dozero /.false. / !.true./ ! 


c     ---- do_annulus ? do we only use sky points potentially in annuli ? ----
      data  do_annulus / .true. / !.false. / !


c     --- Cut off cross-correlation above r_minus ? In other words,
c     do we *plot* cross-correlations only for the shortest geodesic
c     between a pair of points or also for longer geodesics? ---
      data rmincut /.false. /    !.true. /!

      data test /.false. / !.true. / ! 
      data test2 /.false. / !.true. / ! 
      data test3 /.false. / !.true. / ! 
      data test5 / .false. / !.true. / !
      data ch3/'__3'/


C     STATEMENTS FOR EXECUTION:

c     -- nPDS=2 is for testing purposes only
c     -- nPDS=1000 is for the default distribution - make it higher to get
c     significant results ---
      nPDS= 2000 !4000 ! 100 !20! 5000! 30000! 
      if(oneaxis)nPDS= 8000  ! \sim nPDS * int(sqrt(6.0)) 
      nvalsmin = 10 ! 1 ! min number of correlations for a valid estimate

      czero = -1.0e4 !-100.0 microK^2 = -1e4 mK^2
      czero = min(-1e-8, czero) ! if we allow czero > 0, then more tests needed => slower
      if(.not.(dozero)) czero = 0.0 ! build in redundancy


c     -- plotting limits
c      x2plot= 114.6 ! two radians in "degrees"
      x2plot= 180.0 ! "pseudo-degrees"
      y2plot= 1500.0            ! microK^2
c     -- big bin in angle for getting a single estimate of the correlation --
      ang1_d= 0.01 ! 0.01! "degrees"
      ang2_d= 39.99 ! 39.9   ! "degrees"
      dang_d=5.0! 2.0 ! 3.5 !5.0 !1.0                ! interval in angles for binning ("degrees")

      dang= dang_d *pi_180      ! interval in angles for binning (radians)
      angmax= x2plot            ! fast: don't calculate what won't be plotted
      nbins= angmax/dang_d


c     -- mcmc stuff --
      fact_s= 0.5 !0.2

c     -- big bin indexes (lower, upper) inclusive:
c     -                ibin1 .le. ibin .le. ibin2 --
      ibin1= min(max(1, int(ang1_d /dang_d)),nbins) 
      ibin2= min(max(1, int(ang2_d /dang_d)),nbins)

      fact_n= 1.7 ! factor by which to have extra coverage for simple case
      nsimple = nPDS *fact_n

      if(max(nPDS,nsimple).gt.mxsky)then
         if(nowarn.ne.1)then
            print*,'corrS3 WARNING: nPDS, nsimple = ',
     _           nPDS, nsimple, ' too big > mxsky=',mxsky
         endif
         nPDS =max(nPDS,mxsky)
         nsimple =max(nsimple,mxsky)
      endif


c     ---- read circle data from file ----

c      open(1,file='circleinfo'//ch3//'.dat',
c     _     access='sequential',
c     _     form='unformatted',status='old')
c      read(1)ncc,cc1,cc2,H_0,drSLS,nsig
c      read(1)iimax0,jjmax0,maxtheta0, imax,jmax
c      read(1)nthc,glatc,glongc,wmodc,rSLSc,dthMpc
c      read(1)tt1c,tt2c,s1c,s2c
c      read(1)glpix1,gbpix1,glpix2,gbpix2
c      close(1)

      if(iimax.ne.iimax0.or.jjmax.ne.jjmax0.or.
     _     maxtheta.ne.maxtheta0)print*,'ERROR: Dimensions need ',
     _     'to be fixed in corrS3.'

      if(iverb.eq.1)then
         print*,'imax,jmax=',imax,jmax
         print*,'ncc,cc1,cc2,nsig=',ncc,cc1,cc2,nsig

         print*,'nthc(1,2),glatc(1,1,0),glongc(1,1,0)='
         print*,nthc(1,2),glatc(1,1,0),glongc(1,1,0)
         print*,'nthc(1,0),glatc(1,1,0),glongc(1,1,0)='
         print*,nthc(1,0),glatc(1,1,0),glongc(1,1,0)
         print*,'nthc(2,0),glatc(1,1,0),glongc(1,1,0)='
         print*,nthc(2,0),glatc(1,1,0),glongc(1,1,0)
      endif

      ivdel=1
      if(iverb.eq.1)then
         print*,'test: '
         do iv=-imax,imax,ivdel
            do jv=0,jmax,ivdel
               print*,'iv,jv=',iv,jv,
     _              ' nthc(iv,jv),glatc(1,iv,jv),glongc(1,iv,jv)='
               print*,nthc(iv,jv),glatc(1,iv,jv),glongc(1,iv,jv)
            enddo
         enddo
      endif


      
c     ---- temp. diff distribution plot ----
      ivdel=1! 3 !2 !1! 

      dododec=.true.
      if(dododec)then
         ivdel=1
         iv1=1
         if(oneaxis)then  ! since 0.2.19
            iv2=1
         else
            iv2= 6
         endif
         if(iv2_plot.ne.0)then ! default iv2_plot.eq.0: do nothing
            iv1=iv2_plot
            iv2=iv2_plot
         endif
         imax=6
         jmax=0
      endif

c     --- test ---
      if(test)then
         iv=1
         jv=0
         do ith=1,min(20,nthc(iv,jv))
            print*,'iv,tt1c,s[12]c=',
     _           iv,tt1c(ith,iv,jv),s1c(ith,iv,jv),s2c(ith,iv,jv)
         enddo
         print*,' == '
      endif

c     -- convert to microK --
      convmiK = 1e3 ! 1.0 
      
      ch='n' ! do NOT transpose the matrices before multiplying


c     ==== first run through the circles pairs to recalculate the
c     angle    alpha = alph_d *pi_180  
c     (It should be close to the value  ang_in.)    ====


      alpha=0.0
      nalpha=0

      do iv=iv1,iv2,ivdel
         do jv=0,jmax,ivdel
            if(iverb.eq.1)then
               print*,' '
               print*,'corrS3 checking alpha, iv,jv=',iv,jv
            endif
c     ==== get 4-tuple A of independent 4D vectors and its copy B
            flucOK = .true. ! assume by default: there are valid fluc's on circle
            do ivec=1,4
               if(ivec.le.3)then
                  ith= (ivec*nthc(iv,jv))/4

c     --- WARNING: this section could lead to linearly dependent
c     vectors - so don't use them for getting the generator!
                  if(tt1c(ith,iv,jv).lt.-9.0e9.or.
     _                 tt2c(ith,iv,jv).lt.-9.0e9
     _                 )then
                     ith=1
                     do while((ith.lt.nthc(iv,jv)).and.
     _                    (tt1c(ith,iv,jv).lt.-9.0e9.or.
     _                    tt2c(ith,iv,jv).lt.-9.0e9))
                        ith=ith+1
                     enddo
                     if((ith.eq.nthc(iv,jv)).and.
     _                    (tt1c(ith,iv,jv).lt.-9.0e9.or.
     _                    tt2c(ith,iv,jv).lt.-9.0e9))then
                        flucOK = .false.
                     endif
                  endif

c     --- fluctuation on first circle A
                  gl1_8= glpix1(ith, iv,jv)
                  gb1_8= gbpix1(ith, iv,jv)
c     --- fluctuation on second circle B
                  gl2_8= glpix2(ith, iv,jv)
                  gb2_8= gbpix2(ith, iv,jv)
               else
c                  ith= nint( 0.75*real(nthc(iv,jv)) ) 
c     --- centre of first circle A
                  gl1_8 = glongc(1, iv,jv)
                  gb1_8 = glatc(1, iv,jv)
c     --- centre of second circle B
                  gl2_8 = glongc(2, iv,jv)
                  gb2_8 = glatc(2,iv,jv)

               endif

               if(test)print*,'ivec,gl1_8,gb1_8=',ivec,gl1_8,gb1_8
               if(test)print*,'ivec,gl2_8,gb2_8=',ivec,gl2_8,gb2_8

c     --- fluctuation on first circle A - convert to 4D
               flucA(1,ivec)= cos(gb1_8*pi_180)*
     _              cos(gl1_8*pi_180)
               flucA(2,ivec)= cos(gb1_8*pi_180)*
     _              sin(gl1_8*pi_180)
               flucA(3,ivec)= sin(gb1_8*pi_180)
               flucA(4,ivec)= 1.0

c     --- fluctuation on second circle B - convert to 4D
               flucB(1,ivec)= cos(gb2_8*pi_180)*
     _              cos(gl2_8*pi_180)
               flucB(2,ivec)= cos(gb2_8*pi_180)*
     _              sin(gl2_8*pi_180)
               flucB(3,ivec)= sin(gb2_8*pi_180)
               flucB(4,ivec)= 1.0
            enddo         !   do ivec=1,4


c     ---- if an empirical (numerical) value is not available, then use
c     the analytical value ---
            if(flucOK)then
               do ivec=1,3
                  xx8 =
     _                 acos(max(-0.99999, min(0.99999,
     _                 ddot(3,flucA(1,ivec),1,flucA(1,4),1)
     _                 )))
                  alpha=alpha + xx8
                  nalpha=nalpha+1
                  if(test)print*,'corrS3: iv,jv, ivec, alpha(.)=',
     _                 iv,jv, ivec, xx8
                  xx8 =
     _                 acos(max(-0.99999, min(0.99999,
     _                 ddot(3,flucB(1,ivec),1,flucB(1,4),1)
     _                 )))
                  alpha=alpha + xx8
                  nalpha=nalpha+1
                  if(test)print*,'corrS3: iv,jv, ivec, alpha(.)=',
     _                 iv,jv, ivec, xx8
               enddo            !   do ivec=1,4
            else ! if (.not.(flucOK))
c               xx8 = atan(sin(ang_in *pi_180)) 
               xx8= ang_in *pi_180 ! from version 0.1.65
               if(test)print*,'corrS3: ',
     _              'flucOK, iv,jv, ivec, alpha(.)=',
     _              flucOK, iv,jv, ivec, xx8
               alpha= alpha + 2.0d0*xx8
               nalpha=nalpha+2
            endif

         enddo                  !do jv=0,jmax,ivdel
      enddo                     ! do iv=iv1,iv2,ivdel

      if(nalpha.gt.0)then
         alpha=alpha/dble(nalpha)
         alph_d = alpha /pi_180  ! degrees
      else
         alpha=ang_in
      endif


      print*,'corrS3: ang_in, alpha(degrees)=',ang_in,alph_d


c     -- angle to SLS (surface of last scattering): 
c     -- use spherical cosine law and in-radius of fundamental domain = pi/10
c      angSLS = atan(tan(pi/10.0d0) / max(tolcos,cos(alpha*pi_180)))
c      angSLS = atan(tan(pi/10.0d0) / max(tolcos,cos(alpha*pi_180)))
      angSLS = atan(tan(pi/10.0d0) / max(tolcos,cos(alpha)))

      fitaSLS = 0.1545
c     0.00 -> 31.23
c     0.10 -> 34.32
c     0.135 -> 35.40
c     0.14 -> 35.55
c     0.1545 -> 35.9988
c     0.155 -> 36.014
c     0.157 -> 36.076
c     0.18 -> 36.78

c      angSLS = angSLS*(1.0+fitaSLS)

c      angSLS=  0.366921243  ! from 0.1545
      
c      angSLS= 0.36692 !...  35.9988
c      angSLS= 0.36693 !  35.99969736
c      angSLS= 0.366933d0  ! 35.99998604
c      angSLS= 0.366935 ! 36.00018236
c      angSLS= 0.3670 ! -> 36.00647917

c     --- from Jeff Weeks 26.02.2004  - not actually used here in this form
      xx8 = atan( 
     _     (1.0d0-cos(pi/5.0d0))
     _     / max(tolcos, sin(pi/5.0d0) )
c     _     / max(tolcos,cos(alpha*pi_180)) 
     _     / max(tolcos,cos(alpha)) 
     _     )


c      angSLS=xx8

      if(test)print*,'corrS3: xx8 (jeff), angSLS=',xx8,angSLS

      cosrSLS = cos(angSLS)  ! half the angle subtended by a generator
      sinrSLS = sin(angSLS)
      sin2rSLS = real(max(sinrSLS * sinrSLS, 
     _     dble(tolcos)*dble(tolcos)))
      if(test)print*,'corrS3: cosrSLS, sinrSLS=',cosrSLS, sinrSLS



      do iv=iv1,iv2,ivdel
         do jv=0,jmax,ivdel
            if(iverb.eq.1)then
               print*,' '
               print*,'corrS3 getting generator, iv,jv=',iv,jv
            endif
c     ==== get 4-tuple A of independent 4D vectors and its copy B
            do ivec=1,4
               if(ivec.le.3)then
                  ith= (ivec*nthc(iv,jv))/4

c     --- WARNING: this section could lead to linearly dependent
c     vectors - so don't use them for getting the generator!
                  if(tt1c(ith,iv,jv).lt.-9.0.or.
     _                 tt2c(ith,iv,jv).lt.-9.0
     _                 )then
                     ith=1
                     do while((ith.lt.nthc(iv,jv)).and.
     _                    (tt1c(ith,iv,jv).lt.-9.0.or.
     _                    tt2c(ith,iv,jv).lt.-9.0))
                        ith=ith+1
                     enddo
                  endif

c     --- fluctuation on first circle A
                  gl1_8= glpix1(ith, iv,jv)
                  gb1_8= gbpix1(ith, iv,jv)
c     --- fluctuation on second circle B
                  gl2_8= glpix2(ith, iv,jv)
                  gb2_8= gbpix2(ith, iv,jv)
               else
c                  ith= nint( 0.75*real(nthc(iv,jv)) ) 
c     --- centre of first circle A
                  gl1_8 = glongc(1, iv,jv)
                  gb1_8 = glatc(1, iv,jv)
c     --- centre of second circle B
                  gl2_8 = glongc(2, iv,jv)
                  gb2_8 = glatc(2,iv,jv)

               endif

               if(test)print*,'ivec,gl1_8,gb1_8=',ivec,gl1_8,gb1_8
               if(test)print*,'ivec,gl2_8,gb2_8=',ivec,gl2_8,gb2_8

               if(ivec.le.3)then
                  ss8 = sinrSLS
                  cc8 = cosRSLS
               else
                  ss8 = sin(pi/10.0d0)
                  cc8 = cos(pi/10.0d0)
               endif
c     --- fluctuation on first circle A - convert to 4D
               flucA(1,ivec)= ss8*  cos(gb1_8*pi_180)*
     _              cos(gl1_8*pi_180)
               flucA(2,ivec)= ss8*  cos(gb1_8*pi_180)*
     _              sin(gl1_8*pi_180)
               flucA(3,ivec)= ss8*  sin(gb1_8*pi_180)
               flucA(4,ivec)= cc8

c     --- fluctuation on second circle B - convert to 4D
               flucB(1,ivec)= ss8*  cos(gb2_8*pi_180)*
     _              cos(gl2_8*pi_180)
               flucB(2,ivec)= ss8*  cos(gb2_8*pi_180)*
     _              sin(gl2_8*pi_180)
               flucB(3,ivec)= ss8*  sin(gb2_8*pi_180)
               flucB(4,ivec)= cc8
            enddo  ! do ivec=1,4


c     --- backup copies
            do j=1,4
               do i=1,4
                  flucAc(i,j)= flucA(i,j) 
                  flucBc(i,j)= flucB(i,j)
               enddo
            enddo

            if(test)then
               print*,' '
               print*,'matched pixel pairs (3) + centre: '
               call pmat8v('a',flucAc)
               call pmat8v('b',flucBc)
            endif



c     ---- replace the circle centre vector pair by another pair of 
c     matching vectors in the same eigenplane, rotated "forwards" by pi/2 ----
c     see 2.3.3 in Roukema 2005 astro-ph/0409694
c     *  flucA(.,4) = s
c     *  flucB(.,4) = Ms

            if(iverb.eq.1)then
               call pvec8v('s',flucA(1,4))
               call pvec8v('Ms',flucB(1,4))
            endif

            do i=1,4
               svec(i)=flucA(i,4)
               vecMs(i)=flucB(i,4)
            enddo

            s_Ms = flucA(1,4)*flucB(1,4) + 
     _           flucA(2,4)*flucB(2,4) + 
     _           flucA(3,4)*flucB(3,4) + 
     _           flucA(4,4)*flucB(4,4)
            tmp_norm = 1.0d0 - s_Ms * s_Ms
            if(tmp_norm.lt.tolcos)then
               print*,'corrS3: ERROR: normalising t'
               print*,'tmp_norm = ',tmp_norm
               tmp_norm= tolcos
            endif
            tmp_norm= 1.0/sqrt(tmp_norm)

            do i=1,4
               tvec(i)= tmp_norm* (flucB(i,4) - s_Ms * flucA(i,4))
            enddo


            t_Ms = tvec(1)*flucB(1,4) + 
     _           tvec(2)*flucB(2,4) + 
     _           tvec(3)*flucB(3,4) + 
     _           tvec(4)*flucB(4,4)

            do i=1,4
               vecMt(i)= -t_Ms* flucA(i,4) + s_Ms * tvec(i)
            enddo

c     --- check that these are unit vectors ----
            if(test3)then
               xx8 = ddot(4,svec,1,svec,1)
               print*,'svec norm^2 = ',xx8
               xx8 = ddot(4,vecMs,1,vecMs,1)
               print*,'vecMs norm^2 = ',xx8
               xx8 = ddot(4,tvec,1,tvec,1)
               print*,'tvec norm^2 = ',xx8
               xx8 = ddot(4,vecMt,1,vecMt,1)
               print*,'vecMt norm^2 = ',xx8
            endif

c     --- copy tvec, vecMt to flucA, flucB ---
            do i=1,4
               flucA(i,4)= tvec(i)
               flucB(i,4)= vecMt(i)
            enddo
            if(iverb.eq.1)then
               call pvec8v('t',flucA(1,4))
               call pvec8v('Mt',flucB(1,4))
               print*,'s_Ms,t_Ms=',s_Ms,t_Ms
            endif

            if(test)then
               print*,'angles from projection to 2-sphere: '
               do ivec=1,4
                  jvec=mod(ivec,4)+1
                  aa4A = angle(
     _                 real(flucA(1,ivec)),real(flucA(2,ivec)),
     _                 real(flucA(3,ivec)),
     _                 real(flucA(1,jvec)),real(flucA(2,jvec)),
     _                 real(flucA(3,jvec)))
                  print*,'flucA: ivec,jvec, angle (rad) =',
     _                 ivec,jvec, aa4A
                  aa4B = angle(
     _                 real(flucB(1,ivec)),real(flucB(2,ivec)),
     _                 real(flucB(3,ivec)),
     _                 real(flucB(1,jvec)),real(flucB(2,jvec)),
     _                 real(flucB(3,jvec)))
                  print*,'flucB: ivec,jvec, angle (rad) =',
     _                 ivec,jvec, aa4B
               enddo
            endif

c     --- alternative to using fluctuation positions: 
c     * find orthogonal 2-plane and input the pi/5 = 36 deg rotation 
c     by hand;
c     * check that its handedness is correct.

            handOK = .false.
            ihanded = 0 
            do while((.not.(handOK)).and.ihanded.le.1)
               ihanded=ihanded+1
c     ---- put e_i basis in  Xcopy ----
               do j=1,4
                  do i=1,4
                     if(i.eq.j)then
                        Xcopy(i,j)= 1.0d0
                     else
                        Xcopy(i,j)= 0.0d0
                     endif
                  enddo
               enddo
               
               
               if(iverb.eq.1)then
                  print*,'corrS3: creating  u,,,'
               endif
c     --- find most distant axis ---
               j=1
               jbest=j
               tmp_norm= svec(j)*svec(j) + tvec(j)*tvec(j)
               do j=2,4
                  xx8=  svec(j)*svec(j) + tvec(j)*tvec(j)
                  if(xx8.lt.tmp_norm)then
                     jbest=j
                     tmp_norm= xx8
                  endif
               enddo


c     --- use that axis minus its projections in s-t plane
               do i=1,4
                  uvec(i)= Xcopy(i,jbest) 
     _                 - svec(jbest)*svec(i) - tvec(jbest) * tvec(i)
               enddo
c     --- normalise u
               tmp_norm = ddot(4,uvec,1,uvec,1)
               if(tmp_norm.gt.tol_sqrt)then ! avoid sqrt of non-positive
                  tmp_norm = sqrt(tmp_norm) 
                  do i=1,4
                     uvec(i)= uvec(i)/tmp_norm
                  enddo
               else
                  print*,'corrS3 ERROR: uvec too small:',
     _                 ' tmp_norm^2 = ',tmp_norm
                  write(6,'(a,4g10.2)')
     _                 'corrS3 ERROR: svec= ',(svec(j),j=1,4)
                  write(6,'(a,4g10.2)')
     _                 'corrS3 ERROR: tvec= ',(tvec(j),j=1,4)
                  write(6,'(a,i4)')
     _                 'corrS3 ERROR: jbest= ', jbest
                  write(6,'(a,4g10.2)')
     _                 'corrS3 ERROR: Xcopy: ',(Xcopy(i,jbest),i=1,4)
               endif

               if(iverb.eq.1)then
                  call pvec8v('u',uvec)
               endif

c     ----- create v : version 1
               if(iverb.eq.1)then
                  print*,'corrS3: creating  v...',
     _                 ' ihanded=',ihanded
               endif
cc     --- find most distant axis ---
c               j=1
c               jbest=j
c               tmp_norm= svec(j)*svec(j) + tvec(j)*tvec(j) 
c     _              + uvec(j)*uvec(j)
c               do j=2,4
c                  xx8=  svec(j)*svec(j) + tvec(j)*tvec(j)
c     _                 + uvec(j)*uvec(j)
c                  if(xx8.lt.tmp_norm)then
c                     jbest=j
c                     tmp_norm= xx8
c                  endif
c               enddo
c               
cc     --- use that axis minus its projections in s-t plane
c               do i=1,4
c                  vvec(i)= Xcopy(i,jbest) 
c     _                 - svec(jbest)*svec(i) - tvec(jbest) * tvec(i)
c     _                 - uvec(jbest)*uvec(i)
c               enddo
cc     --- normalise v
c               tmp_norm = ddot(4,vvec,1,vvec,1)
c               if(tmp_norm.gt.tol_sqrt)then ! avoid sqrt of non-positive
c                  tmp_norm = sqrt(tmp_norm) 
c                  do i=1,4
c                     vvec(i)= vvec(i)/tmp_norm
c                  enddo
c               else
c                  print*,'corrS3 ERROR: vvec too small:',
c     _                 ' tmp_norm^2 = ',tmp_norm
c               endif

c     ----- create v : version 2
c     --- create v as the cross product of s,t,u
               vvec(1) =  det3x3( 
     -              svec(2),tvec(2),uvec(2),
     -              svec(3),tvec(3),uvec(3),
     -              svec(4),tvec(4),uvec(4))
               vvec(2) = - det3x3( 
     -              svec(1),tvec(1),uvec(1),
     -              svec(3),tvec(3),uvec(3),
     -              svec(4),tvec(4),uvec(4))
               vvec(3) =  det3x3( 
     -              svec(1),tvec(1),uvec(1),
     -              svec(2),tvec(2),uvec(2),
     -              svec(4),tvec(4),uvec(4))
               vvec(4) = - det3x3( 
     -              svec(1),tvec(1),uvec(1),
     -              svec(2),tvec(2),uvec(2),
     -              svec(3),tvec(3),uvec(3))


               if(ihanded.eq.2)then
                  do i=1,4
                     vvec(i)= -vvec(i)
                  enddo
               endif


               if(iverb.eq.1)then
                  call pvec8v('v',vvec)
               endif

c     ---- calculate Mu, Mv
c               cc8 = cos(pi/5.0d0)
c               ss8 = sin(pi/5.0d0)
               cc8 = cos(twist* pi/5.0d0)
               ss8 = sin(twist* pi/5.0d0)
               do i=1,4
                  vecMu(i)= cc8 * uvec(i) + ss8 * vvec(i)
                  vecMv(i)= -ss8 * uvec(i) + cc8 * vvec(i)
               enddo

c     ---- copy into flucA, flucB
               do i=1,4
                  flucA(i,1)= svec(i)
                  flucB(i,1)= vecMs(i)
                  flucA(i,2)= tvec(i)
                  flucB(i,2)= vecMt(i)
                  flucA(i,3)= uvec(i)
                  flucB(i,3)= vecMu(i)
                  flucA(i,4)= vvec(i)
                  flucB(i,4)= vecMv(i)
               enddo

c     == invert A ==
c     -- Copy flucA to Xcopy(.,.).  Xcopy will be modified by the inversion
c     algorithm dgesv.
c     Initialise flucAi to the identity. flucAi will contain  A inverse. --
               do j=1,4
                  do i=1,4
                     Xcopy(i,j)= flucA(i,j) 
                     if(i.eq.j)then
                        flucAi(i,j)= 1.0d0
                     else
                        flucAi(i,j)= 0.0d0
                     endif
                  enddo
               enddo

               if(test)print*,'corrS3: will call dgesv'
               if(test)then
                  print*,'flucA (before dgesv): '
                  print*,'[',(flucA(1,j),', ',j=1,3),
     _                 flucA(1,4),';'
                  print*,(flucA(2,j),', ',j=1,3),
     _                 flucA(2,4),';'
                  print*,(flucA(3,j),', ',j=1,3),
     _                 flucA(3,4),';'
                  print*,(flucA(4,j),', ',j=1,3),
     _                 flucA(4,4),']'
               endif

               if(test)then
                  print*,'Xcopy: '
                  print*,(Xcopy(1,j),j=1,4)
                  print*,(Xcopy(2,j),j=1,4)
                  print*,(Xcopy(3,j),j=1,4)
                  print*,(Xcopy(4,j),j=1,4)
                  print*,'flucAi (before dgesv): '
                  print*,(flucAi(1,j),j=1,4)
                  print*,(flucAi(2,j),j=1,4)
                  print*,(flucAi(3,j),j=1,4)
                  print*,(flucAi(4,j),j=1,4)
               endif

c     ---- invert A using LAPACK routine
c     SUBROUTINE DGESV( N, NRHS, A, LDA, IPIV, B, LDB, INFO )
               call dgesv(4, 4, Xcopy, 4, ipiv, flucAi, 4, info)


               if(test)then
                  print*,'flucAi (after dgesv): '
                  print*,'[',(flucAi(1,j),', ',j=1,3),
     _                 flucAi(1,4),';'
                  print*,(flucAi(2,j),', ',j=1,3),
     _                 flucAi(2,4),';'
                  print*,(flucAi(3,j),', ',j=1,3),
     _                 flucAi(3,4),';'
                  print*,(flucAi(4,j),', ',j=1,3),
     _                 flucAi(4,4),']'
               endif


               if(info.ne.0)then
                  print*,
     _                 'corrS3 ERROR: flucA cannot be inverted,',
     _                 'dgesv.info =', info,
     _                 'iv,jv=',iv,jv
                  stop
               endif

               if(test)print*,'corrS3: will call dgemm'
               if(test)then
                  print*,'flucB (before dgemm): '
                  print*,'[',(flucB(1,j),', ',j=1,3),
     _                 flucB(1,4),';'
                  print*,(flucB(2,j),', ',j=1,3),
     _                 flucB(2,4),';'
                  print*,(flucB(3,j),', ',j=1,3),
     _                 flucB(3,4),';'
                  print*,(flucB(4,j),', ',j=1,3),
     _                 flucB(4,4),']'
               endif

c     --- LAPACK: multiply matrices
               call dgemm(ch,ch, 4,4,4, 1.0d0, flucB, 4, flucAi, 4,
     _              0.0d0, generator(1,1,iv,jv), 4)

               if(test)then
                  print*,'generator (after dgesv): '
                  print*,'[',(generator(1,j,iv,jv),', ',j=1,3),
     _                 generator(1,4,iv,jv),';'
                  print*,(generator(2,j,iv,jv),', ',j=1,3),
     _                 generator(2,4,iv,jv),';'
                  print*,(generator(3,j,iv,jv),', ',j=1,3),
     _                 generator(3,4,iv,jv),';'
                  print*,(generator(4,j,iv,jv),', ',j=1,3),
     _                 generator(4,4,iv,jv),']'
               endif


c     --- CHECK: use angle4D8  to check that length in S^3
c     of generator, i.e. angle in R^4, is pi/5
               do j=1,4
                  do i=1,4
                     if(i.eq.j)then
                        Xcopy(i,j)=1.0d0
                     else
                        Xcopy(i,j)=0.0d0
                     endif
                  enddo
               enddo


c     --- 0.1.75 warning: this section incorrect for arbitrary twist
               do j=1,4
                  aa8 = angle4D8(kcurv, 
     _                 Xcopy(1,j),Xcopy(2,j),Xcopy(3,j),Xcopy(4,j),
     _                 generator(1,j,iv,jv),generator(2,j,iv,jv),
     _                 generator(3,j,iv,jv),generator(4,j,iv,jv))
                  diff8 = abs(aa8- pi/5.0d0)  

c     ---- 0.1.75:          not an ERROR if twist is arbitrary
                  if(hwired)then
                     if(diff8.gt.tola8a)then
                        write(6,'(a,i3,i3,i3,f8.3,f8.3)')
     _                       'corrS3: ERROR: j,iv,jv, aa8, pi/5=',
     _                       j,iv,jv,aa8,pi/5.0d0
                        print*,Xcopy(1,j),Xcopy(2,j),Xcopy(3,j),
     _                       Xcopy(4,j),
     _                       generator(1,j,iv,jv),generator(2,j,iv,jv),
     _                       generator(3,j,iv,jv),generator(4,j,iv,jv)
                     else if(test.or.(diff8.gt.tola8b.and.
     _                       (nowarn.ne.1)))then
                        write(6,'(a,i3,i3,i3,f8.3,f8.3)')
     _                       'corrS3: WARNING: j,iv,jv, aa8, pi/5=',
     _                       j,iv,jv,aa8,pi/5.0d0
                        print*,Xcopy(1,j),Xcopy(2,j),Xcopy(3,j),
     _                       Xcopy(4,j),
     _                       generator(1,j,iv,jv),generator(2,j,iv,jv),
     _                       generator(3,j,iv,jv),generator(4,j,iv,jv)
                     endif
                  endif
               enddo

c     ---- backup copy of generator
               if(ihanded.eq.1)then
                  do j=1,4
                     do i=1,4
                        gencopy(i,j)=generator(i,j,iv,jv)
                     enddo
                  enddo
               endif


c     ---- CHECK: does gen * a = b ?
               call dgemm(ch,ch, 4,4,4, 1.0d0, 
     _              generator(1,1,iv,jv), 4, 
     _              flucA, 4,
     _              0.0d0, Xcopy, 4)
               diff8 = 0.0d0
               do j=1,4
                  do i=1,4
                     Xcopy(i,j)= Xcopy(i,j) - flucB(i,j)
                     diff8=max(diff8,Xcopy(i,j)*Xcopy(i,j))
                  enddo
               enddo
               if(test)call pmat8v('gen_a_minus_b',Xcopy)

               if(diff8.gt.tola8a*tola8a.and.(iverb.eq.1))print*,
     _              'corrS3 WARNING: max^2 of generator * a - b',
     _              ' is ',diff8


c     ---- CHECK: does gen * a_copy = b_copy ?
               call dgemm(ch,ch, 4,4,4, 1.0d0, 
     _              generator(1,1,iv,jv), 4, 
     _              flucAc, 4,
     _              0.0d0, Xcopy, 4)
               diff8 = 0.0d0
               do j=1,4
                  do i=1,4
                     Xcopy(i,j)= Xcopy(i,j) - flucBc(i,j)
c                     diff8=max(diff8,Xcopy(i,j)*Xcopy(i,j))

                     diff8=max(diff8,abs(Xcopy(i,j)))
                  enddo
               enddo
               if(test)call pmat8v('gen_ac_minus_bc',Xcopy)

               if(diff8.gt.tola8a.and.(iverb.eq.1))print*,
     _              'corrS3 WARNING: max of |generator * a - b|',
     _              ' (backup copies)',
     _              ' is ',diff8

               if(ihanded.eq.1)diff8c = diff8

               if(diff8.lt.tolhanded)then
                  handOK = .true.
               elseif(ihanded.eq.2)then
c                  print*,'corrS3 ERROR:',
                  if(iverb.eq.1)then
                     print*,'corrS3 WARNING:',
     _                    '  max of |generator * a - b|',
     _                    ' (backup copies)',
     _                    ' is ',diff8
                     print*,'The generator may be WRONG.'
                     print*,'The previous gen. ',
     _                    'max of |gen*a - b|= ',diff8c
                  endif

c     ---- if the first try gave a smaller difference, then restore it ----
                  if(diff8c.lt.diff8)then
                     do j=1,4
                        do i=1,4
                           generator(i,j,iv,jv)=
     _                          gencopy(i,j)
                        enddo
                     enddo
                     print*,'Using previous generator estimate.'
                  endif
               endif
            enddo !             do while((.not.(handOK)).and.ihanded.le.1)

            do j=1,4
               do i=1,4
                  Xcopy(i,j)= generator(i,j,iv,jv)
               enddo
            enddo
            if(test)print*,'corrS3: will call dgesv for inverse'
            do j=1,4
               do i=1,4
                  if(i.eq.j)then
                     geninv(i,j,iv,jv)=1.0d0
                  else
                     geninv(i,j,iv,jv)=0.0d0
                  endif
               enddo
            enddo

            call dgesv(4, 4, Xcopy, 4, ipiv, geninv(1,1,iv,jv), 
     _           4, info)

c     CHECK: use angle4D8  to check that length in S^3
c            of generator, i.e. angle in R^4, is pi/5
            do j=1,4
               do i=1,4
                  if(i.eq.j)then
                     Xcopy(i,j)=1.0d0
                  else
                     Xcopy(i,j)=0.0d0
                  endif
               enddo
            enddo

            do j=1,4
               aa8 = angle4D8(kcurv, 
     _              Xcopy(1,j),Xcopy(2,j),Xcopy(3,j),Xcopy(4,j),
     _              geninv(1,j,iv,jv),geninv(2,j,iv,jv),
     _              geninv(3,j,iv,jv),geninv(4,j,iv,jv))
               diff8 = abs(aa8- pi/5.0d0)

c     ---- 0.1.75: not an ERROR if twist is arbitrary
               if(hwired)then
                  if(diff8.gt.tola8a)then
                     write(6,'(a,i3,i3,i3,f8.3,f8.3)')
     _                    'corrS3: geninv ERROR: j,iv,jv, aa8,'//
     _                    ' pi/5=',
     _                    j,iv,jv,aa8,pi/5.0d0
                     print*,Xcopy(1,j),Xcopy(2,j),Xcopy(3,j),
     _                    Xcopy(4,j),
     _                    geninv(1,j,iv,jv),geninv(2,j,iv,jv),
     _                    geninv(3,j,iv,jv),geninv(4,j,iv,jv)
                  else if(test.or.(diff8.gt.tola8b.and.
     _                    (nowarn.ne.1)))then
                     write(6,'(a,i3,i3,i3,f8.3,f8.3)')
     _                    'corrS3: geninv WARNING: j,iv,jv, aa8,'//
     _                    ' pi/5=',
     _                    j,iv,jv,aa8,pi/5.0d0
                     print*,Xcopy(1,j),Xcopy(2,j),Xcopy(3,j),
     _                    Xcopy(4,j),
     _                    geninv(1,j,iv,jv),geninv(2,j,iv,jv),
     _                    geninv(3,j,iv,jv),geninv(4,j,iv,jv)
                  endif
               endif
            enddo


            if(info.ne.0)then
               print*,
     _           'corrS3 ERROR: generator cannot be inverted,',
     _           'dgesv.info =', info,
     _           'iv,jv=',iv,jv
               stop
            endif

         enddo
      enddo



c     --- flush ran generator ---
      xx=rnd(0)*23
      do i=1,int(xx) 
         xx=rnd(0)
      enddo


c     === generate max(nsimple,nPDS) random sky positions, excluding 
c     galaxy etc.

      GCm_rad = GCmin *pi_180
c      if(test)print*,'corrS3: random pixels ',
c     _     'galcut,gb,bcut,GCdis,GCm_rad =',
c     _     galcut,gb,bcut,GCdis,GCm_rad



c     ---- store circle *centres* as xyz coordinates (0.3.0) ----
      jv=0
      do iv=iv1,iv2,ivdel
c         do jv=0,jmax,ivdel  ! POSSIBLE TODO: if jmax .ne. 0  then restructure
         gl1_8 = glongc(1, iv,jv)
         gb1_8 = glatc(1, iv,jv)
         cen_x(iv)=  sinrSLS* cos(gb1_8*pi_180)*
     _        cos(gl1_8*pi_180)
         cen_y(iv)=  sinrSLS*  cos(gb1_8*pi_180)*
     _        sin(gl1_8*pi_180)
         cen_z(iv)=  sinrSLS*  sin(gb1_8*pi_180)
      enddo

c     --- units in which diameter of SLS is pi ---
      ang_norm = pi/(2.0*angSLS)

      cosaa8= (cos(angSLS + ang2_d*pi_180/ang_norm) 
     _     - cos(angSLS)*cos(pi/5.0d0))/
     _     (max(tolcos,sin(angSLS) * sin(pi/5.0d0)))
      alpmax= acos(min(cosaa8,1.0d0))

      if(angSLS .gt. -2.0d0 * (angSLS - pi*0.1))then ! face centre separation
         cosaa8= 
     _        ( cos(angSLS - ang2_d*pi_180/ang_norm) - 
     _        cos(angSLS)*cos(pi/5.0d0))/
     _        (max(tolcos,sin(angSLS) * sin(pi/5.0d0)))
         alpmin= acos(min(cosaa8,1.0d0))
      else
         alpmin = 0.0
      endif

      if(iverb.eq.1)then
         print*,'alpmin,alpmax=',alpmin,alpmax
         print*,'alpha, angSLS = ',alpha,angSLS
      endif

c     --- if do_annulus, then  larger separations than ang2[_d] are highly biased
      if(do_annulus)rmincut= .true.  
         

c     ---- wmaplin parameters ---
      measur=.true.
      noise=.false.

c     --- general factor by which to try a larger number of points
c     -   OK to be big, but needs to be finite  - 
      fact_g = 20.0*angSLS/(max(tolalp,ang2_d*pi_180/ang_norm))

      nsky=0
      n_need= max(nPDS,nsimple) ! number needed
      mx_try= int(n_need * fact_g) ! max number to try
      i_try = 1
      do while(nsky.lt.n_need .and. i_try.lt.mx_try)
c      do i=1,max(nPDS,nsimple)
         call spher2(gl,gb)
         xx= cos(gb*pi_180)*cos(gl*pi_180)
         yy= cos(gb*pi_180)*sin(gl*pi_180)
         zz= sin(gb*pi_180)
         GCdis= angle(xx,yy,zz,1.0,0.0,0.0) 
c         if(test)print*,'corrS3: random pixels trying i=',i,
c     _        ' galcut,gb,bcut,GCdis,GCm_rad =',
c     _        galcut,gb,bcut,GCdis,GCm_rad
         
         if((.not.(galcut)).or.
     _        (abs(gb).gt.bcut).and.
     _        (GCdis.gt.GCm_rad))then
            nsky=nsky+1
            gl_8(nsky)= dble(gl)
            gb_8(nsky)= dble(gb)
            gl_4(nsky)= gl
            gb_4(nsky)= gb

c         - convert point nsky  to (x,y,z,w)
            pix(1,nsky)= sinrSLS*  cos(gb_8(nsky)*pi_180)*
     _              cos(gl_8(nsky)*pi_180)
            pix(2,nsky)= sinrSLS*  cos(gb_8(nsky)*pi_180)*
     _              sin(gl_8(nsky)*pi_180)
            pix(3,nsky)= sinrSLS*  sin(gb_8(nsky)*pi_180)
            pix(4,nsky)= cosrSLS

            t_4(nsky)= wmaplin(gl_4(nsky),gb_4(nsky),
     _               temp,errtemp,gallong,gallat) 

c     ---- remove this entry if the pixel is masked ----
            if(t_4(nsky).lt.tNULLup)then
               nsky=nsky-1  
c     ---- only use sky points potentially in annuli  ----
            elseif(do_annulus)then
               maybe=.false.
               jv=0
               do iv=iv1,iv2,ivdel
                  aa8 = angle8(
     _                 pix(1,nsky),pix(2,nsky),pix(3,nsky),
     _                 cen_x(iv),  cen_y(iv),  cen_z(iv)  ) ! radians

c     --- is it on one of the six "first" faces? ---
                  if(alpmin.le.aa8 .and. aa8.le.alpmax)then ! radians
                     maybe=.true.
                     may_ann(iv,1,nsky) = .true.
c     --- else, is it on one of the matching six faces? ---
                  elseif(alpmin.le.(pi-aa8) .and. 
     _                    (pi-aa8).le.alpmax)then
                     maybe=.true.
                     may_ann(iv,2,nsky) = .true.
                  endif
               enddo ! do iv=iv1,iv2,ivdel

               if(.not.(maybe)) nsky=nsky-1 ! .not.(maybe) = certainly not
            endif               !             if(do_annulus)then

         endif
         i_try = i_try + 1
      enddo !  do while(nsky.lt.n_need .and. i_try.lt.mx_try)
      if(iverb.eq.1)
     _     print*,'corrS3: max(nPDS,nsimple)=',max(nPDS,nsimple),
     _     '  nsky=',nsky
      nPDS=min(nPDS,nsky)
      nsimple=min(nsimple,nsky)
      if(iverb.eq.1)print*,' '


c     shifted higher up
c      dang_d=0.5                ! interval in angles for binning (degrees)
c      dang= dang_d *pi_180      ! interval in angles for binning (radians)
c      angmax= 180.0 ! degrees
c      nbins= angmax/dang_d

c     --- initialise bins ---
      do ibin=1,nbins
         ang(ibin)= (real(ibin)-0.5)*dang_d
         corr(ibin,1) = 0.0
         corr(ibin,2) = 0.0
         nvals(ibin,1) = 0
         nvals(ibin,2) = 0
      enddo


      if(iverb.eq.1)print*,'will start simply connected corr fn...'
c     N = e.g. 1000 (testing) to 10000 (on 50GHz processors?) random sky points
c     do i=1,N-1  do j=i+1,N

c     --- units in which diameter of SLS is pi ---
c      tmp_norm = pi/(2.0*angSLS)
c      ang_norm = pi/(2.0*angSLS)

      do iA= 1,nsimple-1
         do iB= iA+1,nsimple
c     use angle4d8 
c         - find the angle they subtend on the 3-sphere  * R_SLS (Mpc)
c            -> add to binning for "normal" correlation
            aa8 = angle4D8(kcurv, 
     _           pix(1,iA),pix(2,iA),pix(3,iA),pix(4,iA),
     _           pix(1,iB),pix(2,iB),pix(3,iB),pix(4,iB))
     _           *ang_norm
c     _           *tmp_norm
c     _           /angSLS
            ibin= int(aa8/dang) +1
            if(ibin.ge.1.and.ibin.le.nbins)then
               if(.not.(doppler))then
                  corr(ibin,1)= corr(ibin,1) + t_4(iA)*t_4(iB)
               elseif(doppler)then
                  corr(ibin,1)= corr(ibin,1) + t_4(iA)*t_4(iB) *
     _                 ddot(3, pix(1,iA),1,pix(1,iB),1) ! 3-space dot product
     _                 /sin2rSLS
               endif

c     _              wmaplin(gl_4(iA),gb_4(iA),
c     _               temp,errtemp,gallong,gallat) *
c     _              wmaplin(gl_4(iB),gb_4(iB),
c     _               temp,errtemp,gallong,gallat) 
               nvals(ibin,1)= nvals(ibin,1) + 1
c               if(test)print*,'corrS3: bins test ',
c     _              'iA,iB,ibin,corr(ibin,1)=',
c     _              iA,iB,ibin,corr(ibin,1)
            endif
         enddo
      enddo

      if(iverb.eq.1)print*,'will start multiply connected corr fn...'
c         - apply the 6 generators and -1 * 6 generators to point i
c            - find the angle each subtends with j on the 3-sphere  * R_SLS (Mpc)
c              add each of the 12 distances to binning for PDS correlation
      do iA= 1,nPDS-1
         do iB= iA+1,nPDS
c     use angle4d8 
c         - find the angle they subtend on the 3-sphere,
c           -> convert to angle on 2-sphere
c            -> add to binning for "normal" correlation
            do iv=iv1,iv2,ivdel
c     --- require membership in matching annuli ---
               if(  (.not.(do_annulus)) .or.
     _              (may_ann(iv,1,iA).and.may_ann(iv,2,iB))
     _              .or.
     _              (may_ann(iv,2,iA).and.may_ann(iv,1,iB))
     _              )then

                  if(do_annulus)then
c     ----  B must be mapped "back" to A ---
                     if(may_ann(iv,1,iA).and.may_ann(iv,2,iB))then
                        inv1=2
                        inv2=2
c     ---- B must be mapped "forward" to A ---
                     elseif(may_ann(iv,2,iA).and.may_ann(iv,1,iB))then
                        inv1=1
                        inv2=1
                     endif
                  else  ! if .not.(do_annulus)
                     inv1=1
                     inv2=2
                  endif
                  do jv=0,jmax,ivdel

                     do inv=inv1,inv2 ! use generator and its inverse
c     --- LAPACK: map pixel iB to one of its images at pix_B2
c     m=4 n=1 k=4
                        if(inv.eq.1)then
                           call dgemm(ch,ch, 4,1,4, 1.0d0, 
     _                          generator(1,1,iv,jv), 4, 
     _                          pix(1,iB), 4,
     _                          0.0d0, pix_B2, 4)
                        else
                           call dgemm(ch,ch, 4,1,4, 1.0d0, 
     _                          geninv(1,1,iv,jv), 4, 
     _                          pix(1,iB), 4,
     _                          0.0d0, pix_B2, 4)
                        endif

c     --- use the 3D (S^3) distance (angle in 4D (R^4)) for bin choice ---
                        aa8 = angle4D8(kcurv, 
     _                       pix(1,iA),pix(2,iA),pix(3,iA),pix(4,iA),
     _                       pix_B2(1),pix_B2(2),pix_B2(3),pix_B2(4))
     _                       *ang_norm
c     _                    *tmp_norm
c     _                    /angSLS
                        ibin= int(aa8/dang) +1

c     c     --- possible check: convert pix_B2 back to a gal long, lat pair
c     c                  call xyz_8rda(pix_B2(1),pix_B2(2),pix_B2(3),
c     c     _                 rr8,delta8,ascen8)
c     call xyz_rda(real(pix_B2(1)),
c     _                    real(pix_B2(2)),real(pix_B2(3)),
c     _                    rr,delta,ascen)
c     ascen = ascen * 15.0d0 ! convert to degrees

c     --- use temperatures from observed positions ----
                        if(ibin.ge.1.and.ibin.le.nbins)then
                           if(.not.(doppler))then
                              corr(ibin,2)= corr(ibin,2) + 
     _                             t_4(iA)*t_4(iB)
                           elseif(doppler)then
                              corr(ibin,2)= corr(ibin,2) + 
     _                             t_4(iA)*t_4(iB) *
     _                             ddot(3, pix(1,iA),1,pix(1,iB),1) ! 3D dot prod
     _                             /sin2rSLS
                           endif
c     _                       wmaplin(gl_4(iA),gb_4(iA),
c     _                       temp,errtemp,gallong,gallat) *
c     _                       wmaplin(gl_4(iB),gb_4(iB),
c     _                       wmaplin(ascen,delta,
c     _                       temp,errtemp,gallong,gallat) 
                           nvals(ibin,2)= nvals(ibin,2) + 1

                           if(test5)then
c     -> in 2nd loop: if t_4(.) .gt. tol  then print lots of stuff:
c     iA,iB  gl_4 gb_4  pix(.,.) iA iB  t_4(  ), pix_B2 ( ... )
c     inv, generator or geninv
                              write(6,'(a,2i5,4f7.1,2g9.2)')
     _                             'cS3_lb: ',iA,iB,
     _                             gl_4(iA),gb_4(iA),
     _                             gl_4(iB),gb_4(iB),
     _                             t_4(iA),t_4(iB)
                              write(6,'(a,8f8.3)')
     _                             'cS3_p: ',
     _                             (pix(jj,iA),jj=1,4),
     _                             (pix(jj,iB),jj=1,4)
                              write(6,'(a,8f8.3)')
     _                             'cS3_B2: ',
     _                             (pix(jj,iA),jj=1,4),
     _                             (pix_B2(jj),jj=1,4)
                              if(inv.eq.1)then
                                 call pmat8v('gen:(001)',
     _                                generator(1,1,iv,jv))
                              else
                                 call pmat8v('gen:(inv):',
     _                                geninv(1,1,iv,jv))
                              endif
                           endif

                        endif
                     enddo      !   do inv=inv1,inv2

                     
                  enddo         !               do jv=0,jmax,ivdel
               endif            ! (may_ann(iv,1,iA).and.may_ann(iv,2,iB))...
            enddo               !            do iv=iv1,iv2,ivdel

         enddo                  !          do iB= iA+1,nPDS
      enddo                     !      do iA= 1,nPDS-1

C     divide by nvals to get av correlations
      nval_1=0
      nval_2=0
      do imodel=1,2
         co1040(imodel)=0.0  ! correlation from e.g. 10-50 degrees
         nv1040(imodel) = 0
         do ibin=1,nbins
c            if(nvals(ibin,imodel).gt.0)then
            if(nvals(ibin,imodel).gt.nvalsmin)then
c               if(test)print*,'corrS3: corr(.,.) before =',
c     _              corr(ibin,imodel)
               if(ibin1.le.ibin.and.ibin.le.ibin2)then
                  co1040(imodel)= co1040(imodel) + corr(ibin,imodel)
     _                 *convmiK*convmiK
                  nv1040(imodel)=  nv1040(imodel) +nvals(ibin,imodel)
               endif
               corr(ibin,imodel)= corr(ibin,imodel)/
     _              real(nvals(ibin,imodel))
     _              *convmiK*convmiK
c               if(test)print*,'corrS3: corr(.,.) after =',
c     _              corr(ibin,imodel)
               if(imodel.eq.1)then
                  nval_1=nval_1+nvals(ibin,imodel)
               else
                  nval_2=nval_2+nvals(ibin,imodel)
               endif
            else
               corr(ibin,imodel)= corrNULL
            endif
         enddo
         if(nv1040(imodel).gt.nvalsmin)then
            co1040(imodel)= co1040(imodel)/real(nv1040(imodel))
         else
            co1040(imodel)= corrNULL
         endif

      enddo
      if(iverb.eq.1)then
         print*,'corrS3: simply c nval_1=',nval_1
         print*,'corrS3: mult c nval_2=',nval_2
      endif

c     --- 0.1.69 --- proposal density for MCMC
      pp=1.0
      do ibin= ibin1,ibin2
         if(corr(ibin,1).gt.corrNULL .and.
     _        corr(ibin,2).gt.corrNULL .and.
     _        corr(ibin1,1).gt.tolcor .and.
     _        corr(ibin,1).gt.
     _        tolcor*corr(ibin1,1) .and.    ! TODO: avoid dependence on units!
     _        nvals(ibin,1).gt.nvalsmin .and.
     _        nvals(ibin2,1).gt.nvalsmin
     _        ) then
            if(test2)print*,'corrS3: corr(ibin,1),corr(ibin,2)=',
     _           corr(ibin,1),corr(ibin,2)
            if(test2)print*,'corrS3: nvals(ibin,1),nvals(ibin2,1)=',
     _           nvals(ibin,1),nvals(ibin2,1)
            if(corr(ibin,2).gt.corr(ibin,1))then  ! weak advantage if ratio > 1
               if(.not.(dozero))then
                  pp= pp * 
     _                 ( 1.0 + gtone * 
     _                 (corr(ibin,2)-corr(ibin,1))/corr(ibin,1) 
     _                 )
               else
                  pp= pp * 
     _                 ( 1.0 + gtone * 
     _                 (corr(ibin,2)-corr(ibin,1))/
     _                 (corr(ibin,1) - czero)
     _                 )
               endif
c     if(test2)print*,'corrS3: pp factor > 1'
            else  !  ratio of likelihoods

c     --- err2 should normally be < 1
               err2= real(nvals(ibin,1))/real(nvals(ibin2,1))  
c               cc= (corr(ibin,2)-corr(ibin,1))/corr(ibin,1)
c     --- difference
               d = corr(ibin,2)-corr(ibin,1)
c     --- sigma (standard deviation)
               if(.not.(dozero))then
                  s = corr(ibin,1) * 
     _                 fact_s   ! factor to force tight sigma's
               else
                  s = (corr(ibin,1) - czero)* 
     _                 fact_s   ! factor to force tight sigma's
               endif

c     -     if number of pairs is low, then give larger sigma (less strict)
               if(err2.lt.1.0 .and.err2.gt.tolcor*tolcor)then
                  s=s*
     _                 sqrt( 1.0/err2 ) 
               endif
               if(test2)print*,'corrS3: d,s=',d,s
c     pp=pp *exp( -0.5* cc*cc *err2 ) ! no need to normalise
               pp= pp * max(tolcor,exp( -0.5 * d*d/(s*s) )) ! no need to normalise
            
            endif
         else  ! if a bin is invalid, use tolcor
            pp= pp * tolcor
         endif
         if(test2)print*,'corrS3: ibin,pp=',ibin,pp
      enddo
      pmcnew= max(pp, pNULL+abs(pNULL))  ! (/me forgot: why not just pNULL ???)
      if(test2)print*,'corrS3: pmcnew=',pmcnew

c     --- 0.1.64 -- print results to stdout --
      if(iverb.eq.1)then
         jj=0
         do kk=1,2
            do ii=1,6
               write(6,'(a,2f9.1,i3,2f6.1,2f9.1)')'corr3d:= ',
     _           glongc(kk,ii,jj),glatc(kk,ii,jj),
     _              galcut,bcut,GCmin,co1040(1), co1040(2)
            enddo
         enddo
      endif


      if(test)then
         print*,'ang,corr,nvals='
         print*,(ang(i),i=1,nbins)
         print*,'corr:'
         print*,'simply c: ',(corr(i,1),i=1,nbins)
         print*,'mult c: ',(corr(i,2),i=1,nbins)
         print*,'nvals:'
         print*,(nvals(i,1),i=1,nbins)
         print*,(nvals(i,2),i=1,nbins)
      endif

      if(iverb.eq.1)then
         do ibin=1,nbins
            write(6,'(a,f6.2,2f9.2,2i8)')'corr3d:== ',
     _           ang(ibin),corr(ibin,1),corr(ibin,2),
     _           nvals(ibin,1),nvals(ibin,2)
         enddo
         print*,'corr3d: '
      endif
      
      if(.not.(noplot))then
c     --- plot
         x1= 0.0 
         if(x2plot.gt.0.0)then
            x2=x2plot
         else
            x2= ang(nbins)+0.5*dang_d
         endif
         y1= -400.0
         if(y2plot.gt.0.0)then
            y2=y2plot
         else
            y2= 2000.0
         endif
         
c     ---- convert to h^{-1}Gpc ----
         print*,'Assuming: rSLSc = ',rSLSc
         do ibin=1,nbins
            dhGpc(ibin)= ang(ibin) *(pi_180) ! convert to max = SLS diameter
     _           *(2.0/pi)      ! convert to SLS diameter = 2.0
     _           *rSLSc *0.001  ! convert to h^{-1}Gpc
         enddo
         x2 = x2 *(2.0/pi)*(pi_180) * rSLSc * 0.001

c     ---- in-radius of PDS ----
         rminus= 18.0       ! pi/10 in R^4 around S^3
     _        *ang_norm         ! see above for aa8 = angle4D8 etc.
     _        *(pi_180) * (2.0/pi)
     _        *rSLSc *0.001     ! convert to h^{-1}Gpc
         ib_minus= int( (pi/10.0) *ang_norm /dang ) +1
         print*,'rminus=',rminus

         if(do_annulus)ib_minus= ibin2+1 ! bigger bins badly biased by method


         write(ch5,'(f5.1)')alph_d
         do i=1,5
            if(ch5(i:i).eq.' ')ch5(i:i)='_'
         enddo
         if(ch5(2:2).eq.'_')ch5(2:2)='0'

         icount=icount+1
         write(ch3,'(i3)')icount
         do i=1,3
            if(ch3(i:i).eq.' ')ch3(i:i)='0'
         enddo

         call pgbegin(0,'circle_corrS3'//
     _        ch5//'_'//ch3//'.ps/vcps',1,1)
         call pgvsize(1.0,6.0,1.0,5.0)
         call pgslw(3)
         call pgscf(2)
         call pgsch(1.2)
         
         call pgwindow(x1,x2,y1,y2)
         call pgbox('bcnts1',0.,0,'bcnts',0.,0)

         call pglabel(
c     _        'dimensionless distance in "degrees"',
c     _        '<\\gdT\\d1\\u\\gdT\\d2\\u> (\\gmK\\u2\\d)',' ')
     _        'spatial separation in h\\u-1\\dGpc',
     _        '<\\gdT\\d1\\u\\gdT\\d2\\u> (\\gmK\\u2\\d)',' ')

c         call pgptxt(x1+0.7*(x2-x1), y1+0.85*(y2-y1),0.0,0.5,
c     _        'circle radius ='//ch5//' \\uo')

c     --- simply connected
c         call pgline(nbins,ang,corr(1,1))
         call pgline(nbins,dhGpc,corr(1,1))

c     --- multiply connected
         call pgsci(4)
         call pgqlw(islw)
         call pgslw(10)
c         call pgline(nbins,ang,corr(1,2))

         if(rmincut)then
            call pgline(ib_minus,dhGpc,corr(1,2))
         else
            call pgline(nbins,dhGpc,corr(1,2)) 
         endif
         call pgslw(islw)
         do i=1,nbins
            print*,'corrS3plot: ',i,dhGpc(i),corr(i,1),corr(i,2)
         enddo

c     --- 0.1.63: add in a thin line for zero correlation ---
         call pgsci(1)
         call pgslw(1)
         call pgmove(x1,0.0)
         call pgdraw(x2,0.0)

         call pgend

      endif                     ! if(.not.(noplot))then


      return
      end



c     ==== Print out a MATrix (real*8) in vector octave format ====
      subroutine pmat8v(string,xmat8)
      character*(*)  string
      real*8  xmat8(4,4)

      do j=1,4
         write(6,'(a,i1,$)')string,j
         print*,' = [ ',
     _        (xmat8(i,j),'; ',i=1,3),
     _        xmat8(4,j),']'
      enddo
      return
      end


c     ==== Print out a VECtor (real*8) in vector octave format ====
      subroutine pvec8v(string,xvec8)
      character*(*)  string
      real*8  xvec8(4)

      write(6,'(a,$)')string
      print*,' = [ ',
     _     (xvec8(i),'; ',i=1,3),
     _     xvec8(4),']'

      return
      end


C     ==== WARNING: must declare this real*8 in calling program
c     ---- WARNING: this is not a numerically stable algorithm
      real*8 function det3x3(a,d,g, b,e,h, c,f,i) ! first down columns
C     INPUTS:
      real*8 a,d,g, b,e,h, c,f,i ! can also give: matrix(3,3)
      
      det3x3 = a*e*i + b*f*g + c*d*h 
     _     - a*f*h - b*d*i - c*e*g
      return
      end
