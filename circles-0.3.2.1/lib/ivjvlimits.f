C     Copyright (C) 2004 B. Roukema et al. <shape-univ at astro.uni.torun.pl>
C     
C     This program is free software; you can redistribute it and/or modify
C     it under the terms of the GNU General Public License as published by
C     the Free Software Foundation; either version 2 of the License, or
C     (at your option) any later version.
C     
C     This program is distributed in the hope that it will be useful,
C     but WITHOUT ANY WARRANTY; without even the implied warranty of
C     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C     GNU General Public License for more details.
C     
C     You should have received a copy of the GNU General Public License
C     along with this program; if not, write to the Free Software
C     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
C  
C     See also http://www.gnu.org/licenses/gpl.html

C     calculate limits in iteration counters iv,jv for main circles loop

      subroutine ivjvlimits(
     _     doT2,dododec,Rfund,rSLS,
     _     do_ivdelshift,doonepair,
     _     imax,jmax,iv1,iv2)


C     INPUTS:
      logical*4  doT2           !T2
      logical*4  dododec        ! Poincare dodecahedral
      real*4   Rfund(3)         ! lengths of axes in h^-1 Mpc
      real*4     rSLS
      logical*4 do_ivdelshift ! for slightly different iv subset e.g. BASI
      logical*4 doonepair ! test one pair only 

C     OUTPUTS:
      integer*4  imax,jmax,iv1,iv2

C     LOCAL:
      logical*4  test

      data test /.false./
      
      if(doT2)then
         
c     21/06/99  ! Factor of 2.0*  added.
         imax= int(2.0*rSLS/Rfund(1))
         jmax= int(2.0*rSLS/Rfund(2))
         
         if(test)print*,'irun,imax,jmax=',irun,imax,jmax


         if(.not.(do_ivdelshift))then
            iv1=-imax
            iv2=imax
         else
            iv1=-imax+1
            iv2=imax
         endif
         
         if(doonepair)then
            jmax=0
            iv1=-1
            iv2=1
         endif
      elseif(dododec)then
         iv1= 1! 6 !1 
         iv2= 6 ! 6
         imax=6 ! probably superfluous (not used)
         jmax=0
      else
         stop 'other models not yet implemented'
      endif
      return
      end


      subroutine chkivjv(doT2,iv,jv, ivjv_valid)
c     INPUTS:
      logical*4    doT2
      integer*4    iv,jv
      
C     OUTPUTS:
      logical*4    ivjv_valid

      if(doT2)then
         ivjv_valid=.false.
         if(iv.gt.0.or.jv.gt.0)ivjv_valid=.true.
      else
         ivjv_valid=.true.
      endif

      return
      end



