C     Copyright (C) 2004 B. Roukema et al. <shape-univ at astro.uni.torun.pl>
C     
C     This program is free software; you can redistribute it and/or modify
C     it under the terms of the GNU General Public License as published by
C     the Free Software Foundation; either version 2 of the License, or
C     (at your option) any later version.
C     
C     This program is distributed in the hope that it will be useful,
C     but WITHOUT ANY WARRANTY; without even the implied warranty of
C     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C     GNU General Public License for more details.
C     
C     You should have received a copy of the GNU General Public License
C     along with this program; if not, write to the Free Software
C     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
C  
C     See also http://www.gnu.org/licenses/gpl.html

      real*4 function dotv(a,b)
      real*4 a(3),b(3)
      dotv=a(1)*b(1)+a(2)*b(2)+a(3)*b(3)
      return
      end

      subroutine crossv(a,b,c)
      real*4 a(3),b(3),c(3)
      c(1)= a(2)*b(3)-a(3)*b(2)
      c(2)= a(3)*b(1)-a(1)*b(3)
      c(3)= a(1)*b(2)-a(2)*b(1)
      return
      end


      subroutine orth_axes(glong3,glat3,Rfund3, thetadeg,
     _     x1,y1,z1,  x2,y2,z2,  xx,yy,zz, 
     _     gl1_3,gb1_3,gl2_3,gb2_3)
c     _     fun_eq(1,1),fun_eq(1,2),fun_eq(1,3),
c     _     fun_eq(2,1),fun_eq(2,2),fun_eq(2,3),
c     _     fun_eq(3,1),fun_eq(3,2),fun_eq(3,3)  )

c     ==== based on the input direction (glong3,glat3) and size (Rfund3), 
c     generates three orthogonal axes; the input direction has modulus
c     unity and the others modulus  Rfund3. Ouput is in B1950 (equat.) ====

      parameter(pi=3.1415926535898)
      parameter(pi_180=pi/180.0)
      parameter(tol=0.01)

      real*4     glong3,glat3,Rfund3   ! inputs

c      real*4     fun_eq(3,3) ! outputs
      real*4     x1,y1,z1, x2,y2,z2, xx,yy,zz ! outputs
      real*4     gl1_3,gb1_3,gl2_3,gb2_3 ! outputs

      logical*4  test

      test=.false. !.true. !

      call gal_b1950(glong3,glat3,ra3,dec3)

      xx= cos(dec3*pi_180) 
     _     *cos(ra3*pi_180*15.0)
      yy= cos(dec3*pi_180) 
     _     *sin(ra3*pi_180*15.0)
      zz= sin(dec3*pi_180)

c     ---- choose a "random" generating vector which is at least pi/4 from 
c     long axis ---
      if(abs(dec3).lt.45.0)then
         xgen=0.0
         ygen=0.0
         zgen=1.0
      else
         xgen=1.0
         ygen=0.0
         zgen=0.0
      endif
c     ---- generate a vector orthog to long axis and normalise it ----
      call cross(xx,yy,zz, xgen,ygen,zgen, x1,y1,z1)

c     -- check amplitude not small (to avoid numerical errors) --
      dd = dot(x1,y1,z1, x1,y1,z1) 
      if(test)print*,'orth_axes: x1 mod=',dd
      if(dd.lt.tol)then
         print*,'orth_axes ERROR: dd=',dd
         print*,'glong3,glat3,xx,yy,zz,xgen,ygen,zgen='
         print*,glong3,glat3,xx,yy,zz,xgen,ygen,zgen
      endif
      dd=sqrt(dd)
      x1=x1/dd
      y1=y1/dd
      z1=z1/dd
      
c     -- check that new vector is orthogonal to  long axis  vector --
      dd= dot(xx,yy,zz, x1,y1,z1)
      if(test)print*,'orth_axes: xx.x1 =',dd
      if(abs(dd).gt.tol)then
         print*,'orth_axes WARNING: dd=',dd
         print*,'glong3,glat3,xx,yy,zz,xgen,ygen,zgen='
         print*,glong3,glat3,xx,yy,zz,xgen,ygen,zgen
         print*,'x1,y1,z1=',x1,y1,z1
      endif

c     ---- generate another vector, orthonormal to long axis ----
      call cross(xx,yy,zz, x1,y1,z1, x2,y2,z2)

c     -- check that new vector is orthogonal to  long axis  vector --
      dd= dot(xx,yy,zz, x2,y2,z2)
      if(test)print*,'orth_axes: xx.x2 =',dd
      if(abs(dd).gt.tol)then
         print*,'orth_axes WARNING: dd=',dd
         print*,'glong3,glat3,xx,yy,zz,xgen,ygen,zgen='
         print*,glong3,glat3,xx,yy,zz,xgen,ygen,zgen
         print*,'x2,y2,z2=',x2,y2,z2
      endif
      
c     ---- final vectors ----
      cth= cos(thetadeg*pi_180)
      sth= sin(thetadeg*pi_180)

      x1n=(cth*x1+sth*x2)*Rfund3
      y1n=(cth*y1+sth*y2)*Rfund3
      z1n=(cth*z1+sth*z2)*Rfund3
      
      x2n=(-sth*x1+cth*x2)*Rfund3
      y2n=(-sth*y1+cth*y2)*Rfund3
      z2n=(-sth*z1+cth*z2)*Rfund3

      x1=x1n
      y1=y1n
      z1=z1n
      
      x2=x2n
      y2=y2n
      z2=z2n

      call xyz_rda(x1,y1,z1, rr,del,alph)
      call b1950_gal(alph,del,gl1_3,gb1_3)  ! added 25/08/99
c      gl1_3= alph*15.0
c      gb1_3= del
      call xyz_rda(x2,y2,z2, rr,del,alph)
      call b1950_gal(alph,del,gl2_3,gb2_3) ! added 25/08/99
c      gl2_3= alph*15.0
c      gb2_3= del

      if(test)print*,'orth_axes: thetadeg,gl1_3,gb1_3,gl2_3,gb2_3=',
     _     thetadeg,gl1_3,gb1_3,gl2_3,gb2_3
      return
      end




