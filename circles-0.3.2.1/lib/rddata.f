C     Copyright (C) 2004 B. Roukema et al. <shape-univ at astro.uni.torun.pl>
C     
C     This program is free software; you can redistribute it and/or modify
C     it under the terms of the GNU General Public License as published by
C     the Free Software Foundation; either version 2 of the License, or
C     (at your option) any later version.
C     
C     This program is distributed in the hope that it will be useful,
C     but WITHOUT ANY WARRANTY; without even the implied warranty of
C     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C     GNU General Public License for more details.
C     
C     You should have received a copy of the GNU General Public License
C     along with this program; if not, write to the Free Software
C     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
C     
C     See also http://www.gnu.org/licenses/gpl.html

      subroutine rddata(doCOBE,doWMAP,
     _     smooth,new,sigdeg,resdeg,
     _     dosmap,dos2map,dos2mean, 
     _     indir2,nindir,
     _     fileraw,nfraw,
     _     filesmooth,nfsmooth,
     _     toyf2,ntoyfile,
     _     filemask,nfmask,iwmask,
     _     binfo2,nbin_format,iwring,
     _     galcut,bcut,
     _     Ssigma,
     _     npix,temp,errtemp,gallong,gallat,tempcp,k_ring)

C     INPUTS:
c     ---- smoothing ? ----
      logical*4 doCOBE          ! read in COBE data?
      logical*4 doWMAP          ! read in WMAP data?
      logical*4 smooth          ! is either a new or old smoothed map required?
      logical*4 new             ! do a new smoothing?

c     if(smooth)then
c     new=.true  means a new smoothed map will be calculated
c     With version 2004-01-05.1 or earlier, this is slow and inefficient,
c     but correct, taking about an hour or so on a 1.7GHz machine.
c     
c     new=.false.  means a previously calculated smoothed map will be read 
c     
c     elseif(.not.(smooth))then
c     the original unsmoothed data will be read in and used
c     endif

      real*4    sigdeg,resdeg   ! maximum sigma, resolution for smoothing (deg)

      logical*4 dosmap,dos2map,dos2mean ! calculating S type statistics


      character*256  indir2    ! directory with input files
      integer        nindir    ! number of characters in  indir
      character*256  fileraw ! safe string version of fileraw
      character*256  filesmooth ! safe string version of filesmooth
      character*256  filemask ! safe string version of filemask
      character*256  binfo2 ! safe string version of bin_format
      character*256  toyf2  ! safe string version of toyfile
      integer        nfraw    ! number of characters in  fileraw
      integer        nfsmooth    ! number of characters in  filesmooth
      integer        nfmask    ! number of characters in  filemask
      integer        nbin_format ! number of characters in  bin_format
      integer        ntoyfile   ! number of characters in toyf2
      integer        iwring      ! is the input file ordering "ring" ordering?
      integer        iwmask       ! flag: want mask file?

c     inputs used for testing only:
      logical*4 galcut ! is the galactic plane removed?
      real*4    bcut            ! galactic latitude cut


c     outputs:
      real*4   ssigma


c     external:
      integer  pxstat  ! pxstat.c

c     COMMON:

c    --  circles-0.1.12 
c     -> now done in pxallo.c with declarations in circles.h
c
c      include 'npix.dec'
c      real*4   temp(maxn),errtemp(maxn) !   temperature and uncert.
c      real*4   gallong(maxn),gallat(maxn) ! galactic
      real*4   temp(*),errtemp(*) !   temperature and uncert.
      real*4   gallong(*),gallat(*) ! galactic

      real*4   tempcp(*) ! temporary copy of temp(*)
      integer*4  k_ring(*)  ! temporary index for ring ordering      

c     real*4   eclong(maxn),eclat(maxn) ! ecliptic

c     --- needed if cobe data is to be read -- commented to economise on memory
c      real*4   eqlong(maxn),eqlat(maxn) ! ra dec in degrees, degrees

c     --- commented out in circles-0.1.12 since it is deprecated ---
c      real*4   xg(maxn),yg(maxn),zg(maxn) ! galactic x,y,z for unit radius

c     parameter(nxint=180,nyint=90)
      include 'nxint.dec'
      integer*4 inear(nxint,nyint) ! nearest pixel number
      real*4    dinear(nxint,nyint)
      real*4    dxint,dyint
c     -- eq. cartesian coords of table --
      real*4    x(nxint,nyint),y(nxint,nyint),z(nxint,nyint) 

c      common /cobecomm/  temp,errtemp,gallong,gallat,eqlong,eqlat,
c     _     dsm,gb0,dxint,dyint,inear,dinear,x,y,z
c      common /wmapcomm/  temp,errtemp,gallong,gallat,
c     _     dsm,gb0,dxint,dyint,inear,dinear,x,y,z
      common /wmapcomm/  npixcp,
     _     dsm,gb0,dxint,dyint,inear,dinear,x,y,z
      

      logical*4 measur,noise    
c     real*4    ranerr(maxn)
      common /noisecomm/ measur,noise

c     local:
      integer    iwverb 
      logical*4  test

c     :end of declarations

      measur=.true.
      noise=.false.
      iwverb= 1 ! probably should better be passed from main() c program

      test= .true.

c     if(doCOBE)then
c     smooth=.true.    !.false. !
c     else
c     smooth=.false.
c     endif



c     ---- the original unsmoothed map is needed for error estimates ----
c     0.1.66 - this is not necessarily useful or used
c     if(.not.(smooth.and.(.not.new)))then

      print*,'doCOBE=',doCOBE
      if(doCOBE)then
         print*,'will call rdCOBE.'
         print*,'rdCOBE disabled because fortran is'//
     _        ' inefficient in memory management.'
         print*,'please change npix.dec, uncomment '//
     _        'the option in circlet2.f, and recompile.'
c     call rdCOBE
         print*,'have called rdcobe.'
c      elseif(doWMAP)then
      elseif(  doWMAP.and. ( (.not.(smooth)) .or.new )  )then
         i= pxstat()
         if(i.ne.0)then
            print*,'rdWMAP warning: pxstat returned ',i,
     _           ' - there is probably a memory allocation problem.'
            print*,'did you forget to call pxallo?'
         endif
         print*,'rddata: checked that memory has been allocated:',
     _        ' status ok.'
         print*,'rddata: npix, npixcp=',npix,npixcp

         print*,'will call rdWMAP; iwring=',iwring
         call rdWMAP(npix,indir2,nindir,
     _        fileraw,nfraw,iwring,
     _        filemask,nfmask,iwmask,
     _        toyf2,ntoyfile,
     _        temp,errtemp,gallong,gallat,tempcp,k_ring)
         print*,'Have called rdWMAP.'

         if(test)print*,'rddata: (temp(i),i=1,5)=',
     _        (temp(i),i=1,5)
      else
         print*,'WARNING: no unsmoothed cmb file read in.'
      endif

c     endif
      
      if(smooth)then
         if(doCOBE)then
            call 
     _           smoothCOBE(new,
     _           dosmap,dos2map,do2smean,angledeg)
            print*,'from file read in, angledeg=',angledeg

         elseif(doWMAP)then
            call smooWMAP(new,sigdeg,resdeg, indir2,nindir,
     _           filesmooth,nfsmooth,
     _           binfo2,nbin_format,
     _           temp,errtemp,gallong,gallat,tempcp)
            print*,'from file read in, sigdeg,resdeg=',
     _           sigdeg,resdeg

            npix=npixcp ! 0.1.66
         else
            stop 'if(smooth): no other options'
         endif
      endif
      

c     ---- statistics of COBE signal S and noise B ("B" for bruit)----
      Smean=0.0
      Ssigma=0.0
      Bmean=0.0
      Bsigma=0.0
      do i=1,npix
         if(.not.(galcut).or.abs(gallat(i)).gt.bcut)then
            Smean=Smean+temp(i)
            Bmean=Bmean+errtemp(i)
         endif
      enddo
      Smean=Smean/real(npix)
      Bmean=Bmean/real(npix)


      do i=1,npix
         if(.not.(galcut).or.abs(gallat(i)).gt.bcut)then
            ss=temp(i)- Smean
            Ssigma=Ssigma+ ss*ss
            ss= errtemp(i) - Bmean
            Bsigma=Bsigma+ ss*ss
         endif
      enddo
      Ssigma=sqrt(Ssigma/real(npix-1))
      Bsigma=sqrt(Bsigma/real(npix-1))

      print*,'signal: Smean,Ssigma=',Smean,Ssigma
      print*,'noise: Bmean,Bsigma=',Bmean,Bsigma
      print*,'Ssigma/Bmean=',Ssigma/Bmean

c     cutnoise= factcutnoise * Ssigma !Bmean
c     print*,'cutnoise= ',cutnoise

      return
      end


      
