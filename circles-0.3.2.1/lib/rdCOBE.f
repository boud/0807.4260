C   circles - calculate identified circles statistics from CMB data (cosmic topology)
C
C   Copyright (C) 2004 Boud Roukema
C
C   This program is free software; you can redistribute it and/or modify
C   it under the terms of the GNU General Public License as published by
C   the Free Software Foundation; either version 2, or (at your option)
C   any later version.
C
C   This program is distributed in the hope that it will be useful,
C   but WITHOUT ANY WARRANTY; without even the implied warranty of
C   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C   GNU General Public License for more details.
C
C   You should have received a copy of the GNU General Public License
C   along with this program; if not, write to the Free Software Foundation,
C   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
C


      subroutine rdCOBE  !(temp,errtemp,gallong,gallat)

      print*,'This is a dummy routine replacing the one ',
     _     'which used to read in COBE data'
      return
      end


C     WARNING: this was the routine for reading COBE data
C     It is now deactivated.

C     Some of the routine lower down in this file are used independently
C     of the COBE data; this is why this file is still distributed.
C     This package is GPL'ed - you are welcome to remove this old stuff
C     so that the new package still works correctly and has less legacy
C     stuff hanging around.

      subroutine rdC_old  !(temp,errtemp,gallong,gallat)

C     ==== to read the DMR ASDS (ie reduced COBE data) files ====
      parameter(pi=3.1415926535898)

c      parameter(npix=6144,maxn=8096) !! WARNING declared more than once
c      parameter(npix=6120,maxn=8096) !! WARNING declared more than once
      include 'npix.dec'

      integer*4  ipix(maxn),nobspix(maxn)
      real*4   temp(maxn),errtemp(maxn) !   Temperature and uncert.
      real*4   eclong(maxn),eclat(maxn) ! ecliptic
      real*4   gallong(maxn),gallat(maxn) ! galactic
      real*4   eqlong(maxn),eqlat(maxn) ! ra dec in degrees, degrees

c      real*4   sortcrit(2,maxn)  !! wrong order !!
c      real*4   sortcrit(maxn,2)

c      parameter(nxint=180,nyint=90)
      include 'nxint.dec'

      integer*4 inear(nxint,nyint)    ! nearest pixel number
      real*4    dinear(nxint,nyint)
      real*4    dxint,dyint
c     -- Eq. Cartesian coords of table --
      real*4    x(nxint,nyint),y(nxint,nyint),z(nxint,nyint) 

      logical*4 measur,noise      ! measured or random; (signal or) noise
      real*4    ranerr(maxn)

      common /noisecomm/ measur,noise

      common /cobecomm/  temp,errtemp,gallong,gallat,eqlong,eqlat,
     _     dsm,gb0,dxint,dyint,inear,dinear,x,y,z  ,ranerr


      character*256  cobefile

c      byte      by_array(40)
      integer*4 intarray(20)
      real*4    r__array(20)
      equivalence(intarray(1),r__array(1))

      integer*4  iswbyte
      external   iswbyte

      logical*4  test

      test=.false. !.true. ! 

      dsm= 3.5*pi/180.0

c     ==== read in file ====

      iunit= 1
      irwmode=0
c      cobefile='/agl22/boud/tape/COBE/dmr_dcmb_galactic_4yr.fts'
      cobefile='/scratch0/boud/COBE/dmr_dcmb_galactic.fits'
c      cobefile='/agl22/boud/tape/COBE/dmr_dsmb_galactic_4yr.fts'
c      cobefile='/home/rausan2NS/pb30/roukema/'//
c     _     'COBE/dmr_dcmb_galactic_4yr.fts'
c     _     'COBE/dmr_dsmb_galactic_4yr.fts'
c      cobefile='/tmp/dmr_dcmb_galactic_4yr.fts'

      print*,'Will read :'
      print*,cobefile
      call ftopen(iunit,cobefile,irwmode,iblocksize,istatus)
      print*,'istatus=',istatus

      nhdu=2
      ihdutype=2
      call ftmahd(iunit,nhdu,ihdutype,istatus)
      print*,'after ftmahd: istatus=',istatus

      do i=1,npix
c         print*,'i=',i
         call ftgtbb(iunit,i,1,40,intarray,istatus)
         if(istatus.ne.0)print*,'i,istatus=',i,istatus
         do k=1,10
            intarray(k)=iswbyte(intarray(k)) !intarray(k)  !
         enddo

         ipix(i)=intarray(1)
         temp(i)=r__array(2)
         nobspix(i)=intarray(3)
         errtemp(i)=r__array(4)

         eclong(i)=r__array(5)
         eclat(i)=r__array(6)
         gallong(i)=r__array(7)
         gallat(i)=r__array(8)
         eqlong(i)=r__array(9)
         eqlat(i)=r__array(10)

c         print*,'ipix(i),temp(i),nobspix(i),errtemp(i)='
c         print*,ipix(i),temp(i),nobspix(i),errtemp(i)
         ranerr(i)=gauss0(0.0,errtemp(i)) ! generate errors from errtemp
      enddo
      

      call ftclos(iunit,istatus)
      if(istatus.ne.0)print*,'i,istatus=',i,istatus

      print*,'COBE sample output: '
      print*,'(ipix(i),temp(i),errtemp(i),gallong(i),gallat(i),i=1,3)'
      do i=1,3
         print*,ipix(i),temp(i),errtemp(i),gallong(i),gallat(i)
      enddo

c     ==== initialise tables ====

c     ---- make gl-gb tables of closest pixel and dist to cl pixel ----
      dyint=180.0/real(nyint)  ! degrees /table unit
      dxint=360.0/real(nxint)  ! degrees /table unit
      print*,'dxint,dyint=',dxint,dyint

      gb0=-90.0
 
      if(test)print*,'rdCOBE: nxint,nyint=',nxint,nyint
      do j=1,nyint
         gb= gb0+ (real(j)-0.5)*dyint
         do i=1,nxint
            if(test.and.i.eq.152.and.j.eq.59)print*,
     _           'rdCOBE test 1 '
            inear(i,j)=-99
            dinear(i,j)=9e9
            gl= (real(i)-0.5)*dxint
            x(i,j)= cos( gb*pi/180.0 )*cos(gl*pi/180.0)
            y(i,j)= cos( gb*pi/180.0 )*sin(gl*pi/180.0)
            z(i,j)= sin( gb*pi/180.0 )
            if(test.and.i.eq.152.and.j.eq.59)print*,
     _           'rdCOBE test 2 '
         enddo
      enddo

      isq = 6                   ! i of the square over which to put values
      do ip=1,npix
         isqb= int(real(isq)/cos(gallat(ip)*pi/180.0))+1
         i0= int( gallong(ip)/dxint )+1
         i1=max(1,i0-isqb)
         i2=min(nxint,i0+isqb)
         j0= int( (gallat(ip)-gb0)/dyint )+1
         j1=max(1,j0-isq)
         j2=min(nyint,j0+isq)
         xx= cos( gallat(ip)*pi/180.0 )*cos(gallong(ip)*pi/180.0)
         yy= cos( gallat(ip)*pi/180.0 )*sin(gallong(ip)*pi/180.0)
         zz= sin( gallat(ip)*pi/180.0 )

         do j=j1,j2
            do i=i1,i2
               dd= angle(xx,yy,zz,x(i,j),y(i,j),z(i,j))
               if(dd.lt.dinear(i,j))then
                  dinear(i,j)=dd
                  inear(i,j)=ip
               endif
            enddo
         enddo
c     ---- continuity betweeen 0deg and 360 deg ----
         if(i0-isqb.lt.1.or.i0+isqb.gt.nxint)then
            i1=1
            i2=min(isqb,nxint)
            if(test)print*,'rdCOBE: i1,i2,j1,j2=',i1,i2,j1,j2
            do j=j1,j2
               do i=i1,i2
                  dd= angle(xx,yy,zz,x(i,j),y(i,j),z(i,j))
                  if(dd.lt.dinear(i,j))then
                     dinear(i,j)=dd
                     inear(i,j)=ip
                  endif
               enddo
            enddo
            i1=max(1,nxint-isqb)
            i2=nxint
            do j=j1,j2
               do i=i1,i2
                  dd= angle(xx,yy,zz,x(i,j),y(i,j),z(i,j))
                  if(dd.lt.dinear(i,j))then
                     dinear(i,j)=dd
                     inear(i,j)=ip
                  endif
               enddo
            enddo
         endif
      enddo

c     -- test --
      j= 45
      print*,'i,j,inear(i,j),gallong(inear(i,j)),gallat(inear(i,j))',
     _     'dinear(i,j)*180.0/pi'
      do i=1,-1
         print*,i,j,inear(i,j),gallong(inear(i,j)),gallat(inear(i,j)),
     _     dinear(i,j)*180.0/pi
c         print*,x(i,j),y(i,j),z(i,j)
      enddo

c     -- check --
      do j=1,nyint
         do i=1,nxint
            if(inear(i,j).lt.1)then
               print*,'WARNING: i,j, inear(i,j)='
               print*,i,j, inear(i,j)
            endif
         enddo
      enddo

      measur=.true.
      noise=.false.
      print*,'cobeDMR(10.0,0.0),cobeDMR(180.0,90.0)='
      xx= cobeDMR(10.0,0.0)
      yy=cobeDMR(180.0,90.0)
      print*,xx,yy

      return
      end


      real*4   function  cobeDMR(gall,galb)
c     ==== output may be either temperature variations or their 
c     uncertainties, depending on whether .not.(noise) or (noise) 
c     is set respectively ====
c     -- the  /noisecomm/  common block should be declared in the 
c     calling subroutine(s).

      parameter(pi=3.1415926535898)

c      parameter(npix=6144,maxn=8096)  !! WARNING declared several times
c      parameter(npix=6120,maxn=8096)  !! WARNING declared several times
      include 'npix.dec'

      real*4   temp(maxn),errtemp(maxn) !   Temperature and uncert.
c      real*4   eclong(maxn),eclat(maxn) ! ecliptic
      real*4   gallong(maxn),gallat(maxn) ! galactic
      real*4   eqlong(maxn),eqlat(maxn) ! ra dec in degrees, degrees

c      parameter(nxint=180,nyint=90)
      include 'nxint.dec'

      integer*4 inear(nxint,nyint)    ! nearest pixel number
      real*4    dinear(nxint,nyint)
      real*4    dxint,dyint
c     -- Eq. Cartesian coords of table --
      real*4    x(nxint,nyint),y(nxint,nyint),z(nxint,nyint) 

      logical*4 measur,noise    
      real*4    ranerr(maxn)
      common /noisecomm/ measur,noise

      common /cobecomm/  temp,errtemp,gallong,gallat,eqlong,eqlat,
     _     dsm,gb0,dxint,dyint,inear,dinear,x,y,z ,ranerr

c      print*,'BEGINNING of  cobeDMR(...)',
c     _     'measur,noise=',measur,noise
      xx= cos( galb*pi/180.0 )*cos(gall*pi/180.0)
      yy= cos( galb*pi/180.0 )*sin(gall*pi/180.0)
      zz= sin( galb*pi/180.0 )

c      isq= 3
c      isqb= 3 !int(real(isq)/cos(gall*pi/180.0))+1      
      isq= 0
      isqb= 0 !int(real(isq)/cos(gall*pi/180.0))+1      
      i0= int(gall/dxint)
      j0= int((galb-gb0)/dyint)
      ilo= min( max(i0-isq,1) , nxint)
      ihi= min( max(i0+isq,1) , nxint)
      jlo= min( max(j0-isqb,1), nyint)
      jhi= min( max(j0+isqb,1), nyint)
      
      tt=0.0
      ww=0.0
      do j=jlo,jhi
         do i=ilo,ihi
            dd=angle(x(i,j),y(i,j),z(i,j),xx,yy,zz) /(sqrt(2.0)*dsm)
            dd=dd*dd
            if(dd.lt.1e-2)then
c     --- case of falling very close to an actual COBE pixel ---
c               dd=1e-15
               if(measur)then
                  if(.not.(noise))then
                     cobeDMR=temp(inear(i,j))
                  else
                     cobeDMR=errtemp(inear(i,j))
                  endif
               elseif(noise)then
                  cobeDMR=ranerr(inear(i,j))
               else
                  print*,'measur,noise=',measur,noise,' not useful.'
               endif
               return           ! closest pixel requires no smoothing !
            endif

            if(measur)then
               if(.not.(noise))then
                  tt=tt+temp(inear(i,j))*exp(-dd)
               else
                  tt=tt+errtemp(inear(i,j))*exp(-dd)
               endif
            elseif(noise)then
               tt=tt+ranerr(inear(i,j))*exp(-dd)
            else
               print*,'measur,noise=',measur,noise,' not useful.'
            endif
            ww=ww+1.0*exp(-dd)
         enddo
      enddo
c     ---- continuity  0 deg = 360 deg --
      if(i0-isq.lt.1.or.i0+isq.gt.nxint)then
         tt=0.0
c         xx=0.0  ! was a bug !!! don't uncomment !
         ww=0.0
         ilo=1
         ihi=min(1+isq,nxint)
         do j=jlo,jhi
            do i=ilo,ihi
               dd=angle(x(i,j),y(i,j),z(i,j),xx,yy,zz)
     _              /(sqrt(2.0)*dsm)
               dd=dd*dd
c               if(dd.lt.1e-15)dd=1e-15
               if(dd.lt.1e-2)then
                  if(measur)then
                     if(.not.(noise))then
                        cobeDMR=temp(inear(i,j))
                     else
                        cobeDMR=errtemp(inear(i,j))
                     endif
                  elseif(noise)then
                     cobeDMR=ranerr(inear(i,j))
                  else
                     print*,'measur,noise=',measur,noise,
     _                    ' not useful.'
                  endif
                  return        ! closest pixel requires no smoothing !
               endif

               if(measur)then
                  if(.not.(noise))then
                     tt=tt+temp(inear(i,j))*exp(-dd)
                  else
                     tt=tt+errtemp(inear(i,j))*exp(-dd)
                  endif
               elseif(noise)then
                  tt=tt+ranerr(inear(i,j))*exp(-dd)
               else
                  print*,'measur,noise=',measur,noise,' not useful.'
               endif

               ww=ww+1.0*exp(-dd)
            enddo
         enddo
         ilo=max(nxint-isq,1)
         ihi=nxint
         do j=jlo,jhi
            do i=ilo,ihi
               dd=angle(x(i,j),y(i,j),z(i,j),xx,yy,zz)/sqrt(2.0)/dsm
               dd=dd*dd
               if(dd.lt.1e-15)dd=1e-15
               if(measur)then
                  if(.not.(noise))then
                     tt=tt+temp(inear(i,j))*exp(-dd)
                  else
                     tt=tt+errtemp(inear(i,j))*exp(-dd)
                  endif
               elseif(noise)then
                  tt=tt+ranerr(inear(i,j))*exp(-dd)
               else
                  print*,'measur,noise=',measur,noise,' not useful.'
               endif

               ww=ww+1.0*exp(-dd)
            enddo
         enddo
      endif

      cobeDMR=tt/ww

      return
      end

      integer*4  function iswbyte(i)
c     === swaps order of 4byte byte sequence ====
      integer*4  i
      integer*4  j,k
      byte       bylist1(4),bylist2(4)
      equivalence (bylist1,j),(bylist2,k)

      j=i
      bylist2(1)=bylist1(4)
      bylist2(2)=bylist1(3)
      bylist2(3)=bylist1(2)
      bylist2(4)=bylist1(1)
      
      iswbyte=k

      return
      end

      subroutine smoothCOBE(new,dosmap,dos2map,dos2mean,angledeg)
c     -- can either smooth the COBE map or replace it by an S-map --

      parameter(pi=3.1415926535898)

c     Input:  --------------
      logical*4 new             !recalculate or read old file ?
      logical*4 dosmap          !do S-map or not? 
      logical*4 dos2map         !calculate S^2-map or not?
      logical*4 dos2mean        ! if S^2, then mean ? (else sd)
c     Input (if(new))/ Ouput (if(.not.(new))):
      real*4    angledeg        !angle over which to smooth

c     Assumption:   --------------
c     rdCOBE is assumed to have already been executed

c     Common:  -------------- 
c     ! temp(.) is modified

c      parameter(npix=6144,maxn=8096)  !! WARNING declared several times
c      parameter(npix=6120,maxn=8096)  !! WARNING declared several times
      include 'npix.dec'

      real*4   temp(maxn),errtemp(maxn) !   Temperature and uncert.
c      real*4   eclong(maxn),eclat(maxn) ! ecliptic
      real*4   gallong(maxn),gallat(maxn) ! galactic
      real*4   eqlong(maxn),eqlat(maxn) ! ra dec in degrees, degrees

c      parameter(nxint=180,nyint=90)
      include 'nxint.dec'
      integer*4 inear(nxint,nyint)    ! nearest pixel number
      real*4    dinear(nxint,nyint)
      real*4    dxint,dyint
c     -- Eq. Cartesian coords of table --
      real*4    x(nxint,nyint),y(nxint,nyint),z(nxint,nyint) 

      logical*4 measur,noise    
      real*4    ranerr(maxn)
      common /noisecomm/ measur,noise

      common /cobecomm/  temp,errtemp,gallong,gallat,eqlong,eqlat,
     _     dsm,gb0,dxint,dyint,inear,dinear,x,y,z ,ranerr

c     Local: --------------
      real*4    tempnew(maxn),weight(maxn)
      real*4    errnew(maxn)    ! for uncertainties in temp
      real*4    smap(maxn)
      real*4    s2mean(maxn),s2sd(maxn),s2err(maxn)
      real*4    xg(maxn),yg(maxn),zg(maxn) ! galactic XYZ coords
      character*16  ch16
      

      if(new)then
         anglerad= angledeg*pi/180.0
         twoarad2= 2.0 *anglerad*anglerad
         smmax= 2.0*anglerad
         
         do ip=1,npix
            xg(ip)= cos(gallat(ip)/180.0*pi)
     _           *cos(gallong(ip)/180.0*pi)
            yg(ip)= cos(gallat(ip)/180.0*pi)
     _           *sin(gallong(ip)/180.0*pi)
            zg(ip)= sin(gallat(ip)/180.0*pi)
            tempnew(ip)=0.0
            errnew(ip)=0.0

            smap(ip)=0.0
            s2mean(ip)=0.0
            s2sd(ip)=0.0
            s2err(ip)=0.0

            weight(ip)=0.0
         enddo

         if((.not.(dosmap)).and.(.not.(dos2map)))then
            do ip=1,npix
               do iq=ip,npix
                  aa=angle(xg(ip),yg(ip),zg(ip),
     _                 xg(iq),yg(iq),zg(iq))
                  if(aa.lt.smmax)then
                     ww =exp(- aa*aa/twoarad2)
                     tempnew(ip)= tempnew(ip) + ww*temp(iq)
                     weight(ip)= weight(ip)+ww
                     tempnew(iq)= tempnew(iq) + ww*temp(ip)
                     weight(iq)= weight(iq)+ww
                     
                     errnew(ip)= errnew(ip)
     _                    + ww*ww* errtemp(ip)*errtemp(ip)
                     errnew(iq)= errnew(iq)
     _                    + ww*ww* errtemp(iq)*errtemp(iq)
                  endif
               enddo
            enddo
            do ip=1,npix
               if(weight(ip).gt.1e-6)then
                  temp(ip)= tempnew(ip)/weight(ip)
c     !                       tempnew(ip) retains unnormalised value 
                  errtemp(ip)= 
     _                 abs(temp(ip)*sqrt(errnew(ip))/tempnew(ip))
               endif
            enddo

         elseif(dos2map)then
            bcut=20.0 
            nth=100.0
            dth= 2.0*pi/real(nth)
            measur=.true.
            noise=.false.

            do ip=1,npix
c     -- random unit vector in g plane --
               if(abs(gallat(ip)).lt.90.0-bcut*1.5)then
                  xx=0.0
                  yy=1.0
                  zz=0.0
c     -- cross product twice -> two unit vectors perp. to original --
c     unit vectors are  (ax,ay,az) and (bx,by,bz) --
                  call cross(xg(ip),yg(ip),zg(ip),xx,yy,zz,
     _                 ax,ay,az)
                  call cross(xg(ip),yg(ip),zg(ip),ax,ay,az,
     _                 bx,by,bz)

                  if(ip.lt.20)then
                     print*,'ip,ax,ay,az,bx,by,bz='
                     print*,ip,ax,ay,az,bx,by,bz
                  endif

c     --- mean around circle perp. to (ip) ---
                  ivalid=0
                  do ith=1,nth
                     theta= dth*real(ith)
                     cth= cos(theta)
                     sth= sin(theta)
                     tx= cth*ax + sth*bx
                     ty= cth*ay + sth*by
                     tz= cth*az + sth*bz
                     call xyz_rda(tx,ty,tz,rr,galb,alph)
                     gall= alph*15.0
                     if(abs(galb).gt.bcut)then
                        ivalid=ivalid+1
c     noise=.false.
                        tt = cobeDMR(gall,galb)
                        s2mean(ip)=s2mean(ip) +tt
                     endif
                  enddo
                  if(ivalid.ge.1)then
                     s2mean(ip)=s2mean(ip)/real(ivalid)
                  else
                     s2mean(ip)=-99.9
                  endif
c     --- dispersion around circle perp. to (ip) ---
                  if(s2mean(ip).gt.-99.0)then
                     ivalid=0
                     do ith=1,nth
                        theta= dth*real(ith)
                        cth= cos(theta)
                        sth= sin(theta)
                        tx= cth*ax + sth*bx
                        ty= cth*ay + sth*by
                        tz= cth*az + sth*bz
                        call xyz_rda(tx,ty,tz,rr,galb,alph)
                        gall= alph*15.0
                        if(abs(galb).gt.bcut)then
                           ivalid=ivalid+1
c     noise=.false.
                           tt = cobeDMR(gall,galb)
                           dtt = tt- s2mean(ip)
                           s2sd(ip)=s2sd(ip) +tt*tt
                        endif
                     enddo
                     if(ivalid.ge.2)then
                        s2sd(ip)=sqrt( s2sd(ip)/real(ivalid-1) )
                     else
                        s2mean(ip)=-99.9
                     endif
                  else
                     s2sd(ip)=-0.099
                  endif
               else
                  s2mean(ip)= -0.099
               endif
            enddo               ! ip =1,npix
            do ip=1,npix
               if(dos2mean)then
                  temp(ip)=s2mean(ip)
               else
                  temp(ip)=s2sd(ip)
               endif
            enddo


         elseif(dosmap)then                   !S-map
            measur=.true.
            bcut=20.0 
            do ip=1,npix
c               print*,'ip=',ip  !! 
               t1= temp(ip)
               s1= max(errtemp(ip), 1e-4)
               s1sq= s1*s1
               s1sq2inv= 1.0/( s1*s1*2.0 )
c               print*,'smap(ip),t1,s1=',smap(ip),t1,s1
               niq=0
               do iq=1,npix

                  if(abs(gallat(iq)).gt.bcut)then  !   16/06/99

                     dd = dot(xg(ip),yg(ip),zg(ip),
     _                    xg(iq),yg(iq),zg(iq))
c     ---- version pour T^1 ----
c     xx= xg(iq) - 2.0 *dd *xg(ip)
c     yy= yg(iq) - 2.0 *dd *yg(ip)
c     zz= zg(iq) - 2.0 *dd *zg(ip)
c     ---- version (non-optimale mais rapide) pour T^2 ----  16/06/99
                     xx= -xg(iq) + 2.0 *dd *xg(iq)
                     yy= -yg(iq) + 2.0 *dd *yg(iq)
                     zz= -zg(iq) + 2.0 *dd *zg(iq)

                     call xyz_rda(xx,yy,zz,rr,galb,alph)

                     if(abs(galb).gt.bcut)then !      16/06/99
                        gall=alph * 15.0
c     galb=del
                        noise=.false.

                        t2= cobeDMR(gall,galb)
c     noise=.true.

c     s2= cobeDMR(gall,galb)
c     s2sq= s2*s2


                        dt= t2-t1
                        niq=niq+1

c     dsmap = dt*dt/(s1*s1+s2*s2)
c     if(dsmap.gt.6e5)then
c     if(dsmap.lt.0.1.or.dsmap.gt.6e5)then
c     print*,'ip,iq,t1,t2,dt,s1,s2,smap(ip),dsmap=',
c     _                    ip,iq,t1,t2,dt,s1,s2,smap(ip),dsmap
c     print*,'gall,galb=',gall,galb
c     endif

c     smap(ip)=smap(ip) + dsmap

c     smap(ip)=smap(ip) + dt*dt/(s1sq +s2sq)
c     smap(ip)=smap(ip) + dt*dt* s1sq2inv ! shortcut approximation
                        smap(ip)=smap(ip) + dt*dt
                     endif      !bcut
                  endif         !bcut

               enddo
               smap(ip)=smap(ip)* s1sq2inv ! shortcut approximation !16/06/99
c               print*,'AFTER smap(ip),t1,s1=',smap(ip),t1,s1

            enddo

            print*,'SECOND loop ' !
            do ip=1,npix
c               temp(ip)=smap(ip)/real(npix) ! overwrite temp(ip)
               temp(ip)=smap(ip)/real(niq) ! overwrite temp(ip) !16/06/99
            enddo
            print*,'AFTER second loop ' !
         endif

         if(.not.(dos2map))then
            if(.not.(dosmap))then
               open(1,file='smooth.dat',access='sequential',
     _              form='unformatted',status='unknown')
               write(1)'smoothed CMB map'
               write(1)angledeg
               write(1)temp,errtemp,weight
               close(1)
            else
               open(1,file='smap.dat',access='sequential',
     _              form='unformatted',status='unknown')
               write(1)'CMB S-map       '
               write(1)angledeg
               write(1)temp,errtemp,weight
               close(1)
            endif
         endif
      else
         if(.not.(dosmap))then
            open(1,file='smooth.dat',access='sequential',
     _           form='unformatted',status='unknown')
            read(1)ch16
            if(ch16.ne.'smoothed CMB map')
     _           print*,'WARNING: strange file!'
            read(1)angledeg
            read(1)temp,errtemp,weight
            close(1)
         else
            open(1,file='smap.dat',access='sequential',
     _           form='unformatted',status='unknown')
            read(1)ch16
            if(ch16.ne.'CMB S-map')
     _           print*,'WARNING: strange file!'
            read(1)angledeg
            read(1)temp,errtemp,weight
            close(1)
         endif
      endif

c      write(6,*)'weight(1..100)=',(weight(i),i=1,100)

      return
      end

      

      real*4 function dot(x1,y1,z1,x2,y2,z2)
      real*4 x1,y1,z1,x2,y2,z2
      dot=x1*x2+y1*y2+z1*z2
      return
      end

      subroutine cross(ax,ay,az,bx,by,bz,cx,cy,cz)
      real*4 ax,ay,az,bx,by,bz,cx,cy,cz
      cx= ay*bz-az*by
      cy= az*bx-ax*bz
      cz= ax*by-ay*bx
      return
      end


      subroutine wrtascCOBE
c     ==== write out COBE data in ASCII format ====
c      parameter(npix=6144,maxn=8096)  !! WARNING declared several times
c      parameter(npix=6120,maxn=8096)  !! WARNING declared several times
      include 'npix.dec'

      real*4   temp(maxn),errtemp(maxn) !   Temperature and uncert.
c      real*4   eclong(maxn),eclat(maxn) ! ecliptic
      real*4   gallong(maxn),gallat(maxn) ! galactic
      real*4   eqlong(maxn),eqlat(maxn) ! ra dec in degrees, degrees

c      parameter(nxint=180,nyint=90)
      include 'nxint.dec'

      integer*4 inear(nxint,nyint)    ! nearest pixel number
      real*4    dinear(nxint,nyint)
      real*4    dxint,dyint
c     -- Eq. Cartesian coords of table --
      real*4    x(nxint,nyint),y(nxint,nyint),z(nxint,nyint) 

      logical*4 measur,noise    
      real*4    ranerr(maxn)
      common /noisecomm/ measur,noise

      common /cobecomm/  temp,errtemp,gallong,gallat,eqlong,eqlat,
     _     dsm,gb0,dxint,dyint,inear,dinear,x,y,z ,ranerr
      

      open(1,file='ascCOBE.dat',access='sequential',
     _     form='formatted',status='unknown')
      write(1,'(a)')'source: http://www.gsfc.nasa.gov/astro/'//
     _     'cobe/cobe_home.html'
      write(1,'(a)')' '
      write(1,'(2a12,2a12)')'gal_long',
     _     'gal_lat','dT/T','\\Delta(dT/T)'
      do i=1,npix
         write(1,'(2f12.1,2f12.4)')gallong(i),gallat(i),temp(i),
     _        errtemp(i)
      enddo
      close(1)

      return
      end
