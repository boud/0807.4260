C     Copyright (C) 2004 B. Roukema et al. <shape-univ at astro.uni.torun.pl>
C     
C     This program is free software; you can redistribute it and/or modify
C     it under the terms of the GNU General Public License as published by
C     the Free Software Foundation; either version 2 of the License, or
C     (at your option) any later version.
C     
C     This program is distributed in the hope that it will be useful,
C     but WITHOUT ANY WARRANTY; without even the implied warranty of
C     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C     GNU General Public License for more details.
C     
C     You should have received a copy of the GNU General Public License
C     along with this program; if not, write to the Free Software
C     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
C     
C     See also http://www.gnu.org/licenses/gpl.html

C     generate a specific hypothesis (3-manifold) to be tested
c     - may be a control test
c     - depends on many options

      subroutine hypoth(doRE97, omm,H_0,cc,w_q, 
     _     glong3,glat3,Rfund3,
     _     irun,nrun,
     _     dopolerot,
     _     donudge,donud3,thnud,
     _     th_d0,
     _     dovaryISW,xISW1,xISW2,
     _     ISWnoise,Ssigma,
     _     fun_eq,
     _     gl1_3,gb1_3,gl2_3,gb2_3, 
     _     xISW,
     _     bISW2)

C     INPUTS:
      logical*4 doRE97          ! .true. if doing RE97 hypothesis
      real*4    omm,H_0,cc,w_q ! local cosmological parameters
      real*4    glong3,glat3    ! long axis posn
      real*4    Rfund3          ! long axis length
      real*4    th_d0           ! zero-point for free angle around long axis
      integer*4 irun,nrun       ! which run, total number of runs
      logical*4 dopolerot ! .true. if rotating about the long axis
      logical*4 donudge         ! .true. if nudging around a position by few deg.
      logical*4 donud3          ! .true. if 2nd nudge param is th_d0
      real*4    thnud
      logical*4 dovaryISW       ! .true. if varying  xISW
      real*4    xISW1,xISW2
      logical*4 ISWnoise        ! .true. if adding noise at ISW scale
      real*4    Ssigma

C     OUTPUTS:
      real*4   fun_eq(3,3)      ! fundamental axes in B1950.0 equatorial coords
      real*4   gl1_3,gb1_3,gl2_3,gb2_3 ! ang posns of two short axes
      real*4   xISW
      real*4   bISW2

C     EXTERNAL:
      real*8   dofz             ! in cosmdist-0.2.1 packge
      external dofz

C     LOCAL:
c     ---- cluster positions ----
      parameter(maxclus=11)
      real*4    ra(maxclus),dec(maxclus),redsh(maxclus)
      real*4    xclus(maxclus),yclus(maxclus),zclus(maxclus)
      character*20 name(maxclus)

c      real*8    red_d           ! local double precision redshift

      parameter(pi=3.1415926535898)
      parameter(pi_180=pi/180.0)

      data      name/'A2163','RX J1347.5','Coma','CL 09104+4109',
     _     'CL 1821+643', 'MS1054-0321',
     _     'RX J1716.6+6708',   !1997AJ....114.1293H  Henry et al 1997
     _     'MS 1137.5+6625',    ! GL1994; LG1995 (L=Luppino)
c     Castander, RSEllis, Frenk et al 1994
     _     'GHO 1603+4313','GHO 1322+3027','Cl1322+3029'  /
c     ! Warning Fabian & Crawford's "J2000" on their (1995) figure is bullshit !
c     data      ra/16.2625, 13.7917, 12.95, 9.175, 18.35, 10.9,
c     data      ra/16.2625, 13.79183, 12.9969, 9.17583, 18.35, 10.9,  !J2000 ?
      data      ra/16.2625, 13.79183, 12.9969, 9.2289, 18.35, 10.9, !J2000 ?
     _     17.2767,  11.625, 
     _     16.05, 13.3667, 13.3667 /
c     data      dec/-6.15, -11.75, 28.22, 41.15, 64.3, -3.35,
c     data      dec/-6.15, -11.7533, 27.9807, 41.1478, 64.3, -3.35,  !J2000 ?
      data      dec/-6.15, -11.7533, 27.9807, 40.9428, 64.3, -3.35, !J2000 ?
     _     67.1333, 66.4167,
     _     43.2167, 30.45, 30.4833 /
      data      redsh/0.201, 0.451, 0.0239, 0.442, 0.297, 0.829,
     _     0.813, 0.782,
     _     0.895, 0.757, 0.697/
      


      if(doRE97)then
c     ---- calculate fund axes in equatorial coords for RE97 candidate ----
         do i=1,maxclus
c     rr= pdWein(q(icc), H_0, cc(icc), redsh(i))
c            rr= DEpdWeiq(omm, H_0, cc,w_q, redsh(i))
            rr= real( dofz(dble(H_0), dble(omm), dble(cc), dble(w_q), 
     _           dble(redsh(i))) )
c     redsh(i)= red_d

            xclus(i)= rr *cos(dec(i)*pi/180.0) 
     _           *cos(ra(i)*pi/12.0)
            yclus(i)= rr *cos(dec(i)*pi/180.0) 
     _           *sin(ra(i)*pi/12.0)
            zclus(i)= rr *sin(dec(i)*pi/180.0)
         enddo
         jcol=1
         fun_eq(jcol,1)= xclus(4)-xclus(3)
         fun_eq(jcol,2)= yclus(4)-yclus(3)
         fun_eq(jcol,3)= zclus(4)-zclus(3)
         jcol=2
         fun_eq(jcol,1)= xclus(2)-xclus(3)
         fun_eq(jcol,2)= yclus(2)-yclus(3)
         fun_eq(jcol,3)= zclus(2)-zclus(3)

         jcol=3
         call cross(fun_eq(1,1),fun_eq(1,2),fun_eq(1,3),
     _        fun_eq(2,1),fun_eq(2,2),fun_eq(2,3),
     _        fun_eq(3,1),fun_eq(3,2),fun_eq(3,3))

         cmod=sqrt(dot(fun_eq(3,1),fun_eq(3,2),fun_eq(3,3),
     _        fun_eq(3,1),fun_eq(3,2),fun_eq(3,3)))

         print*,'icc,omm,H_0,cc,cmod=',
     _        icc,omm,H_0,cc,cmod
         print*,'irun=',irun

         do iax=1,3
            fun_eq(jcol,iax)=fun_eq(jcol,iax)/cmod
         enddo

c     --- following added 01/01/00:
         jcol=3
         call xyz_rda(fun_eq(jcol,1),fun_eq(jcol,2),
     _        fun_eq(jcol,3),
     _        rr,del,alph)
         call b1950_gal(alph,del, gg,bb)
         print*,'RE97 test: gg,bb=',gg,bb

         jcol=1
         call xyz_rda(fun_eq(jcol,1),fun_eq(jcol,2),
     _        fun_eq(jcol,3),
     _        rr,del,alph)
         call b1950_gal(alph,del, gl1_3, gb1_3)

         jcol=2
         call xyz_rda(fun_eq(jcol,1),fun_eq(jcol,2),
     _        fun_eq(jcol,3),
     _        rr,del,alph)
         call b1950_gal(alph,del, gl2_3, gb2_3)
         print*,'RE97 test: gl1_3,gb1_3,gl2_3,gb2_3=',
     _        gl1_3,gb1_3,gl2_3,gb2_3

      else
c     ---- calculate fund axes in equatorial coords for alternative
c     candidate ----
c     --    long axis --
c     glong3=75.0      !! test only !! RE97
c     glat3=18.0       !! test only !! RE97
c     Rfund3=1000.0    !! test only !! RE97
         gl3=glong3
         gb3=glat3
         if(dopolerot)then
            thetadeg= th_d0 + real(irun-1)/real(nrun) * 360.0
         elseif(donudge)then
            thetadeg=th_d0
            nnud= int(sqrt(real(nrun)*1.01)) ! nud=nudge
            jnud= irun/nnud - (nnud)/2
            inud= mod(irun,nnud) - (nnud)/2

            if(.not.(donud3))then
               gl3=mod(gl3+ inud*thnud/cos(gl3*pi_180),
     _              360.0)
               gb3= gb3 + jnud*thnud
            elseif(donud3)then  ! 16.04.2003
               gb3= gb3 + jnud*thnud
               thetadeg= th_d0 + inud*thnud
            endif
         elseif(dovaryISW)then
            thetadeg=th_d0
            xISW= xISW1+ (real(irun-0.5)/real(nrun))
     _           *(xISW2-xISW1)
         else
c     thetadeg=0.0
            thetadeg=th_d0
         endif

c     ---- ISW noise - added 20/08/99 ----
c     -- moved to just before  orth_axes  24/08/99 --
         if(ISWnoise)then
            bISW= max(1e-5, xISW * Ssigma)
            bISW2= bISW * bISW
            print*,' '
            print*,'bISW,bISW2=',bISW,bISW2
            print*,' '
         endif
c     -----


         call orth_axes(gl3,gb3,Rfund3, thetadeg,
     _        fun_eq(1,1),fun_eq(1,2),fun_eq(1,3),
     _        fun_eq(2,1),fun_eq(2,2),fun_eq(2,3),
     _        fun_eq(3,1),fun_eq(3,2),fun_eq(3,3),
     _        gl1_3,gb1_3,gl2_3,gb2_3)
         
      endif


      return
      end


c     -- further modifications of the hypothesis depending on
c     logical options, and conversion to equatorial coordinates --

      subroutine hypot2(
     _     fun_eq,irun,nrun,Rfund3_1,dRfund3,rSLS,
     _     rotate,doRE97,doRfund,dosmrotate,
     _     efund,Rfund,glong,galb)


C     INPUTS:
      real*4   fun_eq(3,3)      ! fundamental axes in B1950.0 equatorial coords
      integer*4 irun,nrun            ! run number, total runs
      real*4    Rfund3_1,dRfund3 ! first fund axis length, increment
      real*4    rSLS            ! radius of Surface of Last Scattering
      logical*4 rotate
      logical*4 doRE97 ! .true. if doing RE97
      logical*4 doRfund ! .true. if modifying Rfund3 scale
      logical*4 dosmrotate ! .true. for smooth gal long rotation ! 17.04.2003

C     OUTPUTS:
      real*4   efund(3,3) ! galactic coords
      real*4   Rfund(3)         ! lengths of axes in h^-1 Mpc
      real*4   glong,galb       ! long and lat finally used

C     LOCAL:
      logical*4 test
      parameter(pi=3.1415926535898)

      data test /.false./

c     ---- convert fund axes to  B1950 xyz, 
c     then to galactic xyz  unit length vectors ----

      do jcol=1,3
         if(test)print*,'jcol=',jcol
         call xyz_rda(fun_eq(jcol,1),fun_eq(jcol,2),
     _        fun_eq(jcol,3),
     _        rr,del,alph)

         Rfund(jcol)= rr
         call b1950_gal(alph,del,glong,galb)
         
c     !! test only !! -> become regular feature for cmbT2.tex
         if(rotate)then
            if(doRE97.or.(dosmrotate))then ! 17.03.2003
c     if(doRE97)then  
c     if(doRE97.or.(.not.(doRfund)))then ! 03.04.2003
c     phi=real(irun) * 360.0/real(nrun)
               phi=real(irun-1) * 360.0/real(nrun-1) ! modif 01/01/00
               glong=(glong + phi)
            elseif(doRfund)then
               Rfund(jcol) = Rfund3_1 +
c     _                    (real(irun)-1.5)*dRfund3
     _              (real(irun)-1.0)*dRfund3 !! modified BFR 13/12/99
            endif
         endif
         
         efund(jcol,1)= cos(galb*pi/180.0) 
     _        * cos(glong*pi/180.0)
         efund(jcol,2)= cos(galb*pi/180.0) 
     _        * sin(glong*pi/180.0)
         efund(jcol,3)= sin(galb*pi/180.0) 
         if(test)print*,'axis jcol: ',jcol,
     _        ' glong,galb=',glong,galb
      enddo

            
c     ---- set third axis to 3.0*rSLS ----
      jcol=3
      Rfund(jcol)= 3.0*rSLS
      if(test)print*,'(Rfund(jcol),jcol=1,3)=',
     _     (Rfund(jcol),jcol=1,3)
      
      
      return
      end

