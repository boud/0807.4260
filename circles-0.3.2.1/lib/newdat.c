/* 
   circles - calculate identified circles statistics from CMB data (cosmic topology)

   Copyright (C) 2005 Paulina Oster, Cezary Migaszewski, Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

*/

#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include "system.h"


#include <stdlib.h>


#define EXIT_FAILURE 1

#if ENABLE_NLS
# include <libintl.h>
# define _(Text) gettext (Text)
#else
# define textdomain(Domain)
# define _(Text) Text
#endif
#define N_(Text) Text


#include "circles.h" 

void NEWDAT(
	    int  *npix,
	    float *temp, float *errtemp
	   )
{
  int   ipix;
  
  printf("newdat: *npix = %d",*npix);
  for (ipix=0; ipix < *npix; ipix++)
    {
      temp[ipix] = -1.0 *  temp[ipix];
    };
};



