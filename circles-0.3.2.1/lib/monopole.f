C     Copyright (C) 2004 B. Roukema et al. <shape-univ at astro.uni.torun.pl>
C     
C     This program is free software; you can redistribute it and/or modify
C     it under the terms of the GNU General Public License as published by
C     the Free Software Foundation; either version 2 of the License, or
C     (at your option) any later version.
C     
C     This program is distributed in the hope that it will be useful,
C     but WITHOUT ANY WARRANTY; without even the implied warranty of
C     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C     GNU General Public License for more details.
C     
C     You should have received a copy of the GNU General Public License
C     along with this program; if not, write to the Free Software
C     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
C  
C     See also http://www.gnu.org/licenses/gpl.html

      subroutine monopole(cmb,doCOBE,doWMAP,
     _     ttm1,ttv1,ttsd1,
     _     temp,errtemp,gallong,gallat)
c     --- calculate monopole of CMB fluctuations, either from data (e.g.
c     WMAP or COBE) or from simulations if someone gets them to work ---

c     INPUTS:
      logical*4  cmb,doCOBE,doWMAP

C     OUTPUTS:
      real*4     ttm1,ttv1,ttsd1

c     EXTERNAL fns:
      real*4    delTgal,cobeDMR,wmaplin
      external  delTgal,cobeDMR,wmaplin

c     INTERNAL:
      logical*4 test

      data test /.false./ !.true.

      ttm1=0.0
      ttv1=0.0
      k=0
      do ith=-44,44
         th= real(ith)*2.0
         cth= cos(th)
         do iphi=1,nint(180.0*cth)
            phi= real(iphi)*2.0/cth
            if(cmb)then
               if(doCOBE)then
                  tt=cobeDMR(phi,th)
               elseif(doWMAP)then
                  tt=wmaplin(phi,th,
     _                 temp,errtemp,gallong,gallat)
               endif
            else
               tt=delTgal(phi,th)
            endif
            ttm1=ttm1+tt
            ttv1=ttv1+tt*tt
            k=k+1
         enddo
      enddo
      ttm1=ttm1/real(k)
      ttv1=ttv1/real(k-1) -ttm1*ttm1
      ttsd1= sqrt(ttv1)
      if(test)print*,'ttm1,ttv1,ttsd1=',ttm1,ttv1,ttsd1
      
      return
      end

