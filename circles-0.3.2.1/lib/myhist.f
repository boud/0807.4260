C     WARNING: better replace this with plotutils - which is GPL,
C     since pgplot is *not* GPL-compatible

c      program test0
      subroutine test0
      call pgbegin(0, '?', 1, 1)
      call pgscf(2)
      call pgvsize(0.75, 7.25, 1.0, 6.25)
      call pgwindow(0.0,1.0,0.0,1.0)
      call pgbox('bcnts', 0., 0, 'bcntsv', 0., 0)
      
      aold=1.0
      bold=0.5
      do while(1+1.eq.2) 
         print*,'Enter a, b: '
         read(5,*,end=55)a,b
         call pgsci(0)
         call myhist(0.5, 0.2, 1.0,0.0, aold,bold)
         call pgsci(1)
         call myhist(0.5, 0.2,  1.0,0.0, a,b)
         aold=a
         bold=b
      enddo
      
 55   continue
      call pgend
      end



      subroutine myhist(xx1,xx2,yy1,yy2,aa,ddelb)
c     ==== plots shaded rectangle bounded by (xx1,xx2,yy1,yy2) with
c     lines at slope  aa  and  y-increment  ddelb  ====

c     In general, this uses  y = a x + b.

      real*4   xx1,xx2,yy1,yy2, aa, ddelb
      real*4   x1,x2,y1,y2, a, delb,  b

      x1=xx1
      x2=xx2
      y1=yy1
      y2=yy2
      a=aa
      delb=abs(ddelb)

      call pgmove(x1,y1) 
      call pgdraw(x2,y1)
      call pgdraw(x2,y2)
      call pgdraw(x1,y2)
      call pgdraw(x1,y1)

      if(x1.gt.x2)then
         xtemp=x1
         x1=x2
         x2=xtemp
      endif
      if(y1.gt.y2)then
         ytemp=y1
         y1=y2
         y2=ytemp
      endif

c     ---- if delb=0, histogram would never finish! in this case no fill is drawn
      if(delb.eq.0.0)then
         print*,'myhist warning: delb=0, therefore no fill is drawn'
      elseif(a.eq.0.0)then
c     ---- if a=0, case is simple ----
         b=y1
         do while(b.le.y2)
            call pgmove(x1,b)
            call pgdraw(x2,b)
            b=b+delb
         enddo
      else

c     ---- if a is less than zero, simply transform x coordinates
c     with a reflection about  x=(x1+x2)/2,  i.e., x-> (x1+x2)-x when plotting --

         if(a.lt.0.0)then
            tr0=x1+x2
            tr1=-1.0
         else
            tr0=0.0
            tr1=1.0
         endif
         a=abs(a)

c     ---- main fill routine for nonzero-sloped fill-in lines ----
         b0=y1-a*x1
         b=b0
         if(a*x2+b0.le.y2)then
            do while((y1-b)/a.lt.x2)
               call pgmove(tr0+tr1*(y1-b)/a,y1)
               call pgdraw(tr0+tr1*x2,a*x2+b)
               b=b-delb
            enddo        
            b=b0+delb
            do while(a*x2+b.lt.y2)
               call pgmove(tr0+tr1*x1,a*x1+b)
               call pgdraw(tr0+tr1*x2,a*x2+b)
               b=b+delb
            enddo
            do while(a*x1+b.lt.y2)
               call pgmove(tr0+tr1*x1,a*x1+b)
               call pgdraw(tr0+tr1*(y2-b)/a,y2)
               b=b+delb
            enddo
         else
            do while(a*x1+b.lt.y2)
               call pgmove(tr0+tr1*x1,a*x1+b)
               call pgdraw(tr0+tr1*(y2-b)/a,y2)
               b=b+delb
            enddo
            b=b0-delb
            do while((y2-b)/a.lt.x2)
               call pgmove(tr0+tr1*(y1-b)/a,y1)
               call pgdraw(tr0+tr1*(y2-b)/a,y2)
               b=b-delb
            enddo
            do while((y1-b)/a.lt.x2)
               call pgmove(tr0+tr1*(y1-b)/a,y1)
               call pgdraw(tr0+tr1*x2,a*x2+b)
               b=b-delb
            enddo
         endif
      endif

      return
      end
