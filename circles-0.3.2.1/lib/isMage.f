C     Copyright (C) 2003 B. Roukema et al. <shape-univ at astro.uni.torun.pl>
C     
C     This program is free software; you can redistribute it and/or modify
C     it under the terms of the GNU General Public License as published by
C     the Free Software Foundation; either version 2 of the License, or
C     (at your option) any later version.
C     
C     This program is distributed in the hope that it will be useful,
C     but WITHOUT ANY WARRANTY; without even the implied warranty of
C     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C     GNU General Public License for more details.
C     
C     You should have received a copy of the GNU General Public License
C     along with this program; if not, write to the Free Software
C     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
C  
C     See also http://www.gnu.org/licenses/gpl.html

C     various functions to remove likely foreground contaminants from
C     an identified circles analysis

C     ------------------------------------------------------------
C     !! warning: must  be declared in calling subroutine 
C     as   logical*4 isMage

      logical*4 function isMage(gall,galb) ! Is (gall,galb) in Mag. stream ?
      wings=10.0! 30.0! 
      gl=mod(gall+720.0,360.0)
      gb=galb
      if(gb.lt.0.0)then
         if(gl.lt.110.0.or.gl.gt.250.0)then
            if( 
c     _           (-60.0.lt.gb.and.gb.lt.(-30.0+wings) ! far tip of stream
c     _           .and.(80.0-wings).lt.gl
c     _           .and.gl.lt.(100.0+wings) ).or.
     _           (-40.0.lt.gb.and.gb.lt.(-25.0+wings)  ! LMC
     _           .and.(270.0-wings).lt.gl
     _           .and.gl.lt.(300.0+wings) ).or.
c     _           (-80.0.lt.gb.and.gb.lt.(-40.0) !SMC
c     _           .and.(280.0-wings).lt.gl
c     _           .and.gl.lt.(320.0+wings) ).or.
c     _           (-87.0.lt.gb.and.gb.lt.(-75.0) ! SGP region
c     _           .and.( (300.0-wings).lt.gl
c     _           .or.gl.lt.(80.0+wings) )).or.
c     _           (-75.0.lt.gb.and.gb.lt.(-60.0) ! between SGP & far tip
c     _           .and. (60.0-wings).lt.gl
c     _           .and.gl.lt.(90.0+wings) ).or.
     _           (.false.) ! to make commenting out easy
     _           )then
               isMage=.true.
            else
               isMage=.false.
            endif
         else
            isMage=.false.
         endif
      else
         isMage=.false.
      endif
      return
      end


C     ------------------------------------------------------------
C     !! warning: must  be declared in calling subroutine 
C     as   logical*4 antiGC

      logical*4 function antiGC(gall,galb) ! Is (gall,galb) in Mag. stream ?
      hwidl=20.0
      hwidb=60.0

      gl=mod(gall+720.0,360.0)
      gb=galb
c      if(gb.lt.0.0)then
      if(gb.lt.hwidb)then
         if(gb.gt.-hwidb)then
            if(gl.gt.180.0-hwidl)then
               if(gl.lt.180.0+hwidl)then
                  antiGC=.true.
               else
                  antiGC=.false.
               endif
            else
               antiGC=.false.
            endif
         else
            antiGC=.false.
         endif
      else
         antiGC=.false.
      endif
      return
      end

C     ------------------------------------------------------------
C     !! warning: must  be declared in calling subroutine 
C     as   logical*4 SGcut

      logical*4 function SGcut(gall,galb) ! Is (gall,galb) in Mag. stream ?
      if(galb.lt.0.0.and.galb.gt.-40.0)then
c      if(-40.0.lt.galb.and.galb.lt.30.0)then
         SGcut=.true.
      else
         SGcut=.false.
      endif
      return
      end


C     ------------------------------------------------------------
C     !! warning: must  be declared in calling subroutine 
C     as   logical*4 inospot

C     Is the cartesian angle (x1x,x1y,x1z)  in an area possibly contaminated
C     by the Ophiuchus complex/Ophiuchus loop, Orion complex? yes/no

      logical*4 function inospot(x1x,x1y,x1z)

C     INPUTS:
      real*4   x1x,x1y,x1z      ! cartesian coordinates of sky position

C     INTERNAL:
      parameter(maxo=3)
      real*4    xospot(maxo),yospot(maxo),zospot(maxo),radiano(maxo)
      real*4    ro_deg(maxo) ! vcircles-0.1.33 ! radius of o-spot in degrees
      real*4    ospotb(maxo),ospotl(maxo),opix(maxo)

      integer*4 ifirst
      logical*4 test

      logical*4 inos            ! local value- in Ophiuchus,Orion complex/loop?

      parameter(pi=3.1415926535898)
      parameter(pi_180=pi/180.0)

      include 'npix.dec'

      data   ospotl/41.0,12.5,176.0/
      data   ospotb/71.0,-24.0,-42.0/

      data   opix/375.0,10.0,100.0/
      data   ro_deg/57.0,9.0,29.0/  ! from astro-ph/9910272, from Cayon et al.
      save   xospot,yospot,zospot,radiano
      save   ospotb,ospotl,opix

      data test /.false./ !.true./! 

      data ifirst/0/
      save ifirst

c     ---- initialise O-complex contamination positions ----
c     xospot, yospot, zospot  are for unit vector
      if(ifirst.ne.12345)then
         ifirst=12345

         do i=1,maxo
            xospot(i)= cos(ospotb(i)*pi_180)*cos(ospotl(i)*pi_180)
            yospot(i)= cos(ospotb(i)*pi_180)*sin(ospotl(i)*pi_180)
            zospot(i)= sin(ospotl(i)*pi_180)
c     radiano(i)= sqrt( (opix(i)* (4.0*pi/6144.0)) /pi )
c     _        *2.0              ! factor greater than CaySmoot pixel area

c            radiano(i)= sqrt( (opix(i)* 
c     _           (4.0*pi/real(npix))) /pi )
c     _           *2.0           ! factor greater than CaySmoot pixel area

c     _        *0.5              ! factor greater than CaySmoot pixel area
c     _           *20.0          !! test only !!
            radiano(i)= ro_deg(i) *pi_180
     _           *0.5           ! factor greater/smaller than CaySmoot 
         enddo
         if(test)then 
            print*,'(xospot(i),yospot(i),zospot(i),'//
     _        'radiano(i),i=1,maxo)'
            print*,'ospot ',(xospot(i),yospot(i),zospot(i),
     _           radiano(i),i=1,maxo)
         endif
      endif

      inos= .false.
      do ios=1,maxo             !1,1 !
         oang= angle(x1x,x1y,x1z, 
     _        xospot(ios),yospot(ios),
     _        zospot(ios))
         if(oang.lt.radiano(ios))
     _        inos=.true.
      enddo

      inospot=inos
      return
      end
