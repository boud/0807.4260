C   circles - calculate identified circles statistics from CMB data (cosmic topology)
C
C   Copyright (C) 2005 Boud Roukema
C     
C     This program is free software; you can redistribute it and/or modify
C     it under the terms of the GNU General Public License as published by
C     the Free Software Foundation; either version 2 of the License, or
C     (at your option) any later version.
C     
C     This program is distributed in the hope that it will be useful,
C     but WITHOUT ANY WARRANTY; without even the implied warranty of
C     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C     GNU General Public License for more details.
C     
C     You should have received a copy of the GNU General Public License
C     along with this program; if not, write to the Free Software
C     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
C  
C     See also http://www.gnu.org/licenses/gpl.html


C     --- correlate opposite discs on the CMB, compare with "solution" pairs
C     added in version 0.1.31

C     rough plan:
C       -> circles_f77.f
C       -> astromisc/lib/
C          gendodec +  prob.f:spher2  + xyz_rda.f
C       
C       -> galactic disk/centre exclusion as per circles_f77.f
C       -> exclude the "solution" region upto  20 degrees. (centres)
C        => circles_f77.f
C       
C       first without gendodec 
C       -> only ltapprox 50 genuinely independent opposite pairs 
C       -> numerical prob distribution
C       
C       -> use gendodec on "the solution" to get the 6 pairs 
C       
C       -> write P(c > c_i)  and \Pi  P(c > c_i)   e.g. (10%)^6 = 10^-6  :)

      subroutine cdiscs(gl_in,gb_in,ang_in,th_in,
     _     iwgcut,bcut,GCmin,
     _     iverb,nowarn,
     _     temp,errtemp,gallong,gallat)


C     INPUTS:
      real*4   gl_in          ! solution longitude
      real*4   gb_in          ! solution latitude
      real*4   ang_in         ! radius of disc
      real*4   th_in          ! solution thdodec -> use in gendodec(...) 
      integer  iwgcut           ! logical - do galcut?
      real*4   bcut            ! galactic latitude cut
      real*4   GCmin           ! galactic centre cut
      integer   iverb,nowarn    ! verbose? hide warnings?
      real*4   temp(*),errtemp(*) !   Temperature and uncert.
      real*4   gallong(*),gallat(*) ! galactic

      real*4   alphyp           !hypothesised angle alpha: As of
                                !circles-0.1.46, alphyp is hardwired,
                                !not input
      real*4   phihyp           !hypothesised phase phi: As of
                                !circles-0.1.46, phihyp is hardwired,
                                !not input
      data alphyp /11.0/
      data phihyp /-36.0/
      parameter(toltrig= 1e-5)

C     EXTERNAL FUNCTIONS
c      logical*4 function inospot(x1x,x1y,x1z)
      logical*4 inospot
      external  inospot


C     INTERNAL:
      logical  galcut
      parameter(pi=3.14159265358979,pi_180=pi/180.0)

      parameter(nface=12)
c     -- centre of each face --
      real*4  xdod(nface),ydod(nface),zdod(nface)
c     -- orthogonal unit vectors to generate circle
      real*4  xdodp(nface,2),ydodp(nface,2),zdodp(nface,2) ! perpendiculars

      real*4  gl_dod(nface),gb_dod(nface) ! gal long lat of 12 faces


c     --- general probability distribution of correlation values of disc pairs ---
      parameter(maxn=10000)  ! 1000 should be enough, 10000 reduces noise
      real*4   cgen(maxn)  ! will be sorted "in place"
      real*4   cgenmK(maxn)  ! *10^6
      parameter(nbins=100)
      real*4   xbins(nbins),bins(nbins)



c     ----
      real*4   cdod(nface) ! correlations of 6 pairs - 50% redundant
      real*4   prob(nface),prob6 ! probabilities
      real*4   cdodmK(nface) ! *10^6

      logical*4  test


      test=.false.! .true. !

      if(iwgcut.eq.1)then
         galcut=.true.
      else
         galcut=.false.
      endif

      if(ang_in.gt.90.0)then
         if(nowarn.ne.1)print*,'cdiscs: WARNING: ang_in > 90.0,',
     _        ' ang_in = ',ang_in
         angloc=90.0
         angmin=angloc
      else
         angloc=ang_in
         angmin= 2.0* angloc
      endif


c     --- input gl_in, gb_in -> XYZ _in ---
      x_in= cos(gl_in*pi_180)*cos(gb_in*pi_180)
      y_in= sin(gl_in*pi_180)*cos(gb_in*pi_180)
      z_in= sin(gb_in*pi_180)

c     ---- main loop to calculate general correlation prob distribution
      igen=0
      itry=0
      ngen=1000

c      do i=1,56142   ! flush
      do i=1,756142   ! flush
         xx=rnd(0)
      enddo


      do while(igen.lt.ngen.and.itry.lt.ngen*100)
         itry=itry+1
         
         if(test)print*,'igen,itry=',igen,itry

         if(test.and.10*((igen-1)/10).eq.igen-1)
     _        print*,'cdiscs: igen,itry=',igen,itry

c     -- get a random sky position
         call spher2(phi,theta) ! degrees
c     -- radians -> degrees

         xgen=cos(phi*pi_180)*cos(theta*pi_180)
         ygen=sin(phi*pi_180)*cos(theta*pi_180)
         zgen=sin(theta*pi_180)

c     --- angle from galactic cut in deg
         GC=angle(xgen,ygen,zgen,1.0,0.0,0.0)/pi_180  ! degrees

         if(test)print*,'cdiscs: xgen,ygen,zgen=',
     _        xgen,ygen,zgen

         aa_in= angle(x_in,y_in,z_in, xgen,ygen,zgen)/pi_180 ! degrees

c     -- if it's not excluded by galactic cut, then increment igen and correlate
         if( !(aa_in.gt.angmin).and.
     _        ( (.not.(galcut)).or.
     _        (galcut.and.((abs(theta).gt.bcut).and.(GC.gt.GCmin)))
     _        )
     _        )
     _        then


            if(test)print*,'cdiscs: phi,theta=',phi,theta,
     _           ' angloc=',angloc
            call cdis_1(phi,theta,xgen,ygen,zgen,
     _           angloc,galcut,bcut,GCmin, !INPUTS
     _           iverb,nowarn,  !INPUTS
     _           temp,errtemp,gallong,gallat, !INPUTS
     _           alphyp,phihyp,
     _           tt1,tt2,itt1,itt2, ! OUTPUTS
     _           beta)             


            if(test)print*,'cdiscs: tt1,tt2=',tt1,tt2

            if(itt1.gt.0.and.itt2.gt.0)then
               igen=igen+1
               cgen(igen)=tt1*tt2
            endif

         endif !          if((.not.(galcut)).or.... (xgen,...)


c         if(test)stop
      enddo !      do while(igen.lt.ngen.and.itry.lt.ngen*100)


      ngen=igen ! update ngen final value (might be less than ngen above)

c     ---- sort these into order ----
      call r4sort(cgen,ngen)

c     ---- plot ----
      do i=1,ngen
         cgenmK(i)=cgen(i)*1e6
      enddo
c      x2=max( -cgenmK(1),cgenmK(ngen) )
      x1=-1200.0
      x2=1200.0
      ioob=10
      call mkbins(ngen,x1,x2,cgenmK,nbins,
     _     xbins,bmin,bmax,bins,ioob)
      y1=0.0
      y2=bmax
      if(test)print*,'bmin,bmax=',bmin,bmax

      call pgbegin(0,'cdiscs.ps/vps',1,1)              
      call pgvsize(1.0,6.0,1.0,5.0)
      call pgslw(3)
      call pgscf(2)
      call pgsch(1.2)
               
      call pgwindow(x1,x2,y1,y2)
      call pgbox('bcnts',0.,0,'bcnts',0.,0)
      call pgline(nbins,xbins,bins)

      call pglabel('disc correlation (mK)\\u2','N','')
      call pgend

c     endif



      print*,'cdiscs: ngen, cgen(1),cgen(ngen/2),cgen(ngen)='
      print*,ngen, cgen(1),cgen(ngen/2),cgen(ngen)
      print*,' '


c     ---- get the twelve positions of the face centres ----
      call gendodec(gl_in,gb_in, th_in, ! INPUTS
     _     xdod,ydod,zdod, ! OUTPUTS
     _     xdodp,ydodp,zdodp)

c     !test: do 1,12 and print results
      ndtest=6
      if(test)ndtest=12

      prob6= 1.0
      beta= 0.0
      nbeta=0
      cdod_m = 0.0 ! mean cdodmK
      do idod=1,ndtest
         call xyz_rda(xdod(idod),ydod(idod),zdod(idod), !INPUTS
     _        rr,dd,aa)         ! OUTPUTS
         gl_dod(idod)= aa*15.0  ! long (deg)
         gb_dod(idod)= dd       ! lat (deg)

         call cdis_1(gl_dod(idod),gb_dod(idod), !INPUTS
     _        xdod(idod),ydod(idod),zdod(idod),
     _        angloc,galcut,bcut,GCmin, !INPUTS
     _        iverb,nowarn,     !INPUTS
     _        temp,errtemp,gallong,gallat, !INPUTS
     _        alphyp,phihyp,
     _        tt1,tt2,itt1,itt2, ! OUTPUTS
     _        beta_out)

         if(itt1.gt.0.and.itt2.gt.0)then
            cdod(idod)=tt1*tt2
            cdodmK(idod)=tt1*tt2 *1e6
            cdod_m= cdod_m + cdodmK(idod)
            beta= beta+ beta_out
            nbeta=nbeta+1
         else
            cdod(idod)=-99.9
         endif
         if(nbeta.gt.0)beta=beta/real(nbeta)
         if(nbeta.gt.0)cdod_m=cdod_m/real(nbeta)
         

c     --- FIX THIS: use something faster if ngen becomes big ! ---
         igen=1
c         do while(igen.le.ngen.and.cdod(idod).gt.cgen(igen)) !v0.1.32
         do while(igen.lt.ngen.and.cdod(idod).gt.cgen(igen))  ! v0.1.33
            igen=igen+1
         enddo
         igen=igen-1  ! be conservative

         if(ngen.ge.1)then
            prob(idod)= real(ngen-igen)/real(ngen)
         else
            prob(idod)=1.0
         endif
         prob6=prob6* prob(idod)
c         print*,'cdiscs: idod, prob(idod)=',idod, prob(idod)
c         print*,'cdiscs: cdodmK(idod)=',cdodmK(idod)
         if(inospot(xdod(idod),ydod(idod),zdod(idod)))then
            write(6,'(a,i4,f9.4,f7.1)')
     _           'cdiscs: INOSPOT idod, p, c=',idod,prob(idod),
     _           cdodmK(idod)
         elseif(inospot(-xdod(idod),-ydod(idod),-zdod(idod)))then
            write(6,'(a,i4,f9.4,f7.1)')
     _           'cdiscs: antipode_INOSPOT idod, p, c=',
     _           idod,prob(idod),
     _           cdodmK(idod)
         else
            write(6,'(a,i4,f9.4,f7.1)')
     _           'cdiscs: idod, p, c=',idod,prob(idod),
     _           cdodmK(idod)
         endif

      enddo

c      print*,'cdiscs: ang_in=',ang_in,' prob6=',prob6
      write(6,'(a,f6.1,a,f7.2,a,g13.3,a,f9.3)')
     _     'cdiscs: ang_in=',ang_in,
     _     ' betabar=',beta/pi_180,' prob6=',prob6,' c=',cdod_m
      if(test)print*,'WARNING: test=.true. prob6 is really (prob6)^2'  !only if test

      return
      end


c     == calculate average temperatures
      subroutine cdis_1(phi,theta,xgen,ygen,zgen,
     _     angloc,galcut,bcut,GCmin, !INPUTS
     _     iverb,nowarn,        !INPUTS
     _     temp,errtemp,gallong,gallat, !INPUTS
     _     alphyp,phihyp,              ! INPUTS
     _     tt1,tt2,itt1,itt2,   ! OUTPUTS
     _     beta)                ! OUTPUTS

C     INPUTS:
      real*4   phi            ! longitude (deg)
      real*4   theta          ! latitude (deg)
      real*4   xgen,ygen,zgen   ! (phi,theta) -> XYZ
      real*4   angloc            ! radius of disc
      logical  galcut           ! logical - do galcut?
      real*4   bcut            ! galactic latitude cut
      real*4   GCmin           ! galactic centre cut
      integer   iverb,nowarn    ! verbose? hide warnings?
      real*4   temp(*),errtemp(*) !   Temperature and uncert.
      real*4   gallong(*),gallat(*) ! galactic
      real*4   alphyp           ! hypothesised circle radius  alpha (degrees)
      real*4   phihyp           ! hypothesised phase  phi  (degrees)

C     OUTPUTS:
      real*4   tt1,tt2          ! mean temperatures at (phi,theta) + antipode
      integer*4   itt1,itt2      ! number of temps used for averaging
      real*4   beta      ! mean "angle" between matching points (degrees)


C     INTERNAL:
      logical*4  test,test2

      parameter(pi=3.14159265358979,pi_180=pi/180.0)

      test=.false.! .true. !
      test2= .false.              ! .true. !


      tt1=0.0                   ! average temp in one disc
      tt2=0.0                   ! average temp in opposite disc
      itt1=0
      itt2=0
      beta=0.0 ! average angle between matching points 

      phihr2 = phihyp*pi_180 *0.5 ! phi/2 in radians
      alph_r = alphyp*pi_180
      cosalp = cos(alph_r)
      cosphi= sin( phihr2 * sin(alph_r) )
      if(test2)print*,'cdis_1: phihyp,alphyp,phihr2, alph_r,cosphi=',
     _     phihyp,alphyp,phihr2, alph_r,cosphi

c      GCmin=20.0 ! hardwired - see pi/9.0 in circles_f77 to fix this!
      resgb= 1.0   ! hardwired - FIX THIS  !!
      gbmin=-90.0
      gbmax=90.0
      ngb= nint((gbmax-gbmin)/resgb)

      glmin=0.0
      glmax=360.0
      ngl_0= nint((glmax-glmin)/resgb)  !  must *cos( galactic latitude )


      if(test)print*,'cdis_1: phi,theta,xgen,ygen,zgen=',
     _        phi,theta,xgen,ygen,zgen
c     ---- loop through all points on sphere and sum temperatures in
c     the two opposite discs ----
      do ib=1,ngb
         gb= gbmin+(real(ib)-0.5)*resgb

         ngl=nint(real(ngl_0) * cos(theta*pi_180) )
         if(ngl.ge.1)then
            resgl= (glmax-glmin)/real(ngl)
         else
            resgl= 0.0
         endif
         do il=1,ngl
            gl= glmin+(real(il)-0.5)*resgl
                  
            x2=cos(gl*pi_180)*cos(gb*pi_180)
            y2=sin(gl*pi_180)*cos(gb*pi_180)
            z2=sin(gb*pi_180)

c     --- angle from galactic cut in deg
            GC=angle(x2,y2,z2,1.0,0.0,0.0)/pi_180  

            if((.not.(galcut)).or.
     _           (galcut.and.((abs(theta).gt.bcut)
     _           .and.(GC.gt.GCmin))))then

               aa_rad=angle(xgen,ygen,zgen,x2,y2,z2)
               aa=aa_rad /pi_180

c     --- convert to distance (radians) orthogonal to plane through circles ---
               aa_rad = min(pi-aa_rad,aa_rad)
               aa_rad= abs( 2.0*(cosalp -cos(aa_rad)) )

               if(test.and.il.eq.10)print*,'cdis_1: gl,gb=',gl,gb,
     _              ' aa=',aa
               if(aa.le.angloc)then ! in first disc
                  tt1=tt1+ wmaplin(gl,gb,
     _                 temp,errtemp,gallong,gallat)
                  itt1=itt1+1
c                  cosphi= sin( phihr2 * sin(alph_r) )
                  beta=beta + 
     _                 sqrt( aa_rad*aa_rad + 
     _                 4.0* cosphi*cosphi )
                  if(test2)print*,'cdis_1: anglog,itt1,dbeta,beta = ',
     _                 angloc,itt1,sqrt( aa_rad*aa_rad + 
     _                 4.0* cosphi*cosphi ),beta
               elseif(aa.ge.180.0-angloc)then
                  tt2=tt2+ wmaplin(gl,gb,
     _                 temp,errtemp,gallong,gallat)
                  itt2=itt2+1
c                  cosphi= sin( phihr2 * sin(alph_r) )
                  beta=beta + 
     _                 sqrt( aa_rad*aa_rad + 
     _                 4.0* cosphi*cosphi )
                  if(test2)print*,'cdis_1/180: anglog,', 
     _                 'itt1,dbeta,beta = ',
     _                 angloc,itt1,sqrt( aa_rad*aa_rad + 
     _                 4.0* cosphi*cosphi ),beta
               elseif(aa.ge.181.0)then
                  if(nowarn.ne.1)print*,'cdis_1: WARNING ',
     _                 'phi,theta,gl,gb,aa=',
     _                 phi,theta,gl,gb,aa
               endif
            endif               !  if((.not.(galcut)).or.  (x2,...)
         enddo                  !               do il=1,ngl
      enddo                     !do ib=1,ngb

      if(itt1.gt.0.and.itt2.gt.0)then
         tt1=tt1/real(itt1)
         tt2=tt2/real(itt2)
      endif
      if(test2)print*,'cdis_1: itt1,itt2,itt1+itt2=',
     _     itt1,itt2,itt1+itt2
      if(test2)print*,'cdis_1: beta=',beta
      if(itt1+itt2.gt.0)then
         beta=beta/real(itt1+itt2) ! mean weighting between opposite discs
      endif

      if(test)print*,'cdis_1: tt1,tt2,itt1,itt2=', tt1,tt2,itt1,itt2
      if(test2)print*,'cdis_1 end: beta=',beta

      return
      end
