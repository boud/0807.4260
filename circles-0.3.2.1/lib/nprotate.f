C     Copyright (C) 2004 B. Roukema et al. <shape-univ at astro.uni.torun.pl>
C     
C     This program is free software; you can redistribute it and/or modify
C     it under the terms of the GNU General Public License as published by
C     the Free Software Foundation; either version 2 of the License, or
C     (at your option) any later version.
C     
C     This program is distributed in the hope that it will be useful,
C     but WITHOUT ANY WARRANTY; without even the implied warranty of
C     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C     GNU General Public License for more details.
C     
C     You should have received a copy of the GNU General Public License
C     along with this program; if not, write to the Free Software
C     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
C     
C     See also http://www.gnu.org/licenses/gpl.html


C     rotate from (gall,galb) to the North Pole

      subroutine nprotate(gall,galb, glin,gbin, glout,gbout)
C     INPUTS:
      real*4  gall,galb         ! definition of rotation in degrees
      real*4  glin,gbin         ! input position in degrees

C     OUTPUT:
      real*4  glout,gbout       ! output position in degrees

C     LOCAL:
      real*4  cart(3)           ! input position in cartesian coords
      real*4  cartout(3)           ! output position in cartesian
      real*4  gbold,glold
      real*4  rotZ(3,3),rotX(3,3),rotZX(3,3) ! for rotations
      real*4  rotZXZ(3,3)


      save   gbold,glold, rotZX,rotZXZ
      
      parameter(pi=3.1415926535898,pi_180=pi/180.0)
      parameter(tol=0.01)

c     --- is this a new rotation or the previous one? ---
      if(abs(gbold-galb).gt.tol.or.abs(glold-gall).gt.tol)then
         gbold=galb
         glold=gall

c     ---- rotation around Z-axis, so that gall,galb is in X-Z plane ----
         phi= -gall*pi_180

         rotZ(1,1)=cos(phi)
         rotZ(1,2)=sin(phi)
         rotZ(1,3)=0.0
         rotZ(2,1)=-sin(phi)
         rotZ(2,2)=cos(phi)
         rotZ(2,3)=0.0
         rotZ(3,1)=0.0
         rotZ(3,2)=0.0
         rotZ(3,3)=1.0

c     ----rotation around Y-axis ----
         phi= pi/2.0 - galb*pi_180

         rotX(1,1)=cos(phi)
         rotX(1,2)=0.0
         rotX(1,3)=sin(phi)
         rotX(3,1)=-sin(phi)
         rotX(3,2)=0.0
         rotX(3,3)=cos(phi)
         rotX(2,1)=0.0
         rotX(2,2)=1.0
         rotX(2,3)=0.0

         call mult(rotZ,rotX,rotZX,3,3,3,3)

c     ---- rotate back around Z-axis ----
         phi= +gall*pi_180

         rotZ(1,1)=cos(phi)
         rotZ(1,2)=sin(phi)
         rotZ(1,3)=0.0
         rotZ(2,1)=-sin(phi)
         rotZ(2,2)=cos(phi)
         rotZ(2,3)=0.0
         rotZ(3,1)=0.0
         rotZ(3,2)=0.0
         rotZ(3,3)=1.0

         call mult(rotZX,rotZ,rotZXZ,3,3,3,3)
      endif

c     --- convert to cartesian coords
      cart(1)= cos(glin*pi_180) * cos(gbin*pi_180)
      cart(2)= sin(glin*pi_180) * cos(gbin*pi_180)
      cart(3)= sin(gbin*pi_180)
      
      call mult(cart,rotZXZ,cartout,1,3,3,3)
         
      call xyz_rda(cartout(1),cartout(2),cartout(3),
     _     rr,dd,aa)
      glout= aa*15.0
      gbout= dd
         
      return
      end

