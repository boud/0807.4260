/*
   circles - calculate identified circles statistics from CMB data (cosmic topology)

   Copyright (C) 2006 Boud Roukema and GSL authors Mark Galassi
   (rosalia@cygnus.com) and James Theiler (jt@nis.lanl.gov) with
   contributions from Jim Davies (jimmyd@nis.lanl.gov), Brian Gough
   (bjg@network-theory.co.uk), Reid Priedhorsky (rp@lanl.gov), Gerard
   Jungman (jungman@lanl.gov) and Mike Booth
   <booth@planck.pha.jhu.edu>.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

*/

#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include "system.h"


#include <stdlib.h>


#define EXIT_FAILURE 1

#if ENABLE_NLS
# include <libintl.h>
# define _(Text) gettext (Text)
#else
# define textdomain(Domain)
# define _(Text) Text
#endif
#define N_(Text) Text

#include "circles.h"

/*  GSL preparation for cubic fit   - "info gsl" for more info
    t_i = iang_p  ! could also be, in principle: *(ang_p4+iang_p)

    [cf example: f_i = ((A \exp(-\lambda t_i) + b) - y_i)/\sigma_i]
    quad:    f_i = ((A t_i^2 + B t_i + C) - y_i)/\sigma_i
    cubic:   f_i = ((D t_i^3 + C t_i^2 + B t_i + A) - y_i)/\sigma_i

    Jacobian:
    J_{ij} = d f_i / d x_j
    where  x_j = A, B, C, D
    i.e.
    d f_i/dA = 1/sigma_i
    d f_i/dB = t_i /sigma_i
    d f_i/dC = t_i^2 / sigma_i
    d f_i/dD = t_i^3 / sigma_i
*/

#include <stdlib.h>
#include <stdio.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_multifit_nlin.h>
#include <gsl/gsl_version.h>



/* #define TEST_PSMOOT */
#define MXANG_LOCAL 500

struct data {
  size_t n;
  double * y;
  double * sigma;
};

/* GSL PARAMETERS: DECLARATIONS */
const gsl_multifit_fdfsolver_type *T;
gsl_multifit_fdfsolver *s;

int status;
size_t i, iter = 0;

size_t n ;  /* must set this to  *nang_p  */
/* const size_t p = 3; */
size_t p = 4;

/* gsl_matrix *covar = gsl_matrix_alloc (p, p); */
gsl_matrix *covar ;

double y[MXANG_LOCAL], sigma[MXANG_LOCAL];

/* struct data d = { n, y, sigma}; */
struct data d ;

gsl_multifit_function_fdf f;

double x_init[4]; /* must set initial values */

/* gsl_vector_view x = gsl_vector_view_array (x_init, p); */
gsl_vector_view x ;

const gsl_rng_type * type;
gsl_rng * r;



int
cubic_f (const gsl_vector * x, void *params,
         gsl_vector * f)
{
  size_t n = ((struct data *)params)->n;
  double *y = ((struct data *)params)->y;
  double *sigma = ((struct data *) params)->sigma;

  double D = gsl_vector_get (x, 0);
  double C = gsl_vector_get (x, 1);
  double B = gsl_vector_get (x, 2);
  double A = gsl_vector_get (x, 3);

  size_t i;

  for (i = 0; i < n; i++)
    {
      /* Model Yi = A * i^2 + B * i + C */
      double t = i;
      double Yi = D * t*t*t + C * t*t + B* t + A;
      gsl_vector_set (f, i, (Yi - y[i])/sigma[i]);
    }

  return GSL_SUCCESS;
};


int
cubic_df (const gsl_vector * x, void *params,
          gsl_matrix * J)
{
  size_t n = ((struct data *)params)->n;
  double *sigma = ((struct data *) params)->sigma;

  /*  double A = gsl_vector_get (x, 0);
      double B = gsl_vector_get (x, 1);
  */

  size_t i;

  for (i = 0; i < n; i++)
    {
      /* Jacobian matrix J(i,j) = dfi / dxj, */
      /* where fi = (Yi - yi)/sigma[i],      */
      /*       Yi = D * i^3 + C * i^2 + B*i + A  */
      /* and the xj are the parameters (A,B,C) */
      double t = i;
      double s = sigma[i];
      /* double e = exp(-lambda * t); */
      gsl_matrix_set (J, i, 0, t*t*t/s);
      gsl_matrix_set (J, i, 1, t*t/s);
      gsl_matrix_set (J, i, 2, t/s);
      gsl_matrix_set (J, i, 3, 1/s);

    }
  return GSL_SUCCESS;
};

int
cubic_fdf (const gsl_vector * x, void *params,
           gsl_vector * f, gsl_matrix * J)
{
  cubic_f (x, params, f);
  cubic_df (x, params, J);

  return GSL_SUCCESS;
};

void
print_state (size_t iter, gsl_multifit_fdfsolver * s)
{
  printf ("iter: %3u x = % 15.8f % 15.8f % 15.8f |f(x)| = %g\n",
          iter,
          gsl_vector_get (s->x, 0),
          gsl_vector_get (s->x, 1),
          gsl_vector_get (s->x, 2),
          gsl_vector_get (s->x, 3),
          gsl_blas_dnrm2 (s->f));
};




void PSMOOT(
            int* mxnperang,
            int* mxang,
            int* nang_p,
            int *nper_ang,
            float *c_ang4,   /*  *c_ang4 values are modified here */
            float *ang_p4
           )
{
  int iq, iang_p;


  /* This smoothing return would need to be updated for GSL versions
     that have <gsl/gsl_multifit_nlinear.h> instead of
     <gsl/gsl_multifit_nlin.h>.
     A GSL version >= 2 will leave the c_ang4 values unmodified (unsmoothed).
 */
#if GSL_MAJOR_VERSION >= 2
  return;
#else
#  if __GNUC__
#    warning psmoot: Using legacy version of GSL.
#  endif
#endif

  /* INITIALISATIONS */

  /* things which the GSL example set as constants but we do not */
  n= *nang_p;

  /* SET INITIAL ESTIMATE for A,B,C:  assume a constant value
     using the middle of the angular range: iang_p= ((*mxang)+1)/2
   */

  iang_p= ((*nang_p)+1)/2-1; /* test only */
  iq= *(nper_ang+iang_p)-1 ;    /* should be OK in general */

  for (iq=0; iq < *(nper_ang+iang_p); iq++)
    {

#ifdef TEST_PSMOOT
      printf("psmoo: iq = %d iang_p = %d \n",iq,iang_p);
#endif
      x_init[0]=  0.0;
      x_init[1]= 0.0;
      x_init[2]= 0.0;
      x_init[3]= (double)*(c_ang4 + iq +(iang_p* (*mxnperang)));
#ifdef TEST_PSMOOT
      printf("psmoo: x_init[2]= %g\n", x_init[3]);
#endif

      covar = gsl_matrix_alloc (p, p);
      /*  d= { n, y, sigma}; */
      d.n= n;
      d.y = y;
      d.sigma = sigma;
      x= gsl_vector_view_array (x_init, p);

      /* MAIN WORK */
      gsl_rng_env_setup();

      type = gsl_rng_default;
      r = gsl_rng_alloc (type);

      f.f = &cubic_f;
      f.df = &cubic_df;
      f.fdf = &cubic_fdf;
      f.n = n;
      f.p = p;
      f.params = &d;


      /* set up y[i] (and sigma[i] - can be a constant dummy) */

      for (i = 0; i < n; i++)
        {
          double t = i;
          y[i] = (double)*(c_ang4 + iq +(i* (*mxnperang))) ;


          /* aim only to get a fit; significance is only marginally
             irrelevant
          */
          if(*nper_ang > 1)
            {
              sigma[i] = 1.0 /sqrt( (double)*nper_ang );
            }
          else
            sigma[i] = 0.1;


#ifdef TEST_PSMOOT
          printf ("psmoo: iq, c_ang4, *c_ang4: %d %d %g\n",
                  iq, c_ang4, *c_ang4);
          printf ("psmoo: probs to fit: %d %g %g\n", i, y[i], sigma[i]);
#endif
        };

      T = gsl_multifit_fdfsolver_lmsder;  /* choose Levenberg-Marquardt solver */
      s = gsl_multifit_fdfsolver_alloc (T, n, p);
      gsl_multifit_fdfsolver_set (s, &f, &x.vector);

#ifdef TEST_PSMOOT
      print_state (iter, s);
#endif
      do
        {
          iter++;
          status = gsl_multiroot_fdfsolver_iterate (s);

#ifdef TEST_PSMOOT
          printf ("status = %s\n", gsl_strerror (status));
          print_state(iter, s);
#endif

          if (status)   /* check if solver is stuck */
            break;

          status =
            gsl_multifit_test_delta (s->dx, s->x,
                                     1e-4, 1e-4);
        }
      while (status == GSL_CONTINUE && iter < 500);


#if GSL_MAJOR_VERSION >= 2
#else
#  if __GNUC__
#    warning psmoot: Using legacy version of GSL.
#  endif
      gsl_multifit_covar (s->J, 0.0, covar);
#endif

#define FIT(i) gsl_vector_get(s->x, i)
#define ERR(i) sqrt(gsl_matrix_get(covar,i,i))

#ifdef TEST_PSMOOT
      printf ("D      = %.5f +/- %.5f\n", FIT(0), ERR(0));
      printf ("C      = %.5f +/- %.5f\n", FIT(1), ERR(1));
      printf ("B      = %.5f +/- %.5f\n", FIT(2), ERR(2));
      printf ("A      = %.5f +/- %.5f\n", FIT(3), ERR(3));

      for (i = 0; i < n; i++)
        {
          printf("psmoo: CHECK data: %g  cubic: %g \n",
                 *(c_ang4 + iq +(i* (*mxnperang))),
                 FIT(0) *(double)(i*i*i) +
                 FIT(1) *(double)(i*i) + FIT(2) * (double)i + FIT(3) );
        };
#endif

      {
        double chi = gsl_blas_dnrm2(s->f);
        printf("chisq/dof = %g\n",  pow(chi, 2.0)/ (n - p));
      };

#ifdef TEST_PSMOOT
      printf ("status = %s\n", gsl_strerror (status));
#endif
      if (status)   /* print error message if there's an error */
        printf ("status = %s\n", gsl_strerror (status));

      /*  replace original values with fit values */
      for (i = 0; i < n; i++)
        {
          *(c_ang4 + iq +(i* (*mxnperang))) =
            (float)( FIT(0) *(double)(i*i*i) +
                     FIT(1) *(double)(i*i) + FIT(2) * (double)i + FIT(3) );
        };

    };  /*   for (iq=0; iq < *(nper_ang+iang_p); iq++) */
  gsl_multifit_fdfsolver_free (s);


  for (iang_p=0; iang_p < *nang_p; iang_p++)
    {

#ifdef TEST_PSMOOT
        printf("psmoot: *(nper_ang+iang_p) = %d\n",*(nper_ang+iang_p));
#endif
      for (iq=0; iq < *(nper_ang+iang_p); iq++)
        {
          /* fortran array ordering: 1st index changes fastest */
          /*      print*,((c_ang(iq,iang_p),iq=1,4),iang_p=1,2) */

#ifdef TEST_PSMOOT
            {
              if(iq<4 && iang_p<2)
                printf("psmoot: *(c_ang4 + iq +(iang_p*mxnperang))= %f\n",
                       *(c_ang4 + iq +(iang_p* (*mxnperang))));

              /* *(c_ang4 + iq +(iang_p* (*mxnperang))) *= 1.1;  test only */
            };
#endif


        };
#ifdef TEST_PSMOOT
      printf("\n");
#endif

    };

};
