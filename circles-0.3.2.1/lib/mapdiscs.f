C   circles - calculate identified circles statistics from CMB data (cosmic topology)
C
C   Copyright (C) 2005 Boud Roukema
C     
C     This program is free software; you can redistribute it and/or modify
C     it under the terms of the GNU General Public License as published by
C     the Free Software Foundation; either version 2 of the License, or
C     (at your option) any later version.
C     
C     This program is distributed in the hope that it will be useful,
C     but WITHOUT ANY WARRANTY; without even the implied warranty of
C     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C     GNU General Public License for more details.
C     
C     You should have received a copy of the GNU General Public License
C     along with this program; if not, write to the Free Software
C     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
C  
C     See also http://www.gnu.org/licenses/gpl.html


c     map discs inside of circles from circleT2.f: 
c
c     open(1,file='circleinfo'//ch3//'.dat',
c     
c     independently from plotcircles.f
      

C     WARNING: this is a somewhat arbitrary hack!
C     Do not use it unless you re really sure you know what you re doing.
C     Better use  bcut=  in the main subroutine below!
c     ---- exclude circles near galactic plane
      logical*4 function ivexclude(iv)
      ivexclude=.false. ! default - do not exclude
c      if(iv.eq.4.or.iv.eq.5)ivexclude=.true.
c      if(iv.eq.4)ivexclude=.true.
      return
      end


c     program  mapdiscs
      subroutine  pdiscs(indir,nindir,
     _     npix,
     _     iverb,nowarn,
     _     temp,errtemp,gallong,gallat, tempcp) ! plot matching discs

C     INPUTS:
      character*(*)  indir     ! directory with input files
      integer        nindir    ! number of characters in  indir

      integer        iverb,nowarn ! flags: verbose, suppress warnings

      real*4   temp(*),errtemp(*) !   Temperature and uncert.
      real*4   gallong(*),gallat(*) ! galactic

      real*4   tempcp(*) ! temporary copy of temp()


C     INTERNAL:
      character*256  indir2 ! safe string version of indir

C     THIS FILE:
      logical*4   iv_ex ! evaluation of ivexclude
      logical*4   ivexclude ! function

      parameter(tolsq=1e-20)
      
C     EXTERNAL:
      integer  pxstat  ! pxstat.c

      real*4    wmaplin
      external  wmaplin
      

      parameter(pi=3.14159265359,pi_180=pi/180.0)
      include 'ncopies.dec'

      integer*4    nthc(-iimax:iimax,0:jjmax)
      real*4    glatc(2,-iimax:iimax,0:jjmax), ! 2 centres
     _     glongc(2,-iimax:iimax,0:jjmax), ! 2 centres
     _     wmodc(-iimax:iimax,0:jjmax), ! dtheta * rSLS
     _     rSLSc,
     _     dthMpc(-iimax:iimax,0:jjmax) ! dtheta * rSLS

      real*4    tt1c(maxtheta, -iimax:iimax,0:jjmax)
      real*4    tt2c(maxtheta, -iimax:iimax,0:jjmax)
      real*4    s1c(maxtheta, -iimax:iimax,0:jjmax)
      real*4    s2c(maxtheta, -iimax:iimax,0:jjmax)
c     ---- 24.02.2004 ----
c     -- store galactic long, lat at each pixel for double-checking --
      real*4    glpix1(maxtheta, -iimax:iimax,0:jjmax)
      real*4    gbpix1(maxtheta, -iimax:iimax,0:jjmax)
      real*4    glpix2(maxtheta, -iimax:iimax,0:jjmax)
      real*4    gbpix2(maxtheta, -iimax:iimax,0:jjmax)


C     INTERNAL:
c     --- for plotting, calculated locally in this routine ---
      parameter (npair=2)
      real*4    rMpc(maxtheta,-iimax:iimax)
      real*4    ttplot(maxtheta,npair,-iimax:iimax)   
      

c     --- circles-0.1.15 
c     See if there is a maximum correlation as a function of
c     (1) the rotation angle (e.g. -36 degrees)
c     (2) the circle radius.

      parameter(maxcir=100)      ! different values of circles radius
c     == correlations ==
      real*4    corr(maxtheta,maxcir,-iimax:iimax)
C     -- mean and median 
      real*4    c_mean(maxtheta)
      real*4    c_med(maxtheta)
C     --- mean and median for diff circle radii
      real*4    c_meac(maxtheta,maxcir)
      real*4    c_medc(maxtheta,maxcir)
      parameter(mxctmp=maxcir*(2*iimax+1))
      real*4    c_tmp(mxctmp) ! for sorting

c     --- optimal (max) correlations 
      real*4    c_opti(iimax,maxcir)   
      real*4    c_opt(maxcir)
c     theta values at optima
      real*4    ctopti(iimax,maxcir)   
      real*4    ctopt(maxcir)
      real*4    ct_hw(maxcir,3)  ! half-width: width, opt-, opt+

c     == absolute differences ==
      real*4    diff(maxtheta,maxcir,-iimax:iimax)
c     -- root mean square difference
      real*4    diffsq(maxtheta,maxcir,-iimax:iimax)
C     -- mean and median 
      real*4    d_mean(maxtheta)
      real*4    dsmean(maxtheta) ! root mean square difference
      real*4    d_med(maxtheta)
C     --- mean and median for diff circle radii
      real*4    d_meac(maxtheta,maxcir)
      real*4    d_medc(maxtheta,maxcir)
c     - root mean square difference
      real*4    dsmeac(maxtheta,maxcir)
      real*4    dsmedc(maxtheta,maxcir)
c      parameter(mxctmp=maxcir*(2*iimax+1))
      real*4    d_tmp(mxctmp) ! for sorting

c     --- optimal (min) rms differences 
      real*4    s_opti(iimax,maxcir)
      real*4    s_opt(maxcir)
c     theta values at optima
      real*4    stopti(iimax,maxcir)
      real*4    stopt(maxcir)
      real*4    st_hw(maxcir,3)  ! half-width

      integer*4 ncorr(maxtheta,maxcir,-iimax:iimax)
      real*4    rMpc_r(maxtheta,maxcir,-iimax:iimax)
      real*4    theta(maxtheta,maxcir,-iimax:iimax) ! version 0.1.18
      real*4    rcir_r(maxcir)


      data jv  /0/ ! hardwired for dodecahedron - only iv is used
      data zerotwist /-36.0/ ! hardwired zero point -> could remeasure
c      data zerotwist /0.0/ ! hardwired zero point -> could remeasure

      
      common /disccomm/ nthc,glatc,glongc,wmodc,rSLSc,dthMpc,
     _     tt1c,tt2c,s1c,s2c,glpix1,gbpix1,glpix2,gbpix2

      character*3  ch3
      character*7  ch7

      logical*4  smooth,new
      logical*4 measur,noise    

      common /noisecomm/ measur,noise ! added circles-0.1.14


      logical*4  interactive,next
      logical*4  bad_iv ! deliberately set non-matching iv's

c     --- for grayscale plot - see  pgplot/src/pggray.f ---
      integer mxdim, i1, i2, j1, j2

c     --- quotation from pggray.f BEGIN ---
C The transformation matrix TR is used to calculate the world
C coordinates of the center of the "cell" that represents each
C array element. The world coordinates of the center of the cell
C corresponding to array element A(I,J) are given by:
C
C          X = TR(1) + TR(2)*I + TR(3)*J
C          Y = TR(4) + TR(5)*I + TR(6)*J
C
C Usually TR(3) and TR(5) are zero -- unless the coordinate
C transformation involves a rotation or shear.  The corners of the
C quadrilateral region that is shaded by PGGRAY are given by
C applying this transformation to (I1-0.5,J1-0.5), (I2+0.5, J2+0.5).
c     --- quotation from pggray.f END ---

      parameter(mxdim= 500)
      real    agray(mxdim,mxdim), fg, bg, tr(6)

      parameter(mxcont=20)
      real*4    cocont(mxcont)  ! correlation contour levels
      real*4    cocon2(mxcont)  ! copy of correlation contour levels e.g. microK
      real*4    trcorr(6)




      character*1  ch1
      character*13 ch13         ! for l,b labels
      character*4  ch4          ! for plot file names
      character*5  ch5          ! for angular radii rcir_r
      character*60 ch60

      logical*4    test,microK
      logical*4    c_all  ! do all the lines in the plots *all*ps?

      test=.true. ! .false.!
      microK=.true.               ! .false.!
      c_all=.false. 

c      print*,'pdisc: nindir=',nindir
c      print*,'pdisc: indir(1:1)=',indir(1:1)
c      print*,'pdisc: indir(1:min(nindir,256)-1)=',
c     _     indir(1:min(nindir,256)-1)
c      print*,'pdisc: indir(1:min(nindir,256))=',
c     _     indir(1:min(nindir,256))
c      indir2=indir(1:min(nindir,256))
c      indir2=indir

      ch3="__3"

      if(test)then
         print*,'mapdiscs: will call chksort as a test'
         call chksort
         print*,' ' 
      endif

c     --- min, max circle radii (cf. angmin, angmax in circles_f77) ---
      rcir_1=  5.0! 19.0!
      rcir_2=  89.0!  23.0 !
c      ncir=min(1,maxcir)
      ncir=21 !42

c     4.0-8.0 deg,  9.0-13.0
      rcir_1= 7.0 ! 9.0
      rcir_2= 15.0 !13.0
      ncir= 21 !11

c      ncir=maxcir
c      nthoff= 30  ! 17.02.2005 - version 0.1.17
      nthoff= 120  ! 
      ithsmoo= 2   ! 
c     smoothing angle =  360.0/real(nthoff) * real(ithsmoo)

      if(ncir.gt.1)then
         dcir= (rcir_2-rcir_1)/(ncir-1)
      else
         dcir=-99.9
      endif
      if(iverb.eq.1)
     _     print*,'mapdiscs: rcir_1,rcir_2,ncir=',rcir_1,rcir_2,ncir

c     ---- read file ----
      open(1,file='circleinfo'//ch3//'.dat',
     _     access='sequential',
     _     form='unformatted',status='old')
      read(1)ncc,cc1,cc2,H_0,drSLS,nparcircles 
      read(1)iimax0,jjmax0,maxtheta0, imax,jmax
      read(1)nthc,glatc,glongc,wmodc,rSLSc,dthMpc
      read(1)tt1c,tt2c,s1c,s2c
c     ---- 24.02.2004 ----
      read(1)glpix1,gbpix1,glpix2,gbpix2
      close(1)

      if(iverb.eq.1)then
         print*,' '
         print*,'(wmodc(iv,jv),iv=1,6)='
         print*,(wmodc(iv,jv),iv=1,6)
      endif

      smooth=.true.
      new=.false.

      interactive=.false. !.true.! 
      next=.false.

      bad_iv=.false. !.true.             ! 


      sigdeg= 1.0
      resdeg= 0.2
      fgcorr= 0.05 !0.015 

c      sigdeg= 5.0
c      resdeg= 1.0
c      fgcorr= 0.005

      bcut= 2.0 ! galactic cut

      xyunits= 0.5 ! degrees
      rrfactor= 2.0! 1.1 !4.0

      factlabel= 1.4
      factplot= 1.1
      facty=  1.1 
      
      fg1= 0.1

      
c      print*,'Will call rdWMAP.'
c      call rdWMAP(npix,indir2,nindir,
c     _     fileraw,nfraw,iwring,
c     _     temp,errtemp,gallong,gallat,tempcp)
c      print*,'Have called rdWMAP.'

c     call rdWMAP

c      call smooWMAP(new,sigdeg,resdeg, indir2,nindir,
c     _     temp,errtemp,gallong,gallat,tempcp))      

c     ---- circles-0.1.14
      i= pxstat()
      if(i.ne.0.and.(nowarn.ne.1))then
         print*,'rdWMAP WARNING: pxstat returned ',i,
     _        ' - there is probably a memory allocation problem.'
         print*,'Did you forget to call pxallo?'
      endif
      if(iverb.eq.1)then
         print*,'rddata: checked that memory has been allocated:',
     _        ' status OK.'
         print*,'rddata: npix, npixcp=',npix,npixcp
      endif


      measur=.true.
      noise=.false.


      dgcl=0.0
      dgcb=0.0

c     ---- simple test xytolb ----
      if(iverb.eq.1)then
         print*,'glpix1 (.,1,0) ',glpix1(1,1,0),glpix1(35,1,0),
     _        glpix1(69,1,0),glpix1(103,1,0),glpix1(137,1,0)
         print*,'gbpix1 (.,1,0) ',gbpix1(1,1,0),gbpix1(35,1,0),
     _        gbpix1(69,1,0),gbpix1(103,1,0),gbpix1(137,1,0)
      endif

      do while(1+1.eq.3)
         write(6,'(a,$)')'Enter iv,ipair,x,y: '
         read(5,*)iv,ipair,x,y
         call xytolb(iv,ipair,x,y, dgcl,dgcb, gall,galb,istatus)
         print*,'gall,galb,istatus=',gall,galb,istatus
      enddo
      print*,' '


c     ---- better test xytolb: should be consistent with plotcircles ----
      do iv=1,6
         rcircle= asin( wmodc(iv,jv) /rSLSc ) /pi_180 ! degrees
c         rcircle= 90.0 !! test only !!

         if(test)print*,'rcircle (degrees)=',rcircle

         do itheta=1,maxtheta
            frac =  (real(itheta)-0.5)/real(maxtheta) ! fraction of 2pi
     _           *2.0*pi         !  -> angle in radians
            rMpc(itheta,iv)= frac
     _           *wmodc(iv,jv)   ! -> Mpc

            xx= cos(frac)*rcircle
            yy= sin(frac)*rcircle
            if(test)print*,'xx,yy=',xx,yy

            do ipair=1,2
               call xytolb(iv,ipair,xx,yy, 0.0,0.0,
     _              gall,galb,istatus)
               
               if(test)print*,'gall,galb=',gall,galb
               if(istatus.eq.0)then
                  ttplot(itheta,ipair,iv)=
     _                 wmaplin(gall,galb,
     _                 temp,errtemp,gallong,gallat)
                  if(test)
     _                 print*,'itheta,ipair,iv,ttplot',
     _                 '(itheta,ipair,iv)=',
     _                 itheta,ipair,iv,ttplot(itheta,ipair,iv)
               else
                  ttplot(itheta,ipair,iv)=-99.9
               endif
            enddo
         enddo

         ch4='____'
         write(ch4(2:2),'(i1)')iv
         iplot=0
         write(ch4(4:4),'(i1)')iplot


         xmax= rMpc(maxtheta,1)
         ymax= 0.1
         call pgbegin(0,'disc'//ch4//'.ps/vps',1,1)              

         call pgvsize(1.0,7.5,1.0,6.0)
         call pgslw(3)
         call pgscf(2)
         call pgsch(1.2)
               
         call pgwindow(0.0,xmax, -ymax,ymax)
         call pgbox('bcnts',0.,0,'bcnts',0.,0)

         call pgsls(1)
         call pgline(maxtheta,rMpc(1,iv),ttplot(1,1,iv))
         call pgsls(3)
         call pgline(maxtheta,rMpc(1,iv),ttplot(1,2,iv))
         call pglabel('distance along circle (h\\u-1\\dMpc)',
     _        '\\gdT/T (mK)',' ')

         call pgend

      enddo   !       do iv=1,6



c     --- circles-0.1.15 
c     See if there is a maximum correlation as a function of
c     (1) the rotation angle (e.g. -36 degrees)
c     (2) the circle radius.

      cmax=-9e9
      dmax=-9e9
      do icir=1,ncir
         rcircle= rcir_1 + real(icir-1)*dcir ! very local copy
         rcir_r(icir)= rcircle
         do iv=1,6
            if(test)print*,'rcircle (degrees)=',rcircle

c            do itheta=1,maxtheta
c               frac =  (real(itheta)-0.5)/real(maxtheta) ! fraction of 2pi
c     _              *2.0*pi     !  -> angle in radians
c               rMpc_r(itheta,icir,iv)= frac
c     _              *wmodc(iv,jv) ! -> Mpc
c            enddo
            do ithoff=1,nthoff
c               frac =  (real(ithoff)-0.5)/real(nthoff) ! fraction of 2pi
               frac =  (real(ithoff-1))/real(nthoff) ! fraction of 2pi
               theta(ithoff,icir,iv)= frac * 360.0 ! angle in degrees
C     --- see also  frac2  below ---
            enddo

            do ithoff=1,nthoff ! ithoff= itheta offset
               corr(ithoff,icir,iv)=0.0
               diff(ithoff,icir,iv)=0.0
               diffsq(ithoff,icir,iv)=0.0
               ncorr(ithoff,icir,iv)=0
            
               do itheta=1,maxtheta
                  frac1 =  (real(itheta)-0.5)/real(maxtheta) ! fraction of 2pi
     _                 *2.0*pi  !  -> angle in radians
c                  frac2 =  (real(itheta+ithoff)-0.5)/real(maxtheta) ! fract of 2pi
c     _                 *2.0*pi  !  -> angle in radians
                  frac2= frac1 + real(ithoff-1)/real(nthoff) *2.0*pi

c     --- version 0.1.26   15.02.2006
                  frac2= frac2 - zerotwist*pi_180 ! cancel zeropoint

c                  frac2= frac2 +2.0*pi/5.0 !pi/2
c     _                 + pi 
c+ 10.0*pi_180

c     -- it doesn't matter if frac2 > 2pi, because it's only used for sin, cos --
                  xx1= cos(frac1)*rcircle
                  yy1= sin(frac1)*rcircle
                  xx2= cos(frac2)*rcircle
                  yy2= sin(frac2)*rcircle
c     if(test)print*,'xx1,yy1=',xx1,yy1,'xx2,yy2=',xx2,yy2
                  
                  ipair=1
                  call xytolb(iv,ipair,xx1,yy1, 0.0,0.0,
     _                 gall,galb,istatus)
                  
c     if(test.and.ithoff.lt.10)print*,'gall,galb=',gall,galb
                  if(istatus.eq.0)then
                     tt1= 
     _                    wmaplin(gall,galb,
     _                    temp,errtemp,gallong,gallat)
                     ipair=2
                     call xytolb(iv,ipair,xx2,yy2, 0.0,0.0,
     _                    gall,galb,istatus)
                     if(istatus.eq.0)then
                        tt2= 
     _                       wmaplin(gall,galb,
     _                       temp,errtemp,gallong,gallat)
                        corr(ithoff,icir,iv)=corr(ithoff,icir,iv)+
     _                       tt1*tt2
                        diff(ithoff,icir,iv)=diff(ithoff,icir,iv)+
     _                       abs(tt1-tt2)
                        diffsq(ithoff,icir,iv)=
     _                       diffsq(ithoff,icir,iv)+
     _                       (tt1-tt2)*(tt1-tt2)
                        ncorr(ithoff,icir,iv)=ncorr(ithoff,icir,iv)+1
                     endif      !                   if(istatus.eq.0)then
                  endif         !                   if(istatus.eq.0)then

               enddo !          ! do itheta=1,maxtheta
               
               if(ncorr(ithoff,icir,iv).gt.0)then
                  corr(ithoff,icir,iv)=corr(ithoff,icir,iv)/
     _                 real(ncorr(ithoff,icir,iv))
                  diff(ithoff,icir,iv)=diff(ithoff,icir,iv)/
     _                 real(ncorr(ithoff,icir,iv))
                  diffsq(ithoff,icir,iv)=diffsq(ithoff,icir,iv)/
     _                 real(ncorr(ithoff,icir,iv))
                  if(diffsq(ithoff,icir,iv).gt.tolsq)then
                     diffsq(ithoff,icir,iv)=sqrt(
     _                    diffsq(ithoff,icir,iv))
                     dmax=max(dmax,diffsq(ithoff,icir,iv))
                  else
                     diffsq(ithoff,icir,iv)= -99.9
                  endif
   
                  cmax=max(cmax,corr(ithoff,icir,iv))
               else
                  corr(ithoff,icir,iv)=-99.9
                  diff(ithoff,icir,iv)=-99.9
                  diffsq(ithoff,icir,iv)=-99.9
               endif

               if(test.and.1+1.eq.3)
     _              print*,'icir,iv,ithoff=',icir,iv,ithoff,
     _              'corr(...),diffsq(...)=',
     _              corr(ithoff,icir,iv),
     _              diffsq(ithoff,icir,iv)
            enddo                 !    do ithoff=1,maxtheta ! ithoff= itheta offset
         enddo                  !          do iv=1,6
      enddo                     ! do icir=1,ncir

      do icir=1,ncir
         do iv=1,6
            ch4='____'
            write(ch4(2:2),'(i1)')iv
            if(icir.le.9)then
               write(ch4(4:4),'(i1)')icir
            elseif(icir.le.99)then
               write(ch4(3:4),'(i2)')icir
            else
               print*,'WARNING: problem in ps filenames!'
               write(ch4(2:4),'(i3)')icir               
               write(ch4(1:1),'(i1)')iv
            endif
                  
            ch5='     '
            write(ch5,'(f5.1)')rcir_r(icir)

c            xmax= rMpc_r(maxtheta,icir,iv)
            xmax= theta(nthoff,icir,iv)
            ymax= cmax
            call pgbegin(0,'s_rot'//ch4//'.ps/vps',1,1)              

            call pgvsize(1.0,7.5,1.0,6.0)
            call pgslw(3)
            call pgscf(2)
            call pgsch(1.2)
               
            call pgwindow(0.0,xmax, -ymax,ymax)
            call pgbox('bcnts',0.,0,'bcnts',0.,0)

            call pgsls(1)
c            call pgline(maxtheta,rMpc_r(1,icir,iv),
            call pgline(nthoff,theta(1,icir,iv),
     _           corr(1,icir,iv))
            call pglabel('offset along circle (degrees)',
     _           '(\\gdT/T)\\d1\\u (\\gdT/T)\\d2\\u  (mK\\u2\\d)',
     _           ' ')

            call pgptxt(0.05*xmax,0.90*ymax,0.0,0.0,
     _           'circle '//ch4(1:1)//'  \\ga='//ch5)

            call pgend

         enddo                  !       do iv=1,6
      enddo                     !do icir=1,ncir


      ymax= cmax
      if(c_all)then
         call pgbegin(0,'s_rot_all.ps/vps',1,1)              

         call pgvsize(1.0,7.5,1.0,6.0)
         call pgslw(1)
         call pgscf(2)
         call pgsch(1.2)
         call pgsls(1)
         do icir=1,ncir
            do iv=1,6
c     xmax= rMpc_r(maxtheta,icir,iv)
               xmax= theta(nthoff,icir,iv)
               call pgwindow(0.0,xmax, -ymax,ymax)
               call pgbox('bcnts',0.,0,'bcnts',0.,0)
c     call pgline(maxtheta,rMpc_r(1,icir,iv),
               call pgline(nthoff,theta(1,icir,iv),
     _              corr(1,icir,iv))
               call pglabel('offset along circle (degrees)',
     _              '(\\gdT/T)\\d1\\u (\\gdT/T)\\d2\\u'//
     _              '(\\gmK\\u2\\d)',
     _              ' ')

            enddo               !       do iv=1,6
         enddo                  !do icir=1,ncir
         call pgend
      endif


      cm_max=-9e9
      dm_max=-9e9
      do itheta=1,maxtheta
         c_mean(itheta)=0.0
         d_mean(itheta)=0.0
         dsmean(itheta)=0.0
         c_med(itheta)=0.0
         ii=0
         do icir=1,ncir
            c_meac(itheta,icir)=0.0
            d_meac(itheta,icir)=0.0
            dsmeac(itheta,icir)=0.0
            c_medc(itheta,icir)=0.0
            ivtot=0
            do iv=1,6
               iv_ex=ivexclude(iv)
c     
               if(  
     _              (.not.iv_ex)
c                    .and. ! WARNING: iv_ex is arbitrary gal cut 
c     --- 0.1.26 commenting out   gbpix1, gbpix2 cuts --- 
c     _              (abs(gbpix1(itheta,iv,jv)).gt.bcut)
c     _              .and.(abs(gbpix2(itheta,iv,jv)).gt.bcut)
     _              )then
                  ivtot=ivtot+1
                  if(corr(itheta,icir,iv).gt.-99.0)then
                     if(ii.gt.mxctmp)then
                        print*,'mapdiscs: WARNING:',
     _                       ' ii,mxctmp=',ii,mxctmp
                     else
                        ii=ii+1
                        c_mean(itheta)=c_mean(itheta) + 
     _                       corr(itheta,icir,iv)
                        c_tmp(ii)=corr(itheta,icir,iv)
                        d_mean(itheta)=d_mean(itheta) + 
     _                       diff(itheta,icir,iv)
                        dsmean(itheta)=dsmean(itheta) + 
     _                       diffsq(itheta,icir,iv)
     _                       *diffsq(itheta,icir,iv)
                        d_tmp(ii)=diff(itheta,icir,iv)

                        c_meac(itheta,icir)=c_meac(itheta,icir)+
     _                       corr(itheta,icir,iv)
                        dsmeac(itheta,icir)=dsmeac(itheta,icir)+
     _                       diffsq(itheta,icir,iv)
     _                       *diffsq(itheta,icir,iv)
                     endif
                  endif
               else
                  if(test.and.1+1.eq.3)
     _                 print*,'bcut-test: itheta,icir,iv=',
     _                 itheta,icir,iv,
     _                 'abs(gb... )= ',
     _                 abs(gbpix1(itheta,iv,jv)),
     _                 abs(gbpix2(itheta,iv,jv))
               endif            !if(.not.(ivexclude(iv)))then
            enddo !            do iv=1,6

c            ivtot=6
            c_meac(itheta,icir)=c_meac(itheta,icir)/real(ivtot)
            if(dsmeac(itheta,icir).gt.tolsq)then
               dsmeac(itheta,icir)=sqrt( 
     _              dsmeac(itheta,icir)/real(ivtot) )
            else
               dsmeac(itheta,icir)=-99.9
            endif
         enddo !          do icir=1,ncir

         if(ii.gt.1)then
            c_mean(itheta)=c_mean(itheta)/real(ii)
            d_mean(itheta)=d_mean(itheta)/real(ii)
            dsmean(itheta)=dsmean(itheta)/real(ii)
            if(dsmean(itheta).gt.tolsq)then
               dsmean(itheta)=sqrt(dsmean(itheta))
            else
               dsmean(itheta)=-99.9
            endif

            cm_max=max(cm_max,c_mean(itheta))
            dm_max=max(dm_max,dsmean(itheta))
            call r4sort(c_tmp,ii)

            if(test.and.(itheta/120)*120.eq.itheta)then
               print*,'mapdiscs: ',
     _              '(c_tmp(j),j=1,ii)=',
     _              (c_tmp(j),j=1,ii)
            endif
            if(2*(ii/2).eq.ii)then
               c_med(itheta)=(c_tmp( (ii+1)/2 ) +
     _              c_tmp( (ii+1)/2 +1)  )*0.5
            else
               c_med(itheta)=c_tmp( (ii+1)/2 ) 
            endif
            cm_max=max(cm_max,c_med(itheta))
         endif
      enddo                     !      do itheta=1,maxtheta


c     --- optimal correlation, rms difference ---
      do icir=1,ncir
         c_opt(icir)=0.0
         s_opt(icir)=0.0
         ivtot=0
         do iv=1,6
            iv_ex=ivexclude(iv)
            if(.not.iv_ex)ivtot=ivtot+1

c     --- correlation 
            cbest= -9e9 ! corr(1,icir,iv)
            thbest= zerotwist + theta(1,icir,iv) 
            if(thbest.gt.180.0)thbest=thbest-360.0
            do ith=1,nthoff
               if(corr(ith,icir,iv).gt.cbest)then
                  cbest= corr(ith,icir,iv)
                  thbest= zerotwist + theta(ith,icir,iv) 
                  if(thbest.gt.180.0)thbest=thbest-360.0
               endif
            enddo
            c_opti(iv,icir)= cbest
            ctopti(iv,icir)= thbest
            if(.not.iv_ex)then
               c_opt(icir)=c_opt(icir)+ c_opti(iv,icir)
               ctopt(icir)=ctopt(icir)+ ctopti(iv,icir)
            endif
            
c     --- rms difference 
            sbest= 9e9! diffsq(1,icir,iv)
            thbest= zerotwist + theta(1,icir,iv) 
            if(thbest.gt.180.0)thbest=thbest-360.0
            do ith=1,nthoff
               if((diffsq(ith,icir,iv).gt.-99.0)
     _              .and.(diffsq(ith,icir,iv).lt.sbest))then
                  sbest= diffsq(ith,icir,iv)
                  thbest= zerotwist + theta(ith,icir,iv) 
                  if(thbest.gt.180.0)thbest=thbest-360.0
               endif
            enddo
            s_opti(iv,icir)= sbest
            stopti(iv,icir)= thbest
            if(.not.iv_ex)then
               s_opt(icir)=s_opt(icir)+ s_opti(iv,icir)
               stopt(icir)=stopt(icir)+ stopti(iv,icir)
            endif
         enddo                  !         do iv=1,6
         c_opt(icir)=c_opt(icir)/real(ivtot)
         ctopt(icir)=ctopt(icir)/real(ivtot)
         s_opt(icir)=s_opt(icir)/real(ivtot)
         stopt(icir)=stopt(icir)/real(ivtot)
      enddo                     !      do icir=1,ncir

c     ---- more robust method: use the mean functions directly
c     --- optimal correlation, rms difference ---
      do icir=1,ncir
c     --- correlation 
         cbest= -9e9            
         thbest= zerotwist + theta(1,icir,1) 
         if(thbest.gt.180.0)thbest=thbest-360.0
         do ith=1,nthoff
            if(c_meac(ith,icir).gt.cbest)then 
               cbest= c_meac(ith,icir)
               thbest= zerotwist + theta(ith,icir,1) 
               if(thbest.gt.180.0)thbest=thbest-360.0
            endif
         enddo
         c_opt(icir)=cbest
         ctopt(icir)=thbest
         if(test)print*,'icir=',icir,c_opt(icir),ctopt(icir)

c     --- findopt version --- WARNING: overrides previous calculation
         modif_y=0
         iperiodic=1
         call findopt(nthoff, c_meac(1,icir), ithsmoo, modif_y,
     _        iperiodic,
     _        imin,imax,ihwmin,ihwmax)


         thbest= zerotwist + theta(imax,icir,1) 
         if(thbest.gt.180.0)thbest=thbest-360.0
         ctopt(icir)= thbest    

         ct_hw(icir,1)= (theta(2,icir,1)-theta(1,icir,1))
     _        * real(ihwmax)

c     --- v0.1.24 --- error bars ----
         ct_hw(icir,2)= ctopt(icir) - ct_hw(icir,1)
         ct_hw(icir,3)= ctopt(icir) + ct_hw(icir,1)

c     --- earlier than 0.1.23 modulus error bars
c         ct_hw(icir,2)= mod( ctopt(icir) - ct_hw(icir,1)+720.0,360.0)
c         ct_hw(icir,3)= mod( ctopt(icir) + ct_hw(icir,1)+720.0 ,360.0)
c         if(ct_hw(icir,2).gt.180.0)
c     _        ct_hw(icir,2)=ct_hw(icir,2) -360.0
c         if(ct_hw(icir,3).gt.180.0)
c     _        ct_hw(icir,3)=ct_hw(icir,3) -360.0



c     --- rms difference
         sbest= 9e9            
         thbest= zerotwist + theta(1,icir,1) 
         if(thbest.gt.180.0)thbest=thbest-360.0
         do ith=1,nthoff
            if((dsmeac(ith,icir).gt.-99.0).and.
     _           (dsmeac(ith,icir).lt.sbest))then
               sbest= dsmeac(ith,icir)
               thbest= zerotwist + theta(ith,icir,1) 
               if(thbest.gt.180.0)thbest=thbest-360.0
            endif
         enddo
         s_opt(icir)=sbest
         stopt(icir)=thbest
         if(test)print*,'icir=',icir,s_opt(icir),stopt(icir)


c     --- findopt version --- WARNING: overrides previous calculation
         modif_y=0
         iperiodic=1
         call findopt(nthoff, dsmeac(1,icir), ithsmoo, modif_y,
     _        iperiodic,
     _        imin,imax,ihwmin,ihwmax)

         thbest= zerotwist + theta(imin,icir,1) 
         if(thbest.gt.180.0)thbest=thbest-360.0
         stopt(icir)= thbest


         st_hw(icir,1)= (theta(2,icir,1)-theta(1,icir,1))
     _        * real(ihwmin)
c     --- v0.1.24 --- error bars ----
         st_hw(icir,2)= stopt(icir) - st_hw(icir,1)
         st_hw(icir,3)= stopt(icir) + st_hw(icir,1)

c     --- earlier than 0.1.23 modulus error bars
c         st_hw(icir,2)= mod( stopt(icir) - st_hw(icir,1)+720.0,360.0)
c         st_hw(icir,3)= mod( stopt(icir) + st_hw(icir,1)+720.0,360.0)
c         if(st_hw(icir,2).gt.180.0)
c     _        st_hw(icir,2)=st_hw(icir,2) -360.0
c         if(st_hw(icir,3).gt.180.0)
c     _        st_hw(icir,3)=st_hw(icir,3) -360.0


      enddo                     !      do icir=1,ncir



      call pgbegin(0,'s_rot_opt.ps/vps',1,1)              
      iv=1
      icir=1
c      xmax= rMpc_r(maxtheta,icir,iv)
      xmax= 90.0
c     --- v0.1.24 and later: 270.0, not 180.0 - to show error bars
      ymax= 200.0 ! 180.0 !    

      call pgvsize(1.0,7.5,1.0,7.5)
      call pgslw(5)
      call pgscf(2)
      call pgsch(1.2)
      call pgsls(1)
      call pgwindow(0.0,xmax, -ymax,ymax)
      call pgbox('bcnts',0.,0,'bcnts',0.,0)
c     ---- individual 6 pairs
      do icir=1,0 ! ncir
         do iv=1,6
c            call pgpoint(1,rcir_r(icir),ctopti(iv,icir),4)
c            call pgpoint(1,rcir_r(icir),stopti(iv,icir),5)
         enddo
      enddo
      call pgline(ncir,rcir_r,ctopt)
      call pgslw(1)
      call pgline(ncir,rcir_r,ct_hw(1,2))
      call pgline(ncir,rcir_r,ct_hw(1,3))
      call pgslw(5)

      call pgsls(3)

      call pgline(ncir,rcir_r,stopt)
      call pgslw(1)
      call pgline(ncir,rcir_r,st_hw(1,2))
      call pgline(ncir,rcir_r,st_hw(1,3))
      call pgslw(3)

      call pgsls(1)

      call pglabel('radius \\ga of circle (degrees)',
     _     'optimal offset angles (degrees)',
     _     ' ')
      call pgend




c     ---- average correlation ----
      call pgbegin(0,'s_rot_stat.ps/vps',1,1)              

      iv=1
      icir=1
c      xmax= rMpc_r(maxtheta,icir,iv)
      xmax= theta(nthoff,icir,iv)
c      xmax= 180.0
      ymax= 1.1*cm_max


      if(microK)then
         ymax= ymax*1000.0*1000.0
         do ith=1,maxtheta
            if(c_mean(ith).gt.-99.0)then
               c_mean(ith)=c_mean(ith)*1000.0*1000.0
            endif
         enddo
      endif

      if(test)print*,'doing s_rot_stat.ps: xmax,ymax=',xmax,ymax

      call pgvsize(1.0,7.5,1.0,6.0)
      call pgslw(3)
      call pgscf(2)
      call pgsch(1.2)
      call pgsls(1)
      call pgwindow(0.0,xmax, -ymax,ymax)
c      call pgwindow(-xmax,xmax, -ymax,ymax)

      call pgbox('bcnts',0.,0,'bcnts',0.,0)
c      call pgline(maxtheta,rMpc_r(1,icir,iv),
      call pgline(nthoff,  theta(1,icir,iv),
     _     c_mean)
      do icir=1,0! ncir
         do iv=1,6
            call pgpoint(1,ctopti(iv,icir),c_opti(iv,icir),4)
            call pgpoint(1,stopti(iv,icir),s_opti(iv,icir),5)
         enddo
         call pgpoint(1,ctopt(icir),c_opt(icir),26)
         call pgpoint(1,stopt(icir),s_opt(icir),26)
      enddo


c      call pgsls(3)
c      call pgline(maxtheta,rMpc_r(1,icir,iv),
c     _     c_med)


c     'optimal offset angles (deg)', 
      call pglabel('offset along circle (degrees)',
     _     '(\\gdT/T)\\d1\\u (\\gdT/T)\\d2\\u  (\\gmK\\u2\\d)',
     _     ' ')


      call pgmove(360.0 + zerotwist, 0.0)
      call pgdraw(360.0 + zerotwist, ymax)
      

      call pgend


c     ------------------------------------------
      call pgbegin(0,'s_rot_diff.ps/vps',1,1)              

      iv=1
      icir=1
c      xmax= rMpc_r(maxtheta,icir,iv)
      xmax= theta(nthoff,icir,iv)
c      xmax= 180.0
      ymax= 1.1*dm_max

      if(microK)then
         ymax= ymax*1000.0
         do ith=1,maxtheta
            dsmean(ith)=dsmean(ith)*1000.0
         enddo
      endif

      if(test)print*,'doing s_rot_diff.ps: xmax,ymax=',xmax,ymax

      call pgvsize(1.0,7.5,1.0,6.0)
      call pgslw(3)
      call pgscf(2)
      call pgsch(1.2)
      call pgsls(1)
c      call pgwindow(0.0,xmax, -ymax,ymax)
      call pgwindow(0.0,xmax, 0.0,ymax)
c      call pgwindow(-xmax,xmax, -ymax,ymax)
      call pgbox('bcnts',0.,0,'bcnts',0.,0)

      call pgsls(1)
c      call pgline(maxtheta,rMpc_r(1,icir,iv),
      call pgline(nthoff, theta(1,icir,iv),
     _     dsmean)

c     'optimal offset angles (deg)', 
      call pglabel('offset along circle (degrees)',
     _     'rms[(\\gdT/T)\\d1\\u - (\\gdT/T)\\d2\\u]  (mK)',
     _     ' ')

      call pgmove(360.0 + zerotwist, 0.0)
      call pgdraw(360.0 + zerotwist, ymax)

      call pgend


c     ---- 0.1.26 ---- contour plot
c            write(ch5,'(f5.1)')rcir_r(icir)
      trcorr(3)=0.0
      trcorr(5)=0.0
      
      trcorr(1)=0.0
      trcorr(2)=xmax/real(nthoff)
      trcorr(4)=rcir_r(1) -dcir
      trcorr(6)= dcir
      ncont=20
      do icont=1,ncont
         cocont(icont)=100.0*real(icont)
c         if(.not.(microK)) 
         cocon2(icont)= cocont(icont) ! 0.1.43
         cocont(icont)=
     _        cocont(icont)*1e-6
      enddo

      do iv=1,6
         ch4='    '
         write(ch4(1:1),'(i1)')iv
         call pgbegin(0,'s_cont_'//ch4(1:1)//'.ps/vps',1,1)              

         call pgvsize(1.0,7.5,1.0,6.0)
         call pgslw(3)
         call pgscf(2)
         call pgsch(1.2)
         
         call pgwindow(0.0,xmax, rcir_r(1),rcir_r(ncir))
         call pgbox('bcnts',0.,0,'bcnts',0.,0)
         call pgcont(corr(1,1,iv),
     _        maxtheta,maxcir, 1,nthoff, 1,ncir,
     _        cocont,ncont,
     _        trcorr)
         do icont=1,ncont
            ii = (icont/2)*2
            if(ii.eq.icont)then
               write(ch7,'(f7.0)')cocon2(icont)
               call pgconl(corr(1,1,iv),
     _              maxtheta,maxcir, 1,nthoff, 1,ncir,
     _              cocont(icont), !ncont,
     _              trcorr,
     _              ch7,20,10)  ! last two parameters recommended by pgplot.doc
            endif
         enddo
         call pglabel('offset along circle (degrees)',
     _        'circle radius \\ga (degrees)',
     _        ' ')
         
         call pgend
      enddo



c     ---- all sigma plots together ----
      if(c_all)then
         call pgbegin(0,'s_rot_dall.ps/vps',1,1)              

         call pgvsize(1.0,7.5,1.0,6.0)
         call pgslw(3)
         call pgscf(2)
         call pgsch(1.2)
         call pgsls(1)
         do icir=1,ncir
            do iv=1,6
c     xmax= rMpc_r(maxtheta,icir,iv)
               xmax= theta(nthoff,icir,iv)

c               call pgwindow(0.0,xmax, -ymax,ymax)
               call pgwindow(0.0,xmax, 0.0,ymax)
               call pgbox('bcnts',0.,0,'bcnts',0.,0)
c     call pgline(maxtheta,rMpc_r(1,icir,iv),
               call pgline(nthoff,theta(1,icir,iv),
     _              diffsq(1,icir,iv))
               call pglabel('offset along circle (degrees)',
     _              'rms[(\\gdT/T)\\d1\\u - '//
     _              '(\\gdT/T)\\d2\\u]  (mK)',
     _              ' ')

               call pgmove(360.0+zerotwist,0.0)
               call pgdraw(360.0+zerotwist,ymax)

            enddo               !       do iv=1,6
         enddo                  !do icir=1,ncir
         call pgend
      endif


c      return 



c     ==== WARNING: the section below, i.e. the old mapdisc part 
c     has not been debugged as of circles-0.1.14  ====


c     ---- 
      do iv=1,6                 ! loop through six pairs of faces
c     --- calculate gray scale plot ---
         iplot=1
         if(interactive)
     _        call pgbegin(0,'/xw',1,1)


c         do iplot=1,4
         do while(iplot.le.4.or.
     _        (interactive.and.(.not.next)) )
            if(interactive.and.next)then
               dgcl=0.0
               dgcb=0.0
            endif
               
            if(iplot.le.2)then
               ipair=iplot
            else
               ipair=1
            endif

            rcircle= asin( wmodc(iv,jv) /rSLSc ) /pi_180 ! degrees
            if(iverb.eq.1)print*,'iv,rcircle=',iv,rcircle
c            if(iplot.le.2)then
c               rr= rcircle
c            else
c               rr= 1.5*rcircle ! 50% more in radius
c            endif
            rr= rcircle *rrfactor 
            diam= 2.0*rr
            np= max(1,nint( diam/ xyunits )) 
            if(iverb.eq.1)print*,'rr,diam,np=',rr,diam,np

            i1=1
            i2=np
            j1=1
            j2=np
            do jj=j1,j2
               do ii=i1,i2
                  agray(ii,jj)=0.0
               enddo
            enddo

            tr(1)= -rr
            tr(4)= -rr
            tr(2)= xyunits
            tr(6)= xyunits
            tr(3)=0.0
            tr(5)=0.0

            if(interactive)then
               tr(1)= -rr 
               tr(4)= -rr 
            endif

            gmax= -9e9
            gmin= 9e9
            do jj=j1,j2
               do ii=i1,i2
                  call ij2xy(ii,jj,tr, xx,yy)
                  call xytolb(iv,ipair, xx,yy, dgcl,dgcb, 
     _                 gall,galb,istat1)
                  if(istat1.eq.0.and.abs(galb).gt.bcut)then
                     tt= wmaplin(gall,galb,
     _                    temp,errtemp,gallong,gallat)
                  else
                     tt= 0.0
                  endif

                  if(iplot.le.2)then
                     agray(ii,jj)= tt

cc                     if(xx.gt.0.0)agray(ii,jj)=fg1 !! test only !! to sort out fg/bg
                  else
                     ip2=2

                     iv2=iv
                     if(bad_iv)iv2=mod(iv-1 +1,6)+1
                     
                     call xytolb(iv2,ip2, xx,yy,  dgcl,dgcb, 
     _                    gall2,galb2,istat2)
                     if(istat1.eq.0.and.istat2.eq.0.and.
     _                    abs(galb).gt.bcut.and.
     _                    abs(galb2).gt.bcut)then
                        tt2= wmaplin(gall2,galb2,
     _                       temp,errtemp,gallong,gallat)
                        if(iplot.eq.3)then
c                           print*,'==iplot3 line'
                           agray(ii,jj)= abs(tt-tt2)
                        elseif(iplot.eq.4)then
c                           print*,'==iplot4 line'
                           agray(ii,jj)= tt*tt2
                           gmax=max(gmax,agray(ii,jj))
                           gmin=min(gmin,agray(ii,jj))
                        endif
                     else
                        agray(ii,jj)=0.0
                     endif
                  endif
               enddo
            enddo

            if(iplot.eq.4)print*,'gmin,gmax=',gmin,gmax
            if(iplot.le.2)then
               fg= fg1 ! mK
               bg= -fg1
            elseif(iplot.eq.3)then
c     --- abs differences --- white = SMALL diff ---
               fg= fg1 ! mK
               bg= 0.0
            elseif(iplot.eq.4)then
c     --- correlations --- reversed shades: white = HIGH numerical value ---
               fg= -fgcorr/2.0
               bg= fgcorr

c               fg= gmin 
c               bg= gmax !*0.5
c               fg= 0.01
c               bg= -fg
            endif

c     --- make plot ---
            xmax= factplot* rr
            ch4='____'
            write(ch4(2:2),'(i1)')iv
            write(ch4(4:4),'(i1)')iplot

            if(.not.interactive)then
               call pgbegin(0,'disc'//ch4//'.ps/vps',1,1)              
            endif

            call pgvsize(1.5,6.5,1.0,6.0*(1.0+facty)/2.0)
            call pgslw(3)
            call pgscf(2)
            call pgsch(1.2)
               
            call pgwindow(-xmax,xmax, -xmax,xmax *facty) 
            call pgbox('bcnts',0.,0,'bcnts',0.,0)

            call pggray(agray,mxdim,mxdim, i1,i2,j1,j2,
     _           fg,bg,tr)

            do ith=1,nthc(iv,jv), nthc(iv,jv)/8
               ang= real(ith)/real(nthc(iv,jv)) *2.0*pi
               cth= cos(ang)
               sth= sin(ang)
               xx= cth*rr
               yy= sth*rr
               call xytolb(iv,ipair, xx,yy, dgcl,dgcb, 
     _              gl,gb,istatus)

               if(ipair.eq.1)then
                  write(ch13,'(a1,f5.1,a1,f5.1,a1)')
     _                 '(',glpix1(ith,iv,jv),
     _                 ',',gbpix1(ith,iv,jv),')'
               else
                  write(ch13,'(a1,f5.1,a1,f5.1,a1)')
     _                 '(',glpix2(ith,iv,jv),
     _                 ',',gbpix2(ith,iv,jv),')'
               endif

               xx= cth*rr *factlabel
               yy= sth*rr *factlabel
               call pgqch(qch)
               call pgsch(0.8)
               call pgptxt(xx,yy,0.0,0.5,ch13)
               call pgsch(qch)

               write(ch60,'(a)')'i= x               '//
     _              '                                        '
               if(iplot.eq.1)then
                  write(ch60(4:5),'(i1,a1)')iv,'A'
                  kk= 5
               elseif(iplot.eq.2)then
                  write(ch60(4:5),'(i1,a1)')iv,'B'
                  kk= 5
               elseif(iplot.eq.3)then
                  write(ch60(4:5),'(i1,a1)')iv,' '
                  write(ch60(7:25),'(a)')' |\\gD(\\gdT/T)|'
                  kk= 25
               else!if(iplot.eq.4)then
                  write(ch60(4:5),'(i1,a1)')iv,' '
                  write(ch60(7:60),'(a)')' (\\gdT/T)\\d1\\u'//
     _                 '(\\gdT/T)\\d2\\u'
                  kk= 60
               endif
               call pgptxt(0.0,xmax* 0.5*(0.9+facty),
     _              0.0,0.0,ch60(1:kk))
            enddo
            if(.not.interactive)call pgend

            if(.not.interactive)then
               iplot=iplot+1
            else
c               write(6,'(a,$)')'Any key to repeat or  n  for next: '
c               read(5,'(a)')ch1
               write(6,'(a,a,$)')'Enter dgcl,dgcb ',
     _              '(degrees, -99 to stop): '
               read(5,*)dgcl,dgcb
               next=.false.
C     2020-05-01: Call to grbpic is commented out as a relic of pgpglot.
C     This subroutine is likely to fail.
c               call grbpic
c               call pgpage
c               if(ch1.eq.'n'.or.ch1.eq.'N')then
               if(dgcl.lt.-98.9)then
                  next=.true.
                  iplot=iplot+1
               endif
            endif
         enddo                  !do iplot=1,4
         if(interactive) call pgend

      enddo                     ! iv=1,6

      end




      subroutine xytolb(iv,ipair,x,y,  dgcl,dgcb, 
     _     gall,galb,istatus)
c     INPUTS:
      integer*4  iv             ! circle number
      integer*4  ipair          ! 1 or 2
      real*4    x,y             ! coords in plot 
      real*4    xyunits         !e.g. in 0.5 deg units
      real*4    dgcl,dgcb       ! interactive shifts to glongc, glatc 

c     OUTPUTS:
      real*4    gall,galb       ! gal long and latitude
      integer*4    istatus         ! set to 2 if there's a gal cut problem


c     COMMON:
      parameter(pi=3.14159265359,pi_180=pi/180.0)
      include 'ncopies.dec'

      integer*4    nthc(-iimax:iimax,0:jjmax)
      real*4    glatc(2,-iimax:iimax,0:jjmax), ! 2 centres
     _     glongc(2,-iimax:iimax,0:jjmax), ! 2 centres
     _     wmodc(-iimax:iimax,0:jjmax), ! dtheta * rSLS
     _     rSLSc,
     _     dthMpc(-iimax:iimax,0:jjmax) ! dtheta * rSLS

      real*4    tt1c(maxtheta, -iimax:iimax,0:jjmax)
      real*4    tt2c(maxtheta, -iimax:iimax,0:jjmax)
      real*4    s1c(maxtheta, -iimax:iimax,0:jjmax)
      real*4    s2c(maxtheta, -iimax:iimax,0:jjmax)
c     -- store galactic long, lat at each pixel for double-checking --
      real*4    glpix1(maxtheta, -iimax:iimax,0:jjmax)
      real*4    gbpix1(maxtheta, -iimax:iimax,0:jjmax)
      real*4    glpix2(maxtheta, -iimax:iimax,0:jjmax)
      real*4    gbpix2(maxtheta, -iimax:iimax,0:jjmax)

      common /disccomm/ nthc,glatc,glongc,wmodc,rSLSc,dthMpc,
     _     tt1c,tt2c,s1c,s2c,glpix1,gbpix1,glpix2,gbpix2

C     INTERNAL:
      logical*4  test

      data tol /1e-4/
      data jv  /0/ ! hardwired for dodecahedron - only iv is used

c      data sign /-1.0/
c      data alpha /-11.0/ ! degrees
      
      save tol,jv
c     save sign,alpha

      test=.false.!.true. !

      istatus=0 ! OK no error by default

      if(abs(x).gt.tol)then
         phi = atan(y/x)
         if(x.lt.-tol) phi= pi + phi
         if(x.gt.tol.and.y.lt.-tol) phi= 2.0*pi + phi
      else
         if(y.gt.0.0)then
            phi= pi/2.0
         else
            phi= 3.0*pi/2.0
         endif
      endif
      if(test)print*,' '
      if(test)print*,'x,y,phi=',x,y,phi

      if(test)print*,'nthc(iv,jv)=',nthc(iv,jv)


      angdeg= sqrt(x*x+y*y)     ! in degrees ! RIGHT
      if(test)print*,'angdeg=',angdeg

c     WRONG
cc     -- 04.03.2004 - rotation not constant beyond or inside dodecahedron --
c      if(ipair.eq.2)then
c         phishift= sign* 0.2*pi 
c     _        *( cos(angdeg*pi_180) /cos(alpha*pi_180) -1.0 )
c         phi= phi+ phishift
c
c         phi= mod(phi,2.0*pi)
c         phi= max(tol, min(2.0*pi-tol, phi))
c
cc         if(phi.gt.(1.0-tol)*2.0*pi)then
cc            phi=max(tol,phi-2.0*pi)
cc         endif
cc         if(phi.lt.0.0)then
cc            phi=min(phi+2.0*pi
cc         endif
c      endif



c      ith= nint(phi/(2.0*pi) *real(nthc(iv,jv)))
c      ith= max(1,min( nthc(iv,jv), ith ))

      phidiscrete= phi/(2.0*pi) *real(nthc(iv,jv))
      ith1= int(phidiscrete)
      fracphi= phidiscrete - real(ith1)

      ith2= ith1+1
      if(ith1.eq.0) ith1= nthc(iv,jv)
      if(ith1.eq.nthc(iv,jv)) ith2= 1
      if(ith1.lt.1.or.ith1.gt.nthc(iv,jv).or.
     _     ith2.lt.1.or.ith2.gt.nthc(iv,jv))then
         print*,'WARNING: phi,nthc(iv,jv),ith1,ith2=',
     _        phi,nthc(iv,jv),ith1,ith2
      endif



      if(test)print*,'ipair,ith1,ith2=',ipair,ith1,ith2

      if(rSLSc.lt.tol)print*,'ERROR rSLSc=',rSLSc

      if(ipair.eq.1.and.
     _     ( (tt1c(ith1,iv,jv).lt.-9.0).or.
     _     (tt1c(ith2,iv,jv).lt.-9.0) )
     _     )then
         istatus=2
         return
      elseif(ipair.eq.2.and.
     _        ( (tt2c(ith1,iv,jv).lt.-9.0).or.
     _        (tt2c(ith2,iv,jv).lt.-9.0) )
     _        )then
         istatus=2
         return
      endif

c     ---- test only ----
      if(1+1.eq.3)then ! should fix:    ith -> ith1, ith2
         if(ipair.eq.1)then
            gall= glpix1(ith,iv,jv)
            galb= gbpix1(ith,iv,jv)
            return
         else !if(ipair.eq.2)then
            gall= glpix2(ith,iv,jv)
            galb= gbpix2(ith,iv,jv)
            return
         endif
      endif
            


c     --- WRONG      angdeg= sqrt(x*x+y*y) / xyunits ! in degrees

c     -- shifted earlier
c      angdeg= sqrt(x*x+y*y) ! in degrees ! RIGHT
c      if(test)print*,'angdeg=',angdeg

      gcl= glongc(ipair,iv,jv) + dgcl
      gcb= glatc(ipair,iv,jv) + dgcb

      xc= cos(gcb*pi_180) *cos(gcl*pi_180)
      yc= cos(gcb*pi_180) *sin(gcl*pi_180)
      zc= sin(gcb*pi_180) 

c      xc= cos(glatc(ipair,iv,jv)*pi_180) 
c     _     *cos(glongc(ipair,iv,jv)*pi_180)
c      yc= cos(glatc(ipair,iv,jv)*pi_180) 
c     _     *sin(glongc(ipair,iv,jv)*pi_180)
c      zc= sin(glatc(ipair,iv,jv)*pi_180) 

      if(ipair.eq.1)then
c         gl= (1.0-fracphi)*glpix1(ith1,iv,jv)
c     _        + fracphi*glpix1(ith2,iv,jv)
         call fracgl(fracphi,
     _        glpix1(ith1,iv,jv),glpix1(ith2,iv,jv),
     _        gl, jstatus)
         if(jstatus.eq.1.and.(.not.(nowarn.eq.1)))then
            print*,'mapdiscs: fracgl: WARNING: ',
     _           'error in longitude interpolation'
            print*,'ith1,ith2,glpix1(ith1,iv,jv),glpix1(ith2,iv,jv)='
            print*,ith1,ith2,glpix1(ith1,iv,jv),glpix1(ith2,iv,jv)
         endif

         gb= (1.0-fracphi)*gbpix1(ith1,iv,jv)
     _        + fracphi*gbpix1(ith2,iv,jv)

      else
c         gl= (1.0-fracphi)*glpix2(ith1,iv,jv)
c     _        + fracphi*glpix2(ith2,iv,jv)
         call fracgl(fracphi,
     _        glpix2(ith1,iv,jv),glpix2(ith2,iv,jv),
     _        gl, jstatus)
         if(jstatus.eq.1.and.(.not.(nowarn.eq.1)))then
            print*,'mapdiscs: fracgl: WARNING: ',
     _           'error in longitude interpolation'
            print*,'ith1,ith2,glpix2(ith1,iv,jv),glpix2(ith2,iv,jv)='
            print*,ith1,ith2,glpix2(ith1,iv,jv),glpix2(ith2,iv,jv)
         endif

         gb= (1.0-fracphi)*gbpix2(ith1,iv,jv)
     _        + fracphi*gbpix2(ith2,iv,jv)
      endif



      x2= cos(gb*pi_180)*cos(gl*pi_180)
      y2= cos(gb*pi_180)*sin(gl*pi_180)
      z2= sin(gb*pi_180)

      dd= dot(xc,yc,zc,x2,y2,z2)
c     --- normal to (xc,yc,zc) not yet normalised ---
      xn= x2 - xc *dd
      yn= y2 - yc *dd
      zn= z2 - zc *dd

c     --- normalise it ---
      xmod= sqrt(xn*xn+yn*yn+zn*zn)
      xn= xn/xmod
      yn= yn/xmod
      zn= zn/xmod

      cth= cos(angdeg*pi_180)
      sth= sin(angdeg*pi_180)
      x3= cth*xc + sth*xn
      y3= cth*yc + sth*yn
      z3= cth*zc + sth*zn

      call xyz_rda(x3,y3,z3, radius,dd,aa)
      gall= aa*15.0
      galb= dd
      
      if(test)print*,'gall,galb=',gall,galb

      return
      end


      subroutine ij2xy(i,j,tr,x,y)
C     INPUTS:
      integer  i,j
      real*4   tr(6)
C     OUTPUTS:
      real*4   x,y
      
      x = tr(1) + tr(2)*i + tr(3)*j
      y = tr(4) + tr(5)*i + tr(6)*j
      return
      end

      
      subroutine fracgl(frac,gl1,gl2,gl_out,jstatus) ! in degrees
c     ==== linearly interpolate 2 longitudes, checking for 360.0 boundary ====

      jstatus=0
      delgmx= 20.0 ! generous max
      
      if(abs(gl1-gl2).lt.delgmx)then
         gl_out= (1.0-frac)* gl1 + frac*gl2
      elseif(abs(gl1+360.0 -gl2).lt.delgmx)then
         gl_out= (1.0-frac)* (gl1+360.0) + frac*gl2
      elseif(abs(gl2+360.0 -gl1).lt.delgmx)then
         gl_out= (1.0-frac)* gl1 + frac*(gl2+360.0)
      else
         gl_out= gl1  ! arbitrary choice of one or another longitude - soft fail
         jstatus=1 ! warning - longitudes are too far apart
      endif
      return
      end


      
