/* 
   circles - calculate identified circles statistics from CMB data (cosmic topology)

   Copyright (C) 2004 Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

*/

#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include "system.h"


#include <stdlib.h>


#define EXIT_FAILURE 1

#if ENABLE_NLS
# include <libintl.h>
# define _(Text) gettext (Text)
#else
# define textdomain(Domain)
# define _(Text) Text
#endif
#define N_(Text) Text


#include "circles.h" 


char *xmalloc ();
char *xrealloc ();
char *xstrdup ();

int want_debug = 0;

/* allocate pixel data arrays */

int PXALLO(int *want_verbose, int *circles_npix,
	   float **temp, float **errtemp, float **gallong, float **gallat,
	   float **tempcp, int **k_ring)
{
  if(*want_verbose)
    printf("Will try allocating memory for temp()...\n");
  /*  (float **)*temp= xmalloc(CIRCLES_BYTES_IN_TEMP* CIRCLES_NPIX_MAXN); */
  *temp= (float *)xmalloc(CIRCLES_BYTES_IN_TEMP* CIRCLES_NPIX_MAXN);

  if(want_debug)
    {
      printf("pxallo: before   **temp= 4.3  value of **temp = %f;\n",**temp);
      **temp= 4.3;
      printf("pxallo: after   **temp= 4.3  value of **temp = %f;\n",**temp);
    };
  
  if(*temp==0)
    printf("This program will fail because insufficient memory (RAM+swap space?) is available. Sorry.\n");
  else
    if(*want_verbose)
      {
	/* printf("*want_verbose = %d",*want_verbose); */
	printf("Successfully allocated space for %d bytes at memory location %x \n", CIRCLES_BYTES_IN_TEMP* CIRCLES_NPIX_MAXN, *temp);
      };
  

  if(*want_verbose)
    printf("Will try allocating memory for errtemp()...\n");
  *errtemp= (float *)xmalloc(CIRCLES_BYTES_IN_TEMP* CIRCLES_NPIX_MAXN);
  if(*errtemp==0)
    printf("This program will fail because insufficient memory (RAM+swap space?) is available. Sorry.\n");
  else
    if(*want_verbose)
      {
	/* printf("*want_verbose = %d",*want_verbose); */
	printf("Successfully allocated space for %d bytes at memory location %x \n", CIRCLES_BYTES_IN_TEMP* CIRCLES_NPIX_MAXN, *errtemp);
      };
  


  if(*want_verbose)
    printf("Will try allocating memory for gallong()...\n");
  *gallong = (float *)xmalloc(CIRCLES_BYTES_IN_TEMP* CIRCLES_NPIX_MAXN);
  if(*want_verbose)
    printf("Will try allocating memory for gallat()...\n");
  *gallat= (float *)xmalloc(CIRCLES_BYTES_IN_TEMP* CIRCLES_NPIX_MAXN);
  if(*want_verbose)
    printf("Will try allocating memory for tempcp()...\n");
  *tempcp= (float *)xmalloc(CIRCLES_BYTES_IN_TEMP* CIRCLES_NPIX_MAXN);
  if(*want_verbose)
    printf("Will try allocating memory for k_ring()...\n");
  *k_ring= (int *)xmalloc(CIRCLES_BYTES_IN_TEMP* CIRCLES_NPIX_MAXN);
  if(*tempcp==0 || *k_ring==0)
    printf("This program will fail because insufficient memory (RAM+swap space?) is available. Sorry.\n");
  else
    if(*want_verbose)
      {
	/* printf("*want_verbose = %d",*want_verbose); */
	printf("Successfully allocated space for 4 * %d bytes at memory locations %x and %x and %x\n", 
	       CIRCLES_BYTES_IN_TEMP* CIRCLES_NPIX_MAXN, 
	       *gallong, *gallat, *tempcp, *k_ring);
      };
  


  *circles_npix= CIRCLES_NPIX;

  if(*temp != 0 && *errtemp !=0 && *gallong !=0 && *gallat != 0 
     && *tempcp!=0 && *k_ring!=0)
    pxalloc_status = 0;
  else
    pxalloc_status = 1;

  return 0;
}


/* allocate tempcp = copy of temp */

/* WARNING: deactivated as of circles-0.1.12 */

int PXAL_2(int *want_verbose, float **tempcp)
{
  if(*want_verbose)
    printf("Will try allocating memory for tempcp()...\n");
  *tempcp= (float *)xmalloc(CIRCLES_BYTES_IN_TEMP* CIRCLES_NPIX_MAXN);
  
  if(*tempcp==0)
    printf("Smoothing will fail because insufficient memory (RAM+swap space?) is available for the temporary array tempcp. Sorry.\n");
  else if(*want_verbose)
      {
	/* printf("*want_verbose = %d",*want_verbose); */
	printf("Successfully allocated space for %d bytes at memory location %x \n", CIRCLES_BYTES_IN_TEMP* CIRCLES_NPIX_MAXN, *tempcp);
      };
  

  if(*tempcp != 0)
    pxtempcp_status = 0;
  else
    pxtempcp_status = 1;

  return pxtempcp_status;
}


/* allocate tempcp = copy of temp */

int PXFR_2(int *want_verbose)
{

  /*  float *pp; */
  if(*want_verbose)
    printf("Will try freeing memory for tempcp()...");

  if(pxtempcp_status == 0)
    {
      if(*want_verbose)
	{
	  printf("It seems that the previous memory allocation\n");
	  printf(" was successful. So the pointer will be freed.\n");
	};
      /*      pp= *tempcp; */
      free(tempcp);
      pxtempcp_status = -1;  /* status back to "no allocation tried" */
      if(*want_verbose)
	printf("Have freed up  tempcp.\n");
    }
  else
    {
      if(*want_verbose)
	printf("No tempcp memory allocated, so none will be freed.\n");
    };
  return pxtempcp_status;
}
