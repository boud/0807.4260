C   circles - calculate identified circles statistics from CMB data (cosmic topology)
C
C   Copyright (C) 2004 Boud Roukema
C
C   This program is free software; you can redistribute it and/or modify
C   it under the terms of the GNU General Public License as published by
C   the Free Software Foundation; either version 2, or (at your option)
C   any later version.
C
C   This program is distributed in the hope that it will be useful,
C   but WITHOUT ANY WARRANTY; without even the implied warranty of
C   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C   GNU General Public License for more details.
C
C   You should have received a copy of the GNU General Public License
C   along with this program; if not, write to the Free Software Foundation,
C   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
C

C     ==== to read the WMAP linear combination data file ====
      subroutine rdWMAP(npix,indir2,nindir,
     _     fileraw,nfraw,iwring,
     _     filemask,nfmask,iwmask,
     _     toyf2,ntoyfile,
     _     temp,errtemp,gallong,gallat,tempcp,k_ring)


C     INPUTS:
      character*256  indir2    ! directory with input files
      integer        nindir    ! number of characters in  indir
      character*256  fileraw ! safe string version of fileraw
      character*256  filemask ! safe string version of filemask
      character*256  toyf2  ! safe string version of toyfile
      integer        nfraw    ! number of characters in  fileraw
      integer        nfmask    ! number of characters in  filemask
      integer        ntoyfile   ! number of characters in toyf2
      integer        iwring      ! is the input file ordering "ring" ordering?
      integer        iwmask       ! flag: want mask file?

C     INTERNAL:
      real*8   pi,pi_two
      parameter(pi=3.1415926535898,pi_two=pi+pi)
      parameter(pi_180=3.1415926535898/180.0)

c      parameter(npix=6144,maxn=8096) !! WARNING declared more than once
c      parameter(npix=6120,maxn=8096) !! WARNING declared more than once

c     -> now done in pxallo.c with declarations in circles.h
c
c      include 'npix.dec'

c      integer*4  ipix(maxn)
c     ,nobspix(maxn) 

c     -> now done in pxallo.c with declarations in circles.h
c
c      real*4   temp(maxn),errtemp(maxn) !   Temperature and uncert.
c      real*4   gallong(maxn),gallat(maxn) ! galactic
      real*4   temp(*),errtemp(*) !   Temperature and uncert.
      real*4   gallong(*),gallat(*) ! galactic

      real*4   tempcp(*) ! temporary copy of temp(*)
      integer*4  k_ring(*)  ! temporary index for ring ordering


c      real*4   eclong(maxn),eclat(maxn) ! ecliptic
c      real*4   eqlong(maxn),eqlat(maxn) ! ra dec in degrees, degrees

c      real*4   sortcrit(2,maxn)  !! wrong order !!
c      real*4   sortcrit(maxn,2)

c      parameter(nxint=180,nyint=90)
      include 'nxint.dec'

      integer*4 inear(nxint,nyint)    ! nearest pixel number
      real*4    dinear(nxint,nyint)
      real*4    dxint,dyint
c     -- Eq. Cartesian coords of table --
      real*4    x(nxint,nyint),y(nxint,nyint),z(nxint,nyint) 

      logical*4 measur,noise      ! measured or random; (signal or) noise
c      real*4    ranerr(maxn)


c     ---- convert pixel number to  angle using isolat package ----
c     e.g. http://cosmo.torun.pl/GPLdownload/dodec/isolat-0.1.11.tar.gz

c      parameter(n_res=512) ! hardwired for WMAP
      parameter(inorth= 0) !  multiple of pi/2 of North pole = -1,0,1 or 2 
      parameter(ifpixel = 1)    ! first pixel number 
      parameter(j_warn = 0)  ! j_warn = 1 turns off warnings
      real*8          theta,phi

c      logical*4       doerrupper


C     --- toy simulation - dummy parameters for calling getvectors( )
      real*4   efund(3,3) ! DUMMY ! galactic coords
      real*4   Rfund(3)         ! DUMMY ! lengths of axes in h^-1 Mpc
      logical*4 dododec         ! do Poincare dodecahedral space ?
      logical*4 doT2            ! do 2-torus model ?
      logical*4 ivjv_valid     ! test on validity of iv,jv
      
      parameter(n_blob=4)
      real*4    th_blob(n_blob) ! blob angles around circle for toy simulation
      real*4    A_blob(n_blob) ! amplitudes of blobs 
      integer   np_blob ! how many pixels is the half-width of a blob?
      real*4    pixrad ! arc-length of a pixel in radians
      integer   isimtype ! type of simulation
c      parameter(isimtype=1) ! blobs along matched circles
      parameter(isimtype=2) ! copy/paste discs of the input map


C     EXTERNAL:
      integer  pxstat  ! pxstat.c

C     COMMON:
      common /noisecomm/ measur,noise

c      common /wmapcomm/  temp,errtemp,gallong,gallat, eqlong,eqlat,

c      common /wmapcomm/  temp,errtemp,gallong,gallat, 
c     _     dsm,gb0,dxint,dyint,inear,dinear,x,y,z  ,ranerr
      common /wmapcomm/ npixcp,
     _     dsm,gb0,dxint,dyint,inear,dinear,x,y,z

c      common /erruppcomm/ doerrupper,errupper ! shared with plotdodec (at least)
      include 'errupp.dec'

      character*256  cmbfile,cmbmask

c      byte      by_array(40)
c      integer*4 intarray(20)
c      real*4    r__array(20)
c     --- as of version 0.1.48
      integer*4 intarray(1024)
      real*4    r__array(1024)
      equivalence(intarray(1),r__array(1))

      integer*4  iswbyte
      external   iswbyte


      logical*4  domask
      logical*4  test,test2,test3,test4,test5a,test5b,test6

      logical*4  oldtoy

      integer*4  iwarn1  ! read file error
      parameter(mxwar1= 10) ! max no of warnings type 1

      integer    iwverb  ! verbose interaction ?

c      data th_blob /0.0, 20.0, 50.0, 110.0/
c      data th_blob /0.0, 60.0, 180.0, 345.0/  ! different order to above!

c      data th_blob /0.0, 20.0, 50.0, 110.0/  !! was in *radians* 0.2.5.3 and earlier
c     -> this should be equivalent to:
c      data th_blob /0.0, 65.9, 344.8, 182.5/ !! degrees
c      data A_blob /2.0, 2.0, 1.0, 1.0/ 

c      data A_blob /1.0, 1.0, 1.0, 1.0/ 

c     -- 0.2.5.5 --
      data th_blob /0.0, 60.0, 180.0, 345.0/ !! degrees
      data A_blob /1.0, -1.0, -1.0, 1.0/ 
      data np_blob /100/         ! how many pixels is the half-width of a blob?

c      data th_blob /0.0, 120.0, 240.0, 300.0/
c      data A_blob /10.0, 10.0, 2.0, 2.0/ 

      test= .false. !.true. ! 
      test2= .false. !.true. !
      test3= .false. !.true. !
      test4= .false. !  print x1x,x1y, etc. ! .true. !
      test5a= .false. !  artificially simple initial point in par space.true. ! 
      test5b= .false. !  print tmp_map of toy simulation !WARNING: big file ! .true. !
      test6= .false. !  this reveals the "secret" inputs! turn it off except for testing!.true. ! 


      domask= .false.              !.true. !

      iwverb=1 

      dsm= 3.5*pi/180.0

      doT2 = .false.
      dododec= .true.

c     ==== copy npix to common block ====
      npixcp=npix
      
      print*,'rdWMAP beginning: npix=',npix

c     ==== check that memory has been allocated ====
      i= pxstat()
      if(i.ne.0)then
         print*,'rdWMAP WARNING: pxstat returned ',i,
     _        ' - there is probably a memory allocation problem.'
         print*,'Did you forget to call pxallo?'
      endif
      if(test)then
         print*,'rdWMAP: before print*,temp(1), temp(1)= 4.3'
         print*,'temp(1)=',temp(1)
         temp(1)=4.3
         print*,'rdWMAP: after temp(1)= ',temp(1)
      endif

c     !! test only !!
c      if(ntoyfile.gt.0)then
c         print*,'toyfile = ',toyf2(1:ntoyfile)
c         stop 'debug'
c      endif


c     ==== read in file ====

      iunit= 1
      irwmode=0

c      if(nindir.gt.0.and.indir2(nindir:nindir).ne.'/')then
c         cmbfile=indir2(1:nindir)//'/'//'map_ilc_yr1_v1.fits'
c      else
c         cmbfile=indir2(1:min(nindir,256))//'map_ilc_yr1_v1.fits' ! kaz
c      endif


c     --- 0.1.38  26.09.2006 - ignore directory name  indir2  
c     -   if fileraw starts from root: '/'  
      if(fileraw(1:1).eq.'/')then
         cmbfile=fileraw(1:nfraw)
c     --- 0.1.28  17.03.2006
      elseif(nindir.gt.0.and.indir2(nindir:nindir).ne.'/')then
         cmbfile=indir2(1:min(nindir,256))//'/'
     _        //fileraw(1:nfraw)
      else
         cmbfile=indir2(1:min(nindir,256))
     _        //fileraw(1:nfraw)
      endif

c     -   if fileraw starts from root: '/'  
      if(filemask(1:1).eq.'/')then
         cmbmask=filemask(1:nfmask)
c     --- 0.1.28  17.03.2006
      elseif(nindir.gt.0.and.indir2(nindir:nindir).ne.'/')then
         cmbmask=indir2(1:min(nindir,256))//'/'
     _        //filemask(1:nfmask)
      else
         cmbmask=indir2(1:min(nindir,256))
     _        //filemask(1:nfmask)
      endif

      istatus=0
      print*,'istatus=',istatus
      print*,'Will read :'
      print*,cmbfile
      call ftopen(iunit,cmbfile,irwmode,iblocksize,istatus)
      print*,'irwmode,iblocksize=',irwmode,iblocksize
      print*,'istatus=',istatus

      nhdu=2
      ihdutype=2
      call ftmahd(iunit,nhdu,ihdutype,istatus)
      print*,'after ftmahd: istatus=',istatus

      n_long=8

      if(iwring.eq.1)n_long=4096  ! TOH format

c      errupper= 10.0e-3 ! mK - estimate of upper limit to error /pixel 

c     ---- version 0.1.48: prepare nested-vs-ring ordering index  k_ring if needed
      if(iwring.eq.1)then
         do iring=1,npix
            k_ring(iring)=-99  ! null value
         enddo

         do inest=ifpixel,ifpixel-1+npix
c     --- convert nested ordering index  inest  into (phi,theta)
            call getang(n_res, inorth, ifpixel, j_warn, inest, !inputs
     _           phi,theta, nr_out) ! outputs

            if(j_warn.eq.0.and.nr_out.ne.n_res)then
               print*,'rdWMAP: WARNING: after getang:',
     _              ' n_res .ne. nr_out = ',
     _              n_res,nr_out
            endif

            if(phi.gt.pi_two)phi=phi-pi_two

            j_nest = 1- iwring
c     --- convert (phi,theta) into ring ordering index  iring
            call getpix(n_res, inorth, ifpixel, j_warn, j_nest, 
     _           phi,theta,
     _           iring, nr_out)
            
            if(j_warn.eq.0.and.nr_out.ne.n_res)then
               print*,'rdWMAP: WARNING: after getpix: ',
     _              'n_res .ne. nr_out = ',
     _              n_res,nr_out
            endif
            if(iring.lt.ifpixel.or.iring.gt.ifpixel-1+npix)then
               print*,'rdWMAP WARNING: inest,iring=',
     _              inest,iring
            else
               k_ring(iring)= inest
            endif

c            k_ring(iring)= iring !! test only !! 0.1.48
         enddo

         ibad=0
         do iring=1,npix
            if(k_ring(iring).lt.1.or.
     _           k_ring(iring).gt.npix)then
               print*,'rdWMAP: WARNING: iring,k_ring(iring)=',
     _              iring,k_ring(iring)
               ibad=ibad+1
            endif
         enddo
         if(ibad.gt.0)print*,'rdWMAP WARNING: ibad=',ibad
            

      endif !  if(iwring.eq.1)then
      if(test2)print*,'rdWMAP: created index  k_ring(.)'

c     ---- read in temperature values and store them in temp() etc.

      iwarn1= 0

c      do i=1,npix
      if(iwring.ne.1)then
         nrecords= npix
      elseif(iwring.eq.1)then
         nrecords= npix/(n_long/4)
      endif

      do i=1,nrecords ! 0.1.48
         if(test)print*,'i=',i
         call ftgtbb(iunit,i,1,n_long,intarray,istatus)
         if(istatus.ne.0.and.iwarn1.lt.mxwar1)then
            print*,'rdWMAP WARNING readerror',
     _           'i,istatus=',i,istatus
            print*,'probably something is wrong with the',
     _           ' following file: ',
     _           cmbfile
            iwarn1= iwarn1+1
         endif
         if(test)print*,'rdWMAP: after a call to ftgtbb '

         do k=1,n_long/4
            intarray(k)=iswbyte(intarray(k)) !intarray(k)  !
         enddo

         if(test)print*,'rdWMAP: after iswbyte line'

c         ipix(i)=i
         if(test)print*,'i,temp(i)=',i,temp(i)

         if(iwring.ne.1)then
            temp(i)=r__array(1)
            if(test)print*,'after temp assignment: i,temp(i)=',
     _           i,temp(i)
         elseif(iwring.eq.1)then
            do j=1,n_long/4
               i_ring = (i-1)*(n_long/4) + j
               if(test2)then
                  print*,'rdWMAP A: i,j,i_ring,k_ring(i_ring)=',
     _                 i,j,i_ring,k_ring(i_ring)
               endif
               temp(k_ring(i_ring))= r__array(j)
               if(test)print*,'after temp assignment: i,temp(.)=',
     _              i,temp(k_ring(i_ring))
            enddo
         endif


         if(test)print*,'rdWMAP: after an evaluation to temp(1)'

c         nobspix(i)=r__array(2) ! this is 'counts ' - is this no of obsvns??
         if(iwring.ne.1)then
            ncount=r__array(2)  ! this is 'counts ' - is this no of obsvns??
         endif

         if(doerrupper)then
            if(iwring.ne.1)then
               errtemp(i)= errupper
            endif
         else
            stop 'rdWMAP: no alternative to errupper defined.'
         endif

         if(domask)then
c            temp(i)=nobspix(i)  ! = 1.0 or 0.0
            if(iwring.ne.1)then
               temp(i)=ncount   ! = 1.0 or 0.0
            endif
         endif

         if(iwring.ne.1)then
c     --- call isolat routine to convert pixel number to angle on sky  ---
            call getang(n_res, inorth, ifpixel, j_warn, i,
     _           phi,theta, nr_out)
            
            if(j_warn.eq.0.and.nr_out.ne.n_res)then
               print*,'rdWMAP: WARNING: n_res .ne. nr_out = ',
     _              n_res,nr_out
            endif
            
            gallong(i)= phi/(pi/180.0)
c         gallat(i)= theta/(pi/180.0) -90.0 
            gallat(i)= -(theta/(pi/180.0) -90.0) ! 24.04.2003
         elseif(iwring.eq.1)then
            do j=1,n_long/4
               i_ring = (i-1)*(n_long/4) + j
               if(test2)then
                  print*,'rdWMAP B: i,j,i_ring,k_ring(i_ring)=',
     _                 i,j,i_ring,k_ring(i_ring)
               endif


               call getang(n_res, inorth, ifpixel, j_warn, 
     _              k_ring(i_ring),   ! use the nested pixel number
     _              phi,theta, nr_out)

               gallong(k_ring(i_ring))= phi/(pi/180.0)
               gallat(k_ring(i_ring))= -(theta/(pi/180.0) -90.0) 

               if(test)print*,'after temp assignment: i,temp(.)=',
     _              i,temp(k_ring(i_ring))
            enddo
         endif

c     --- test only ---
c         if(abs(gallat(i)).gt.88.0.or.i.lt.5)print*,
c     _        'i,gallat(i),gallong(i)=',
c     _        i,gallat(i),gallong(i)

c         eclong(i)=r__array(5)
c         eclat(i)=r__array(6)
c         gallong(i)=r__array(7)
c         gallat(i)=r__array(8)
c         eqlong(i)=r__array(9)
c         eqlat(i)=r__array(10)

c         print*,'ipix(i),temp(i),nobspix(i),errtemp(i)='
c         print*,ipix(i),temp(i),nobspix(i),errtemp(i)

c     disabled in 0.1.12 -> add back later as option
c         ranerr(i)=gauss0(0.0,errtemp(i)) ! generate errors from errtemp
      enddo
      

      call ftclos(iunit,istatus)
      if(istatus.ne.0)print*,'i,istatus=',i,istatus

      print*,'WMAP sample output: '
c      print*,'(ipix(i),temp(i),errtemp(i),gallong(i),gallat(i),i=1,3)'
      print*,'(i,temp(i),errtemp(i),gallong(i),gallat(i),i=1,3)'
      do i=1,5
c         print*,ipix(i),temp(i),errtemp(i),gallong(i),gallat(i)
         print*,i,temp(i),errtemp(i),gallong(i),gallat(i)
      enddo


c     ---- 0.2.5 replace map read in by a toy simulation ----
      if(ntoyfile.gt.0)then
         call spher2(gl_in,gb_in) ! gal long, lat uniform rand on sphere
         the_in = rnd(0)*72.0  ! third free parameter for dodec orientation
         ang_in= 10.0 + rnd(0)*40.0 ! 10 deg < circle size < 50 deg
         twist= rnd(0)*10.0  ! 0.0 < twist < 10.0  (units of 36 deg = pi/5)

         if(test5a)then  ! VERY simple case for debugging
            gl_in=0.0
            gb_in=90.0
            the_in=0.0
            ang_in= 3.0 ! try to focus on opposite faces only 
            twist= 2.5
         endif
            

c     -  --- try to read old existing file, continue if this fails ---
c     -  WARNING: this requires the artificial map to NOT use any randoms
         oldtoy= .false. ! by default, a previous run does not exist
         open(17,file=toyf2(1:ntoyfile),access='sequential',
     _        form='unformatted',status='old',err=333)
         read(17,err=333,end=333)xx1,xx2,xx3,xx4,xx5
         gl_in=xx1
         gb_in=xx2
         the_in=xx3
         ang_in=xx4
         twist=xx5
         oldtoy= .true. 
         if(test6)print*,'Read in: gl_in,...=',
     _        gl_in,gb_in,the_in,ang_in,twist
 333     continue
         close(17)

c     --- write out the "secret" input data to a file ---
c     -  --- use e.g.  od -t fF <toyf2>  to read this on the command line ---
         if(.not.(oldtoy))then
            open(17,file=toyf2(1:ntoyfile),access='sequential',
     _           form='unformatted',status='unknown')
            write(17)gl_in,gb_in,the_in,ang_in,twist
            close(17)
         endif

         if(isimtype.eq.1)then
c     --- set white noise background, remove all real signal ---
            do i=1,npix
               temp(i)= 2.0*rnd(0)-1.0
            enddo
         elseif(isimtype.eq.2)then
c     --- make a copy of the input map which will be used for copy/paste ---
            bheight = 5.0  ! scale height in degrees of artificial map
            do i=1,npix
c               tempcp(i)= temp(i)
c               tempcp(i)= rnd(0) * exp(- abs(gallat(i)) / bheight )
c               tempcp(i)= rnd(0)*0.2*max(0.0, 5.0-abs(gallat(i)))
               cc = cos(4.0*gallat(i)*pi_180)
               tollog= 1e-2 ! TODO: shift to declaration section
               tempcp(i)= 
     _              exp(5.0*log(max(tollog,abs(cc)))) ! cos^5(4b)
c     _              4.0*exp(11.0*log(max(tollog,abs(cc)))) ! 4cos^{11}(4b)
               if(cc.lt.0.0)tempcp(i)= -tempcp(i)
               tempcp(i)=tempcp(i)* (100.0-abs(gallat(i)))*0.01 
     _              * gallong(i)/360.0
               temp(i)= 0.0
            enddo
         endif

c     -- loop through 6 faces
         jv=0 ! dummy
         mv=0 ! dummy
         nv=0 ! dummy
c     ---  the units of rSLS and pixrad must be consistent
         rSLS = 1.0
         pixdeg = 0.5* 0.11 ! WMAP: 0.11 degrees 
         if(test4)pixdeg = 5.0 !! test only !!
         pixrad = pixdeg *pi_180 !  -> radians
c         np_blob = 50  ! how many pixels is the half-width of a blob?
         tolsqrt = pixrad*pixrad
         tolang = 1e-5

c     --- discs radius 31.7deg (half of supplement to dihedral angle,
c     i.e.  1/2 ( pi - acos(-1/sqrt(5))) -> copied/pasted
c     --  this value will *not* be used for the initial start point, since all circle
c     sizes smaller than this become valid --
         if(isimtype.eq.2)ang_in= 31.717 ! wrong.eq.58.28  

         ntheta = 36 ! intervals of 10 degrees
         iv1=1
         iv2=6
c         if(test5)iv2=1  ! problem: probably makes xi_simple too low

         do iv=iv1,iv2  ! 1,6 
c     ---   get dodec vectors for making circles

            call getvectors(iv,jv,mv,nv,doT2,dododec, ! inputs
     _           efund,Rfund, rSLS, ntheta,
     _           gl_in,gb_in, the_in, ang_in, ! important inputs
     _           vx1,vy1,vz1, ax1,ay1,az1,bx1,by1,bz1, ! outputs
     _           vx2,vy2,vz2, ax2,ay2,az2,bx2,by2,bz2,
     _           wmod, 
     _           ivjv_valid, nth,dtheta)


c     ---- blob simulation type ----
            if(isimtype.eq.1)then
               isign=-1
               do ith=1,n_blob
                  isign = -isign 
                  th= th_blob(ith) * pi_180
                  cth= cos(th)
                  sth= sin(th)
                  
                  x1x= vx1 + wmod*(cth*ax1 + sth*bx1)
                  x1y= vy1 + wmod*(cth*ay1 + sth*by1)
                  x1z= vz1 + wmod*(cth*az1 + sth*bz1)

                  do ix= -np_blob, np_blob
                     do iy= -np_blob, np_blob
                        do iz= -np_blob, np_blob

                           call xyz_rda(
     _                          x1x + real(ix)*pixrad,
     _                          x1y + real(iy)*pixrad,
     _                          x1z + real(iz)*pixrad,
     _                          rr,del,alph)
c     ---- place to add a new simulated temperature
                           gl_add= alph*15.0
                           gb_add= del
                           phi = gl_add *pi/180.0
                           theta = (-gb_add + 90.0) * pi/180.0

                           call getpix(n_res, inorth, ifpixel, 
     _                          j_warn, j_nest, 
     _                          phi,theta,
     _                          ipix, nr_out)

                           if(j_warn.eq.0.and.nr_out.ne.n_res)then
                              print*,'rdWMAP: WARNING: ',
     _                             'after getpix: ',
     _                             'n_res .ne. nr_out = ',
     _                             n_res,nr_out
                           endif

c     -- fractional distance from centre of blob --
                           rr= sqrt(max(tolsqrt,
     _                          real(ix*ix+iy*iy+iz*iz)))/
     _                          real(np_blob)
c     -- add simulated temp --
                           temp(ipix) = temp(ipix) + 
     _                          rnd(0) /(rr*rr) *
     _                          A_blob(ith)
                        enddo   !   do iz= -np_blob, np_blob
                     enddo      !   do iy= -np_blob, np_blob
                  enddo         !   do ix= -np_blob, np_blob

                  th=th_blob(ith) * pi_180
     _                 + pi + twist *0.2*pi ! as in circles_f77.f
                  cth= cos(th)
                  sth= sin(th)

                  x2x= vx2 + wmod*(cth*ax2 + sth*bx2)
                  x2y= vy2 + wmod*(cth*ay2 + sth*by2)
                  x2z= vz2 + wmod*(cth*az2 + sth*bz2)

                  do ix= -np_blob, np_blob
                     do iy= -np_blob, np_blob
                        do iz= -np_blob, np_blob

                           call xyz_rda(
     _                          x2x + real(ix)*pixrad,
     _                          x2y + real(iy)*pixrad,
     _                          x2z + real(iz)*pixrad,
     _                          rr,del,alph)
c     ---- place to add a new simulated temperature
                           gl_add= alph*15.0
                           gb_add= del
                           phi = gl_add *pi/180.0
                           theta = (-gb_add + 90.0) * pi/180.0

                           call getpix(n_res, inorth, ifpixel, 
     _                          j_warn, j_nest, 
     _                          phi,theta,
     _                          ipix, nr_out)

                           if(j_warn.eq.0.and.nr_out.ne.n_res)then
                              print*,'rdWMAP: WARNING: ',
     _                             'after getpix: ',
     _                             'n_res .ne. nr_out = ',
     _                             n_res,nr_out
                           endif

c     -- fractional distance from centre of blob --
                           rr= sqrt(max(tolsqrt,
     _                          real(ix*ix+iy*iy+iz*iz)))/
     _                          real(np_blob)
c     -- add simulated temp --
                           temp(ipix) = temp(ipix) + 
     _                          rnd(0) /(rr*rr) *
     _                          A_blob(ith)
                        enddo   !   do iz= -np_blob, np_blob
                     enddo      !   do iy= -np_blob, np_blob
                  enddo         !   do ix= -np_blob, np_blob

               enddo            ! do ith=1,n_blob
            elseif(isimtype.eq.2)then
               d_ang = pixrad
               a1 = 2.0
               a2 = ang_in ! degrees
               n_ang = max(1,int( (a2-a1)/pixdeg )) +1
               d_ang = (a2-a1)/real(n_ang) ! degrees
               if(test4)print*,'angs: ',ang_in,d_ang,a1,a2,n_ang,d_ang

c     --- loop in radius of disc
               do iang=1,n_ang  
                  aa = max(a1 + (real(iang)-0.5)*d_ang, tolang)
c                  n_th= int( 360.0/ (aa*pi/180.0) ) +1  ! totally wrong !
                  n_th= int( 2.0*pi * aa / pixdeg ) +1
                  d_th= 360.0/real(n_th) ! degrees
                  a_fact = aa/(max(2.0,ang_in)) ! proportional radius
                  if(test4)print*,'iang: ',iang,n_th,d_th,a_fact

c     --- loop around a circle at that radius
                  do ith= 1,n_th  
                     th= (real(ith)-0.5)*d_th  * pi_180
                     cth= cos(th)
                     sth= sin(th)
                  
                     x1x= vx1 + a_fact* wmod*(cth*ax1 + sth*bx1)
                     x1y= vy1 + a_fact* wmod*(cth*ay1 + sth*by1)
                     x1z= vz1 + a_fact* wmod*(cth*az1 + sth*bz1)

                     call xyz_rda(
     _                    x1x,
     _                    x1y,
     _                    x1z,
     _                    rr,del,alph)
c     ---- place to add a new simulated temperature
                     gl_add= alph*15.0
                     gb_add= del
                     phi = gl_add *pi_180
                     theta = (-gb_add + 90.0) * pi_180
                     
                     if(test4.and. ith.le.n_th/3
     _                    )write(6,'(a,i3,8f9.3)')'x1: ',
     _                    iv,aa,x1x,x1y,x1z,gl_add,gb_add,phi,theta
                     
                     j_nest = 1 ! TODO: assumes nested ordering: risk of user error
                     call getpix(n_res, inorth, ifpixel, 
     _                    j_warn, j_nest, 
     _                    phi,theta,
     _                    ipix1, nr_out)
                     
                     if(j_warn.eq.0.and.nr_out.ne.n_res)then
                        print*,'rdWMAP: WARNING: ',
     _                       'after getpix: ',
     _                       'n_res .ne. nr_out = ',
     _                       n_res,nr_out
                     endif
                     

c                     th=th + pi + pi ! !! test only !! twist *0.2*pi ! as in circles_f77.f
                     th=th + pi + twist *0.2*pi ! as in circles_f77.f 

                     cth= cos(th)
                     sth= sin(th)

                     x2x= vx2 + a_fact* wmod*(cth*ax2 + sth*bx2)
                     x2y= vy2 + a_fact* wmod*(cth*ay2 + sth*by2)
                     x2z= vz2 + a_fact* wmod*(cth*az2 + sth*bz2)
                     
                     call xyz_rda(
     _                    x2x,
     _                    x2y,
     _                    x2z,
     _                    rr,del,alph)
c     ---- place to add a new simulated temperature
                     gl_add= alph*15.0
                     gb_add= del
                     phi = gl_add *pi_180
                     theta = (-gb_add + 90.0) * pi_180
                     
                     if(test4.and. ith.le.n_th/3
     _                    )write(6,'(a,i3,8f9.3)')'x2: ',
     _                    iv,aa,x2x,x2y,x2z,gl_add,gb_add,phi,theta

                     call getpix(n_res, inorth, ifpixel, 
     _                    j_warn, j_nest, 
     _                    phi,theta,
     _                    ipix2, nr_out)
                     
                     if(j_warn.eq.0.and.nr_out.ne.n_res)then
                        print*,'rdWMAP: WARNING: ',
     _                       'after getpix: ',
     _                       'n_res .ne. nr_out = ',
     _                       n_res,nr_out
                     endif
                     
c     ---- copy/paste from disc1 to disc2 ----
                     temp(ipix1)= tempcp(ipix1)
                     temp(ipix2)= tempcp(ipix1)
c                     temp(ipix2)= tempcp(ipix2)  ! no change !! test only !!


                  enddo         !                  do ith= 1,n_th  
               enddo            !                do iang=1,n_ang  

            endif               ! if(isimtype.eq.1)then

         enddo                  ! do iv=1,6 

c     -- TODO: write a proper FITS file in 1D (WMAP) or 2D (FITS projection) format
         if(test5b)then   !! test only !! writes a big output file !!
            open(19,file='tmp_map',access='sequential',
     _           status='unknown',form='formatted')
            do jj=1,npix,10  ! select just some of the non-zero points
               if(abs(temp(jj)).gt.1e-6)
     _              write(19,'(3f10.4)')gallong(jj),gallat(jj),
     _              temp(jj)
            enddo
            close(19)
         endif

      endif




c     ---- 0.1.70 read in and apply mask ---
      if(iwmask.eq.1)then
         istatus=0
         print*,'istatus=',istatus
         print*,'Will read :'
         print*,cmbmask
         call ftopen(iunit,cmbmask,irwmode,iblocksize,istatus)
         print*,'irwmode,iblocksize=',irwmode,iblocksize
         print*,'istatus=',istatus
         nhdu=2
         ihdutype=2
         call ftmahd(iunit,nhdu,ihdutype,istatus)
         print*,'after ftmahd: istatus=',istatus
         
         n_long=8
         nrecords= npix
         iwarn1=0

         imasked= 0
         do i=1,nrecords 
            if(test3)print*,'i=',i
            call ftgtbb(iunit,i,1,n_long,intarray,istatus)
            if(istatus.ne.0.and.iwarn1.lt.mxwar1)then
               print*,'rdWMAP WARNING readerror',
     _              'i,istatus=',i,istatus
               print*,'probably something is wrong with the',
     _              ' following file: ',
     _              cmbmask
               iwarn1= iwarn1+1
            endif
            if(test3)print*,'rdWMAP: after a call to ftgtbb '
            
            do k=1,n_long/4
               intarray(k)=iswbyte(intarray(k)) !intarray(k)  !
            enddo

            if(test3)print*,'rdWMAP: after iswbyte line'

c         ipix(i)=i
c         if(test)print*,'i,temp(i)=',i,temp(i)
c         temp(i)=r__array(1)  ! ignore the first value, it is not used


c         nobspix(i)=r__array(2) ! this is 'counts ' - is this no of obsvns??
            ncount=int(r__array(2)) ! this is 'counts ' - is this no of obsvns??

            if(test3)print*,'rdWMAP: tried to read: i,ncount=',
     _           i,ncount

            if(test3)print*,'rdWMAP: i, temp(i)=',i,temp(i)
c     ---- nullify the temperature if it's masked ----
            if(ncount.lt.1)then
               temp(i)= tNULL
               imasked= imasked+1
            endif

            if(test3)print*,'rdWMAP: after mask: ',
     _           'i, ncount, temp(i)=',i,ncount,temp(i)

c         print*,'ipix(i),temp(i),nobspix(i),errtemp(i)='
c         print*,ipix(i),temp(i),nobspix(i),errtemp(i)

         enddo  !  do i=1,nrecords 
         print*,'rdWMAP: imasked,npix=',imasked,npix,
     _        ' masked frac = ',
     _        real(imasked)/real(npix) * 100.0 ,'%'

      endif  !      if(iwmask.eq.1)then


c     ==== initialise tables ====

c     ---- make gl-gb tables of closest pixel and dist to cl pixel ----
      dyint=180.0/real(nyint)  ! degrees /table unit
      dxint=360.0/real(nxint)  ! degrees /table unit
      print*,'dxint,dyint=',dxint,dyint

      gb0=-90.0
 
      if(test)print*,'rdWMAP: nxint,nyint=',nxint,nyint
      do j=1,nyint
         gb= gb0+ (real(j)-0.5)*dyint
         do i=1,nxint
            if(test.and.i.eq.152.and.j.eq.59)print*,
     _           'rdWMAP test 1 '
            inear(i,j)=-99
            dinear(i,j)=9e9
            gl= (real(i)-0.5)*dxint
            x(i,j)= cos( gb*pi/180.0 )*cos(gl*pi/180.0)
            y(i,j)= cos( gb*pi/180.0 )*sin(gl*pi/180.0)
            z(i,j)= sin( gb*pi/180.0 )
            if(test.and.i.eq.152.and.j.eq.59)print*,
     _           'rdWMAP test 2 '
         enddo
      enddo


      measur=.true.
      noise=.false.
      print*,'wmaplin(10.0,0.0),wmaplin(180.0,89.0)='
      xx= wmaplin(10.0,0.0,
     _     temp,errtemp,gallong,gallat)
      yy=wmaplin(180.0,89.0,
     _     temp,errtemp,gallong,gallat)
      print*,xx,yy

      return
      end 

C     Unused and untested
C      real*4   function wmaprot(gall,galb)
Cc     ==== gives wmap temp fluctuation at gall, galb,
Cc     after a rotation from (glrot,gbrot) to the "north pole" ====
Cc     12.01.2004
C
C      common /rotcomm/ glrot0,gbrot0
C
C      call nprotate(glrot0,gbrot0, gall,galb, glout,gbout)
C      wmaprot=wmaplin(glout,gbout,
C     _     temp,errtemp,gallong,gallat)
C
C      return
C      end




      real*4   function  wmaplin(gall,galb,
     _     temp,errtemp,gallong,gallat)
c     ==== output may be either temperature variations or their 
c     uncertainties, depending on whether .not.(noise) or (noise) 
c     is set respectively ====
c     -- the  /noisecomm/  common block should be declared in the 
c     calling subroutine(s).

C     INPUTS:
      real*4   gall,galb        ! galactic longitude, latitude input

c      parameter(npix=6144,maxn=8096)  !! WARNING declared several times
c      parameter(npix=6120,maxn=8096)  !! WARNING declared several times
c      include 'npix.dec'

c      real*4   temp(maxn),errtemp(maxn) !   Temperature and uncert.
c      real*4   gallong(maxn),gallat(maxn) ! galactic
      real*4   temp(*),errtemp(*) !   Temperature and uncert.
      real*4   gallong(*),gallat(*) ! galactic


c      real*4   eclong(maxn),eclat(maxn) ! ecliptic
c      real*4   eqlong(maxn),eqlat(maxn) ! ra dec in degrees, degrees


C     EXTERNAL:
      integer  pxstat  ! pxstat.c


C     INTERNAL:
      real*8    pid,twopid
      parameter(pi=3.1415926535898)
      parameter(pid=3.1415926535898d0,twopid=2.0d0*pid)


c      parameter(nxint=180,nyint=90)
      include 'nxint.dec'

      integer*4 inear(nxint,nyint)    ! nearest pixel number
      real*4    dinear(nxint,nyint)
      real*4    dxint,dyint
c     -- Eq. Cartesian coords of table --
      real*4    x(nxint,nyint),y(nxint,nyint),z(nxint,nyint) 

      logical*4 measur,noise    

c     disabled in 0.1.12 -> add back later as option
c      real*4    ranerr(maxn)

c     ---- convert pixel number to  angle using isolat package ----
c     e.g. http://cosmo.torun.pl/GPLdownload/dodec/isolat-0.1.11.tar.gz

c      parameter(n_res=512) ! hardwired for WMAP
      parameter(inorth= 0) !  multiple of pi/2 of North pole = -1,0,1 or 2 
      parameter(ifpixel = 1)    ! first pixel number 
      parameter(j_warn = 0)  ! j_warn = 1 turns off warnings
      real*8          theta,phi, told
      parameter(told=1e-6)

      common /noisecomm/ measur,noise

c      common /wmapcomm/  temp,errtemp,gallong,gallat,eqlong,eqlat,
      common /wmapcomm/ npixcp,
     _     dsm,gb0,dxint,dyint,inear,dinear,x,y,z 

c      print*,'BEGINNING of  wmaplin(...)',
c     _     'measur,noise=',measur,noise

      npix=npixcp
c      print*,'rdWMAP beginning: npix=',npix
      i= pxstat()
      if(i.ne.0)then
         print*,'rdWMAP WARNING: pxstat returned ',i,
     _        ' - there is probably a memory allocation problem.'
         print*,'Did you forget to call pxallo?'
      endif


      theta=((-galb)+ 90.0) *pi/180.0 
      phi= gall*pi/180.0

      theta=min(max(told,theta),pid) ! getpix does not check limits
      phi=min(max(told,phi),twopid)


      j_nest = 1  !! nested ordering is used in rdWMAP() above
      call getpix(n_res, inorth, ifpixel, j_warn, j_nest, 
     _     phi,theta,
     _     ipix, nr_out)

c      xx= cos( galb*pi/180.0 )*cos(gall*pi/180.0)
c      yy= cos( galb*pi/180.0 )*sin(gall*pi/180.0)
c      zz= sin( galb*pi/180.0 )

      
      if(measur)then
         if(.not.(noise))then
            wmaplin=temp(ipix)
         else
            wmaplin=errtemp(ipix)
         endif
      elseif(noise)then
         print*,'The noise option was disabled in circles-0.1.12,'
         print*,'and could be added in as an option in a later'
         print*,'version. This code is GPL - please feel free'
         print*,'to update the package and redistribute the new'
         print*,'version according to the terms of the GPL. :)'
c     disabled in 0.1.12 -> add back later as option
c         wmaplin=ranerr(ipix)
      else
         print*,'measur,noise=',measur,noise,' not useful.'
         wmaplin=-99.9
      endif

      return
      end


      subroutine  smooWMAP(new,sigdeg,resdeg, indir2,nindir,
     _     filesmooth,nfsmooth,
     _     binfo2,nbin_format,
     _     temp,errtemp,gallong,gallat,tempcp)
c     !! modifies (at least) the array temp

c     INPUTS:
      logical*4       new ! is it a newly smoothed file?
      real*4          sigdeg ! # 1 sigma width in degrees
      real*4          resdeg    ! # resolution of smoothing in degrees


      character*256  indir2    ! directory with input files
      integer        nindir    ! number of characters in  indir
      character*256  filesmooth ! safe string version of filesmooth
      integer        nfsmooth    ! number of characters in  filesmooth
      character*256  binfo2 ! safe string version of bin_format
      integer        nbin_format ! number of characters in  bin_format


      parameter(pi=3.1415926535898)
      parameter(pi_180=pi/180.0)

c      include 'npix.dec'

      character*256   smoofile ! for reading in 

c      real*4   temp(maxn),errtemp(maxn) !   Temperature and uncert.
c      real*4   gallong(maxn),gallat(maxn) ! galactic
c      real*4   eqlong(maxn),eqlat(maxn) ! ra dec in degrees, degrees

      real*4   temp(*),errtemp(*) !   Temperature and uncert.
      real*4   gallong(*),gallat(*) ! galactic

c      real*4   tempcp(maxn) ! copy of temp while working on it
      real*4   tempcp(*) ! copy of temp while working on it

      include 'nxint.dec'

      integer*4 inear(nxint,nyint)    ! nearest pixel number
      real*4    dinear(nxint,nyint)
      real*4    dxint,dyint
c     -- Eq. Cartesian coords of table --
      real*4    x(nxint,nyint),y(nxint,nyint),z(nxint,nyint) 

      logical*4 measur,noise    
c     disabled in 0.1.12 -> add back later as option
c      real*4    ranerr(maxn)

c      parameter(n_res=512) ! hardwired for WMAP
      parameter(inorth= 0) !  multiple of pi/2 of North pole = -1,0,1 or 2 
      parameter(ifpixel = 1)    ! first pixel number 
      parameter(j_warn = 0)  ! j_warn = 1 turns off warnings
      real*8          theta,phi

      external        wmaplin
      real*4          wmaplin

C     EXTERNAL:
      integer  pxstat  ! pxstat.c

      include 'errupp.dec'

C     INTERNAL:
      character*17    ch17

      real*8          zp8,rr8,dd8,aa8

      integer         iwverb

      parameter(NBL_BY= 8192)  ! number of bytes in a block
      parameter(NF8_BL= NBL_BY/8) ! number of 8byte floats in a block
      real*8          temp8(NF8_BL)

      common /noisecomm/ measur,noise

c      common /wmapcomm/  temp,errtemp,gallong,gallat,eqlong,eqlat,
      common /wmapcomm/ npixcp,
     _     dsm,gb0,dxint,dyint,inear,dinear,x,y,z 


      iwverb= 1  ! verbose


      npix=npixcp
      i= pxstat()
      if(i.ne.0)then
         print*,'smooWMAP WARNING: pxstat returned ',i,
     _        ' - there is probably a memory allocation problem.'
         print*,'Did you forget to call pxallo?'
      endif
      
c     --- allocate memory for tempcp(*) ----
c      i= pxal_2(iwverb,tempcp)
c      if(i.ne.0)then
c         print*,'smooWMAP WARNING: pxal_2 returned ',i,
c     _        ' - there is probably a memory allocation problem.'
c      endif
      


c      if(nindir.gt.0.and.indir2(nindir:nindir).ne.'/')then
c         smoofile=indir2(1:nindir)//'/'//'smooW1d.dat' 
c      else
c         smoofile=indir2(1:min(nindir,256))//'smooW1d.dat'
c      endif

c     --- 0.1.38  26.09.2006 - ignore directory name  indir2  
c     -   if filesmooth starts from root: '/'  
      if(filesmooth(1:1).eq.'/')then
         smoofile=filesmooth(1:nfsmooth)
c     --- 0.1.28  17.03.2006
      elseif(nindir.gt.0.and.indir2(nindir:nindir).ne.'/')then
         smoofile=indir2(1:min(nindir,256))//'/'
     _        //filesmooth(1:nfsmooth)
      else
         smoofile=indir2(1:min(nindir,256))
     _        //filesmooth(1:nfsmooth)
      endif

c      smoofile='/scratch0/boud/WMAP/smooW5d.dat'
c      smoofile='/scratch2/boud/WMAP/smooW5d.dat'

c      print*,'BEGINNING of  wmaplin(...)',
c     _     'measur,noise=',measur,noise

c     --- some hardwired warnings on reasonable smoothing parameters ---
      if(sigdeg.gt.10.0.or.resdeg.lt.0.1)then
         print*,'smooWMAP WARNING: sigdeg, resdeg=',sigdeg,resdeg
      endif

      tol=1e-4
      if(new)then  ! if not yet smoothed

c     ---- loop through all pixels ----
         sig2 = 2.0*sigdeg*sigdeg ! degrees
         sig2rad = sig2 *pi_180 *pi_180 ! radians
         resrad= resdeg*pi_180
         res2 = resdeg*resdeg

c     do ipix=1,npix            ! loop through 3M pixels
         ipix1=1
         ipix2=npix

c     ---- test only ----
c         ihigh = 261600! 2097153        !523744         !
c         ipix1=ihigh-30000
c         ipix2=ihigh
c     ----

         do ipix=ipix1,ipix2 
            gall=gallong(ipix)
            galb=gallat(ipix)

c     ---- if not close to pole, then use  (b,l/cos(b))  smoothing ----
c            if(abs(galb).lt.90.0-40.0*sigdeg)then !! test only !!

            if(abs(galb).lt.90.0-4.0*sigdeg)then
c               print*,'smooWMAP TEST - non-pole calc, ipix=',ipix
               cosb= cos(galb*pi_180)
               cosb2= cosb*cosb
               nb= nint(2.0*sigdeg/resdeg)+1 ! go to 2 sigma
               nl= nb
               
               tt=0.0           ! temperature
               ww=0.0           ! weight
               do i=-nb,nb
                  gb= galb + real(i)*resdeg
                  do j=-nl,nl
                     gl= mod(gall + real(j)*resdeg/cosb + 720.0,
     _                    360.0)
                     if(gl.eq.0.0)gl=resdeg*0.5
                     
c                     aa2= (gl-gall)*(gl-gall)*cosb2 +    ! risk of 360 edge
c     _                    (gb-galb)*(gb-galb) ! angle squared from pixel

                     aa= real(i)
                     bb= real(j)
                     aa2= (aa*aa +bb*bb)*res2
                     dw = exp(-aa2/sig2)
                     ww= ww + dw
                     tt= tt + dw * wmaplin(gl,gb,
     _                    temp,errtemp,gallong,gallat)
                  enddo
               enddo
               if(ww.lt.tol)then
                  print*,'smooWMAP WARNING: ipix,gall,galb,ww=',
     _                 ipix,gall,galb,ww
               endif
               tempcp(ipix) = tt/ww
            else
c               print*,'smooWMAP TEST - pole calculation, ipix=',ipix
c     ---- if close to pole, use X-Y projection ---
               xp= cos( galb*pi_180 )*cos(gall*pi_180) ! radians
               yp= cos( galb*pi_180 )*sin(gall*pi_180) ! radians
               zp= sin( galb*pi_180 ) ! radians
               zp8= dble(zp)
               
               nx= nint(2.0*sigdeg/resdeg) +1 ! go to 2 sigma
               ny= nx
               
               tt=0.0           ! temperature
               ww=0.0           ! weight
               do i=-nx,nx
                  xx= xp + real(i)*resrad  ! radians
                  do j=-ny,ny
                     yy= yp + real(j)*resrad  ! radians
c                     call xyz_rda(xx,yy,zp, ! use fixed z of the pixel
c     _                    rr,dd,aa)
c                     call xyz_rda8(dble(xx),dble(yy),zp8,
                     call xyz_8rda(dble(xx),dble(yy),zp8,
     _                    rr8,dd8,aa8)
                     gb= real(dd8)
                     gl= real(aa8*15.0d0) ! hours -> degrees

c                     print*,'i,j,gb,gl=',i,j,gb,gl !! test only !!
                     if(gl.eq.0.0)gl=resdeg*0.5

                     if(gb.gt.90.0-tol)gb=90.0-tol
                     if(gb.lt.-90.0+tol)gb=-90.0+tol
                     
c                     aa2= (gl-gall)*(gl-gall) +   
c     _                    (gb-galb)*(gb-galb) ! angle squared from pixel
                     aa2=  (xp-xx)*(xp-xx) + (yp-yy)*(yp-yy) ! radians

                     dw= exp(-aa2/sig2rad)
                     ww= ww + dw
                     tt= tt + dw * wmaplin(gl,gb,
     _                    temp,errtemp,gallong,gallat)
                  enddo
               enddo
               if(ww.lt.tol)then
                  print*,'smooWMAP WARNING: ipix,gall,galb,ww=',
     _                 ipix,gall,galb,ww
               endif
               tempcp(ipix) = tt/ww
            endif
            
         enddo                  !       do ipix=1,npix           

c     --- copy to temp() ----
         do ipix=ipix1,ipix2 ! 1,npix
            temp(ipix)=tempcp(ipix)  
         enddo

c     --- write out ----
         open(1,file='smooWMAP.dat',access='sequential',
     _        form='unformatted',status='unknown')
         write(1)'smoothed WMAP map'
         write(1)sigdeg,resdeg

c         write(1)temp
c     -> changed in version 0.1.12 -> memory allocated by pxallo, not by fortran
         write(1)(temp(i),i=1,npix)

         close(1)

      else! if the file is claimed to already exist
c         open(1,file='smooWMAP.dat',access='sequential',
         if(binfo2(1:nbin_format).eq."default")then
            open(1,file=smoofile,access='sequential',
     _           form='unformatted',status='old')
            read(1)ch17
            if(ch17.ne.'smoothed WMAP map')
     _           print*,'WARNING: strange file!'
            
            read(1)sigd,resd
c     -> changed in version 0.1.12 -> memory allocated by pxallo, not by fortran
c     read(1)temp

            npixcp= 12 * n_res*n_res ! 0.1.71
            npix=npixcp         ! 0.1.71

            read(1)(temp(i),i=1,npix)
            
            close(1)
         elseif(binfo2(1:nbin_format).eq.'f8')then
c            print*,'Smooth file format "f8" will soon be implemented.',
c     _           ' But not yet.'

c            if(8*ibs8.ne.ibs)print*,'rdWMAP:rdSMOO ERROR: ',
c     _           'ibs,ibs8*8 =', ibs,ibs8*8 

            npixcp= 12 * n_res*n_res ! 0.1.66
            npix=npixcp         ! 0.1.66

            nblock = npix/NF8_BL
            print*,'rdSMOO: npix,NBL_BY,NF8_BL,nblock = ',
     _           npix,NBL_BY,NF8_BL,nblock
            open(1,file=smoofile,access='direct',
     _           form='unformatted',
     _           status='old',recl=NBL_BY)
            do irec=1,nblock
               print*,'irec=',irec ! test only !!!!!!
               read(1,rec=irec)temp8
c               print*,'i,temp8(1),temp8(2)=',i,temp8(1),temp8(2)  !! test only !!!
               do i= 1, NF8_BL
c               do i= 1+(irec-1)* NF8_BL, irec*NF8_BL
                  itemp= i + (irec-1)*NF8_BL

                  temp(itemp)=real(temp8(i))* 1000.0 ! convert to mK

c     --- added 0.1.66 ---
                  call getang(n_res, inorth, ifpixel, j_warn, 
     _                 itemp,
     _                 phi,theta, nr_out)
                  gallong(i)= phi/(pi/180.0)
                  gallat(i)= -(theta/(pi/180.0) -90.0) 
               enddo
c               print*,'i,temp8(1),temp8(2)=',i,temp8(1),temp8(2)  !! test only !!!
            enddo
            close(1)
         else
            print*,'Smooth file format ',binfo2(1:nbin_format),
     _           ' is unrecognised. :( Sorry.'
            stop
         endif

         if(abs((sigd-sigdeg)/sigdeg).gt.tol.or.
     _        abs((resd-resdeg)/resdeg).gt.tol)then
            print*,'WARNING: probably wrong file, ',
     _           'sigdeg,resdeg,sigd,resd=',
     _           sigdeg,resdeg,sigd,resd
         endif
      endif



c     --- free up memory for tempcp(*) ----
c      i= pxfr_2(iwverb)
c      if(i.ne.-1)then   ! -1 means memory freed up or no allocation attempt
c         print*,'smooWMAP WARNING: pxal_2 returned ',i,
c     _        ' - there was a problem freeing up memory :( '
c      endif
      

      return
      end

