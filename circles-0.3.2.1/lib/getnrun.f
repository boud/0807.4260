C     Copyright (C) 2004 B. Roukema et al. <shape-univ at astro.uni.torun.pl>
C     
C     This program is free software; you can redistribute it and/or modify
C     it under the terms of the GNU General Public License as published by
C     the Free Software Foundation; either version 2 of the License, or
C     (at your option) any later version.
C     
C     This program is distributed in the hope that it will be useful,
C     but WITHOUT ANY WARRANTY; without even the implied warranty of
C     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C     GNU General Public License for more details.
C     
C     You should have received a copy of the GNU General Public License
C     along with this program; if not, write to the Free Software
C     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
C  
C     See also http://www.gnu.org/licenses/gpl.html

C     --- calculate total number of "runs" for Poincare dodec case ----
      subroutine getnrun(dododec, dodran, resdodec,
     _     bmindodec,bmaxdodec,glmindodec,glmaxdodec,
     _     thmin,thmax,
     _     angmin,angmax,
     _     twimin,twimax,
     _     nangle, nrunin, mcmc, oneaxis, mc2done,
     _     nrun,nthdomax, ! outputs
     _     gl_pre,gb_pre,th_pre,angpre,twipre )  ! outputs
      
C     INPUTS:
      logical*4  dododec        ! Poincare dodecahedral
      logical*4  dodran         ! random: ignore most input parameters
      real*4     resdodec       ! resolution of grid in degrees
      real*4     bmindodec      ! minimum latitude (cf 90 = NGP)
      real*4     bmaxdodec,glmindodec,glmaxdodec ! etc
      real*4     thmin,thmax  ! min/max rotation of dodec
      real*4     angmin,angmax  ! min/max angular radius of circles
      real*4     twimin,twimax  ! min/max twist (orientation)
      integer*4  nangle         ! number of angular radii
      integer*4  nrunin         ! input suggestion of nrun (e.g. random)
      logical*4  mcmc           ! do an MCMC search of parameter space?
      logical*4  oneaxis           ! do an MCMC search of parameter space?
      logical*4  mc2done        ! is the (MC)^2 done (converged) yet ? 0=N, 1=Y

C     OUTPUTS:
      integer*4  nrun
      integer*4  nthdomax ! same as nthdodec (in nruncomm)
c     ! g long, lat, theta, angle for initial "previous" irun !mcmc
      real*4     gl_pre,gb_pre,th_pre,angpre,twipre

C     INTERNAL:
      integer*4  ifirst
      parameter(pi=3.1415926536,pi_180=pi/180.0)
      parameter(tolcos=1e-2)

C     COMMON:
c      parameter(mxlb= 10000)    ! OK for resdodec = 1 deg
c     --- for single circle pairs - 27.02.2004 ---
      parameter(mxlb= 200000)    ! OK for resdodec = 1 deg
      real*4     gl_ilb(mxlb),gb_ilb(mxlb)
      real*4     del_l,del_b,del_thd,del_a,del_twi   ! MCMC

c      parameter(mxrun= 1000000)
      include 'npar.dec'
      parameter(mxrun=mxpar)

      integer*4  ilbirun(mxrun),ithirun(mxrun),iangirun(mxrun)
      real*4     delang ! angular radius interval

      common /nruncomm/ nlb,nthdodec,nang,delang, gl_ilb,gb_ilb,
     _     ilbirun,ithirun,iangirun,
     _     del_l,del_b,del_thd,del_a,del_twi


C     LOCAL:
      logical*4  test

      save ifirst
      data ifirst/0/


      if(.not.(dododec))then
         stop 'getnrun: dododec is .false.'
      endif

      if(ifirst.ne.12345)then
         ifirst=12345

         if(.not.(mcmc))then
            delb= resdodec
c     nb= max(1, int( (90.0-bmindodec)/delb ) )
            nb= max(1, int( (bmaxdodec-bmindodec)/delb ) )
            ii=0
c     nlequat= 360.0/resdodec
            
            do ib=1,nb
c     gb= 90.0- (real(ib)-0.5)*delb
               gb= bmaxdodec- (real(ib)-0.5)*delb
               
               dell= resdodec/cos(gb*pi_180)
c     nl= max(1, int( 360.0/dell ))
               nl= max(1, int( (glmaxdodec-glmindodec)/dell ))
               
               do il=1,nl
                  ii=ii+1
                  if(ii.gt.mxlb)then
                     print*,'getnrun ERROR: ii,mxlb=',ii,mxlb
                  else
c     gl_ilb(ii)= (real(il)-0.5)*dell
                     gl_ilb(ii)= glmindodec + (real(il)-0.5)*dell
                     gb_ilb(ii)= gb
                  endif
               enddo
            enddo
            if(ii.gt.mxlb)then
               nlb=mxlb
               print*,'getnrun ERROR: ii,mxlb=',ii,mxlb            
            else
               nlb= ii
            endif
         else ! if (mcmc)

            nrun= mxrun  ! mc2done is normally used to stop the chain, not nrun

c     ---- initial resolution for search
c            del_l= resdodec  ! this is for  b = 0
            del_b= resdodec
            del_thd= resdodec
            del_a= resdodec
c            del_a= 0.5 * resdodec ! test only !
c            del_twi= resdodec / (pi/5.0 * 180.0/pi) ! pi/6 converted to degrees
            del_twi= resdodec / 36.0
c            del_twi= 2.0 * resdodec / 36.0 ! test only !

c     --- flush ran generator ---
            xx=rnd(0)*37
            do i=1,int(xx) 
               xx=rnd(0)
            enddo

c     --- WARNING: 72.0 is hardwired for Poinc Dodec Space, but this 
c     is not really used for the mcmc parameter space search ---
c            nthdomax=max(1, int( 72.0 / resdodec )) 
            nthdomax=1

         endif

      endif


c     --- shifted here in 0.1.74 ---

      if(mcmc)then
         
         gb_pre= bmaxdodec - rnd(0)*(bmaxdodec-bmindodec)
c     dell= resdodec/cos(gb_ilb(ii)*pi_180)            
         gl_pre= glmindodec + rnd(0)*(glmaxdodec-glmindodec)

c     ---- if testing just one axis, then choose it arbitrarily
c     from the whole sphere; in this case th_pre is irrelevant ----
         if(oneaxis)call spher2(gl_pre,gb_pre) 
         
         th_pre= thmin + rnd(0) * (thmax-thmin)
         angpre= angmin + rnd(0) * (angmax-angmin)
         twipre= twimin + rnd(0) * (twimax-twimin)
         
c         del_l= resdodec * cos(gb_pre*pi_180) !must be reevaluated 
         del_l= resdodec / max(tolcos,abs(cos(gb_pre*pi_180)) ) ! since 0.3.0
         return                 ! otherwise nrun gets overwritten
      endif

      twipre = 0.5* (twimin + twimax)  ! default: constant twist if not mcmc


c      nthdodec= max(1, int( 72.0 / resdodec ))! 72.0 is hardwired for Poinc dodec

      nthdodec= max(1, int( (thmax-thmin) / resdodec )) !

      nthdomax= nthdodec
      print*,'nthdodec=',nthdodec


c      nang= max(1, int( (angmax-angmin)/resdodec ))
      nang= nangle
      if(nang.le.1)then
         delang=0.0
      else
         delang=(angmax-angmin)/real(nang-1)
      endif

      nrun= nlb *nthdodec *nang

c     --- 0.1.35 ---
      if(dodran)then
         nrun= min(nrunin,mxrun)
c         nlb=int(nrun/(max(1,nthdodec)*max(1,nang)))
         nlb=int(nrun/(max(1,nthdodec)*max(1,nang))) + 1  ! 0.1.36
         nrun= nlb *nthdodec *nang ! 0.1.36
      endif

      if(min(nthdodec,nang).lt.1)then
         print*,'getnrun: WARNING: nthdodec or nang < 1:',
     _        nthdodec,nang
      endif

      irun=0

c     --- 0.1.35 shifted iang from outermost loop to innermost loop ---
      do ilb=1,nlb
         do ith=1,nthdodec
            do iang=1,nang
               irun=irun+1
               if(irun.le.nrun)then
                  ilbirun(irun)=ilb
                  ithirun(irun)=ith
                  iangirun(irun)=iang
               endif
            enddo
         enddo
      enddo
      

      print*,'getnrun: nlb,nthdodec,nang=',nlb,nthdodec,nang

      return
      end





C     --- calculate data for a  "run" for Poincare dodec case ----
      subroutine getirun(dododec, dodran, resdodec,
c     _     bmindodec,bmaxdodec,glmindodec,glmaxdodec,
     _     thmin,
     _     angmin,angmax,a_minh,a_maxh,
     _     twimin,twimax,
     _     irun, mcmc, mc2done,
     _     restart,gl_sta,gb_sta,th_sta,angsta,twista,
     _     gl_pre,gb_pre,th_pre,angpre,twipre, kstep_type,
     _     gl,gb,th,ang, twist,
     _     ilb,ith,iang,isco_)
      
C     INPUTS:
      logical*4  dododec        ! Poincare dodecahedral
      logical*4  dodran         ! random: ignore most input parameters
      real*4     resdodec       ! resolution of grid in degrees
      real*4     bmindodec      ! minimum latitude (cf 90 = NGP)
      real*4     bmaxdodec,glmindodec,glmaxdodec ! etc
      real*4     thmin,thmax  ! min/max rotation of dodec
      real*4     angmin,angmax  ! min/max angular radius of circles
      real*4     twimin,twimax  ! min/max twist (orientation)

      integer*4  irun
      logical*4  mcmc           ! do an MCMC search of parameter space?
      integer    mc2done        ! is the (MC)^2 done (converged) yet ? 0=N, 1=Y
c     !restart: glong, lat, theta, angle, twist
      logical*4  restart  ! if true, then the ???sta parameters must be defined
      real*4     gl_sta,gb_sta,th_sta,angsta,twista 
c     !previous: glong, lat, theta, angle, twist
      real*4     gl_pre,gb_pre,th_pre,angpre,twipre 
      integer*4  kstep_type ! since 0.2.5.12

C     OUTPUTS:
      real*4     gl,gb,th,ang,twist   ! g long, lat, theta, angle for this irun
      integer*4  ilb,ith,iang
      logical*4  isco_        ! is this a control run?

C     INTERNAL:
      parameter(thsafe= 10.0)  ! great circle degrees safe margin for ran theta
      parameter(pi=3.1415926536,pi_180=pi/180.0)
      parameter(tolcos=1e-2)

C     SAVED:
      real*4 gl_old,gb_old,th_old
      save   gl_old,gb_old,th_old


C     COMMON:
c      parameter(mxlb= 10000) ! OK for resdodec = 1 deg
      parameter(mxlb= 200000)    ! OK for resdodec = 1 deg
      real*4     gl_ilb(mxlb),gb_ilb(mxlb)
      real*4     del_l,del_b,del_thd,del_a,del_twi   ! MCMC

c      parameter(mxrun= 1000000)
      include 'npar.dec'
      parameter(mxrun=mxpar)

      integer*4  ilbirun(mxrun),ithirun(mxrun),iangirun(mxrun)
      real*4     delang ! angular radius interval

      common /nruncomm/ nlb,nthdodec,nang, delang, gl_ilb,gb_ilb,
     _     ilbirun,ithirun,iangirun,
     _     del_l,del_b,del_thd,del_a,del_twi


C     LOCAL:
      logical*4   test,test5,test6

      data test /.false. / !.true./
      data test5 /.false. / !.true./ ! 
      data test6 /.false. / !.true./ ! 
      


      if(.not.(dododec))then
         stop 'getirun: dododec is .false.'
      endif

      tolang= 0.01 ! angles smaller than 1 degree are clearly meaningless


c      ilb= mod( irun-1, nlb ) +1
c      ithdodec= mod( irun - ilb -1, nlb*nthdodec ) +1
c      iang= mod( irun - ilb -(ithdodec-1)*nthdodec -1, 
c     _     nlb*nthdodec*nang ) +1
      

c     --- If you change the "proposal density" to a non-symmetric
c     function in terms of old and new states, then it should be
c     separated out into an individual function and used in the
c     selection criterion in circles_f77( ).

      if(mcmc)then
c     ---- first set all 4 parameters equal to last step, one 
c     -    will be modified
         if(test)print*,'getirun: gl_pre,gb_pre,th_pre,angpre=',
     _        gl_pre,gb_pre,th_pre,angpre
         gl=gl_pre
         gb=gb_pre
         th=th_pre
         ang=angpre
         twist=twipre
c     ---- randomly choose which parameter to change
c     TODO: should this cycle through parameters ?
c     -- since 0.2.5.12 - possible override of default step cycle --
         if(kstep_type.eq.45)then
            ip = int(rnd(0)*1.99 + 0.005) +1 +3 ! either 4 or 5 = alpha or twist
         elseif(kstep_type.eq.5)then
            ip = 5 ! twist
         else ! default = random over all dimensions
            ip = int(rnd(0)*4.99 + 0.005) +1 ! 5 parameters since 0.1.75
         endif


c     ---- change it
c         step = rnd(0)*2.0 -1.0  !  -1.0 .le. step .le. +1.0
c     TODO: should there be constraints on gl,gb,th ?   
c     ang,twi are constrained; the other 3 parameters are not.
         if(ip.eq.1)then
c            del_l= resdodec * cos(gb_pre*pi_180) 
c            del_l= resdodec / max(tolcos,cos(gb_pre*pi_180) ) ! 0.2.0.pre4
            del_l= resdodec / max(tolcos,abs(cos(gb_pre*pi_180)) ) ! since 0.3.0

            if(test6)then
               print*,'getirun: del_l = ',del_l
               print*,'resdodec, tolcos, gb_pre, pi_180=',
     _              resdodec, tolcos, gb_pre, pi_180
            endif
c            gl= gl + step* del_l
            gl= gauss0(gl_pre, del_l)
         elseif(ip.eq.2)then
c            gb= gb + step* del_b
            gb= gauss0(gb_pre, del_b)
         elseif(ip.eq.3)then
c            th= th + step* del_thd
            th= gauss0(th_pre, del_thd)
         elseif(ip.eq.4)then
c     --- this could potentially get stuck near ang{min,max} ----
c            ang= max(angmin,min(angmax, ang + step* del_a))
c            ang= max(angmin, min(angmax, gauss0(angpre, del_a)))
            ang= max(a_minh, min(a_maxh, gauss0(angpre, del_a))) ! since 0.3.2

c     --- 0.2.1-0.2.3: bounce off limits by 1.0*del_a instead of getting
c     stuck.

c     0.2.4: This "bounce" step in alpha has been temporarily reverted
c     in order that 0.2.4 is exactly identical (apart from comments)
c     to the version of the code used for MCMC chains for Roukema et
c     al. (2008, in preparation). It would be reasonable to bring it
c     back in some future version, though probably there should be
c     an "unstable" branch, e.g. 0.3.*, while 0.2.* is corrected only
c     for definite bugs (or security problems, though that's hard to
c     imagine for a program like this).

c            ang= gauss0(angpre, del_a)
c            if(ang.le.angmin)then
c               ang= angmin + del_a
c            elseif(ang.ge.angmax)then
c               ang= angmax - del_a
c            endif

         elseif(ip.eq.5)then
c     --- this could potentially get stuck near twi{min,max} ----
c            twist= max(twimin, min(twimax, 
c     _           gauss0(twipre, del_twi)))
            twist= gauss0(twipre, del_twi)
         endif

         if(test5)then  !! test only !!
c            twist=twipre + 0.5
            twist= twista + real(irun-1)*0.5  ! previous line can stall
         endif

         isco_ =.false.

         if(test)print*,'getirun: gl,gb,th,ang=',
     _        gl,gb,th,ang


c     ---- these values don't have much meaning for MCMC
         ilb=1
         ith=1  ! could be important for output
         iang=1

         if(restart)then   ! as of circles-0.2.0.pre6
            gl_pre= gl_sta
            gb_pre= gb_sta
            th_pre= th_sta
            angpre= angsta
            twipre= twista

            gl=gl_pre
            gb=gb_pre
            th=th_pre
            ang=angpre
            twist=twipre
         endif
         
         return
      endif

      twist = 0.5*(twimin+twimax)  ! default: constant twist if not mcmc

      ilb=ilbirun(irun)
      ith=ithirun(irun)
      iang= iangirun(irun)

      if(ilbirun(irun).lt.1)print*,'getirun: ERROR: ',
     _     'irun,ilbirun(irun)=',irun,ilbirun(irun)

      gl= gl_ilb(ilbirun(irun))
      gb= gb_ilb(ilbirun(irun))
c     th= (real(ithirun(irun))-0.5) *resdodec


c      ang= angmax - (real(iangirun(irun))-0.5)* delang  ! start from angmax
c     -- 04.03.2004 - should include endpoints see definition of delang --
      ang= angmax - (real(iangirun(irun)-1))* delang ! start from angmax


      th= thmin+ (real(ithirun(irun))-0.5) *resdodec

c     --- 19.07.2006 boud: circles-0.1.35:  .and.ilb.gt.1 ---
c     --- 29.06.2006 boud: circles-0.1.30 ---
      isco_ =.false.
      if(dodran.and.ilb.gt.1)then
         isco_ =.true.
         if(ith.eq.1.and.iang.eq.1)then ! first for this (gl,gb) pair
c     --- flush ran generator ---
            xx=rnd(0)*26
            do i=1,int(xx) 
               xx=rnd(0)
            enddo
            
            call spher2(gl,gb)
            
c         th= rnd(0)*360.0
c     --- 17.07.2006 boud: circles-0.1.35 ---
c     -     th is expressed in great circle degrees  (?) -
            if(ang.gt.tolang)then
               xx=rnd(0)*(
     _              max(180.0, 360.0 - thsafe/sin(ang*pi_180)))
               th=thmin + xx
               if(th.gt.360.0)th=th-360.0
            else
               th= rnd(0)*360.0
            endif

            gl_old=gl
            gb_old=gb
            th_old=th
         else   ! only randomise gl,gb,th;  not  ang
            gl=gl_old
            gb=gb_old
            th=th_old
         endif
      endif


      return
      end

