/* 
   circles - calculate identified circles statistics from CMB data (cosmic topology)

   Copyright (C) 2004 Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

*/

#include <stdio.h>
#include <math.h>
#include <gsl/gsl_integration.h>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>

#include <stddef.h>
#include <errno.h>
#include <string.h>

#include "config.h"

#define CIRCLES_WANT_DEBUG 0

/* 
   USER PARAMETERS - BEGIN
*/

/*
  Most of these parameters are defaults which can be overridden by
  setting them as environment variables. Both .h declarations and
  environment variables are overridden by command line options.
*/

/* directory where the input files in the following lines are found */
#define CIRCLES_INPUT_DIRECTORY "./"
#define NLEN_CIRCLES_INPUT_DIRECTORY 2

/* status file: if the --status option is selected, then this file
   must contain either the string "START" or else the last parameter
   set 
   DEFAULT:  no status file  (empty string) */
#define CIRCLES_STATUS_FILE ""
#define NLEN_CIRCLES_STATUS_FILE 0

/* 
   WMAP ILC (or other) map in FITS format, as distributed by the WMAP 
   group, e.g.
   http://lambda.gsfc.nasa.gov/product/map/m_products.cfm
#define CIRCLES_CMB_FILE_RAW "map_ilc_yr1_v1.fits"
*/

#define CIRCLES_CMB_FILE_RAW "wmap_ilc_3yr_v2.fits"
#define NLEN_CIRCLES_CMB_FILE_RAW 20

/* binary file of smoothed WMAP data, output by circles package 
#define CIRCLES_CMB_FILE_SMOOTH "smooW1d.dat"
*/
#define CIRCLES_CMB_FILE_SMOOTH "smoo_3yr_W1d.dat"
#define NLEN_CIRCLES_CMB_FILE_SMOOTH 16

/* text file with point sources in WMAP map */
#define CIRCLES_CMB_FILE_POINTS "wmap_pnt_src_cat_yr1_v1p1.txt"
#define NLEN_CIRCLES_CMB_FILE_POINTS 29

/* toy simulation file
   DEFAULT:  no toy file  (empty string) */
#define CIRCLES_TOY_FILE ""
#define NLEN_CIRCLES_TOY_FILE 0


/* mask file 
e.g. http://lambda.gsfc.nasa.gov/data/map/dr2/ancillary/wmap_kp2_r9_mask_3yr_v2.fits
*/
#define CIRCLES_CMB_FILE_MASK "wmap_kp2_r9_mask_3yr_v2.fits"
#define NLEN_CIRCLES_CMB_FILE_MASK 28

/* number of pixels in WMAP (or Planck...) file */
#define CIRCLES_NPIX 3145728

/* CIRCLES_NPIX_MAXN is   CIRCLES_NPIX + 1024, i.e. just a bit bigger
   to play safe */
#define CIRCLES_NPIX_MAXN 3146752 

#define CIRCLES_BYTES_IN_TEMP 4  /* bytes in real*4, real*8 ... */

/*
   USER PARAMETERS - END
   You probably should NOT change parameters below unless you're really
   sure you know what you're doing.
*/

/* 
   big arrays declared here as pointers 
   - these are indexed by pixel number up to maximum CIRCLES_NPIX
*/



float *temp;  /* Temperature  */
float *errtemp;  /*  uncertainty in Temperature */
float *gallong, *gallat; /* galactic longitude and latitude*/
float *tempcp;          /* temporary copy of *temp array */
int   *k_ring;          /* temporary ring ordering index */

int  circles_npix;  /* preprocessor constant CIRCLES_NPIX as variable */
int  pxalloc_status;
int  pxtempcp_status;
/* status of allocating the above memory  
  alloc = temp + errtemp + gallong + gallat
  tempcp = tempcp
   -1: no allocation tried
   0: allocation OK
   1: error in allocating
*/


/* main fortran program for circles calculations */

#define CIRC_F F77_FUNC_(circ_f,CIRC_F)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
void CIRC_F(
	    /* inputs */
	    char* input_directory, int32_t *nlen_input_directory,
	    char* status_file, int32_t *nlen_status_file,
	    char* cmb_file_raw, int32_t *nlen_cmb_file_raw,
	    char* cmb_file_smooth, int32_t *nlen_cmb_file_smooth,
	    char* cmb_file_mask, int32_t *nlen_cmb_file_mask,
	    char* toy_file, int32_t *nlen_toy_file,
	    char* binary_format, int32_t *nlen_binary_format,
	    int32_t *want_verbose, int32_t *want_no_warn,
	    int32_t *want_ring,     /* flag: do ring ordering for input file? */
	    int32_t *want_mask,     /* flag: use a mask file? */
	    int32_t  *want_cdiscs,
	    int32_t *want_corrS3,   /* flag: do S3 spatial correlation fn? */
	    char* ch_nrandom, int32_t *nlen_nrandom,
	    int32_t  *npix,
	    float *temp, float *errtemp, float *gallong, float *gallat,
	    float *tempcp, int32_t *k_ring,
	    int32_t  *want_new, int32_t *want_use_unsmoothed,
	    /* outputs */
	    int32_t  *want_random,
	    int32_t  *want_ransmooth
	   );


#define NEWDAT F77_FUNC(newdat,NEWDAT)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
void NEWDAT(
	    int  *npix,
	    float *temp, float *errtemp
	   );


/* allocate pixel data arrays */
#define PXALLO F77_FUNC(pxallo,PXALLO)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
int PXALLO(
	    int *want_verbose,
	    int *circles_npix,
	    float **temp, float **errtemp, float **gallong, float **gallat,
	    float **tempcp, int **k_ring
	    );

/* check status of allocation of pixel data arrays */
#define PXSTAT F77_FUNC(pxstat,PXSTAT)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
int PXSTAT();


/* allocate tempcp = copy of temp */
#define PXAL_2 F77_FUNC_(pxal_2,PXAL_2)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
int PXAL_2(
	    int *want_verbose,
	    float **tempcp
	    );

/* free up gallong, gallat, tempcp */
#define PXFR_2 F77_FUNC_(pxfr_2,PXFR_2)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
int PXFR_2(
	    int *want_verbose
	    );



/* plot statistics for dodecahedral hypothesis */

#define PDODEC F77_FUNC(pdodec,PDODEC)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
void PDODEC(
	    int* want_iv, 
	    int* want_random,
	    int* want_ransmooth
	   );

#define PSMOOT F77_FUNC(psmoot,PSMOOT)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
void PSMOOT(
	    int* mxnperang,
	    int* mxang,
	    int* nang_p,
	    int *nper_ang,
	    float *c_ang4,
	    float *ang_p4
	   );

#define PCIRCL F77_FUNC(pcircl,PCIRCL)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
void PCIRCL(
	      char* input_directory, int *nlen_input_directory,
	      int* want_SZ
	      );

/* plot matching discs, not just circles */
#define PDISCS F77_FUNC(pdiscs,PDISCS)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
void PDISCS(
	    char* input_directory, int *nlen_input_directory,
	    int  *npix,
	    int *want_verbose, int *want_no_warn,
	    float *temp, float *errtemp, float *gallong, float *gallat,
	    float *tempcp
	   );

/* correlate opposite discs  */
#define CDISCS F77_FUNC(cdiscs,CDISCS)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
void CDISCS(
	    float *gl_in, float *gb_in, float *ang, float *thdodec,
	    int *galcut,
	    float *bcut, float *gcmin,
	    int *want_verbose, int *want_no_warn,
	    float *temp, float *errtemp, float *gallong, float *gallat
	   );

/* analyse Markov Chain Monte Carlo (MCMC) log files  */
#define ANALYS F77_FUNC(analys,ANALYS)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
void ANALYS(
	    char* analysis_files, int *nlen_analysis_files,
	    char* analysis_pars, int *nlen_analysis_pars
	   );

