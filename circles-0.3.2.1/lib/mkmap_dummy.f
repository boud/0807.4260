C   circles - calculate identified circles statistics from CMB data (cosmic topology)
C
C   Copyright (C) 2004 Boud Roukema
C
C   This program is free software; you can redistribute it and/or modify
C   it under the terms of the GNU General Public License as published by
C   the Free Software Foundation; either version 2, or (at your option)
C   any later version.
C
C   This program is distributed in the hope that it will be useful,
C   but WITHOUT ANY WARRANTY; without even the implied warranty of
C   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C   GNU General Public License for more details.
C
C   You should have received a copy of the GNU General Public License
C   along with this program; if not, write to the Free Software Foundation,
C   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
C

C     WARNING: this is only a dummy file with dummy functions
C     In principle, this could be replaced by functions for making
C     CMB simulation maps.

      subroutine chooseamps
      return
      end

      real*4  function delTgal(gall,galb)
      delTgal = -9e9 ! dummy value
      return
      end

