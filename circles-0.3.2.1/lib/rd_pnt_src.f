C     Copyright (C) 2004 B. Roukema et al. <shape-univ at astro.uni.torun.pl>
C     
C     This program is free software; you can redistribute it and/or modify
C     it under the terms of the GNU General Public License as published by
C     the Free Software Foundation; either version 2 of the License, or
C     (at your option) any later version.
C     
C     This program is distributed in the hope that it will be useful,
C     but WITHOUT ANY WARRANTY; without even the implied warranty of
C     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C     GNU General Public License for more details.
C     
C     You should have received a copy of the GNU General Public License
C     along with this program; if not, write to the Free Software
C     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
C  
C     See also http://www.gnu.org/licenses/gpl.html

c     read in point source catalogue


      subroutine rd_pnt(glpnt,gbpnt,indir2,nindir)

      include 'npnt.dec'

C     INPUTS:
      real*4  glpnt(mxpnt),gbpnt(mxpnt)
      character*256  indir2 ! safe string version of indir
      integer        nindir    ! number of characters in  indir

      character*256 flnm
      integer     iwmapid !    WMAPID    WMAP Source ID
      integer      RAh !      Right ascension (J2000)
      integer       RAm  !     Right ascension (J2000)
      real          RAs !      Right ascension (J2000)
      character*1  decsign !  DE-       Sign of declination (J2000)
      integer       DEd  !     Declination (J2000)
      integer       DEm !      Declination (J2000)
      integer      DEs !      Declination (J2000)
      real        GLAT    !  Galactic latitude
      real        GLON    !  Galactic longitude
      real         Flux_K !   K-band peak flux density
      real       Flux_Ka  ! Ka-band peak flux density
      real       Flux_Q   ! Q-band peak flux density
      real       Flux_V   ! V-band peak flux density
      real       Flux_W   ! W-band peak flux density
      real       e_Flux_K ! Uncertainty in K-band peak flux density
      real       e_Flux_Ka! Uncertainty in Ka-band peak flux density
      real       e_Flux_Q ! Uncertainty in Q-band peak flux density
      real       e_Flux_V ! Uncertainty in V-band peak flux density
      real       e_Flux_W ! Uncertainty in W-band peak flux density
      real       Alpha    ! Flux Spectral Index
      real       e_Alpha  ! Uncertainty in Flux Spectral Index
      character*16 name5GHz ! A16   ---     ID5      *5 GHz counterpart name
      character*1  mflag   !  [M] for sources with multiple 5 GHz ids

      character*1  ch1

c      flnm='/scratch2/boud/WMAP/wmap_pnt_src_cat_yr1_v1p1.txt'
c      flnm='/scratch0/boud/WMAP/wmap_pnt_src_cat_yr1_v1p1.txt'

      if(nindir.gt.0.and.indir2(nindir:nindir).ne.'/')then
         flnm=indir2(1:nindir)//'/'//'wmap_pnt_src_cat_yr1_v1p1.txt'
      else
         flnm=indir2(1:min(nindir,256))//
     _        'wmap_pnt_src_cat_yr1_v1p1.txt'
      endif

      print*,'Will try to open: '
      print*,'__',flnm,'__'

      open(1,file=flnm,status='old',access='sequential',
     _     form='formatted')

c     read comments
      do i=1,53
         read(1,'(a)')ch1
c         print*,'ch1=',ch1
      enddo


      nobject=208
      do ii=1,nobject
         read(1,'(I3,a1,2I2,F4.1,2A1,3I2,2F7.2,a1,5F5.1,7F4.1,A,A)')
     _        iwmapid,ch1,rah,ram,ras,ch1,decsign,ded,dem,des,
     _        glon,glat
c     _        ch1,flux_k,flux_ka,flux_q,flux_v,flux_w,
c     _        e_flux_k,e_flux_ka,e_flux_q,e_flux_v,e_flux_w,
c     _        alpha,e_alpha,
c     _        name5GHz,mflag
c         print*,iwmapid,rah,ram,ras,decsign,ded,dem,des
c         print*,'ii,glat,glon=',ii,glat,glon
         glpnt(ii)= glon
         gbpnt(ii)= glat
      enddo
      close(1)

      return
      end
