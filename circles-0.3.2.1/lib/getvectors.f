C     Copyright (C) 2004 B. Roukema et al. <shape-univ at astro.uni.torun.pl>
C     
C     This program is free software; you can redistribute it and/or modify
C     it under the terms of the GNU General Public License as published by
C     the Free Software Foundation; either version 2 of the License, or
C     (at your option) any later version.
C     
C     This program is distributed in the hope that it will be useful,
C     but WITHOUT ANY WARRANTY; without even the implied warranty of
C     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C     GNU General Public License for more details.
C     
C     You should have received a copy of the GNU General Public License
C     along with this program; if not, write to the Free Software
C     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
C  
C     See also http://www.gnu.org/licenses/gpl.html


c     -- get the vectors necessary for going around a circle --
      subroutine getvectors(
     _     iv,jv,mv,nv,
     _     doT2,dododec,
     _     efund,Rfund, rSLS, ntheta,
     _     gall,
     _     galb,
     _     thdodec,
     _     ang,
     _     vx1,vy1,vz1, ax1,ay1,az1,bx1,by1,bz1,
     _     vx2,vy2,vz2, ax2,ay2,az2,bx2,by2,bz2,
     _     wmod,
     _     ivjv_valid, nth,dtheta)

C     INPUTS:
      integer*4  iv,jv,mv,nv    ! counters; only iv,jv for T2
      logical*4  doT2           !T2
      logical*4  dododec        ! Poincare dodecahedral
      real*4   efund(3,3) ! galactic coords
      real*4   Rfund(3)         ! lengths of axes in h^-1 Mpc
      real*4   rSLS             ! r surface of last scattering
      integer*4  ntheta         ! no of thetas for great circle

c     dodecahedral inputs (all in degrees) ! declared since 0.2.5
      real*4   gall  ! gal longitude
      real*4   galb  ! gal latitude
      real*4   thdodec ! third dodecahedral free parameter
      real*4   ang  ! circle radius 


C     OUTPUTS:
      real*4   vx1,vy1,vz1         ! vector to centre of circle
      real*4   ax1,ay1,az1,bx1,by1,bz1 ! orthonorm vec to step around circle
      real*4   vx2,vy2,vz2         ! vector to centre of circle
      real*4   ax2,ay2,az2,bx2,by2,bz2 ! orthonorm vec to step around circle
      real*4   wmod             ! rSLS *sin(angular radius of circle)
      logical*4  ivjv_valid     ! test on validity of iv,jv
      integer*4  nth            ! no of thetas for actual circle
      real*4     dtheta         ! in radians (will be used with wmod)

C     LOCAL:
      logical*4  test
      parameter(pi=3.1415926535898,pi_180=pi/180.0)

      parameter(nface=12)
c     -- centre of each face --
      real*4  xdod(nface),ydod(nface),zdod(nface)
c     -- orthogonal unit vectors to generate circle
      real*4  xdodp(nface,2),ydodp(nface,2),zdodp(nface,2) ! perpendiculars

      data test /.false./

      if(doT2)then
c     -- vector to topological image of observer --
         vx1 = 0.5* (real(iv)*Rfund(1)*efund(1,1)
     _        + real(jv)*Rfund(2)*efund(2,1))
         vy1 = 0.5* (real(iv)*Rfund(1)*efund(1,2)
     _        + real(jv)*Rfund(2)*efund(2,2))
         vz1 = 0.5* (real(iv)*Rfund(1)*efund(1,3)
     _        + real(jv)*Rfund(2)*efund(2,3))
         
         vmod = sqrt(vx1*vx1 + vy1*vy1 + vz1*vz1)
         
         if(test)print*,'iv,jv,vmod=',iv,jv,vmod

         
c     --- set ivjv_valid = .true. only if vector is within SLS ---
         ivjv_valid=.false.
c         if(vmod.lt.0.99*rSLS*2.0)then  ! 0.5 now included in vx,vy,vz
         if(vmod.lt.0.99*rSLS)then
            ivjv_valid=.true.
         else
            return   
         endif


c     -- unit vector --
         if(test)print*,'= iv,jv,vmod=',iv,jv,vmod
         vxun = vx1/vmod
         vyun = vy1/vmod
         vzun = vz1/vmod
c     -- two perpendicular unit vectors  a  b  for generating circles, 
c     "long" axis used as vector always non-parallel --
         jcol=3
         call cross(vxun,vyun,vzun,
     _        efund(jcol,1),efund(jcol,2),
     _        efund(jcol,3),
     _        ax1,ay1,az1)
c     -- in fact for T^2 , b  is parallel to  the efund(3,.)
         call cross(vxun,vyun,vzun,
     _        ax1,ay1,az1,  bx1,by1,bz1)

         vx2= -vx1
         vy2= -vy1
         vz2= -vz1

         ax2=ax1
         ay2=ay1
         az2=az1

         bx2=bx1
         by2=by1
         bz2=bz1


c     -- radius of identified circle --
c         wmod = sqrt(rSLS * rSLS - 0.25*vmod*vmod)
         wmod= sqrt(rSLS * rSLS - vmod*vmod)


c     --- how many intervals in theta ? ---
         nth= int(real(ntheta)*wmod/rSLS)
c     print*,'ntheta,nth=',ntheta,nth

         if(nth.lt.1)then
            ivjv_valid=.false.
            return
         endif

         dtheta= 2.0*pi /real(nth)
            
         if(test)then
            print*,'iv,jv,vx,vy,vz=',
     _           iv,jv,vx,vy,vz
         endif
      elseif(dododec)then
c     --- assumes that iv=1 will be called first ---
c         if(iv.eq.1)then
c         endif

         call gendodec(gall,galb, thdodec,
     _        xdod,ydod,zdod, 
     _        xdodp,ydodp,zdodp)

         iv2= mod(iv+6-1,12)+1

         wmod= sin(ang*pi_180) * rSLS
         vmod= cos(ang*pi_180) * rSLS ! version 0.1.65

c     version 0.1.65: s/* rSLS/* vmod/  -> otherwise alpha is wrong when >> 10 deg
         vx1= xdod(iv)  * vmod
         vy1= ydod(iv)  * vmod
         vz1= zdod(iv)  * vmod
         vx2= xdod(iv2)  * vmod
         vy2= ydod(iv2)  * vmod
         vz2= zdod(iv2)  * vmod
         
         ax1= xdodp(iv,1)
         bx1= xdodp(iv,2)
         ay1= ydodp(iv,1)
         by1= ydodp(iv,2)
         az1= zdodp(iv,1)
         bz1= zdodp(iv,2)

         ax2= xdodp(iv2,1)
         bx2= xdodp(iv2,2)
         ay2= ydodp(iv2,1)
         by2= ydodp(iv2,2)
         az2= zdodp(iv2,1)
         bz2= zdodp(iv2,2)

c         wmod= sin(ang*pi_180) * rSLS

         if(1.le.iv.and.iv.le.6.and.
     _        jv.eq.0)ivjv_valid=.true.

c     --- how many intervals in theta ? ---
         nth= int(real(ntheta)*wmod/rSLS)
c         print*,'getvectors: ntheta,nth,wmod=',ntheta,nth,wmod

         if(nth.lt.1)then
            ivjv_valid=.false.
            return
         endif

         dtheta= 2.0*pi /real(nth)

      else
         stop 'other options not yet programmed'
      endif

      return
      end
