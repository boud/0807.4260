C     Copyright (C) 2004 B. Roukema et al. <shape-univ at astro.uni.torun.pl>
C     
C     This program is free software; you can redistribute it and/or modify
C     it under the terms of the GNU General Public License as published by
C     the Free Software Foundation; either version 2 of the License, or
C     (at your option) any later version.
C     
C     This program is distributed in the hope that it will be useful,
C     but WITHOUT ANY WARRANTY; without even the implied warranty of
C     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C     GNU General Public License for more details.
C     
C     You should have received a copy of the GNU General Public License
C     along with this program; if not, write to the Free Software
C     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
C  
C     See also http://www.gnu.org/licenses/gpl.html
      
      subroutine pdodec(want_iv,iw_ran,iransm) ! plot statistics for
                                               ! dodecahedral hypothesis
C     INPUTS:
      integer*4 want_iv
      integer*4 iw_ran  ! \equiv want_random \equiv (int)dodran
      integer*4 iransm ! smooth out probs from random circle positions? yes/no = 1/0

C     LOCAL:
      logical*4 do_iv ! v0.1.34  plot individual iv's as lines for  iplot.eq.3
      logical*4 dodran  ! (logical)iw_ran
      logical*4 ang_even ! 0.1.35  are all the  nper_ang(*) equal to each other?


c     ==== plot "sigma" and "corr" parameters etc  ====
      parameter(pi=3.14159265359,pi_180=pi/180.0)
      
      parameter(maxflnm=3)
      character*256   flnm(maxflnm)
      character*3     ch3
      character*40    chxaxis

c      parameter(maxcurv=4)
      include 'npar.dec'
      real*4    q(maxcurv),cc(maxcurv)
      real*4    rSLS            ! distance to SLS in h^-1 Mpc
      real*4    drSLS           ! thickness of SLS in h^-1 Mpc

c     ---- difference statistics ----
c      parameter(mxsig=200)
c      parameter(mxsig=1000)

      parameter(mxsig=mxpar)
      real*4    sigma(mxsig,maxcurv)
      real*4    dmean(mxsig,maxcurv)
      real*4    sfreq(mxsig,maxcurv)
      real*4    corr(mxsig,maxcurv),cwght(mxsig,maxcurv)
      real*4    cdopp(mxsig,maxcurv)
      real*4    smooth(mxsig,maxcurv) ! just for playing 22/06/99

c     --- v0.1.34 ---
      include  'ncopies.dec'
c     ---- correlations of individual (iv=iv1,iv2) circle pairs
      real*4    co_iv(mxpar,-iimax:iimax,0:jjmax)   

      real*4    glpar(mxsig) ! g long
      real*4    gbpar(mxsig) ! g lat
      real*4    thpar(mxsig) ! theta of dodecahedron rotation
      real*4    angpar(mxsig) ! ang radius of circle

      logical*4 iscontrol(mxsig) ! 0.1.35: is it in the control sample?

c     ---- plot mode instead of scatter plot ---
      logical*4  plmode ! plot mode   !! OLD and for median, not mode!
c      parameter(mxang=6000)
      parameter(mxang=100)
      parameter(minang=5)
      real*4    cbmplot(mxang),angplot(mxang)

      parameter(maxpair=20000)
c      parameter(maxpair=20)

      parameter(mxdlist=1,idlist=1)      
      real*4    dlist(maxpair,mxdlist,maxcurv)
      integer*4  npair(mxsig,maxcurv)

c     --- single sig value --- (circleT2.f initial version)
      real*4    sigma_ci(maxcurv,mxsig)
      real*4    dmean_ci(maxcurv,mxsig)
      real*4    sfreq_ci(maxcurv,mxsig)
      real*4    corr_ci(maxcurv,mxsig),cwght_ci(maxcurv,mxsig)
      real*4    cdopp_ci(maxcurv,mxsig)

c     ---- for plotting ----
      parameter(mxplot= mxsig)
      real*4    sigmap(mxsig)
      real*4    dmeanp(mxsig)
      real*4    corrp(mxsig)
      real*4    cdoppp(mxsig)

c     --- v0.1.34 ---
      real*4    co_ivp(mxsig,-iimax:iimax,0:jjmax)
      real*8    co_iv8(mxsig,-iimax:iimax,0:jjmax)
      real*4    probiv(-iimax:iimax,0:jjmax),probtot  ! probabilities
      

      real*4    glparp(mxsig)    ! g long
      real*4    gbparp(mxsig) ! g lat
      real*4    thparp(mxsig) ! theta of dodecahedron rotation
      real*4    angpp(mxsig) ! ang radius of circle
      real*4    sfreqp(mxsig) ! number of valid pixel pairs

      real*4    corrq(mxsig)
      real*4    glparq(mxsig)    ! g long
      real*4    gbparq(mxsig) ! g lat


c      parameter(mxbin=100)
c      real*4    angbin(mxbin),angfreq(mxbin)
      parameter(mxangpair=mxang*mxang)
      real*4    ang12(mxangpair)


c     --- for sorting into probability distribs as a fn of angle ---
c     -- mxang defined above for old routine
      parameter (mxtol= 3)  ! tolerance for mxsig/mxang
c      parameter (mxnperang = mxtol * (mxsig/mxang))
      parameter (mxnperang = mxtol * (mxsig/minang)) ! 0.1.42
      real*4    c_ang(mxnperang,mxang) ! correlations at each angle
      real*8    c_ang8(mxnperang,mxang) ! correlations at each angle

      real*4    c_angt(mxang,mxnperang) ! transpose: correlations at each angle
      integer*4 nang_p ! number of angles
      integer*4 nper_ang(mxang)  ! number of corrns at iang-th angle
      real*4    ang_p(mxang)     ! mean angle in each angle bin
      parameter(tol_ang= 0.01)  ! tolerance for real -> integer
c     --- v0.1.37 ---
      real*4    probia(mxang)    ! probability all iv's this highly correlated
c     - for each iang_p, ii, jj, should only contribute a prob once -
      logical*4 pr_done(mxang,-iimax:iimax,0:jjmax) 


      logical*4 test, test2, test3, smoolog
c      character*2  ch2
      character*1  ch1
c      character*3  ch3


      parameter(nface=12)
c     -- centre of each face --
      real*4  xdod(nface),ydod(nface),zdod(nface)
      real*4  gldod(nface),gbdod(nface)

c     -- orthogonal unit vectors to generate circle
      real*4  xdodp(nface,2),ydodp(nface,2),zdodp(nface,2) ! perpendiculars

      real*4    erffun
      external  erffun

c     ---- plotting of KS prob2 added for counter.tex -- added 24/08/99 --- 

      logical*4 doxarbit ! arbitrary x-axis
      logical*4 doxgl,doxgb ! do gl or gb as x-axis
      logical*4 doxth         ! show as fn of thpar in x-axis
      logical*4 doxang ! show as fn of angle in x-axis
      logical*4 doxoffset ! small random x-offset to separate points
      logical*4 doline ! pgline or pgpoint?
      logical*4 doangf         ! filter by angle

      real*4    angf1,angf2
      real*4    xarbit(mxpar)
c     --- 0.1.34 ---
      real*4    xar_iv(mxpar)  ! for iv, use only ithcmax values not ithsmin

      logical*4 doacmax ! hardwire acmax
      logical*4 doasmax         ! hardwire asmax

      logical*4 doascii ! ascii output

      logical*4 domode ! do mode of 3 files? ! not yet implemented

      logical*4 do4plots ! do the standard 4 plots
      logical*4 doskydist ! plot sky distribution of selected points

      logical*4 docweight ! use cwght for corr because of g plane etc

      logical*4 docolour ! colour plot(s), line styles

c      logical*4       doerrupper
c      common /erruppcomm/ doerrupper,errupper ! shared with rdWMAP
      include 'errupp.dec'

      data flnm /'sigmaall.dat',' ',' '/
c      data flnm /
c     _     '../sigma_39z.dat',
c     _     '../sigma_39m.dat',
c     _     '../sigma_39p.dat'
c     _     /
c      data flnm /'sigmaall_res5d_w5d.dat'/

c      data nflnm /1/

      data tol /1e-4/

      test=.false. !.true. ! 
      test2=.false. !.true. ! 
      test3=.true. ! .false. !
      smoolog=.false. !.true.            ! 

      doxarbit=.true. !.false. !
      doxgl = .false. !.true. !
      doxgb = .false. !.true.            !
      doxang = .true.            !.false.          !
      doxth = .false.           !.true.            !

      doxoffset = .true.        !.false.       !

      do4plots=  .true.         !.false.!
c      do_iv= .true.! .false.!
      if(want_iv.eq.1)then
         do_iv=.true.
      else
         do_iv=.false.
      endif
      if(iw_ran.eq.1)then
         dodran=.true.
      else
         dodran=.false.
      endif
      print*,'plotdodec: dodran=',dodran


      doskydist= .false.        !.true. !
      nbin= 90
      bin1= 0.0
      bincut= 0.0! 30.0  ! don't count angles below bincut
      bin2= 180.0
      
      dodec1= 63.43             ! separation between adjacent faces in degrees
      dodec2= 116.57            ! separation between non-adjacent faces deg
      

      docweight= .false. !.true.           !
      aa= 11.0 ! alpha in degrees
      delth= 0.5 ! resdodec in degrees = angular resolution around circle
      var_t= 0.07*0.07 ! variance in temperatures mK^2 - rough estimate
      cwght0= 2.0*  ! factor of two in defn of cwght = tt1*tt1+tt2*tt2
     _     2.0*pi* aa /delth * var_t
      angp0= aa

      docolour= .false. !.true.!

      doline=.false. ! .true.!
      doangf= .false. !.true.!
      angf1= 37.0! 5.0! 30.0! 8.0 !                  !8.0
      angf2= angf1 +1.0 !38.0 !40.0 !14.0
c      angf1=27.0
c      angf2=37.0

      doacmax=.true.! .false. ! 
      acmax0 = 1.0
      if(docweight)acmax0=1.5
      doasmax=.true.            ! .false. ! 
      asmax0 = 80.0 /1.5

      doascii= .true.!.false. !
c      corrmin=  1.4! 0.20 !99.9! 0.7! 0.37! 0.5 ! 99.9!
      corrmin=  -9e9 !  to print out everything
      sigmax=  -99.9! 15.0! 33.0

      domode=.false.            !.true.! 
      if(domode)then
         stop 'domode  not yet implemented'
         nflnm=3
      else
         nflnm=1
      endif

      plmode= .false. !.true.!
      if(plmode)nflnm=1

      dlin1= -1.5
      dlin2= 1.5
      dlog1= -1.9
      dlog2=0.3
      slin1= 0.0
      slin2= 0.15 !2.5
      clin1= -0.03
      clin2=0.03
      plin1=-0.015
      plin2=0.015

      isym=1
c     ...

c     -- which lambda_0 values to plot --
      icc1=1
      icc2=1
      iccstep=1

c     1,2 !24,25           ! 1,ncc,5      

      admax=-9e9
      asmax=-9e9
      acmax=-9e9
      apmax=-9e9

      print*,'plotdodec starting: nflnm=',nflnm
      do iflnm=nflnm,1,-1 !read 1st file last so that it's not overwritten
         print*,'iflnm=',iflnm

c     ---- read sigma's from file ----
         open(1,file=flnm(iflnm),access='sequential',
     _        form='unformatted',status='old')  ! 02.04.2003
         read(1)glong,galb,thetadeg,Rfund1,Rfund2,Rfund3,
     _        gl1_3,gb1_3,gl2_3,gb2_3, 
     _        xISW_0,ntd  ! added 24/08/99
         read(1)ncc,cc1,cc2,H_0,drSLS,ipar1,ipar2,
     _        mxpair

         nsig= ipar2-ipar1+1
         print*,'nsig=',nsig

         print*,' '
         print*,'iflnm,glong,galb,thetadeg=',
     _        iflnm,glong,galb,thetadeg
         print*,'Rfund1,Rfund2,Rfund3=',Rfund1,Rfund2,Rfund3,' : ',
     _        xISW_0,ntd


         write(6,'(a,f10.1,a,f10.1,a,f10.1,a,f10.1,a)'),
     _        'gl1_3,gb1_3,gl2_3,gb2_3=',
     _        gl1_3,' & ',gb1_3,' & ',gl2_3,' & ',gb2_3, ' & '
         print*,'ncc,cc1,cc2,H_0,drSLS,ipar1,ipar2,nsig,mxpair='
         print*,ncc,cc1,cc2,H_0,drSLS,ipar1,ipar2,nsig,mxpair


         read(1)((dmean(i,icc),i=1,nsig),icc=1,maxcurv)
         read(1)((sigma(i,icc),i=1,nsig),icc=1,maxcurv)
         read(1)((sfreq(i,icc),i=1,nsig),icc=1,maxcurv)
         read(1)(( corr(i,icc),i=1,nsig),icc=1,maxcurv)
         read(1)((cdopp(i,icc),i=1,nsig),icc=1,maxcurv)
         read(1)((cwght(i,icc),i=1,nsig),icc=1,maxcurv)
         read(1)((npair(i,icc),i=1,nsig),icc=1,maxcurv)

         read(1)(glpar(i),i=1,nsig)
         read(1)(gbpar(i),i=1,nsig)
         read(1)(thpar(i),i=1,nsig)
         read(1)(angpar(i),i=1,nsig)
         read(1)(iscontrol(i),i=1,nsig)

         read(1)(((dlist(ip,i,icc),ip=1,mxpair),
     _        i=1,mxdlist),icc=1,ncc)

c     ---- v0.1.34 ----
         read(1)iv1,iv2,ivdel,jmax,ivdel2,nangle
         print*,'plotdodec check: read in: ',
     _        'iv1,iv2,ivdel,jmax,ivdel=',
     _        iv1,iv2,ivdel,jmax,ivdel2
         read(1)(((co_iv(i,iv,jv),i=1,nsig),
     _        iv=iv1,iv2,ivdel),
     _        jv=0,jmax,ivdel)
c     print*,'plotdodec: (co_iv(1,iv,0),iv=1,6)=',
c     _           (co_iv(1,iv,0),iv=1,6)


         close(1)

         if(domode.or.plmode)then
            print*,'will call initmodeang',nflnm,iflnm,nsig
            print*,'plotdodec: domode/plmode Not yet implemented!'
c            call initmodeang(nflnm,iflnm,nsig,corr,sigma,angpar)
         endif

         if(plmode)then
            print*,'plotdodec: domode/plmode Not yet implemented!'
c            call setmodeang( 
c     _           nangplot,cbmplot,angplot) 
            iang=1
c            tol=1e-4
            cbmmax= -99.9
            do while(iang.lt.nangplot.and.angplot(iang).lt.angf1)
               iang=iang+1
            enddo
            if(cbmplot(iang).gt.cbmmax)then
               cbmmax=cbmplot(iang)
               ibest= iang
            endif
            do while(iang.lt.nangplot.and.angplot(iang).lt.angf2)
               if(cbmplot(iang).gt.cbmmax)then
                  cbmmax=cbmplot(iang)
                  ibest= iang
               endif
               iang=iang+1
            enddo
            print*,'ibest,angplot(ibest)=',ibest,angplot(ibest)
            
         endif

c     ----

         icc=1
         icc0=1

         print*,'dmean(i,icc),icc=1,10=',(dmean(i,icc),i=1,10)
         print*,'sigma(i,icc)^2,icc=1,10=',(sigma(i,icc),i=1,10)
         print*,'sfreq(i,icc),icc=1,10=',(sfreq(i,icc),i=1,10)
         print*,'corr(i,icc),icc=1,10=',(corr(i,icc),i=1,10)
         print*,'cdopp(i,icc),icc=1,10=',(cdopp(i,icc),i=1,10)
c         endif

         glmin=9e9
         glmax=-9e9
         gbmin=9e9
         gbmax=-9e9
         angmin=9e9
         angmax=-9e9

         do icc=1,ncc
            do ipar=1,nsig
               dmean_ci(icc,ipar)=dmean(ipar,icc)
               if( sigma(ipar,icc) .gt.0.0)then
c     sigma_ci(icc,iflnm)= sqrt( sigma(ipar,icc ) )  !not the sd.!
                  xx= sigma(ipar,icc ) 
     _                 - dmean(ipar,icc)*dmean(ipar,icc) 
                  if(xx.gt.0.0)then
                     sigma_ci(icc,ipar)= sqrt(xx)
c     --- if errupper is just a constant, then don't use it ---
                     if(doerrupper)then
                        sigma_ci(icc,ipar)= 
     _                       sigma_ci(icc,ipar)*errupper
     _                       *1000.0 ! convert to microK
                     endif

                  else
                     sigma_ci(icc,ipar)= -0.99
                  endif
               else
                  sigma_ci(icc,ipar)= -0.99
               endif


               sfreq_ci(icc,ipar)=sfreq(ipar,icc)
               corr_ci(icc,ipar)=corr(ipar,icc)
               cdopp_ci(icc,ipar)=cdopp(ipar,icc)
               cwght_ci(icc,ipar)=cwght(ipar,icc)

               admax=max(admax,abs(dmean_ci(icc,ipar)))
               asmax=max(asmax,abs(sigma_ci(icc,ipar)))
               acmax=max(acmax,abs(corr_ci(icc,ipar)))
               apmax=max(apmax,abs(cdopp_ci(icc,ipar)))
               glmin=min(glmin, glpar(ipar))
               glmax=max(glmax, glpar(ipar))
               gbmin=min(gbmin, gbpar(ipar))
               gbmax=max(gbmax, gbpar(ipar))


               if(test)then
                  if(angpar(ipar).lt.angmin)
     _                 print*,'ipar,angmin,angpar=',
     _                 ipar,angmin,angpar(ipar)
               endif
               


               angmin=min(angmin, angpar(ipar))
               angmax=max(angmax, angpar(ipar))
            enddo
         enddo ! do icc=1,ncc
         if(test)print*,'ipar=',ipar
      enddo  !      do iflnm=1,nflnm

      print*,'admax,asmax,acmax,apmax=',admax,asmax,acmax,apmax
      print*,'glmin,glmax,gbmin,gbmax,angmin,angmax=',
     _     glmin,glmax,gbmin,gbmax,angmin,angmax
      print*,' '

      do icc=1,ncc
         do ipar=1,nsig
            sigma(ipar,icc)= sigma_ci(icc,ipar)
         enddo
      enddo





c     --- curv pars ---
      if(ncc.eq.1)then
         dcc=99.9
      else
         dcc=(cc2-cc1)/real(ncc-1)
      endif
      do icc=1,ncc
         cc(icc)=cc1+(real(icc-1))*dcc
         q(icc)= 0.5-1.5*cc(icc)
      enddo
      do icc=1,ncc
         print*,' '
         print*,'icc,cc(icc),dmean(1,icc),sigma(1,icc),',
     _        'sfreq(1,icc),corr(1,icc),cdopp(1,icc),cwght(1,icc)'
         print*,icc,cc(icc),dmean(1,icc),sigma(1,icc),
     _        sfreq(1,icc),corr(1,icc),cdopp(1,icc),cwght(1,icc)
      enddo


c     ==== filter for plots and outputs ====
      if(ncc.gt.1)print*,'WARNING: assuming ncc=1'
      icc=ncc

      ip=0
      n_ivp=0
      do isig=1,nsig
         if( (.not.(doangf)).or.
     _        ( 
     _        (doangf.and.angf1.lt.angpar(isig)
     _        .and.angpar(isig).lt.angf2)
     _        )
     _        )then
            ip=ip+1

            sigmap(ip)=sigma(isig,icc)
            dmeanp(ip)=dmean(isig,icc)
            corrp(ip)=corr(isig,icc)
            cdoppp(ip)=cdopp(isig,icc)

c     --- for iv, use only ithcmax values not ithsmin            
            if(2*(ip/2).eq.ip.and.(.not.(iscontrol(ip))))then
               n_ivp=n_ivp+1
               do jv=0,jmax,ivdel
                  do iv=iv1,iv2,ivdel
                     co_ivp(n_ivp,iv,jv)=co_iv(isig,iv,jv)
                     co_iv8(n_ivp,iv,jv)=dble(co_iv(isig,iv,jv)) !0.1.36
                  enddo
               enddo
            endif


            if(docweight)then
c     --- this is really just a normalisation by the number of pixels,
c     done using cwght since nthc is not stored for every calculation ---
               if(cwght(isig,icc).gt.tol)then
                  corrp(ip)= corr(isig,icc)
     _                 *(cwght(isig,icc)/cwght0) !
     _                 *(angp0/angpar(isig))
               endif
            endif

            
            glparp(ip)=glpar(isig)
            gbparp(ip)=gbpar(isig)
            thparp(ip)=thpar(isig)
            angpp(ip)=angpar(isig)
            sfreqp(ip)=sfreq(isig,icc) ! WARNING: overwrites if several icc's


            if(domode)then
            print*,'plotdodec: domode/plmode Not yet implemented!'
c               call getmodeang( sigmap(ip),corrp(ip),angpp(ip),
c     _              sigout,corout)
               sigmap(ip)=sigout
               corrp(ip)=corout
               acmax0= 5.0
               doasmax=.true.   ! .false. ! 
               asmax0= 10.0
            endif
         endif
      enddo
      np=ip
      print*,'plotting: np=',np

      if(np.le.0)stop 'np too small'
      

c     ==== ascii output ====
      if(doascii)then
         do ip=1,np
c     ---- deprecated: small sigma test: sigmap(ip).lt.sigmax
c            if(corrp(ip).gt.corrmin.or.sigmap(ip).lt.sigmax)then
            if(corrp(ip).gt.corrmin)then
               write(6,'(a,f10.5,f8.0,4f8.1)')
     _              'highcor: ',corrp(ip), !,sigmap(ip),
     _              sfreqp(ip),
     _              glparp(ip),gbparp(ip),thparp(ip),angpp(ip)
               call gendodec(glparp(ip),gbparp(ip),thparp(ip),
     _              xdod,ydod,zdod, 
     _              xdodp,ydodp,zdodp)
               do idod=1,7
                  call xyz_rda(xdod(idod),ydod(idod),zdod(idod), 
     _                 rr,del,alph)
                  gldod(idod)= alph*15.0
                  gbdod(idod)= del
               enddo
               write(6,'(a,14f6.1)')
     _              'dod: ',(gldod(idod),gbdod(idod),idod=1,6)
               write(6,'(a,4f8.1,f8.0,f8.1,f10.5)')
     _              'dod2: ',
     _              (gldod(idod),gbdod(idod),idod=1,2),
     _              sfreqp(ip),angpp(ip),corrp(ip)
            endif
         enddo
      endif


c     ==== 0.1.35: sort into probability distribs as a fn of angle ====
      if(do_iv)then
         nang_p= max(2,nangle)  ! input parameter early in circles_f77
         
         del_ang= (angmax-angmin)/real(nang_p-1)


c     --- note: angpp() is parametrised by ip=1,nsig, not iang_p=1,nang_p ---
         print*,'plotdodec: nang_p=',nang_p
         do iang_p=1,nang_p
            nper_ang(iang_p)= 0
            ang_p(iang_p)= angmin+ (real(iang_p-1))*del_ang
         enddo

         ang_even=.false.
         if(dodran)then

c     ---- add correlations to  c_ang(., iang_p) ----
            if(test2)print*,'plotdodec: before add correlations...'
            do ip=1,nsig
               if(2*(ip/2).eq.ip.and.(iscontrol(ip)))then
                  do jv=0,jmax,ivdel
                     do iv=iv1,iv2,ivdel
                        iang_p= int( (angpp(ip)-angmin)/del_ang 
     _                       +tol_ang )
     _                       +1 ! zeroth bin becomes iang_p=1
                        
                        if(iang_p.ge.1.and.iang_p.le.nang_p)then
                           nper_ang(iang_p)= nper_ang(iang_p)+1
                           if(nper_ang(iang_p).gt.mxnperang)then
                              print*,'plotdodec WARNING: ',
     _                             'nper_ang(iang_p),mxnperang = ',
     _                             nper_ang(iang_p),mxnperang 
                           else
                              c_ang(nper_ang(iang_p), iang_p) = 
     _                             co_iv(ip,iv,jv)
                           endif
                        endif

                     enddo
                  enddo
               endif
            enddo

c     ---- sort ----
            if(test2)print*,'plotdodec: before r4sort...'
            do iang_p=1,nang_p
               call r4sort(c_ang(1,iang_p),nper_ang(iang_p))
               if(test2)print*,'iang_p=',iang_p,' c_ang(.)=',
     _              (c_ang(iq,iang_p),iq=1,nper_ang(iang_p))
            enddo

c     --- check if  all the  nper_ang(*)  are equal to each other, since
c     it makes life easier, but better check to be doubly sure ---
            ang_even=.true.
            do iang_p=2,nang_p
               if(test2)print*,'plotdodec: iang_p,',
     _              'nper_ang(iang_p)=',
     _              iang_p, nper_ang(iang_p)
               if(nper_ang(iang_p).ne.nper_ang(1))then
                  ang_even=.false.
                  print*,'plotdodec WARNING: ',
     _                 'iang_p,nper_ang(iang_p),ang_even=',
     _                 iang_p,nper_ang(iang_p),ang_even
               endif
            enddo


c     --- smooth out c_ang( , ) values as a function of ang_p( ) ---
c            call psmooth(mxnperang,mxang,c_ang,ang_p)
            if(test2)then
               print*,'before psmoot: ',
     _              '((c_ang(iq,iang_p),iq=1,4),iang_p=1,2)='
               print*,((c_ang(iq,iang_p),iq=1,4),iang_p=1,2)
            endif
            if(iransm.eq.1)then   ! 0.1.42
               call psmoot(mxnperang,mxang,nang_p,nper_ang,
     _              c_ang,ang_p)

               do iang_p=1,nang_p  ! 0.1.42 -> resort after smoothing
                  call r4sort(c_ang(1,iang_p),nper_ang(iang_p))
               enddo
            endif

            if(test2)then
               print*,'after psmoot: ',
     _              '((c_ang(iq,iang_p),iq=1,4),iang_p=1,2)='
               print*,((c_ang(iq,iang_p),iq=1,4),iang_p=1,2)
            endif
            
c     --- transpose (for convenience, plotting etc; requires nper_ang constant) ---
            do iang_p=1,nang_p
               do iq=1,nper_ang(iang_p) ! use iq to distinguish from ip, iang_p
                  c_angt(iang_p,iq)= c_ang(iq,iang_p)
                  c_ang8(iq,iang_p)= dble(c_ang(iq,iang_p))
               enddo
            enddo

c     --- probability calculation ---
            print*,'plotdodec: n_ivp= ',n_ivp
            probtot= 1.0
            do iang_p=1,nang_p   
               probia(iang_p)=1.0 ! initialise probabilities at each angle
               do jv=0,jmax,ivdel
                  do iv=iv1,iv2,ivdel
                     pr_done(iang_p,iv,jv)=.false.
                  enddo
               enddo
            enddo

            do jv=0,jmax,ivdel
               do iv=iv1,iv2,ivdel
                  if(test2)print*,'jv,iv=',jv,iv

                  iqmax= 1
                  ivp_max= 1
                  ia_max= 1     ! max of iang_p
c     do iang_p=1,nang_p ! WRONG
                  do ivp=1,n_ivp
                     iang_p= 
     _                    int( (angpp(ivp*2)-angmin)/del_ang 
     _                    +tol_ang ) ! as above
     _                    +1    ! zeroth bin becomes iang_p=1       ! as above
                     
                     call hunt_g(c_ang8(1,iang_p),
     _                    nper_ang(iang_p),
     _                    co_iv8(ivp,iv,jv), iq)
                     if(test3)print*,'iang_p,co_ivp(),',
     _                    'angpp(),iq= ',
     _                    iang_p,co_iv8(ivp,iv,jv),angpp(ivp*2),iq
                     if(iq.gt.iqmax)then ! if this is a higher prob, then update
                        iqmax=iq ! type is iang_p
                        ivp_max=ivp
                        ia_max=iang_p
                     endif
                     
                     if(.not.(pr_done(iang_p,iv,jv)))then
                        probia(iang_p)= probia(iang_p)*
     _                       (1.0 - (real(iq)/
     _                       real(max(1,nper_ang(iang_p)))))
                        pr_done(iang_p,iv,jv)= .true.
                     endif
                  enddo
                  probiv(iv,jv)= 1.0 - (real(iqmax)/
     _                 real(max(1,nper_ang(ia_max))))

                  probtot= probtot* probiv(iv,jv)
                  if(test2)then
                     print*,'ivp_max,co_ivp(ivp_max,iv,jv),iqmax=',
     _                    ivp_max,co_ivp(ivp_max,iv,jv),iqmax
                     print*,'ivp_max,co_iv8(ivp_max,iv,jv),iqmax=',
     _                    ivp_max,co_iv8(ivp_max,iv,jv),iqmax
                     print*,'angpp(ivp_max*2)=',angpp(ivp_max*2)
                     print*,'ang_p(ia_max)=',ang_p(ia_max)
                     print*,'c_ang8(iqmax,ivp_max),...=',
     _                    c_ang8(iqmax,ia_max),
     _                    c_ang8(iqmax+1,ia_max)
                  endif
                  print*,'plotdodec: iv,jv,probiv(iv,jv),',
     _                 'diff angles probtot=',
     _                 iv,jv,probiv(iv,jv),probtot
               enddo            !            do iv=iv1,iv2,ivdel
            enddo               !         do jv=0,jmax,ivdel

            print*,'plotdodec: '
            prmin=1.0
            ia_pmin=1
            do iang_p=1,nang_p
               print*,'plotdodec: ',
     _              'iang_p,ang_p(.),probia(.)=',
     _              iang_p,ang_p(iang_p),probia(iang_p)
               if(probia(iang_p).lt.prmin)then
                  prmin=probia(iang_p)
                  ia_pmin=iang_p
               endif
            enddo
            print*,'probia minimum (same angle):',
     _           ' ang_p(ia_pmin),prmin=', 
     _           ang_p(ia_pmin),prmin

         endif                  ! if(dodran)then
      endif


c     ==== plots ====
      fact=1.1
      if(doacmax)then
         acmax= acmax0
      endif
      if(doasmax)then
         asmax= asmax0
      endif

      if(do4plots)then
         do iplot=1,4
            x1=cc(1)
            x2=cc(ncc)
            if(iplot.eq.1)then
c               y1=dlin1
c               y2=dlin2
               y1=-admax *fact
               y2=admax *fact
            elseif(iplot.eq.2)then
c               y1=slin1
c               y2=slin2
c               y1=-asmax
               y1=0.0
               y2=asmax *1.5
               if(doangf)then
                  y1= 2.0
                  y2= 7.0
               endif
            elseif(iplot.eq.3)then
c               y1=clin1*2.0
c               y2=clin2*2.0
               y1= -acmax *fact
               y2=acmax *fact
               if(doangf)then
                  y1= 0.2
                  y2= 1.0
               endif
            elseif(iplot.eq.4)then
c               y1=plin1*2.0
c               y2=plin2*2.0
               y1=-apmax *fact
               y2=apmax *fact
            endif

c            if(.not.(dolong.or.doRGpc))then
c               y1=-0.1
c               y2=0.3
c            endif

            if(test)print*,'iplot,y1,y2=',iplot,y1,y2


c     write(ch2(1:2),'(i2)')iflnm
c     ch2(2:2)='A'
c     if(ch2(1:1).eq.' ')ch2(1:1)='_'

c     --- v0.1.33 -- shifted this earlier and replaced system( )
            if(iplot.eq.1)then
               ch1='d'
            elseif(iplot.eq.2)then
               ch1='s'
            elseif(iplot.eq.3)then
               ch1='c'
            elseif(iplot.eq.4)then
               ch1='p'
            endif


            if(2+2.eq.5)then !.or.(.not.(dolong).and.iplot.eq.1))then

               call pgbegin(0,'circ_cc_'//ch1//'.ps/vps',1,1)              
c               call pgbegin(0,'/vps',1,1)              
               call pgvsize(1.0,6.0,1.0,5.0)
               call pgslw(3)
               call pgscf(2)
               call pgsch(1.2)
               
               call pgwindow(x1,x2,y1,y2)
               call pgbox('bcnts',0.,0,'bcnts',0.,0)
c     endif

               if(iplot.eq.1)then
                  call pglabel('\\gl\\d0',
     _                 '<d>',' ')
               elseif(iplot.eq.2)then
                  call pglabel('\\gl\\d0',
     _                 '<\\gs\\u2\\d>\\u1/2',' ')
c     _              '<\\gs\\u2\\d(r\\dij\\u)>',' ')
c     call pgline(ncc,cc,sav)
               elseif(iplot.eq.3)then
                  call pglabel('\\gl\\d0','<S>',' ')
c     call pgline(ncc,cc,cav)
               elseif(iplot.eq.4)then
                  call pglabel('\\gl\\d0',
     _                 '<S\\dDopp\\u>',' ')
c     call pgline(ncc,cc,pav)
               endif

               do ipar=1,nsig
                  if(ipar.eq.1)call pgslw(5)
                  
                  if(iplot.eq.1)then
                     call pgline(ncc,cc,dmean_ci(1,ipar))
                     if(ipar.eq.1)
     _                    call pgpoint(ncc,cc,dmean_ci(1,ipar),17)
                     if(ipar.eq.17)
     _                    call pgpoint(ncc,cc,dmean_ci(1,ipar),3)
                  elseif(iplot.eq.2)then
                     call pgline(ncc,cc,sigma_ci(1,ipar))
                     if(ipar.eq.1)
     _                    call pgpoint(ncc,cc,sigma_ci(1,ipar),17)
                     if(ipar.eq.17)
     _                    call pgpoint(ncc,cc,sigma_ci(1,ipar),3)
                  elseif(iplot.eq.3)then
                     call pgline(ncc,cc,corr_ci(1,ipar))
                     if(ipar.eq.1)
     _                    call pgpoint(ncc,cc,corr_ci(1,ipar),17)
                     if(ipar.eq.17)
     _                    call pgpoint(ncc,cc,corr_ci(1,ipar),3)
                  elseif(iplot.eq.4)then
                     call pgline(ncc,cc,cdopp_ci(1,ipar))
                     if(ipar.eq.1)
     _                    call pgpoint(ncc,cc,cdopp_ci(1,ipar),17)
                     if(ipar.eq.17)
     _                    call pgpoint(ncc,cc,cdopp_ci(1,ipar),3)
                  endif
                  if(ipar.eq.1)call pgslw(3)
               enddo

               call pgend


c               call system(
c     _           'mv pgplot.ps circ'//ch2//
c     _           '_'//ch1//'.ps')
c     _              'mv pgplot.ps circ_cc_'//ch1//'.ps')

c     print*,'Line following system call ... cc...'
            endif !



c     ---- function of longitude ----

            if(doxarbit)then
               do i=1,nsig
                  xarbit(i)=real(i)
               enddo
               x1=0.0
               x2=real(nsig)+1.0
               if(doxgl)then
                  do i=1,nsig
                     xarbit(i)= glparp(i)
                  enddo
                  print*,'glmin,glmax=',glmin,glmax
                  x1= 0.0
                  x2= 360.0
               elseif(doxgb)then
                  do i=1,nsig
                     xarbit(i)= gbparp(i)
                  enddo
                  print*,'gbmin,gbmax=',gbmin,gbmax
                  x1= gbmin - 1.0
                  x2= 91.0
               elseif(doxang)then
                  do i=1,nsig
                     xarbit(i)= angpp(i)! angpar(i)
c     --- for iv, use only ithcmax values not ithsmin
                     if(2*(i/2).eq.i)then
                        xar_iv(i/2)=angpp(i)
                     endif
                  enddo
                  x1= angmin-1.0
                  x2= angmax+1.0
                  if(doangf)then
                     x1= angf1 -1.0
                     x2= angf2 +1.0
                  endif
               elseif(doxth)then
                  do i=1,nsig
                     xarbit(i)= thparp(i)
                  enddo
                  x1= -3.0
                  x2= 75.0
               endif
               if(doxoffset)then
                  do i=1,nsig
c                     xarbit(i)=xarbit(i)+
c     _                    (randt()-0.5)* 0.03*(x2-x1)
                     xarbit(i)=xarbit(i)+
     _                    (rnd(17)-0.5)* 0.03*(x2-x1)  ! 17 is a dummy seed
                  enddo
               endif
            endif

            
            if(doxarbit.or.
     _           (.not.(doxarbit).and.iplot.eq.1))then
               call pgbegin(0,'circ_long_'//ch1//'.ps/vcps',1,1) ! v0.1.33
c               call pgbegin(0,'/vps',1,1)              
               call pgvsize(1.0,6.0,1.0,5.0)
               call pgslw(3)
               call pgscf(2)
               call pgsch(1.2)
               
               call pgwindow(x1,x2,y1,y2)
               call pgbox('bcnts',0.,0,'bcnts',0.,0)
            endif

            if(doxarbit)then
               chxaxis='x (arbitrary)'
               if(doxgl)then
                  chxaxis='g longitude'
               elseif(doxgb)then
                  chxaxis='g latitude'
               elseif(doxang)then
                  chxaxis='angular radius of circle (degrees)'
               elseif(doxth)then
                  chxaxis='rotation defining dodec (degrees)'
               endif
            endif

            if(iplot.eq.1)then
               call pglabel(chxaxis, ! 'long',
     _              'd',' ')
            elseif(iplot.eq.2)then
               call pglabel(chxaxis, ! 'long',
c     _              '<\\gs\\u2\\d>',' ')
c     _              '\\gs',' ')
     _              '\\gs (\\gmK)',' ')
            elseif(iplot.eq.3)then
               call pglabel(chxaxis, ! 'long',
     _              'S',' ')
            elseif(iplot.eq.4)then
               call pglabel(chxaxis, ! 'long',
     _              'S\\dDopp\\u',' ')
            endif

            do icc=1,ncc
               if(icc.eq.1)then
                  call pgsls(1)
               elseif(icc.eq.2)then
                  call pgsls(2) ! dashed
               elseif(icc.eq.3)then
                  call pgsls(4) ! dotted
               endif

               if(doxarbit)then
                  if(iplot.eq.1)then
                     if(doline)then
                        call pgline(np,xarbit,dmeanp)
                     else
                        call pgpoint(np,xarbit,dmeanp,isym)
                     endif
                  elseif(iplot.eq.2)then
                     if(doline)then
                        call pgline(np,xarbit,sigmap)
                     else
                        call pgpoint(np,xarbit,sigmap,isym)
                     endif
                  elseif(iplot.eq.3)then
                     if(plmode)then
                        call pgline(nangplot,angplot,cbmplot)
                     elseif(.not.(smoolog))then
                        if(.not.(do_iv))then
                           if(doline)then
                              call pgline(np,xarbit,corrp)
                           else
                              call pgpoint(np,xarbit,corrp,isym)
                           endif
c     --- v0.1.34 ---
                        else
                           do jv=0,jmax,ivdel
                              do iv=iv1,iv2,ivdel
                                 call pgqci(iqci) ! save colour index
                                 call pgqls(iqls) ! save line style index
                                 
                                 if(docolour)then
                                    call pgsci(iv+1) ! set colour index
                                    call pgsls(iv) ! set line style index
                                 endif

                                 call pgline(n_ivp,xar_iv,
     _                                co_ivp(1,iv,jv))
                                 if(docolour)then
                                    call pgsci(iqci) ! restore colour index
                                    call pgsls(iqls) ! restore line style index
                                 endif

c                                 print*,'plotdodec TEST ONLY:',
c     _                                'np=',np,'; ',
c     _                                (ii,xarbit(ii),co_ivp(ii,iv,jv)
c     _                                ,ii=1,np)
                              enddo
                           enddo
c     --- test only ---
                           if(test2)print*,'ang_even=',ang_even
                           if(ang_even)then
                              n90= max(1, nint(0.9*real(nper_ang(1))))
                              n80= max(1, nint(0.8*real(nper_ang(1))))
                              n1s= max(1, nint(0.8413*
     _                             real(nper_ang(1))))
                              n2s= max(1, nint(0.9772*
     _                             real(nper_ang(1))))
                              n3s= max(1, nint(0.9987*
     _                             real(nper_ang(1))))
                              n4s= max(1, nint(0.999968*
     _                             real(nper_ang(1))))
                              if(test2)then
                                 print*,'plotdodec: ',
     _                                'n1s,n2s,n3s=',n1s,n2s,n3s
                                 print*,'(c_angt(i,n1s),i=1,20)=',
     _                                (c_angt(i,n1s),i=1,20)
                                 print*,'(c_angt(i,n2s),i=1,20)=',
     _                                (c_angt(i,n2s),i=1,20)
                              endif
                              call pgqls(isls)  ! store line style
                              call pgsls(4)
                              call pgline(nang_p,ang_p,
     _                             c_angt(1,n1s))
                              call pgline(nang_p,ang_p,
     _                             c_angt(1,n2s))
                              call pgline(nang_p,ang_p,
     _                             c_angt(1,n3s))
c                              call pgline(nang_p,ang_p,
c     _                             c_angt(1,n4s))
                              call pgsls(isls)  ! restore line style
                           endif
                           
                        endif
                     else
                        if(doline)then
                           call pgline(nsig,xarbit,smooth(1,icc)) !! test only !!
                        else
                           call pgpoint(nsig,xarbit,smooth(1,icc),isym) !! test only !!
                        endif
                     endif
                  elseif(iplot.eq.4)then
                     if(doline)then
                        call pgline(np,xarbit,cdoppp)
                     else
                        call pgpoint(np,xarbit,cdoppp,isym)
                     endif
                  endif
               else
                  if(iplot.eq.1)then
                     call pgsls(1)
                  elseif(iplot.eq.2)then
                     call pgsls(2) ! dashed
                  elseif(iplot.eq.3)then
                     call pgsls(4) ! dotted
                  elseif(iplot.eq.4)then
                     call pgsls(3) ! dash-dotted ??
                  endif

c                  if(iplot.eq.1)then
c                     call pgline(nsig,xISW,dmean(1,icc))
c                  elseif(iplot.eq.2)then
c                     call pgline(nsig,xISW,sigma(1,icc))
c                  elseif(iplot.eq.3)then
c                     if(.not.(smoolog))then
c                        call pgline(nsig,xISW,corr(1,icc))
c                     else
c                        call pgline(nsig,xISW,smooth(1,icc)) !! test only !!
c                     endif
c                  elseif(iplot.eq.4)then
c                     call pgline(nsig,xISW,cdopp(1,icc))
c                  endif
               endif
               if(ipar.eq.1)call pgslw(3)
            enddo
            if(doxarbit.or.
     _           (.not.(doxarbit).and.iplot.eq.4))then
               call pgend
c               call system(
c     _              'mv pgplot.ps circ_long_'//ch1//'.ps')
            endif

         enddo                  !         do iplot=1,4
      endif !      if(do4plots)then

         


c     ---- skydist  (probably not functioning) ----

         if(doskydist)then
            iq=0
            do ip=1,np
               if(corrp(ip).gt.corrmin.or.sigmap(ip).lt.sigmax)then
                  iq=iq+1
                  corrq(iq)=corrp(ip)
c                  glparq(iq)= 360.0 - glparp(ip)
                  glparq(iq)= glparp(ip)  ! v0.1.33  nprotcart.f correction
            
                  gbparq(iq)= gbparp(ip)
               endif
            enddo
            nq=iq
            print*,'nq=',nq
            print*,' '

            if((nq*(nq+1)/2).gt.mxangpair)then
               print*,'Warning: nq = ',nq,' might be too big.'
            endif

            ierr1=0
               ii=0
               do iq1=1,nq-1
                  do iq2=iq1+1,nq
                     xx1=cos(gbparq(iq1)*pi_180)
     _                    *cos(glparq(iq1)*pi_180)
                     yy1=cos(gbparq(iq1)*pi_180)
     _                    *sin(glparq(iq1)*pi_180)
                     zz1=sin(gbparq(iq1)*pi_180)

                     xx2=cos(gbparq(iq2)*pi_180)
     _                    *cos(glparq(iq2)*pi_180)
                     yy2=cos(gbparq(iq2)*pi_180)
     _                    *sin(glparq(iq2)*pi_180)
                     zz2=sin(gbparq(iq2)*pi_180)

c                     ii=ii+1
                     aa= angle(xx1,yy1,zz1, xx2,yy2,zz2)
     _                    /pi_180 ! convert to degrees
                     if(aa.gt.bincut)then
                        ii=ii+1
                        if(ii.gt.mxangpair)then
                           ii=ii-1
                           ierr1=ierr1+1
                           if(ierr1.lt.5)then
                              print*,'WARNING: nq = ',nq,
     _                             ' is too big.'
                              print*,'will overwrite last value'
                           endif
                        endif
                        ang12(ii)= aa
                     endif
                  enddo
               enddo !               do iq1=1,nq-1
               nq2= ii
               y1=0.0
               y2=5.0 * (real(nq2)/real(nbin))
               
               call pgbegin(0,'angdist.ps/ps',1,1)
               call pgvsize(1.0,6.0,1.0,5.0)
               call pgslw(3)
               call pgscf(2)
               call pgsch(1.2)
               
               call pgwindow(bin1,bin2,y1,y2)
               call pgbox('bcnts',0.,0,'bcnts',0.,0)
               
               call pghist(nq2, ang12, bin1,bin2, nbin, 1)
               call pglabel('angle between correlated circles',
     _              'frequency',' ')

               call pgmove(dodec1,y1)
               call pgdraw(dodec1,y2)
               call pgmove(dodec2,y1)
               call pgdraw(dodec2,y2)

               call pgend
c            endif !            if((nq*(nq+1)/2.gt.mxangpair)then
               

            
            x1=0.0
            x2=360.0
            y1=0.0
            y2=90.0
            call pgbegin(0,'skyplot.ps/ps',1,1)              
            call pgvsize(1.0,8.0,1.0,5.0)
            call pgslw(3)
            call pgscf(2)
            call pgsch(1.2)
               
            call pgwindow(x1,x2,y1,y2)
            call pgbox('bcnts',0.,0,'bcnts',0.,0)
            
            call pgqch(qch)
            call pgsch(0.5)
            call pgpoint(nq,glparq,gbparq,17)
            call pgsch(qch)

            call pglabel('gal longitude','gal latitude',' ')
            call pgend
         endif

      end
