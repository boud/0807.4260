C     Copyright (C) 2005 B. Roukema et al. <shape-univ at astro.uni.torun.pl>
C     
C     This program is free software; you can redistribute it and/or modify
C     it under the terms of the GNU General Public License as published by
C     the Free Software Foundation; either version 2 of the License, or
C     (at your option) any later version.
C     
C     This program is distributed in the hope that it will be useful,
C     but WITHOUT ANY WARRANTY; without even the implied warranty of
C     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C     GNU General Public License for more details.
C     
C     You should have received a copy of the GNU General Public License
C     along with this program; if not, write to the Free Software
C     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
C  
C     See also http://www.gnu.org/licenses/gpl.html


C     == plot matched circles and optionally test extended SZ effect ==

      subroutine pcircl(indir,nindir,j_sz)
c     --- plots T1 T2 and sig(T1), sig(T2) for requested circles ---
c
c     Input data (from file):

c      parameter(iimax=50,jjmax=50, maxtheta=100)
c      parameter(iimax=50,jjmax=50, maxtheta=720)

C     INPUTS:
      character*(*)  indir     ! directory with input files
      integer        nindir    ! number of characters in  indir
      integer        j_sz      ! SZ test: 1 = do / 0 = do not do 


C     INTERNAL:
      character*256  indir2 ! safe string version of indir

      include 'ncopies.dec'

      integer*4  imax,jmax

      parameter(pi=3.141592659,pi_180=pi/180.0)

      integer*4    nthc(-iimax:iimax,0:jjmax)
      real*4    glatc(2,-iimax:iimax,0:jjmax), ! 2 centres
     _     glongc(2,-iimax:iimax,0:jjmax), ! 2 centres
     _     wmodc(-iimax:iimax,0:jjmax), ! dtheta * rSLS
     _     rSLSc,
     _     dthMpc(-iimax:iimax,0:jjmax) ! dtheta * rSLS

c     ! warning only evaluated if  (iv.gt.0.or.jv.gt.0)

      real*4    tt1c(maxtheta, -iimax:iimax,0:jjmax)
      real*4    tt2c(maxtheta, -iimax:iimax,0:jjmax)
      real*4    s1c(maxtheta, -iimax:iimax,0:jjmax)
      real*4    s2c(maxtheta, -iimax:iimax,0:jjmax)

c     ---- 24.02.2004 ----
c     -- store galactic long, lat at each pixel for double-checking --
      real*4    glpix1(maxtheta, -iimax:iimax,0:jjmax)
      real*4    gbpix1(maxtheta, -iimax:iimax,0:jjmax)
      real*4    glpix2(maxtheta, -iimax:iimax,0:jjmax)
      real*4    gbpix2(maxtheta, -iimax:iimax,0:jjmax)


c     --- SZ - Sunyaev-Zel'dovich Effect effect on circle matching
      parameter(dt_sz= 0.005)  ! SZ decrement in milli-Kelvin
      parameter(ang_sz= 0.3)   ! angle in degrees of SZ effect
      parameter(m_rich= 2)  ! minimum richness of clusters


      include 'npnt.dec'
c     -- Revised northern "Abell Catalog"
      real*4     glon(nr__),glat(nr__) ! gal long, lat
      integer*4  rich(nr__)

c     -- Southern "Abell Catalog"
      real*4     glon_1(nr__1),glat_1(nr__1) 
      integer*4  rich_1(nr__1)

c     -- Supplementary southern clusters
      real*4     glon_2(nr__2),glat_2(nr__2) 
      integer*4  rich_2(nr__2)

      parameter(mxACO=10000)
      real*4     xACO(mxACO),yACO(mxACO),zACO(mxACO)
      logical    oldACO(mxACO)

c     Local: 

      character*1 ch1
      character*2 ch2
      character*3 ch3
      character*60  label,label2
      real*4    rr(maxtheta)
      real*4    tplus(maxtheta,2),tminus(maxtheta,2)

      parameter(mxseg=10)
c      integer*4 i1(3),i2(3),nseg
      integer*4 i1(mxseg),i2(mxseg),nseg ! 21.04.2003

      parameter(maxlis= maxtheta*(2*iimax+1)*(jjmax+1)*2)
      real*4    tdifflis(maxlis) ! list of temp differences

      parameter(nbins=100)
      real*4    tdbin(nbins),tdN(nbins),gauss(nbins)

      logical*4 dobig, yesno, doprintval
      logical*4 dolong !08.04.2003
      logical   flag
      logical*4 doerr !21.04.2003

      logical*4 x2fixed !25.04.2003

c     ---- 25.12.2003 ----
      logical*4 doonepair ! test one pair only 

c     ---- 25.12.2003 ----
      logical*4 doppler ! test if could be doppler effect (near anticorrn)


c     ---- 21.02.2004 for dodec version ----
      logical*4 dododec

c     --- 25.02.2004 for pnt_src catalogue ---
      logical*4 dopntsrc

c     --- 08.12.2005 - for propaganda film for European Commission ---
      logical*4 docolour
      logical*4 dopopular ! for popularisation/media


c      include 'npnt.dec'
      real*4  glpnt(mxpnt),gbpnt(mxpnt)
      real*4  xpnt(mxpnt),ypnt(mxpnt),zpnt(mxpnt)

      logical*4  verbose

      logical*4  test

      test= .true. ! .false.!
      verbose=  .false.! .true. !

c      data ch3/'_RE'/
      data ch3/'__3'/

      dobig= .true.!.false. !
      dolong= .true.             !.false. !
      doprintval=.true. !.false.             !

      doerr=.false.
      x2fixed=.true. !.false.!

      doonepair=.true. ! .false.!

      doppler=.false.!.true. ! 

      dododec=.true.

      dopntsrc=.false. !.true.
      angpnt= 0.5 *pi_180 !


      docolour=.true. !.false.!
      dopopular=.false.!.true. !


c     ---- HARDWIRED ----
c      curvfact= pdWein(-0.55,100.0,0.7,1100.0)
c     _     /pdWein(0.5,100.0,0.0,1100.0)

      omm= 2.0*(-0.55+0.7)
      w_q= -1.0
c      curvfact= DEpdWeiq(omm,100.0,0.7,w_q, 1100.0)
c     _     /DEpdWeiq(omm,100.0,0.0,w_q, 1100.0)
      curvfact= real(dofz(100.0d0,dble(omm),0.7d0,dble(w_q), 
     _     1100.0d0))
     _     /real(dofz(100.0d0,dble(omm),0.0d0,dble(w_q), 
     _     1100.0d0))
      curvfact = 1.0

c     ---- read circle data from file ----

      open(1,file='circleinfo'//ch3//'.dat',
     _     access='sequential',
     _     form='unformatted',status='old')
c      read(1)ncc,cc1,cc2,H_0,drSLS,nsig,drsigma !24.02.2004
      read(1)ncc,cc1,cc2,H_0,drSLS,nsig !,drsigma
      read(1)iimax0,jjmax0,maxtheta0, imax,jmax
      read(1)nthc,glatc,glongc,wmodc,rSLSc,dthMpc
      read(1)tt1c,tt2c,s1c,s2c
      read(1)glpix1,gbpix1,glpix2,gbpix2
      close(1)

      if(iimax.ne.iimax0.or.jjmax.ne.jjmax0.or.
     _     maxtheta.ne.maxtheta0)print*,'Dimensions need ',
     _     'to be fixed in plotcircles.'

      print*,'imax,jmax=',imax,jmax
      print*,'ncc,cc1,cc2,nsig,drsigma=',ncc,cc1,cc2,nsig,drsigma

      print*,'nthc(1,2),glatc(1,1,2),glongc(1,1,2)='
      print*,nthc(1,2),glatc(1,1,2),glongc(1,1,2)
      print*,'nthc(1,0),glatc(1,1,2),glongc(1,1,2)='
      print*,nthc(1,0),glatc(1,1,2),glongc(1,1,2)
      print*,'nthc(2,0),glatc(1,1,2),glongc(1,1,2)='
      print*,nthc(2,0),glatc(1,1,2),glongc(1,1,2)

      ivdel=1
      print*,'test: '
      do iv=-imax,imax,ivdel
         do jv=0,jmax,ivdel
            print*,'iv,jv=',iv,jv,
     _           ' nthc(iv,jv),glatc(1,iv,jv),glongc(1,iv,jv)='
            print*,nthc(iv,jv),glatc(1,iv,jv),glongc(1,iv,jv)
         enddo
      enddo

      if(dopntsrc)then

         indir2=indir(1:min(nindir,256))

         call rd_pnt(glpnt,gbpnt,indir2,nindir)
         print*,'have read WMAP point source catalogue'
         do ii=1,mxpnt
            xpnt(ii)=cos(gbpnt(ii)*pi_180)*cos(glpnt(ii)*pi_180)
            ypnt(ii)=cos(gbpnt(ii)*pi_180)*sin(glpnt(ii)*pi_180)
            zpnt(ii)=sin(gbpnt(ii)*pi_180)
         enddo
      endif

      if(j_sz.eq.1)then
         call readACO(
     _        glon,glat,rich,
     _        glon_1,glat_1,rich_1,
     _        glon_2,glat_2,rich_2)
         iACO=0
         do i=1,nr__
            if(rich(i).ge.m_rich)then
               iACO=iACO+1
               xACO(iACO)=cos(glat(i)*pi_180)*cos(glon(i)*pi_180)
               yACO(iACO)=cos(glat(i)*pi_180)*sin(glon(i)*pi_180)
               zACO(iACO)=sin(glat(i)*pi_180)
            endif
         enddo
         do i=1,nr__1
            if(rich_1(i).ge.m_rich)then
               iACO=iACO+1
               xACO(iACO)=cos(glat_1(i)*pi_180)*cos(glon_1(i)*pi_180)
               yACO(iACO)=cos(glat_1(i)*pi_180)*sin(glon_1(i)*pi_180)
               zACO(iACO)=sin(glat_1(i)*pi_180)
            endif
         enddo
         do i=1,nr__2
            if(rich_2(i).ge.m_rich)then
               iACO=iACO+1
               xACO(iACO)=cos(glat_2(i)*pi_180)*cos(glon_2(i)*pi_180)
               yACO(iACO)=cos(glat_2(i)*pi_180)*sin(glon_2(i)*pi_180)
               zACO(iACO)=sin(glat_2(i)*pi_180)
            endif
         enddo
         nACO=iACO
         print*,'nACO=',nACO


         sz_rad= ang_sz *pi_180 ! convert angular size of SZE to radians
         fsz= 0.0
         isz=0
         do jj=0,jjmax0
            do ii=-iimax0,iimax0
c               do ith=1,maxtheta0

c     --- for a new circle pair, refresh the list of available clusters ---
               do iACO=1,nACO
                  oldACO(iACO)=.false.
               enddo

               ith=1
               do while(ith.le.maxtheta0)

c     --- find closest cluster near a circle point on circle 1 ---
                  gl1= glpix1(ith,ii,jj)
                  gb1= gbpix1(ith,ii,jj)
                  xx= cos(gb1*pi_180)*cos(gl1*pi_180)
                  yy= cos(gb1*pi_180)*sin(gl1*pi_180)
                  zz= sin(gb1*pi_180)
                  ibest=-99
                  abest=sz_rad

                  do iACO=1,nACO
                     if(.not.oldACO(iACO))then
                        aa= angle(xx,yy,zz,
     _                       xACO(iACO),yACO(iACO),zACO(iACO))
                        if(aa.lt.abest)then
                           ibest=iACO
                           abest=aa
                        endif
                     endif
                  enddo

c     --- find closest cluster near a circle point on circle 2---
                  gl2= glpix2(ith,ii,jj)
                  gb2= gbpix2(ith,ii,jj)
                  xx= cos(gb2*pi_180)*cos(gl2*pi_180)
                  yy= cos(gb2*pi_180)*sin(gl2*pi_180)
                  zz= sin(gb2*pi_180)
                  ibest2=-99
                  abest2=sz_rad

                  do iACO=1,nACO
                     if(.not.oldACO(iACO))then
                        aa2= angle(xx,yy,zz,
     _                       xACO(iACO),yACO(iACO),zACO(iACO))
                        if(aa2.lt.abest2)then
                           ibest2=iACO
                           abest2=aa2
                        endif
                     endif
                  enddo

c     --- if a cluster is found, step forward to find the closest
c     point along the circle to the cluster number  (ibest), since
c     the effect should be strongest there ---
                  if(ibest.gt.0.and.ibest2.lt.0)then ! circle 1
                     ith_=ith+1
                     aa_= 0.99*abest
                     ithbest=ith
                     do while(ith_.le.maxtheta0.and.aa_.lt.abest)
                        gl1_= glpix1(ith_,ii,jj)
                        gb1_= gbpix1(ith_,ii,jj)
                        xx_= cos(gb1_*pi_180)*cos(gl1_*pi_180)
                        yy_= cos(gb1_*pi_180)*sin(gl1_*pi_180)
                        zz_= sin(gb1_*pi_180)
                        aa_= angle(xx_,yy_,zz_,
     _                       xACO(ibest),yACO(ibest),zACO(ibest))
                        if(aa_.lt.abest)then
                           ithbest=ith_
                           abest=aa_
                        endif
                        ith_=ith_+1
                     enddo
                     ith_=ith_-1
c     --- a cluster has been found at ibest near pixel ith_ ---
                  else
                     ith_=ith
                  endif !                   if(ibest.gt.0)then
                     

                  if(ibest2.gt.0.and.ibest.lt.0)then  ! circle 2
                     ith_=ith+1
                     aa_= 0.99*abest2
                     ithbest=ith
                     do while(ith_.le.maxtheta0.and.aa_.lt.abest2)
                        gl2_= glpix2(ith_,ii,jj)
                        gb2_= gbpix2(ith_,ii,jj)
                        xx_= cos(gb2_*pi_180)*cos(gl2_*pi_180)
                        yy_= cos(gb2_*pi_180)*sin(gl2_*pi_180)
                        zz_= sin(gb2_*pi_180)
                        aa_= angle(xx_,yy_,zz_,
     _                       xACO(ibest2),yACO(ibest2),zACO(ibest2))
                        if(aa_.lt.abest2)then
                           ithbest=ith_
                           abest2=aa_
                        endif
                        ith_=ith_+1
                     enddo
                     ith_=ith_-1
c     --- a cluster has been found with ibest=iACO near pixel ithbest ---
                  else
                     ith_=ith
                  endif !                   if(ibest.gt.0)then
                     
                  if(ibest*ibest2.lt.0)then
                     print*,'found a non-matching cluster near a',
     _                    ' circle'
                     print*,'ibest,ibest2=',ibest,ibest2,
     _                    ' abest,abest2 /pi_180=',
     _                    abest/pi_180,abest2/pi_180

                     dt_old= tt2c(ithbest,ii,jj) -
     _                    tt1c(ithbest,ii,jj) ! default T diff
                     if(ibest.gt.0)then
                        dt_new= dt_old + dt_sz
                     else
                        dt_new= dt_old - dt_sz
                     endif
                     if(abs(dt_new).gt.abs(dt_old))then
                        fsz=fsz+1.0
                        write(6,'(a,f8.4$)')'fsz(i)= +1, dt_old=',
     _                       dt_old
                     else
                        fsz=fsz-1.0
                        write(6,'(a,f8.4$)')'fsz(i)= -1, dt_old=',
     _                       dt_old
                     endif
                     if(ibest.gt.0)then
                        print*,'abest=',abest
                     else
                        print*,'abest2=',abest2
                     endif

                     isz=isz+1

                     if(ibest.gt.0)then
                        oldACO(ibest)=.true.
                     else
                        oldACO(ibest2)=.true.
                     endif
                  endif         ! if(ibest*ibest2.lt.0)then
               
                  ith=ith_+1
               enddo            !                do while(ith.le.maxtheta0)
            enddo !            do ii=-iimax0,iimax0
         enddo!          do jj=0,jjmax0

         nsz=isz
         print*,'SZ: nsz=',nsz
         if(nsz.gt.0)then
            fsz=fsz/real(nsz)
            sigfsz= 0.5/sqrt(real(nsz))
            print*,'SZ: fsz,sigfsz=',fsz,sigfsz
            print*,' ' 
         endif
         

      endif  !      if(j_sz.eq.1)then




      if(doppler)then
         do ith=1,maxtheta0
            do ii=-iimax0,iimax0
               do jj=0,jjmax0
                  tt2c(ith,ii,jj)= -tt2c(ith,ii,jj)
               enddo
            enddo
         enddo
      endif
      
c     ---- temp. diff distribution plot ----
      itd=0
      ivdel=1! 3 !2 !1! 

      if(doonepair)then
         ivdel=1
         jmax=0
         iv1=-1
         iv2=1
      endif

c     --- test ---
      if(test)then
         iv=1
         jv=0
         do ith=1,min(20,nthc(iv,jv))
            print*,'iv,tt1c,s[12]c=',
     _           iv,tt1c(ith,iv,jv),s1c(ith,iv,jv),s2c(ith,iv,jv)
         enddo
         print*,' == '
      endif

c     -- convert to microK --
      convmiK = 1e3 ! 1.0 

      do iv=-imax,imax,ivdel
         do jv=0,jmax,ivdel
            do ith=1,nthc(iv,jv)
               t1=tt1c(ith,iv,jv) *convmiK
               t2=tt2c(ith,iv,jv) *convmiK
               if(t1.gt.-9.0.and.t2.gt.-9.0)then
                  s1= s1c(ith,iv,jv)
                  s2= s2c(ith,iv,jv)
c                  if(s1*s1+s2*s2.gt.1e-20)then
                  if(s1*s1+s2*s2.gt.1e-20 ! added 24/08/99
     _                 .and.(min(s1,s2).gt.1e-10))then
                     itd=itd+1
                     tdifflis(itd)= (t1-t2) 
     _                    ! / sqrt(s1*s1+s2*s2)  ! commented out circles-0.1.15
                  endif
c                              yy= max(s1*s1+s2*s2,1e-20) !
c                              ss= xx /sqrt(yy)

               endif
            enddo
         enddo
      enddo
      ntd=itd
      print*,'ntd=',ntd
      tdmax=0.1 *convmiK
      tdmin= -tdmax
      ioob=1
      call mkbins(ntd,tdmin,tdmax,tdifflis, 
     _     nbins,tdbin,bmin,bmax,tdN,ioob)


c      call pgbegin(0,'?',1,1)              
      call pgbegin(0,'pgplot1.ps/ps',1,1)              
      call pgvsize(1.0,6.0,1.0,5.0)
      call pgscf(2)
      call pgsch(1.2)
      
      if(abs(bmax).lt.1e-4)bmax = 100 ! added in circles-0.1.13


      call pgwindow(tdmin,tdmax,-0.05*bmax,1.05*bmax)
      call pgbox('bcnts',0.,0,'bcnts',0.,0)


      dtd=tdbin(2)-tdbin(1)
      print*,'dtd=',dtd
      do ib=1,nbins
         call myhist(tdbin(ib)-0.5*dtd,tdbin(ib)+0.5*dtd, 
     _        0.0,tdN(ib),0.0,1e4)
c         gauss(ib)= 1.0/sqrt(2.0*3.14159) *
      enddo
      call pgend
c      print*,'Type enter to continue: '  ! 0.1.13 commented out
c      read(5,'(a)')ch1
c      stop

c     ---- circles plot ----

      y1=-0.3 !-0.1
      y2= 0.3 !0.1
      islw1=2
      islw2=4

      if(docolour)then
         islw2=4 ! 8 
         iscol1=11
         iscol2=13
      endif
      if(dopopular)then
         islw2 = 8 ! is for popular talks only
         iscol1 = 2
         iscol2 = 4
         isls2 = 1
      endif




      if(.not.(dobig))then
         call pgbegin(0,'?',3,6)              
c         call pgbegin(0,'?',2,6)              
         flag=.true.
         call pgask(flag)
c         call pgvsize(1.0,6.0,0.5,9.5)

         call pgvsize(0.5,2.5,0.5,1.5)

c         call pgvsize(0.5,3.5,0.5,1.5)

         call pgslw(islw1)
         call pgscf(2)
         call pgsch(2.1)
      endif

c      jv=2
      inotplane=0
      ivdel=1



      do iv=-imax,imax,ivdel
         do jv=0,jmax,ivdel
c            if(iv.gt.0.or.jv.gt.0)then
            call inset(iv,jv,yesno)
            yesno=.true. ! test only !
            if(yesno)then

c     ---- check inputs ----
c               print*,'nthc(iv,jv),glatc(1,iv,jv),glongc(1,iv,jv)='
c               print*,nthc(iv,jv),glatc(1,iv,jv),glongc(1,iv,jv)
c               print*,'nthc(iv,jv),glatc(2,iv,jv),glongc(2,iv,jv)='
c               print*,nthc(iv,jv),glatc(2,iv,jv),glongc(2,iv,jv)
c               print*,'wmodc(iv,jv),dthMpc(iv,jv)='
c               print*,wmodc(iv,jv),dthMpc(iv,jv)

               sepn= sqrt(rSLSc*rSLSc - wmodc(iv,jv)*wmodc(iv,jv))
c               write(label(1:12),'(a2,i3,a4,i3)')'i=',iv,', j=',jv
               label='                 '
               write(label(1:12),'(a2,i3,a4,i3)')'i=',iv 
               label(13:17)=' \\gD='

               write(label(18:24),'(f7.0)')sepn *2.0 ! 21/06/99
     _              *curvfact

               label(25:39)=' h\\u-1\\dMpc    '

c     --- 0.1.67 ---
               write(label2(1:26),'(2f6.1,a2,2f6.1)')
     _              glongc(1,iv,jv),glatc(1,iv,jv),'; ',
     _              glongc(2,iv,jv),glatc(2,iv,jv)

c               print*,'Starting plot'

               nseg=0
               ith=0
               iptmax=0
               iptot=0
               ipm0=1
c               do while(ith.lt.nthc(iv,jv).and.nseg.lt.3)
               do while(ith.lt.nthc(iv,jv).and.nseg.lt.mxseg)
                  ith=ith+1
c     _                 (tt1c(ith,iv,jv).lt.-9.0).and.nseg.lt.3)
                  do while(ith.lt.nthc(iv,jv).and.
     _                 ( (tt1c(ith,iv,jv).lt.-9.0 .or.
     _                 tt2c(ith,iv,jv).lt.-9.0) ! 0.2.20
     _                 .and.nseg.lt.mxseg )  
     _                 )
                     ith=ith+1
                  enddo
                  if(tt1c(ith,iv,jv).ge.-9.0 .and.
     _                 tt2c(ith,iv,jv).ge.-9.0) then  ! 0.2.20
                     nseg=nseg+1
                     i1(nseg)=ith
                     ith=ith+1
                     do while(ith.lt.nthc(iv,jv).and.
     _                    (tt1c(ith,iv,jv).ge.-9.0) .and.
     _                    (tt2c(ith,iv,jv).ge.-9.0))  ! 0.2.20
                        ith=ith+1
                     enddo
c                     if(ith.eq.nthc(iv,jv))then
                     if(ith.eq.nthc(iv,jv).and.
     _                    tt1c(ith,iv,jv).ge.-9.0 .and.
     _                    tt2c(ith,iv,jv).ge.-9.0)then  ! 0.2.20
                        i2(nseg)=nthc(iv,jv)
                     else
                        i2(nseg)=ith-1
                     endif
                     iptmax=max(iptmax,i2(nseg)-i1(nseg)+1)
                     if(i2(nseg)-i1(nseg)+1.gt.ipm0)
     _                    iptot=iptot+ i2(nseg)-i1(nseg)+1
                  endif
               enddo

c     print*,'nseg,i1,i2=',nseg,i1,i2
c     print*,'(tt2c(i,iv,jv),i=1,nthc(iv,jv))='
c     print*,(tt2c(i,iv,jv),i=1,nthc(iv,jv))
c               if(iv.eq.1.and.jv.ge.5.and.jv.le.6)then
c                  print*,'nseg,i1,i2=',nseg,i1,i2
c                  print*,'(tt2c(i,iv,jv),i=1,nthc(iv,jv))='
c                  print*,(tt2c(i,iv,jv),i=1,nthc(iv,jv))
c               endif
               if(verbose.and.iv.ge.1)then
                  print*,'circ::head: i, glatc(1), glongc(1), ',
     _                 'glatc(2), glongc(2)'
                  write(6,'(a,i3,4f6.1)')'circ:: ',iv, !jv,
     _                 glatc(1,iv,jv),glongc(1,iv,jv),
     _                 glatc(2,iv,jv),glongc(2,iv,jv)

                  print*,'circ:::head: i, ith, ',
     _                 '(glatc(j), glongc(j), t(j), j=1,2)'
                  tNULLup= -9.99e9  * 0.99 ! careful about numerical errors
                  do ith=1,nthc(iv,jv)
                     if(tt1c(ith,iv,jv).gt.tNULLup .and.
     _                    tt2c(ith,iv,jv).gt.tNULLup)then
                        write(6,
     _                       '(a,i3,i5,f8.1,f6.1,f10.4,'//
     _                       'f8.1,f6.1,f10.4)')
     _                       'circ::: ',iv, ith,
     _                       glpix1(ith,iv,jv),gbpix1(ith,iv,jv),
     _                       tt1c(ith,iv,jv),
     _                       glpix2(ith,iv,jv),gbpix2(ith,iv,jv),
     _                       tt2c(ith,iv,jv)
                     endif
                  enddo
               endif



c               if(nseg.gt.0.and.iptmax.gt.1)then
               if(nseg.gt.0.and.iptmax.gt.ipm0.and.iptot.gt.1)then
                  inotplane=inotplane+1
                  print*,'circle centre: '
                  print*,'jv,iv,kglatc(1,iv,jv),glongc(1,iv,jv)',jv
c                  print*,iv,jv,glatc(1,iv,jv),glongc(1,iv,jv)
                  write(6,'(i4,a3,f7.1,a3,f7.1,a5)')
     _                 iv,' & ',nint(glatc(1,iv,jv)*10.0)/10.0,' & ',
     _                 nint(glongc(1,iv,jv)*10.0)/10.0,'    &'
                  print*,'jv,iv,glatc(2,iv,jv),glongc(2,iv,jv)',jv
                  print*,iv,jv,glatc(2,iv,jv),glongc(2,iv,jv)
                  write(6,'(i4,a3,f7.1,a3,f7.1)')
     _                 iv,' & ',nint(glatc(2,iv,jv)*10.0)/10.0,' & ',
     _                 nint(glongc(2,iv,jv)*10.0)/10.0
                  print*,'opening angle= acos(wmodc(iv,jv)/rSLSc)'
                  print*,acos(wmodc(iv,jv)/rSLSc)
                  print*,' '

                  do ith=1,nthc(iv,jv)
                     rr(ith)=real(ith)*dthMpc(iv,jv)
                     tplus(ith,1)=tt1c(ith,iv,jv) + s1c(ith,iv,jv)
                     tminus(ith,1)=tt1c(ith,iv,jv) - s1c(ith,iv,jv)
                     tplus(ith,2)=tt2c(ith,iv,jv) + s2c(ith,iv,jv)
                     tminus(ith,2)=tt2c(ith,iv,jv) - s2c(ith,iv,jv)
                  enddo
                  x1=0.0
                  if(.not.(x2fixed))then
                     x2=rr(nthc(iv,jv))
                  else
                     if(nthc(0,1).gt.0)then
                        rr01=rr(nthc(0,1))
                     else
                        rr01=-99
                     endif
                     if(nthc(1,0).gt.0)then
                        rr10=rr(nthc(1,0))
                     else
                        rr10=-99
                     endif
                     x2=max(rr01,rr10)
c                     x2=max(rr(nthc(0,1)),rr(nthc(1,0)))
                        
                  endif
                  
                  ch2='  '
                  write(ch2,'(i2)')iv
                  if(ch2(1:1).eq.' ')ch2(1:1)='_'
                  if(dobig)then
c                     call pgbegin(0,'?',1,1)              
                     if(docolour)then
                        call pgbegin(0,'circle_'//ch2//'.ps/cps',1,1)
                     else
                        call pgbegin(0,'circle_'//ch2//'.ps/ps',1,1)
                     endif

                     if(.not.(dolong))then
                        call pgvsize(1.0,6.0,1.0,5.0)
                     else
c                        call pgvsize(0.5,9.5,1.0,5.0)
                        call pgvsize(1.0,9.5,1.0,5.0)
                     endif
                     call pgslw(islw1)
                     call pgscf(2)
                     call pgsch(1.2)
                     
c     --- scale in hinvGpc ----
                     call pgwindow(x1*0.001,x2*0.001,y1,y2)
                     call pgbox('bcnts1',0.,0,'bcnts',0.,0)
c     --- data in hinvMpc ----
                     call pgwindow(x1,x2,y1,y2)
                     call pgbox('',0.,0,'',0.,0)




                  else
c                     if(inotplane.gt.1)call pgpage
                     call pgpage
                     call pgwindow(x1,x2,y1,y2)
                     call pgbox('bcnts1',0.,0,'bcnts',0.,0)
                  endif

c                  call pglabel('r\\dij\\u\\gh (h\\u-1\\dMpc)',
c                  call pglabel('\\gh (h\\u-1\\dMpc)',

                  if(.not.(docolour))then
                     call pglabel(
     _                    'distance along circle (h\\u-1\\dMpc)',
     _                    '\\gdT (mK)',' ')
                  else
                     call pgscf(1)
                     call pgslw(4)
                     if(.not.(dopopular))then
                        call pglabel(
c     _                    'distance along circle (h\\u-1\\dmegaparsec)',
     _                       'distance along circle (h\\u-1\\dGpc)',
     _                       'temperature fluctuation \\gdT'//
     _                       ' (milliKelvin)',' ')
c     _                    'matching temperature fluctuations in'//
c     _                    ' cosmic microwave background (WMAP)')
                     elseif(dopopular)then
                        call pgscf(2)
                        call pglabel(
     _                       'distance along circle (h\\u-1\\dGpc)',
     _                       'temperature fluctuation \\gdT'//
     _                       ' (milliKelvin)',
     _                       'implied matched circles')

                     endif
                  endif

                  call pgqch(qch)
                  if(.not.(dobig))call pgsch(2.6)
                  call pgptxt(x1+(x2-x1)*0.05,
c     _                 y1+(y2-y1)*0.05,0.0,0.0, label(1:39))
     _                 y1+(y2-y1)*0.05,0.0,0.0, 
     _                 label(1:12) 
     _                 // label2(1:26) ) !  --- 0.1.67 ---
                  call pgsch(qch)


                  do iseg=1,nseg
                     call pgslw(islw2)
                     call pgsls(1)
                     if(docolour)then
                        call pgqci(iscol0)  ! store (Query) old value
                        call pgsci(iscol1)
                     endif
c     ---   line style isls=3  for southern centres ---
                     if(glatc(1,iv,jv).le.0.0)then
c                        call pgsls(2)
                        call pgsls(isls2)
                     else
                        call pgsls(1)
                     endif

c     call pgline(nthc(iv,jv),rr,tt1c(1,iv,jv))
                     call pgline(i2(iseg)-i1(iseg)+1,rr(i1(iseg)),
     _                    tt1c(i1(iseg),iv,jv))

                     call pgsls(3)
                     if(docolour)then
                        call pgsci(iscol2)
                        call pgsls(1)
                     endif

c     ---   line style isls=3  for southern centres ---
                     if(glatc(2,iv,jv).le.0.0)then
c                        call pgsls(2)
                        call pgsls(isls2)
                     else
                        call pgsls(1)
                     endif

                     call pgline(i2(iseg)-i1(iseg)+1,rr(i1(iseg)),
     _                    tt2c(i1(iseg),iv,jv))
                     if(docolour)then
                        call pgsci(iscol0)  ! reSet to old value
                     endif

                     if(doprintval.and.iv.eq.3.and.jv.eq.6)then
                        print*,'A: ',i2(iseg)-i1(iseg)+1
                        do ijk=i1(iseg),i2(iseg)
                           print*,rr(ijk),
     _                          tt1c(ijk,iv,jv),0.0, ! 0.0=dummy
     _                          tt2c(ijk,iv,jv)
                        enddo
                     endif

                     if(doerr)then!  21.04.2003
                        call pgslw(islw1)
                        call pgsls(1)
                        call pgline(i2(iseg)-i1(iseg)+1,rr(i1(iseg)),
     _                       tplus(i1(iseg),1))
                        call pgline(i2(iseg)-i1(iseg)+1,rr(i1(iseg)),
     _                       tminus(i1(iseg),1))
                        call pgsls(3)
                        call pgline(i2(iseg)-i1(iseg)+1,rr(i1(iseg)),
     _                       tplus(i1(iseg),2))
                        call pgline(i2(iseg)-i1(iseg)+1,rr(i1(iseg)),
     _                       tminus(i1(iseg),2))
c     call pgline(nthc(iv,jv),rr,tminus(1,2))
                     endif

                     if(doprintval.and.iv.eq.3.and.jv.eq.6)then
                        print*,'B: ',i2(iseg)-i1(iseg)+1
                        do ijk=i1(iseg),i2(iseg)
                           print*,
     _                          rr(ijk),tplus(ijk,1),
     _                          rr(ijk),tminus(ijk,1),
     _                          rr(ijk),tplus(ijk,2),
     _                          rr(ijk),tminus(ijk,2)
                        enddo
                     endif
                  enddo
                  if(dopntsrc)then
                     do ith=1,nthc(iv,jv)
                        xx1= cos(gbpix1(ith,iv,jv)*pi_180)*
     _                       cos(glpix1(ith,iv,jv)*pi_180)
                        yy1= cos(gbpix1(ith,iv,jv)*pi_180)*
     _                       sin(glpix1(ith,iv,jv)*pi_180)
                        zz1= sin(gbpix1(ith,iv,jv)*pi_180)
                        do ipt=1,mxpnt
                           aa= angle(xpnt(ipt),ypnt(ipt),zpnt(ipt),
     _                          xx1,yy1,zz1)
                           if(aa.lt.angpnt)then
                              call pgqch(gch)
                              call pgsch(2.0)
                              call pgpoint(1,rr(ith),0.0,4)
                              call pgsch(gch)
                              print*,'point source curve 1:',
     _                             iv,aa/pi_180,
     _                             glpix1(ith,iv,jv),
     _                             gbpix1(ith,iv,jv),
     _                             ipt,glpnt(ipt),gbpnt(ipt)
                           endif
                        enddo

                        xx2= cos(gbpix2(ith,iv,jv)*pi_180)*
     _                       cos(glpix2(ith,iv,jv)*pi_180)
                        yy2= cos(gbpix2(ith,iv,jv)*pi_180)*
     _                       sin(glpix2(ith,iv,jv)*pi_180)
                        zz2= sin(gbpix2(ith,iv,jv)*pi_180)
                        do ipt=1,mxpnt
                           aa= angle(xpnt(ipt),ypnt(ipt),zpnt(ipt),
     _                          xx2,yy2,zz2)
                           if(aa.lt.angpnt)then
                              call pgqch(gch)
                              call pgsch(2.0)
                              call pgpoint(1,rr(ith),0.0,5)
                              call pgsch(gch)
                              print*,'point source curve 2:',
     _                             iv,aa/pi_180,
     _                             glpix1(ith,iv,jv),
     _                             gbpix1(ith,iv,jv),
     _                             ipt,glpnt(ipt),gbpnt(ipt)
                           endif
                        enddo
                     enddo      !                     do ith=1,nth(iv,jv)
                  endif         !                  if(dopntsrc)then

                  if(dobig)then
                     call pgend
                  else
                     call pgsls(1)
                  endif
               else
                  print*,'iv,jv=',iv,jv,' NO points out of g plane'
               endif            !          if(nseg.gt.0)then
            endif               !             if(iv.gt.0.or.jv.gt.0)then

         enddo                  !do jv=0,jmax
      enddo                     !      do iv=-imax,imax

      if(.not.(dobig))call pgend

      print*,'inotplane=',inotplane

      end

      subroutine inset(iv,jv,yesno) ! Is (iv,jv) in the set to plot ?

      integer    iv,jv ! inputs
      logical*4  yesno ! output

      parameter(maxn=18)
      integer    ivv(maxn),jvv(maxn)
      
      data ivv/0, -6,-5,-4,-4, -6,-6,-6,-5, 4,5, -3,-3,
     _     -2,-1,5,6,6/
      data jvv/2,  3, 2, 1, 2,  4, 5, 6, 5, 6,5,  3, 6,
     _      2, 1,6,5,6/

      yesno=.false.
      i=1
      do while((.not.(yesno)).and.i.le.maxn)
         if(ivv(i).eq.iv.and.jvv(i).eq.jv)then
            yesno=.true.
         endif
         i=i+1
      enddo
      return
      end
