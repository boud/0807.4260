/* 
   circles - calculate identified circles statistics from CMB data (cosmic topology)

   Copyright (C) 2004 Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

#include <termios.h>
#include <grp.h>
#include <pwd.h>
*/

#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include "system.h"


#ifdef HAVE_ARGP_H
#  include <argp.h>
#endif  /* HAVE_ARGP_H */

#include <stdlib.h>


#define EXIT_FAILURE 1

#if ENABLE_NLS
# include <libintl.h>
# define _(Text) gettext (Text)
#else
# define textdomain(Domain)
# define _(Text) Text
#endif
#define N_(Text) Text


#include "lib/circles.h" 


char *xmalloc ();
char *xrealloc ();
char *xstrdup ();

#ifdef HAVE_ARGP_H
  static error_t parse_opt (int key, char *arg, struct argp_state *state);
  static void show_version (FILE *stream, struct argp_state *state);
#else
  static void show_version (FILE *stream);
#endif


/* argp option keys */
enum {DUMMY_KEY=129
      ,NOWARN_KEY='w'
      ,DIRECTORY_KEY='d'
};

/* Option flags and variables.  These are initialized in parse_opt.  */

char *oname;			/* --output=FILE */
FILE *ofile;
char *input_directory = NULL;	/* --directory=DIR */
int32_t  nlen_input_directory;

char *status_file = NULL;	/* --status=FILE */
int32_t  nlen_status_file;

char *cmb_file_raw = NULL;	/* --cmb_file_raw=FILE */
int32_t  nlen_cmb_file_raw;
char *cmb_file_smooth = NULL;	/* --cmb_file_smooth=FILE */
int32_t  nlen_cmb_file_smooth;

char *cmb_file_points = NULL;	/* --cmb_file_points=FILE */
int32_t  nlen_cmb_file_points;
char *cmb_file_mask = NULL;	/* --cmb_file_mask=FILE */
int32_t  nlen_cmb_file_mask;

char *toy_file = NULL;	/* --toy-simulation=FILE */
int32_t  nlen_toy_file;

char *binary_format = NULL;    /* --binary-format=BINARY_FORMAT */
int32_t  nlen_binary_format;

char *analysis_files = NULL;	/* --analysis=FILE */
int32_t  nlen_analysis_files;
char *analysis_pars = NULL;  /* parameters for analysis */
int32_t  nlen_analysis_pars;

int32_t want_verbose;		/* --verbose */
int32_t want_no_warn;		/* --no-warn */
int32_t want_ring;		        /* --ring */
int32_t want_mask;		        /* --mask */
int32_t want_corrS3;	        /* --spatial-correlation */
int32_t want_random_tmp;         /* temporary, preliminary version of want_random */
int32_t want_statistics_plot;       /* --statistics */
int32_t want_circles_plot;          /* --circles */
int32_t want_phase_plot;            /* --plot-phase */
int32_t want_SZ;                    /* --SZ */
int32_t want_cdiscs;                /* --correlate-discs */
int32_t want_iv;                /* --plot-six-curves */
char* ch_nrandom;           /*   --nrandom=NRANDOM */ 
int32_t want_random;            /* used if  --nrandom=NRANDOM */ 
int32_t want_ransmooth;         /* used if  --nrandom=NRANDOM, evaluated by CIRC_F */ 
int32_t want_new;               /* new CMB map file? */
int32_t want_use_unsmoothed;        /* use the unsmoothed file?*/

#ifdef HAVE_ARGP_H
static struct argp_option options[] =
{
  { "output",      'o',           N_("FILE"),      0,
    N_("send Output to FILE instead of standard output"), 0 },
  { "status",      'N',           N_("FILE"),      0,
    N_("require file with New or continuing status info"), 0 },
  { "toy-simulation",      't',           N_("FILE"),      0,
    N_("generate and use a Toy simulation"), 0 },
  { "circles",      'c',           NULL,      0,
    N_("plot Circles"), 0 },
  { "new",      'n',           NULL,      0,
    N_("read a new CMB sky map"), 0 },
  { "use-unsmoothed",      'u',           NULL,      0,
    N_("use Unsmoothed file "), 0 },
  { "statistics",      's',           NULL,      0,
    N_("plot Statistics"), 0 },
  { "plot-phase",  'P',             NULL,       0,
    N_("plot Phase"), 0 }, 
  { "plot-six-curves",  'S',             NULL,       0,
    N_("plot Six curves, implies -s"), 0 }, 
  { "correlate-discs",  'C',             NULL,       0,
    N_("Correlate discs"), 0 }, 
  { "SZ",  'z',             NULL,       0,
    N_("effect of sZ effect on circles"), 0 }, 
  /*  { "angmax",  'a', N_("ANGMAX"),       0,
      N_("minimum Angular radius of circle"), 0 },
      { "angmax",  'b', N_("ANGMAX"),       0,
      N_("maximum angular radius of circle"), 0 }, 
  */
  { "nrandom",      'r',      N_("NRANDOM"),      0,
    N_("do n Runs of Random positions/phases; implies -sS; negative value implies no smoothing"), 0 },
  { "ring",      'R',      NULL,      0,
    N_("Ring ordering for TOH format (unsmoothed) input file"), 0 },
  { "spatial-correlation",      'x',      NULL,      0,
    N_("spatial correlation Xi in covering space S^3"), 0 },
  { "verbose",     'v',           NULL,            0,
    N_("Print more information"), 0 },
  { "no-warn",     NOWARN_KEY,    NULL,            0,
    N_("disable Warnings"), 0 },
  { "directory",   DIRECTORY_KEY, N_("DIR"),       0,
    N_("Read input files from Directory DIR"), 0 },
  { "cmb_file_raw",  'i', N_("FILE"),       0,
    N_("cmb fits file of input data"), 0 },
  { "cmb_file_smooth",  'm', N_("FILE"),       0,
    N_("smoothed cmb file"), 0 },
  { "cmb_file_points",  'p', N_("FILE"),       0,
    N_("file with positions of point sources"), 0 },
  { "mask",      'K',      NULL,      0,
    N_("use a masK file"), 0 },
  { "cmb_file_mask",  'k', N_("FILE"),       0,
    N_("mask file"), 0 },
  { "analysis",  'a', N_("FILE"),       0,
    N_("analyse MCMC logs listed in FILE"), 0 },
  { "analysis-parameters",  'A', N_("PARAMETER_STRING"),       0,
    N_("parameters for analysis (requires -a)"), 0 },
  { "binary_format",  'b', N_("BINARY_FORMAT"),       0,
    N_("use binary format ['default', 'f8']"), 0 },
  { NULL, 0, NULL, 0, NULL, 0 }
};

/* The argp functions examine these global variables.  */
const char *argp_program_bug_address = "<boud at astro.uni.torun.pl>";
void (*argp_program_version_hook) (FILE *, struct argp_state *) = show_version;

static struct argp argp =
{
  /*  options, parse_opt, N_("[FILE...]"), */
  options, parse_opt, N_(" "),
  N_("calculate identified circles statistics from CMB data (cosmic topology)"),
  NULL, NULL, NULL
};
#endif  /* HAVE_ARGP_H */


int
main (int argc, char **argv)
{

  char* result_getenv;  /* result of call to getenv - may be NULL */
  int   nlen_result_getenv;
  int   nlen_nrandom;
  /*  char*  ch_nrandom;  */

  int want_debug = 0;
  ch_nrandom = "none";  /* default set in circ_f [circles_f77.f] */

  textdomain(PACKAGE);

  oname = "stdout";
  ofile = stdout;

  /* 
     INPUT PARAMETERS 
     certain input parameters can be input in many different ways 
  */

  /* firstly, try the header file */
#ifdef CIRCLES_INPUT_DIRECTORY
  nlen_input_directory= NLEN_CIRCLES_INPUT_DIRECTORY;
  input_directory = xmalloc(nlen_input_directory+1);
  strncpy(input_directory,CIRCLES_INPUT_DIRECTORY, nlen_input_directory+1);
  if(want_verbose)  /* this will not work!!! want_verbose not yet evaluated */
    printf("CIRCLES_INPUT_DIRECTORY= %s\n", input_directory);
#else
  input_directory = NULL;
  nlen_input_directory= 0;
#endif
  /* secondly, try for environment variables */
#ifdef HAVE_STDLIB_H
  result_getenv= getenv("CIRCLES_INPUT_DIRECTORY");   
  if(result_getenv != NULL)
    {
      if(want_verbose)   /* this will not work!!! not yet want_verbose  */
	printf("CIRCLES_INPUT_DIRECTORY= %s\n", result_getenv);
      nlen_result_getenv = strlen(result_getenv);
      free(input_directory);
      input_directory= xstrdup(result_getenv);
      nlen_input_directory= strlen(input_directory);
    };
#endif
  /* third try: command line option will override both the above */


  /* firstly, try the header file */
#ifdef CIRCLES_STATUS_FILE
  nlen_status_file= NLEN_CIRCLES_STATUS_FILE;
  status_file = xmalloc(nlen_status_file+1);
  strncpy(status_file, CIRCLES_STATUS_FILE, nlen_status_file+1);
  if(want_verbose)  /* this will not work!!! want_verbose not yet evaluated */
    printf("CIRCLES_STATUS_FILE= %s\n", status_file);
#else
  status_file = NULL;
  nlen_status_file= 0;
#endif

  /* secondly, try for environment variables */
#ifdef HAVE_STDLIB_H
  result_getenv= getenv("CIRCLES_STATUS_FILE");   
  if(result_getenv != NULL)
    {
      if(want_verbose)   /* this will not work!!! not yet want_verbose  */
	printf("CIRCLES_STATUS_FILE= %s\n", result_getenv);
      nlen_result_getenv = strlen(result_getenv);
      free(status_file);
      status_file= xstrdup(result_getenv);
      nlen_status_file= strlen(status_file);
    };
#endif
  /* third try: command line option will override both the above */



#ifdef CIRCLES_CMB_FILE_RAW
  nlen_cmb_file_raw= NLEN_CIRCLES_CMB_FILE_RAW;
  cmb_file_raw= xmalloc(nlen_cmb_file_raw+1);
  strncpy(cmb_file_raw, CIRCLES_CMB_FILE_RAW, nlen_cmb_file_raw+1);
  if(want_verbose)
    printf("CIRCLES_CMB_FILE_RAW= %s\n", cmb_file_raw);
#else
  cmb_file_raw = NULL;
  nlen_cmb_file_raw= 0;
#endif
  /* secondly, try for environment variables */
#ifdef HAVE_STDLIB_H
  result_getenv= getenv("CIRCLES_CMB_FILE_RAW");   
  if(result_getenv != NULL)
    {
      if(want_verbose)
	printf("CIRCLES_CMB_FILE_RAW= %s \n", result_getenv);
      nlen_result_getenv = strlen(result_getenv);
      free(cmb_file_raw);
      cmb_file_raw= xstrdup(result_getenv);
      nlen_cmb_file_raw= strlen(cmb_file_raw);
    };
#endif
  /* third try: command line option will override both the above */


#ifdef CIRCLES_CMB_FILE_SMOOTH
  nlen_cmb_file_smooth= NLEN_CIRCLES_CMB_FILE_SMOOTH;
  cmb_file_smooth =xmalloc(nlen_cmb_file_smooth+1);
  strncpy(cmb_file_smooth, CIRCLES_CMB_FILE_SMOOTH, nlen_cmb_file_smooth+1);
  if(want_verbose)
    printf("CIRCLES_CMB_FILE_SMOOTH= %s\n", cmb_file_smooth);
#else
  cmb_file_smooth = NULL;
  nlen_cmb_file_smooth= 0;
#endif
  /* secondly, try for environment variables */
#ifdef HAVE_STDLIB_H
  result_getenv= getenv("CIRCLES_CMB_FILE_SMOOTH");   
  if(result_getenv != NULL)
    {
      if(want_verbose)
	printf("CIRCLES_CMB_FILE_SMOOTH= %s \n", result_getenv);
      nlen_result_getenv = strlen(result_getenv);
      free(cmb_file_smooth);
      cmb_file_smooth= xstrdup(result_getenv);
      nlen_cmb_file_smooth= strlen(cmb_file_smooth);
    };
#endif
  /* third try: command line option will override both the above */

#ifdef CIRCLES_CMB_FILE_POINTS
  nlen_cmb_file_points= NLEN_CIRCLES_CMB_FILE_POINTS;
  cmb_file_points = xmalloc(nlen_cmb_file_points+1);
  strncpy(cmb_file_points, CIRCLES_CMB_FILE_POINTS, nlen_cmb_file_points+1);
  if(want_verbose)
    printf("CIRCLES_CMB_FILE_POINTS= %s\n", cmb_file_points);
#else
  cmb_file_points = NULL;
  nlen_cmb_file_points= 0;
#endif
  /* secondly, try for environment variables */
#ifdef HAVE_STDLIB_H
  result_getenv= getenv("CIRCLES_CMB_FILE_POINTS");   
  if(result_getenv != NULL)
    {
      if(want_verbose)
	printf("CIRCLES_CMB_FILE_POINTS= %s \n", result_getenv);
      nlen_result_getenv = strlen(result_getenv);
      free(cmb_file_points);
      cmb_file_points= xstrdup(result_getenv);
      nlen_cmb_file_points= strlen(cmb_file_points);
    };
#endif
  /* third try: command line option will override both the above */


#ifdef CIRCLES_CMB_FILE_MASK
  nlen_cmb_file_mask= NLEN_CIRCLES_CMB_FILE_MASK;
  cmb_file_mask = xmalloc(nlen_cmb_file_mask+1);
  strncpy(cmb_file_mask, CIRCLES_CMB_FILE_MASK, nlen_cmb_file_mask+1);
  if(want_verbose)
    printf("CIRCLES_CMB_FILE_MASK= %s\n", cmb_file_mask);
#else
  cmb_file_mask = NULL;
  nlen_cmb_file_mask= 0;
#endif
  /* secondly, try for environment variables */
#ifdef HAVE_STDLIB_H
  result_getenv= getenv("CIRCLES_CMB_FILE_MASK");   
  if(result_getenv != NULL)
    {
      if(want_verbose)
	printf("CIRCLES_CMB_FILE_MASK= %s \n", result_getenv);
      nlen_result_getenv = strlen(result_getenv);
      free(cmb_file_mask);
      cmb_file_mask= xstrdup(result_getenv);
      nlen_cmb_file_mask= strlen(cmb_file_mask);
    };
#endif
  /* third try: command line option will override both the above */



  analysis_files = NULL;
  nlen_analysis_files= 0;

  /* secondly, try for environment variables */
#ifdef HAVE_STDLIB_H
  result_getenv= getenv("CIRCLES_ANALYSIS_FILES");   
  if(result_getenv != NULL)
    {
      if(want_verbose)
	printf("CIRCLES_ANALYSIS_FILES= %s \n", result_getenv);
      nlen_result_getenv = strlen(result_getenv);
      free(analysis_files);
      analysis_files= xstrdup(result_getenv);
      nlen_analysis_files= strlen(analysis_files);
    };
#endif
  /* third try: command line option will override both the above */

  analysis_pars = NULL;
  nlen_analysis_pars= 0;

  /* secondly, try for environment variables */
#ifdef HAVE_STDLIB_H
  result_getenv= getenv("CIRCLES_ANALYSIS_PARS");   
  if(result_getenv != NULL)
    {
      if(want_verbose)
	printf("CIRCLES_ANALYSIS_PARS= %s \n", result_getenv);
      nlen_result_getenv = strlen(result_getenv);
      free(analysis_pars);
      analysis_pars= xstrdup(result_getenv);
      nlen_analysis_pars= strlen(analysis_pars);
    };
#endif
  /* third try: command line option will override both the above */


  /* firstly, try the header file */
#ifdef CIRCLES_TOY_FILE
  nlen_toy_file= NLEN_CIRCLES_TOY_FILE;
  toy_file = xmalloc(nlen_toy_file+1);
  strncpy(toy_file, CIRCLES_TOY_FILE, nlen_toy_file+1);
  if(want_verbose)  /* this will not work!!! want_verbose not yet evaluated */
    printf("CIRCLES_TOY_FILE= %s\n", toy_file);
#else
  toy_file = NULL;
  nlen_toy_file= 0;
#endif
  /* secondly, try for environment variables */
#ifdef HAVE_STDLIB_H
  result_getenv= getenv("CIRCLES_TOY_FILE");   
  if(result_getenv != NULL)
    {
      if(want_verbose)   /* this will not work!!! not yet want_verbose  */
	printf("CIRCLES_TOY_FILE= %s\n", result_getenv);
      nlen_result_getenv = strlen(result_getenv);
      free(toy_file);
      toy_file= xstrdup(result_getenv);
      nlen_toy_file= strlen(toy_file);
    };
#endif
  /* third try: command line option will override both the above */



  want_verbose = 1;
  want_no_warn = 0;
  want_ring = 0;
  want_mask = 0;
  want_corrS3 = 0;
  want_random_tmp = 0;
  want_statistics_plot = 0;
  want_circles_plot = 0;
  want_phase_plot = 0;
  want_SZ = 0;
  want_cdiscs = 0;
  want_iv = 0;
  
  nlen_binary_format= 7;
  binary_format = xmalloc(nlen_binary_format+1);
  strncpy(binary_format,"default",nlen_binary_format+1); /* standard circles format - 4-byte floats after 17 other bytes */
  want_new= 0;
  want_use_unsmoothed= 0;

#if HAVE_ARGP_H
  argp_parse(&argp, argc, argv, 0, NULL, NULL);
#else
  printf("Warning: no command line options since a recent glibc is not installed.\n");
#endif  /* HAVE_ARGP_H */

  /* If random version is requested and corrS3 is NOT requested, then
     automatically imply -s and -S.  want_random itself is set in circles_f77.f
  */
  if(want_random_tmp && (want_corrS3 != 1)){
    want_iv = 1;                 /* --plot-six-curves */
    want_statistics_plot = 1;    /* --statistics */
  };


  /* simulate low RAM conditions */
  
  /* 
  printf("will try allocating another 10Mb... \n");
  xmalloc(10000000);
  */


  /* TODO: do the work */

  /* try to allocate memory for the big arrays, get back number of pixels */
  /*  PXALLO(want_verbose, &circles_npix); */


  pxalloc_status = -1;
  pxtempcp_status = -1;

  PXALLO(&want_verbose, &circles_npix,
	 &temp,&errtemp,&gallong,&gallat, &tempcp, &k_ring);

  printf("after pxallo: circles_npix = %d\n",circles_npix);

  if(want_debug)
    {
      printf("temp = %x , *temp = %x , *temp = %f\n",temp,*temp,*temp);
      printf("temp = %x , *temp = %x , *temp = %f\n",temp,*temp,*temp);
      printf("circles.c main: before   *temp= 7.8  value of *temp = %f;\n",*temp);
      *temp= 7.8;
      printf("circles.c main: after   *temp= 7.8  value of *temp = %f;\n",*temp);
      printf("temp = %x , *temp = %x, %f\n",temp,*temp,*temp);
      printf("temp = %x , *temp = %f\n",temp,*temp);
    };


  if (nlen_analysis_files > 0)
    {
      ANALYS(analysis_files, &nlen_analysis_files,
	     analysis_pars, &nlen_analysis_pars
	     );

      exit(0);
    };

  nlen_nrandom= strlen(ch_nrandom);

  printf("DEBUG line515: nlen_input_directory = %d\n", nlen_input_directory);
  CIRC_F(
	 input_directory, &nlen_input_directory,
	 status_file, &nlen_status_file,
	 cmb_file_raw,    &nlen_cmb_file_raw,
	 cmb_file_smooth, &nlen_cmb_file_smooth,
	 cmb_file_mask, &nlen_cmb_file_mask,
	 toy_file, &nlen_toy_file,
	 binary_format, &nlen_binary_format,
	 &want_verbose,&want_no_warn,
	 &want_ring,
	 &want_mask,
	 &want_cdiscs, 
	 &want_corrS3,
	 ch_nrandom,&nlen_nrandom,
	 &circles_npix,
	 temp,errtemp,gallong,gallat,tempcp,k_ring,
	 &want_new, &want_use_unsmoothed,
	 &want_random, &want_ransmooth
	 ); 

  /* printf("want_random = %d\n",want_random); */

  if(want_statistics_plot)
    {
      PDODEC( &want_iv,
	      &want_random,
	      &want_ransmooth
	      );
    };

  if(want_circles_plot)
    {
      PCIRCL(
	       input_directory, &nlen_input_directory,
	       &want_SZ
	       );
    };

  if(want_phase_plot)
    {
      PDISCS(
	     input_directory, &nlen_input_directory,
	     &circles_npix,
	     &want_verbose,&want_no_warn,
	     temp,errtemp,gallong,gallat,tempcp
	     );
    };


  exit (0);
}


#ifdef HAVE_ARGP_H
/* Parse a single option.  */
static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  switch (key)
    {
    case ARGP_KEY_INIT:
      /* Set up default values.  */
      break;
    case 'o':			/* --output */
      oname = xstrdup (arg);
      ofile = fopen (oname, "w");
      if (!ofile)
	argp_failure (state, EXIT_FAILURE, errno,
		      _("Cannot open %s for writing"), oname);
      break;
    case 'c':
      want_circles_plot = 1;  /* --circles */
      break;
    case 'n':
      want_new = 1;  /* --new */
      break;
    case 'u':
      want_use_unsmoothed = 1;  /* --use-unsmoothed */
      break;
    case 's':
      want_statistics_plot = 1;  /* --statistics */
      break;
    case 'P':
      want_phase_plot = 1;  /* --plot-phase */
      break;
    case 'S':
      want_iv = 1;   /* --plot-six-curves */
      want_statistics_plot = 1;  /* --statistics */
      break;
    case 'C':
      want_cdiscs = 1;   /* --correlate-discs */
      break;
    case 'z':
      want_SZ = 1;             /* --SZ */
      want_circles_plot = 1;
      break;
    case 'r':                   /* --nrandom=NRANDOM */
      ch_nrandom = xstrdup(arg);
      want_random_tmp = 1;
      /*  want_iv = 1;   */   /* --plot-six-curves */
      /*  want_statistics_plot = 1; */   /* --statistics */
      break;
    case 'R':                 /* --ring */
      want_ring = 1;
      break;
    case 'x':                 /* --spatial-correlation */
      want_corrS3 = 1;
      break;
    case 'v':			/* --verbose */
      want_verbose = 1;
      break;
    case NOWARN_KEY:		/* --no-warn */
      want_no_warn = 1;
      break;
    case DIRECTORY_KEY:		/* --directory */
      /*      input_directory = xstrdup (optarg); */
      free(input_directory);
      input_directory = xstrdup (arg);
      nlen_input_directory= strlen(input_directory);
      break;
    case 'N':		/* --status */
      free(status_file);
      status_file = xstrdup (arg);
      nlen_status_file= strlen(status_file);
      break;
    case 'i':
      free(cmb_file_raw);
      cmb_file_raw= xstrdup(arg);
      nlen_cmb_file_raw= strlen(cmb_file_raw);
      break;
    case 'm':
      free(cmb_file_smooth);
      cmb_file_smooth= xstrdup(arg);
      nlen_cmb_file_smooth= strlen(cmb_file_smooth);
      break;
    case 'p':
      free(cmb_file_points);
      cmb_file_points= xstrdup(arg);
      nlen_cmb_file_points= strlen(cmb_file_points);
      break;
    case 'k':
      free(cmb_file_mask);
      cmb_file_mask= xstrdup(arg);
      nlen_cmb_file_mask= strlen(cmb_file_mask);
      break;
    case 't':		/* --toy-simulation */
      free(toy_file);
      toy_file = xstrdup (arg);
      nlen_toy_file= strlen(toy_file);
      break;
    case 'a':
      free(toy_file);
      analysis_files= xstrdup(arg);
      nlen_analysis_files= strlen(analysis_files);
      break;
    case 'A':	  /* --analysis-parameters */
      free(analysis_pars);
      analysis_pars= xstrdup(arg);
      nlen_analysis_pars= strlen(analysis_pars);
      break;
    case 'K':                 /* --mask */
      want_mask = 1;
      break;
    case 'b':
      free(binary_format);
      binary_format= xstrdup(arg);
      nlen_binary_format= strlen(binary_format);
      break;
    case ARGP_KEY_ARG:		/* [FILE]... */
      /* TODO: Do something with ARG, or remove this case and make
         main give argp_parse a non-NULL fifth argument.  */
      break;

    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}
#endif  /* HAVE_ARGP_H */


/* Show the version number and copyright information.  */
static void
#ifdef HAVE_ARGP_H
show_version (FILE *stream, struct argp_state *state)
#else
     show_version (FILE *stream)
#endif  /* HAVE_ARGP_H */
{
#ifdef HAVE_ARGP_H
  (void) state;
#endif  /* HAVE_ARGP_H */
  /* Print in small parts whose localizations can hopefully be copied
     from other programs.  */
  fputs(PACKAGE" "VERSION"\n", stream);
  fprintf(stream, _("Written by %s.\n\n"), "Boud Roukema");
  fprintf(stream, _("Copyright (C) %s %s\n"), "2004", "Boud Roukema");
  fputs(_("\
This program is free software; you may redistribute it under the terms of\n\
the GNU General Public License.  This program has absolutely no warranty.\n"),
	stream);
}
