C   isolat - test a library for an isolatitude, equi-solid-angle, hierarchical pixel-coordinate system
C
C   Copyright (C) 2004 Boud Roukema
C
C   This program is free software; you can redistribute it and/or modify
C   it under the terms of the GNU General Public License as published by
C   the Free Software Foundation; either version 2, or (at your option)
C   any later version.
C
C   This program is distributed in the hope that it will be useful,
C   but WITHOUT ANY WARRANTY; without even the implied warranty of
C   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C   GNU General Public License for more details.
C
C   You should have received a copy of the GNU General Public License
C   along with this program; if not, write to the Free Software Foundation,
C   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  


      subroutine testf(i_a2p, 
     _     nres_s,j_nres, 
     _     inorth, ifpixel,
     _     i_serial, i_verb, j_warn, j_nest, i_plot
     _     )

      include 'isolatcom.f'

C     INPUTS:
      character*(*)  nres_s ! n_res as a string 
      character*256  string
      integer        j_nres !length of the string   nres_s
      integer        inorth  ! convention for North Pole in theta
      integer        ifpixel  ! first pixel number 
      integer        i_a2p  ! angle2pixel ?
      integer        i_serial,i_verb,j_warn,j_nest,i_plot  

C     OUTPUTS:
C     INTERNAL:


c     ==== This first section does some processing of the
c     various command line parameters. Skip to the sections
c     with calls to   getpix  and  getang  if you want to see
c     where they are called. ====

      if(i_verb.eq.1)then
         q_verb=.true.
      else
         q_verb=.false.
      endif
      
      if(i_a2p.eq.1)then
         q_a2p=.true.
         if(q_verb)print*,' angle -> pixel conversion '
      else
         q_a2p=.false.
         if(q_verb)print*,' pixel -> angle conversion '
      endif

      n_res =  4 ! default 
      if(j_nres.gt.0)then
         if(debug)print*,'j_nres=',j_nres
         if(debug)print*,'nres_s =__',nres_s(1:j_nres),
     _        '__, j_nres=',j_nres
c         string='  '//nres_s(1:j_nres)
c         read(string(1:j_nres+2),'(i)')n_res
         read(nres_s(1:j_nres),*)n_res
      endif
      if(q_verb)print*,'n_res = ',n_res

      if(i_serial.eq.1)then
         stop 'Serial pixeling not yet implemented.'
         q_serial= .true.
         if(q_verb)print*,' serial pixel ordering ' 
      else
         q_serial=.false.
         if(q_verb)print*,' parallel pixel ordering ' 
      endif


      if(j_warn.eq.1)then
         q_warn=.true.
         if(q_verb)print*,'Will print warnings. '
      else
         q_warn=.false.
c         j_warn=i_warn        ! j_warn.eq.1 if warnings should be suppressed
c     print*,'WARNING: you have turned off warnings (except for',
c     _        ' this one ;).'
      endif
         
      if(q_a2p)then
         if(q_verb)write(6,'(a,$)')'Enter phi, theta: '
         read(5,*)phi,theta
         if(q_verb)print*,'Input phi,theta are:',phi,theta

          if(q_verb)print*,'Will call getpix:'

c     ==== call to getpix ====
c          j_nest= 1   ! WARNING: hard_wired 
          call getpix(n_res, inorth, ifpixel, 
     _         j_warn, j_nest,
     _         phi, theta, ipix2, n_res2)
c     ====

         if(.not.(i_plot.eq.1))then
            if(q_verb)write(6,'(a,$)')'ipix2='
            print*,ipix2
         endif
         if((.not.(q_warn)).and.(n_res2.ne.n_res))then
            print*,'WARNING after getpix: n_res, n_res2=',
     _           n_res,n_res2
         endif

         if(q_verb)print*,'after getpix: n_res, n_res2=',
     _           n_res,n_res2

      else
         if(q_verb)write(6,'(a,$)')'Enter pixel number: '
         read(5,*)ipix
         if(q_verb)print*,'Input pixel number is: ',ipix
         if(debug)print*,'before getang:',
     _        'n_res, inorth, ipix, phi, theta=',
     _        n_res, inorth, ipix, phi, theta
         if(q_verb)print*,'Will call getang:'

c         call getang(n_res, inorth, ifpixel, q_warn, ipix,   
c     _        phi, theta)

c     --- integer j_warn is probably safer than logical for f77 <-> C
c     interaction ---

c     ==== call to getang ====
         call getang(n_res, inorth, ifpixel, j_warn, ipix,   
     _        phi, theta, n_res2)
c     ====

         if(.not.(i_plot.eq.1))then  ! plotting output conflicts with this 
            if(q_verb)write(6,'(a,$)')'phi, theta='       
            print*,phi,theta
         endif

         if((.not.(q_warn)).and.(n_res2.ne.n_res))then
            print*,'WARNING after getang: n_res, n_res2=',
     _           n_res,n_res2
         endif

         if(q_verb)print*,'after getang: n_res, n_res2=',
     _           n_res,n_res2

c         if(ipix2.ne.ipix)print*,'ERROR ipix,ipix2= ',ipix,ipix2
      endif

      return
      end

