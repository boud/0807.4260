/* 
   isolat - test a library for an isolatitude, equi-solid-angle, hierarchical pixel-coordinate system

   Copyright (C) 2004 Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

*/

/*  makes a plot similar to Fig~3 of astro-ph/9905275, which presents 
    a partial outline of the isolatitude pixelisation system 
*/


#include <stdio.h>

#include "config.h"

/* #if (defined(HAVE_PLOT) && defined(HAVE_X11) && defined(HAVE_XAW)) */
#if (defined(HAVE_LIBPLOT) && defined(HAVE_LIBX11) && defined(HAVE_LIBXAW))
#include <plot.h>
#endif


#include <math.h>

#include "system.h"
#include "isolat.h"

int plot_projection(int n_resolution_atoi)
{
#define NPIXEL_LABEL 5
#define PIXEL_LABEL_FORMAT "%5d"

  char  pixel_label[NPIXEL_LABEL];  /* label for pixel number */
  int   npixel;        /* total number of pixels */
  double  phi,theta;   /* output angles */
  double pi=3.14159265259;
  double phi_pi,ss;    /* phi/pi, sin(theta)  */

  int   ipixel;     /* for looping through pixels */
  int   jpixel;  /* for testing  getpix  0.1.15 */   
  int   n_res3 =  -99;            /* for testing  getpix  0.1.15 */   
  int   test_getpix = 1 ;    /* for testing  getpix  0.1.15 */   

  int   ihandle,iold_handle;    /* for handling "plotter" objects */
  double  x1=-0.2,x2=2.2, y1=-1.2,y2=1.2 ;        /* plot limits*/
  double  xx1=0.0,xx2=2.0, yy1=-1.0,yy2=1.0 ;        /* box */

  /* printf("first line of plot_projection\n");  */

#if (defined(HAVE_LIBPLOT) && defined(HAVE_LIBX11) && defined(HAVE_LIBXAW))

  /* start up a plot */
  if(( ihandle = pl_newpl("meta", stdin, stdout, stderr) ) < 0)
    {
      fprintf(stderr, "plot_projection: newpl problem opening plot device\n");
      return 1;
    };
  /* start up a plot */
  if(( iold_handle= pl_selectpl(ihandle) ) < 0)
    {
      fprintf(stderr, "plot_projection: selectpl problem opening plot device\n");
      return 1;
    };
  /* start up a plot */
  if(( pl_openpl()) < 0)
    {
      fprintf(stderr, "plot_projection: problem opening plot device\n");
      return 1;
    };

  /*  printf("plot_projection: will call pl_fspace\n");  */

  /* pl_fspace(0.0, 2.0, -1.0, 1.0); */  /* plotting box */
  pl_fspace(x1,y1, x2,y2);   /* plotting box */
  /*  pl_fspace(-0.2,  -1.2, 2.2, 1.2); */  /* plotting box */
  pl_flinewidth(0.02);              /* thick lines for publishable plots */
  pl_pencolorname("black"); 

  /* WARNING: number of pixels hardwired for n_resolution = 2^n case */
  npixel= 12 * n_resolution_atoi * n_resolution_atoi 
    - (1-ifirst_pixel);           /* correct for initial pixel number */
  /* printf("plot_projection:  npixel = %d \n",npixel);  */

  pl_erase (); 

  pl_fmove(xx1,yy1);
  pl_fcont(xx2,yy1);
  pl_fcont(xx2,yy2);
  pl_fcont(xx1,yy2);
  pl_fcont(xx1,yy1);

  /* test only 
     pl_fmove(1.0, 0.0);
     pl_fcontrel(1.0,1.0);
  */


  /* printf("plot_projection: will start loop\n");  */

  /* loop through pixels */
  for (ipixel=ifirst_pixel; ipixel <= npixel; ipixel++)
    {

      GETANG( &n_resolution_atoi,
	      &inorth,
	      &ifirst_pixel,
	      &want_no_warn,
	      &ipixel,
	      &phi,
	      &theta,
	      &n_res2 
	      );

      if(test_getpix == 1){
	GETPIX( &n_resolution_atoi,
		&inorth,
		&ifirst_pixel,
		&want_no_warn,
		&want_nested,
		&phi,
		&theta,
		&jpixel,
		&n_res3 
		);
      };

	      
      phi_pi = phi/pi;
      if(phi_pi < 0.0)
	phi_pi = phi_pi + 2.0;
      ss = sin(theta);


      /* printf("ipixel= %d  phi_pi = %f  ss= %f \n",ipixel,phi_pi,ss);   */

      pl_fmove( phi_pi, ss);            /* move to position  phi_pi, ss */


      /* convert pixel number to string  */
      if(!test_getpix == 1)
	{
	  sprintf(pixel_label, PIXEL_LABEL_FORMAT, ipixel);
	} 
      else
	{
	  sprintf(pixel_label, PIXEL_LABEL_FORMAT, jpixel);  /* if test_getpix */
	};
      

      pl_alabel('c','c', pixel_label); 
    };

  if( pl_closepl() < 0)
    {
      fprintf(stderr, "plot_projection: problem closing plot device\n");
      return 1;
    };

  /*  printf("iold_handle = %d\n",iold_handle); */
  if( pl_selectpl(iold_handle) < 0)
    {
      fprintf(stderr, "plot_projection: selectpl problem closing plot device\n");
      return 1;
    };

  if( pl_deletepl(ihandle) < 0)
    {
      fprintf(stderr, "plot_projection: deletepl problem closing plot device\n");
      return 1;
    };


#else
    {
      printf("isolat: plot_projection: Missing libraries. Run ./configure to see which libraries are missing. Look at Makefile.am for hints on how to find, install and link these libraries. \n");
    }
#endif


  return 0;
}
