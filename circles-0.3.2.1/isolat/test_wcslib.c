/* 
   isolat - test a library for an isolatitude, equi-solid-angle, hierarchical pixel-coordinate system

   Copyright (C) 2004 Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

*/

#include <stdio.h>
#include <sys/types.h>

#include <string.h>


#include "system.h"

#include <string.h>

#include "isolat.h"

#include "math.h"

#ifdef HAVE_LIBWCS
#include <wcs.h>   /* wcslib headers */
#endif

#define DEBUG 0
#define DEBUG1 0
#define DEBUG2 1

int test_wcslib(int n_resolution_atoi, int ipix_in,  /*  inputs */
			      int* ipix_out) /* output(s) */
{

#ifdef HAVE_LIBWCS

  struct wcsprm* wcsprm_struct;
  int ncoord;
  int nelem; /* not used if ncoord=1 */
  /* 
     const double pixcrd_in[1][2];
     double pixcrd_out[1][2];
     double imgcrd[1][2];
     double world[1][2];
     int istatus[1][2]; 
  */
  double pixcrd_in[2];
  double pixcrd_out[2];
  double imgcrd[2];
  double world[2];
  int istatus[2];
  
  double phi,theta;  

  char char8[8];

  /*  char wcs_cards[400]; */
  char wcs_cards[2880];

  /* WARNING: These must be exactly 80 characters long. */
  char wcs_card_axes[80]= 
    "WCSAXES =                    2 /  5* no of pixels along side of one square      ";
  char wcs_card_naxis[80]= 
    "NAXIS   =                    2 /  2-dimensional image                           ";
  char wcs_card_naxis1[80]= 
    "NAXIS1  =                      /  x axis                                        ";
  char wcs_card_naxis2[80]= 
    "NAXIS2  =                      /  y axis                                        ";

  char wcs_card_ctype1[80]=
    "CTYPE1  = 'GLON-HPX'           /  galactic longitude                            ";
  char wcs_card_ctype2[80]=
    "CTYPE2  = 'GLAT-HPX'           /  galactic latitude                             ";
  char wcs_card_pv1_1[80]=
    "PV1_1   =                    4 /   H=4 => standard WMAP type projection         ";
  char wcs_card_pv1_2[80]=
    "PV1_2   =                    3 /   K=3 => standard WMAP type projection         ";
  char wcs_card_pv2_1[80]=
    "PV2_1   =                    4 /   H=4 => standard WMAP type projection         ";
  char wcs_card_pv2_2[80]=
    "PV2_2   =                    3 /   K=3 => standard WMAP type projection         ";
  char fits_blank_line[80]=
    "                                                                                ";

  int ioffset=0;

  int ncards=7;
  int relax= 1;  /* 0 = strict FITS, 1 = informal extensions */
  int ctrl= 3;  /* 0 = no info, 3 = most verbose */
  int nreject;
  int nwcs;
  struct wcsprm** array_of_wcsprm_struct;  

  int ilocal;

  /* BEGIN statement section of this function */


  /* TODO: feed in &n_resolution_atoi to set up wcspih stuff */
  /* strcpy( wcs_cards, "test"); */
  if(DEBUG && 0)
    {
      printf("begin  test_wcslib\n");
      printf("%s",wcs_cards);
      printf("AA after wcs_cards\n");

      printf("wcs_cards= %d  &(wcs_cards[0])= %d &(wcs_cards[80]) = %d\n",
	     wcs_cards,&(wcs_cards[0]),&(wcs_cards[80]) );

      printf("strlen(wcs_cards)= %d \n",strlen(wcs_cards));
    };

  if(DEBUG1 && 0)
    strcpy(wcs_cards,"");

  /* WCSAXES ? */
  ioffset+= 80;
  strncpy( &(wcs_cards[ioffset]), wcs_card_axes,80);
  /* strncpy( &(wcs_cards[ioffset+10]), char8, 7); NOT NAXIS1 or NAXIS2 !*/ 



  /*  strncpy( &(wcs_cards[0]), wcs_card_axes,80); */
  strncpy( &(wcs_cards[ioffset]), wcs_card_naxis,80);
  if(DEBUG1)
    printf("strlen(wcs_cards)= %d \n",strlen(wcs_cards));
  
  /* NAXIS1  */
  ioffset+= 80;
  strncpy( &(wcs_cards[ioffset]), wcs_card_naxis1,80);
  sprintf(char8,"%7d",
	   n_resolution_atoi *5
	   );
  strncpy( &(wcs_cards[ioffset+10]), char8, 7);
  wcs_cards[ioffset+80]='\0';
  /*  wcs_cards[ioffset+80]=NULL; */
  if(DEBUG1)
    printf("aa_ strlen(wcs_cards)= %d \n",strlen(wcs_cards));
  

  /* NAXIS2  */
  ioffset+= 80;
  if(DEBUG1 && 0)
    printf("bb_ ioffset= %d\n",ioffset);
  strncpy( &(wcs_cards[ioffset]), wcs_card_naxis2,80);
  strncpy( &(wcs_cards[ioffset+10]), char8, 7); 
  wcs_cards[ioffset+80]='\0';
  /* wcs_cards[ioffset+80]=NULL; */
  if(DEBUG1)
    printf("bb_ strlen(wcs_cards)= %d \n",strlen(wcs_cards));




  if(DEBUG1)
    printf("cc_ strlen(wcs_cards)= %d \n",strlen(wcs_cards));

  ioffset+= 80;
  strncpy( &(wcs_cards[ioffset]), wcs_card_ctype1,80); 
  ioffset+= 80;
  strncpy( &(wcs_cards[ioffset]), wcs_card_ctype2,80);

  ioffset+= 80;
  strncpy( &(wcs_cards[ioffset]), wcs_card_pv1_1,80);
  ioffset+= 80;
  strncpy( &(wcs_cards[ioffset]), wcs_card_pv1_2,80);
  ioffset+= 80;
  strncpy( &(wcs_cards[ioffset]), wcs_card_pv2_1,80);
  ioffset+= 80;
  strncpy( &(wcs_cards[ioffset]), wcs_card_pv2_2,80);

  /*  wcs_cards[ioffset+80]= NULL; */
  if(DEBUG1)
    printf("strlen(wcs_cards)= %d \n",strlen(wcs_cards));
  if(DEBUG1)
    {
      printf("B: \n");
      printf("%s",wcs_cards);
      printf("BB after wcs_cards\n");
    };

  /*  for (ilocal=5; ilocal < 36; ilocal++)
    {
      strncpy( &(wcs_cards[ilocal*80]), fits_blank_line,80);
    };
  */


  if(DEBUG2)
    printf("test_wcslib: WARNING - still in development phase\n");

  ilocal= wcspih(wcs_cards,
		 ncards,relax,ctrl,
		 &nreject,&nwcs,
		 &array_of_wcsprm_struct
		 );
  if(DEBUG2)
    printf("after wcspih, ilocal= %d\n",ilocal);

  

  /* TODO: convert 
     ipix_in   into   (double){ pixcrd_in[0],pixcrd_in[1] } */

  ncoord =1;
  nelem=2;



  wcsp2s(wcsprm_struct,ncoord,nelem, pixcrd_in,  /* inputs */
	 imgcrd, &phi, &theta, world, istatus);    /* outputs */
  wcss2p(wcsprm_struct,ncoord,nelem, world,     /* inputs */
	 &phi,&theta, imgcrd, pixcrd_out, istatus);  /* outputs */  


  /* TODO: convert 
     (double){ pixcrd_out[0],pixcrd_out[1] }  into  ipix_out 
  */

  *ipix_out= ipix_in;  /* !!!test only!!! */
  return 0;

#endif
};
