C   isolat - test a library for an isolatitude, equi-solid-angle, hierarchical pixel-coordinate system
C
C   Copyright (C) 2004 Boud Roukema
C
C   This program is free software; you can redistribute it and/or modify
C   it under the terms of the GNU General Public License as published by
C   the Free Software Foundation; either version 2, or (at your option)
C   any later version.
C
C   This program is distributed in the hope that it will be useful,
C   but WITHOUT ANY WARRANTY; without even the implied warranty of
C   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C   GNU General Public License for more details.
C
C   You should have received a copy of the GNU General Public License
C   along with this program; if not, write to the Free Software Foundation,
C   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  


C     --- Warning: include 'isolatcom.f' must be first declaration!
      implicit real*8 (a-h,o-z)


c     COMMON BLOCKS
      logical        q_a2p  ! query angle2pixel = 1, pixel2angle = 0
      integer        n_res     ! n resolution - pixels per side of unit sq. interval
      logical        q_serial  ! query serial =1 , parallel = 0
      logical        q_verb  ! query verbose
      logical        q_warn ! query warning_disable


c     --- separate common blocks for maximum portability ---
      common /cmm_int/ n_res
      common /cmm_log/ q_a2p, q_serial, q_verb, q_warn

c     INITIALISED PARAMETERS
      real*8         pi,pi_180
      parameter(pi=3.14159265358979323846d0,pi_180=pi/180.0d0)


      real*8         toldef ! default tolerance
      parameter(toldef=1.0d-6)     


c     ---- parameters that should probably be made into command options ----

      logical*4       debug
      parameter(debug=.false.) !.true.) ! 



