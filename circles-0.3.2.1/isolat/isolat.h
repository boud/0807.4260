/* 
   isolat - test a library for an isolatitude, equi-solid-angle, hierarchical pixel-coordinate system

   Copyright (C) 2004 Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

*/


/* main fortran test function */


#define TESTF F77_FUNC(testf,TESTF)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
void TESTF(
		  int *want_angle_to_pixel,
		  char *n_resolution_string, 
		  int *nlen_n_resolution_string, 
		  int *inorth,
		  int *ifirst_pixel,
		  int *want_serial,
		  int *want_verbose,
		  int *want_no_warn,
		  int *want_nested,
		  int *want_plot
		 );




/* routine for converting pixel number to an angular position in the sky */

#define GETANG F77_FUNC(getang,GETANG)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
void GETANG(
	    /*  INPUTS  */
	    int *n_resolution_atoi,
	    int *inorth,
	    int *ifirst_pixel,
	    int *want_no_warn,
	    int *ipix_in,              /* input pixel number */
	    /*  OUTPUTS  */
	    double *phi,
	    double *theta,
	    int *n_res2
	    );

#define GETPIX F77_FUNC(getpix,GETPIX)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
void GETPIX(
	    /*  INPUTS  */
	    int *n_resolution_atoi,
	    int *inorth,
	    int *ifirst_pixel,
	    int *want_no_warn,
	    int *want_nested,
	    double *phi,
	    double *theta,
	    /*  OUTPUTS  */
	    int *ipix,              /* input pixel number */
	    int *n_res2
	    );




#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
void test_global_projection(   
			    int want_verbose,
			    int want_no_warn
			    );



#define ANG2XY F77_FUNC(ang2xy,ANG2XY)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
void ANG2XY(
	    int *want_no_warn,
	    double  *phi,
	    double  *theta,
	    double  *x,
	    double  *y
	    );

#define XY2ANG F77_FUNC(xy2ang,XY2ANG)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
void XY2ANG(
	    int *want_no_warn,
	    double  *x,
	    double  *y,
	    double  *phi,
	    double  *theta
	    );


/* void test_C(
		  int pixel_2_angle,
		  int n_resolution,
		  int ordering_parallel,
		  int ipixel,
		  double theta,
		  double phi
		 );
*/


int plot_projection(int n_resolution_atoi);


/* global variables */

/* Option flags and variables.  These are initialized in parse_opt.  */

char *language;			/* --language */
int  nlen_language;
int want_C;     

int want_angle_to_pixel;        /* --angle-to-pixel */

char *n_resolution_string;      /* --n-resolution */
int  nlen_n_resolution_string; 
/*  int n_resolution;     string -> integer conversion not done in this routine */
int  n_resolution_atoi;   /* integer conversion used for plot_projection */
int  n_res2;        /* final resolution; may differ from requested value */

char *theta_north;			/* --theta-north */
int  nlen_theta_north;
int inorth;     

char *first_pixel_string;       /* --first-pixel */
int  nlen_first_pixel_string;
int ifirst_pixel;               

int want_serial;                /* --serial-ordering */
int want_verbose;               /* --verbose */
int want_no_warn;               /* --no-warn */
int want_nested;            /* not yet on command line */
int want_plot;                  /* --plot */
int want_global_projection;     /* --global-projection */
int want_wcslib;                /* --wcslib */

/* declarations for wcslib-4.1 or later - see wcs.h */

int test_wcslib(int n_resolution_atoi, int ipix_in,  /*  inputs */
			      int* ipix_out); /* output(s) */



