/* 
   isolat - test a library for an isolatitude, equi-solid-angle, hierarchical pixel-coordinate system

   Copyright (C) 2004 Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

*/

#include <stdio.h>
#include <sys/types.h>

#include <string.h>


#include "system.h"

#include "isolat.h"

#include "math.h"

void test_global_projection(   
			    int want_verbose,
			    int want_no_warn
			    )
{

  int latitude,longitude; /* these are int */
  int maxlines=6 ;  /* how many curves of constant lat/longitude per pi/2 ? */

  double theta,phi,xx,yy;
  double theta_inv,phi_inv;  /* inverse values of theta, phi */

  double test_tolerance= 1e-4;

  int  want_debug = 0; /* 1 ; */

  /*  printf("this compiles OK. Now please do something useful!\n"); */
  if(want_verbose)
    {
      printf("Will test full spherical projection,");
      printf("kindly provided by Mark Calabretta");
      printf(" ");
      printf("To plot the results, try e.g.   isolat -g |graph -T X -m0 -S ");

    }

  for (latitude=-maxlines; latitude < maxlines; latitude++)
	{
	  theta=((double)latitude +0.5)/((double)maxlines)* M_PI_2;
	  
	  for (longitude=0; longitude< 360; longitude++)
	    {
	      
	      phi=((double)longitude +0.5)/180.0 * M_PI;

	      /* so that output can be fed to graph, reverse the order
		 in every second line */
	      if( 2*(latitude/2)!=latitude ) phi= 2.0*M_PI -phi;

	      if(want_debug)
		{
		  printf("\n new try: phi = %.5f ,theta = %.5f\n",phi,theta);
		};


	      ANG2XY(&want_no_warn, &phi, &theta, &xx,&yy);
	      printf("%.4f %.4f   ",xx,yy);
	      
	      XY2ANG(&want_no_warn, &xx,&yy, &phi_inv, &theta_inv);
	      
	      if( (fabs(phi_inv-phi) > test_tolerance ||
		   fabs(theta_inv-theta) > test_tolerance)
		  && !want_no_warn )
		{
		  printf("\nWARNING: phi = %.5f ,phi_inv = %.5f ",phi,phi_inv);
		  printf("theta = %.5f, theta_inv =%.5f \n",theta,theta_inv);
		};
	    };
	}; /*   for (latitude=-maxlines; latitude < maxlines; latitude++) */


  for (longitude=0 ; longitude < 4*maxlines; longitude++)
	{
	  phi=((double)longitude +0.5)/((double)maxlines)* M_PI_2;
	  
	  for (latitude=-90; latitude< 90; latitude++)
	    {
	      
	      theta=((double)latitude +0.5)/180.0 *M_PI;

	      /* so that output can be fed to graph, reverse the order
		 in every second line */
	      if( 2*(longitude/2)!=longitude ) theta= -theta;

	      ANG2XY(&want_no_warn, &phi, &theta, &xx,&yy);
	      printf("%.4f %.4f   ",xx,yy);
	      
	      XY2ANG(&want_no_warn, &xx,&yy, &phi_inv, &theta_inv);
	      
	      if( (fabs(phi_inv-phi) > test_tolerance ||
		   fabs(theta_inv-theta) > test_tolerance)
		  && !want_no_warn )
		{
		  printf("WARNING: phi = %.5f ,phi_inv = %.5f",phi,phi_inv);
		  printf("theta = %.5f, theta_inv =%.5f \n",theta,theta_inv);
		};
	    };
	}; /*   for (longitude=0 ; longitude < 4*maxlines; longitude++) */

	      
}

