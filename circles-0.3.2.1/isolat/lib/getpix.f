C   isolat - test a library for an isolatitude, equi-solid-angle, hierarchical pixel-coordinate system
C
C   Copyright (C) 2004 Boud Roukema
C
C   This program is free software; you can redistribute it and/or modify
C   it under the terms of the GNU General Public License as published by
C   the Free Software Foundation; either version 2, or (at your option)
C   any later version.
C
C   This program is distributed in the hope that it will be useful,
C   but WITHOUT ANY WARRANTY; without even the implied warranty of
C   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C   GNU General Public License for more details.
C
C   You should have received a copy of the GNU General Public License
C   along with this program; if not, write to the Free Software Foundation,
C   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

c     -- converts angular coordinates on the sphere to a pixel number --

      subroutine getpix(n_res, inorth, ifpixel, j_warn, j_nest,
     _     phi_in, theta_in,    ! inputs
     _     ipix,n_res2) ! outputs

      implicit real*8 (a-h,o-z)

c     INPUTS:
      integer    n_res             ! number of sublevels of 12 initial pixels +1
      integer    inorth         ! multiple of pi/2 of North pole = -1,0,1 or 2 
      integer    ifpixel        ! first pixel number 
      integer    j_warn         ! turn off warnings?
      real*8     phi_in            ! longitude (0 to 2pi)  
      real*8     theta_in          ! latitude (-pi/2 to +pi/2) if(inorth.eq.1)
      

C     OUTPUTS:
      integer     ipix           ! pixel number
      integer     n_res2         ! value of n_res actually used

C     INTERNAL variables:
      parameter(pi=3.141592653589793d0, pi_180=pi/180.0d0, 
     _     pi_4=pi/4.0d0, pi_2= pi/2.0d0)

c     8192 is the maximum n_res value for integer*4 (signed 32-bit integers)
      parameter(nrmax=8192) 

      parameter(mxlevel=13) ! maximum number of levels = log(nrmax)/log(2.0)
      integer    iprime(mxlevel)  ! prime factor at each level
      integer    nlevel           ! number of levels


c     --- how many square unit intervals (regions) ----
      parameter(nsquare=12)

c     --- division between equatorial and polar regions ---
      real*8     thstar         ! asin(2.0/3.0) = "theta star"
      real*8     sthstar         ! sin(asin(2.0/3.0)) = 2.0/3.0

      integer*4  insquare,itwo,itwo2  ! for binary operations

      logical    north          ! are we in the northern hemisphere?

      logical    q_warn         ! turn off warnings?
      integer*4  iwarn1
      logical    test,test2

      data  iwarn1/0/
      save  iwarn1

      data test /.false./ !.true./ !
      save test
      data test2 /.false./ !.true./ !
      save test2


c      j_nest = 0 !! test only !!

      if(test2)then
         print*,'getpix: phi_in, theta_in, ipix (before)=',
     _        phi_in, theta_in, ipix 
         print*,'getpix: n_res2=',n_res2
      endif
      q_warn=.false.
      if(j_warn.eq.1)q_warn=.true.

c     ==== check that inputs are reasonable ====
      if(n_res.lt.1 .or. n_res.gt.nrmax)then
         print*,'getpix: n_res=',n_res,' is out of range '
         stop
      endif
      
c     --- number of levels in hierarchy ---
c      nlevel=nint(log(dble(n_res))/log(2.0d0)) +1

      call getprimes(n_res,j_warn, iprime,nlevel)
      n_res2=1
      do i=1,nlevel
         n_res2=n_res2*iprime(i)
      enddo
      if(n_res2.eq.0)print*,'getpix ERROR: n_res2 = ',n_res2

      nlevel= nlevel +1  ! add one level for number of unit intervals
      if(test)print*,'getpix: nlevel=',nlevel


c     --- total number of pixels ---
      n_ressq=  n_res2*n_res2
      npix=nsquare *n_ressq

      if(inorth.eq.1)then
         theta= theta_in
      elseif(inorth.eq.-1)then
         theta= - theta_in
      elseif(inorth.eq.0)then
         theta= pi_2 - theta_in
      else                      ! if(inorth.eq.2)
         theta = theta_in - pi_2
      endif

      tol= 1d-2
      phmin = -tol
      phmax = pi*2.0d0 + tol
      thmax = pi/2.0d0 + tol
      if((.not.(q_warn)).and.(iwarn1.lt.3).and.
     _     (theta.lt.-thmax.or.
     _     theta.gt.thmax.or.
     _     phi_in.lt.phmin.or.
     _     phi_in.gt.phmax))then
         print*,'getpix: WARNING phi_in,theta_in,theta=',
     _           phi_in,theta_in,theta
         print*,'Are you using degrees instead of radians?'
         print*,'Is theta in the range (-pi/2, +pi/2)?'
         print*,'getpix requires phi,theta in real*8 radians.'
         iwarn1=iwarn1+1
         
         phi= mod(phi_in, 2.0d0*pi)
      else
         phi= phi_in
      endif


      thstar= asin(2.0d0/3.0d0)
      sthstar= 2.0d0/3.0d0

c     ==== find number of region ====
      sinth= sin(theta)

c     -- Is this in an equatorial base pixel?

c     --- See Fig.~2 of astro-ph/0409533-v1
c     The square unit intervals are shown as squares where the
c     distance in horizontal direction from a square centre to its vertex
c     is pi/4, or in units of pi, this distance is 0.25.
c     Vertical directions can be put on the same scale by
c     dividing   sinth   by  (sthstar/0.25) i.e. by ((2/3)/0.25)

c     Now write two parameters yplus, yminus, so that the equator 
c     lies along the line   yplus=yminus.

      yplus= phi/pi + sinth/(sthstar/0.25d0)  
      yminus= phi/pi - sinth/(sthstar/0.25d0)

c     Now discretise to the sizes of the square unit intervals.
c     There's a 0.25 offset because square 5 (in Fig. 2) starts from phi/pi = -0.25
      iyplus= int(2.0d0*(yplus +0.25d0))+1
      iyminus= int(2.0d0*(yminus +0.25d0))+1

      if(test)print*,'yplus,yminus,iyplus,iyminus=',
     _     yplus,yminus,iyplus,iyminus


      if(iyplus.eq.iyminus)then ! if equatorial base pixel
         ir= 4+iyplus

c     --- the phi < 0 half of region 5 of 
c     Fig.~(2) of astro-ph/0409533-v1 is a special case :
c     convert from  phi > 7pi/8 to  -pi/8 < phi < 0 
        if(iyplus.eq.5)ir=5  ! equivalent to   ir=mod(4+iyplus-1,4)+1

      else
         iphi= int(phi/(pi/2.0d0))+1
         if(theta.gt.0.0d0)then
            ir=iphi  ! north
            north=.true.
         else
            ir=8+iphi ! south
            north=.false.
         endif
      endif

      if(test)print*,'found ir=',ir


c     === find fractional position relative to corners (1 and 2), (3 and 4)
      if(iyplus.eq.iyminus)then ! if equatorial base pixel

c     --- See Eq.~(19) of astro-ph/0409533-v1, which is for (wlog) eq. region 5
         pp= 4.0* (phi/pi -   0.5d0*dble(ir-5))

        
c     --- the phi < 0 half of region 5 of 
c     Fig.~(2) of astro-ph/0409533-v1 is a special case :
c     convert from  phi > 7pi/8 to  -pi/8 < phi < 0 
         if(iyplus.eq.5)pp= pp - 8.0d0  ! need to multiply by 4 as a few lines above

         s_sth= sinth/sthstar +1.0d0
         frac_i= (pp + s_sth) *0.5d0
         frac_j= (-pp + s_sth) *0.5d0

         if(test)print*,'getpix: frac_i,frac_j=',frac_i,frac_j

      else ! if polar
         if(sinth.gt.0.0)then
            north=.true.
         else
            north=.false.
         endif

         if(north)then
            pp= phi/pi -0.5d0*dble(ir-1)
         else
            pp= phi/pi -0.5d0*dble(ir-9)
         endif

c     --- the polar conversion is not as simple as for the equatorial region,
c     so it is done in a separate routine, fr2pol ----

         call pol2fr(north, q_warn, pp,sinth, frac_i,frac_j)

c     ---

      endif


c     ==== convert these fractions to the odd and even binary elements
c     of  insquare - the value within the "S^2" quadrilateral = base pixel ib
c     ====


      if(nlevel.eq.1)then
         insquare=0
      elseif(nlevel.gt.1)then
         itwo=1                 ! powers of "two" in two dimensions
         itwo1=1                 ! powers of "two" in one dimension

c     -- convert frac_i, frac_j to integers 
c         ifrac= int( frac_i* dble((2**(nlevel-1))) )
c         ifrac= max(0,min(2**(nlevel-1),ifrac))
c         jfrac= int( frac_j* dble((2**(nlevel-1))) )
c         jfrac= max(0,min(2**(nlevel-1),jfrac))

         ifrac= int( frac_i* dble(n_res2) )
         ifrac= max(0,min(n_res2,ifrac))
         jfrac= int( frac_j* dble(n_res2) )
         jfrac= max(0,min(n_res2,jfrac))

         if(test)print*,'ifrac,jfrac=',ifrac,jfrac

c     --- NESTED ordering ---
         if(j_nest.eq.1)then    ! 0.1.18

            insquare=0
            do kk=1,nlevel-1    ! 
               kprime= iprime(kk) 
               itwo12=iprime(kk)*itwo1
               itwo2=iprime(kk)*itwo
c     itwo4=4*itwo
               
               if(kprime.gt.2 .or. test)then
                  jremainder=  ifrac/itwo1 - (ifrac/itwo12)*kprime
                  insquare=insquare+ jremainder*itwo
               else             ! retain the faster version for binary level
                  if((ifrac/itwo12)*2.eq.(ifrac/itwo1)) then ! frac in dirn frac_i
                     continue
                  else
                     insquare=insquare+ itwo
                  endif
               endif

               if(kprime.gt.2 .or. test)then
                  jremainder=  jfrac/itwo1 - (jfrac/itwo12)*kprime
                  insquare=insquare+ jremainder*itwo2
               else             ! retain the faster version for binary level
                  if((jfrac/itwo12)*2.eq.(jfrac/itwo1))then ! frac in dirn frac_j
                     continue
                  else
                     insquare=insquare+ itwo2
                  endif
               endif
               
               if(test)print*,'insquare,itwo,itwo2=',
     _              insquare,itwo,itwo2

               itwo=itwo* kprime*kprime
               itwo1=itwo1 *kprime
            enddo
            if(test)print*,'ifrac,jfrac=',ifrac,jfrac

         endif  !  if(j_nest.eq.1)then    ! 0.1.18

      endif !      if(nlevel.ge.1)then


c     --- final result 
c      ipix= (ir-1)* (4**(nlevel-1)) + insquare  
      ipix= (ir-1)* n_ressq + insquare  
     _      + ifpixel  ! convention for first pixel value  0 or 1 
      
      if(j_nest.ne.1)then       ! 0.1.18

c     --- 0.1.18 ---
c     -- see Figs 2, 3 of Roukema & Lew astro-ph/0409533 --
         if(nlevel.gt.1)then

            if(2**(nlevel-1).ne.n_res2)then
               print*,'ERROR: n_res2 = ',n_res2, 
     _              ' does not seem to be a power of 2.'
               stop
            endif

            delyy= 0.2/dble(n_res2) ! one pixel (vertical/horizontal)
            if(ir.le.4)then
               xx = dble(ir)*0.2
               yy = 0.5 + delyy *0.5
            elseif(ir.le.8)then
               xx = 0.1 + dble(ir-5)*0.2
               yy = 0.4 + delyy *0.5
            else
               xx = dble(ir-8)*0.2
               yy = 0.3 + delyy *0.5
            endif

            xx= xx + dble((ifrac-jfrac))*0.5*delyy
c     _     *dble(n_res2-1)/dble(n_res2)
            yy= yy + dble((ifrac+jfrac))*0.5 *delyy
c     _     /dble(n_res2)
c     _     *dble(n_res2-1)/dble(n_res2)

            ipix= int(xx*1000+yy*10) !! test only !!
            ipix= int(xx*1000)
            ipix= int(yy*1000)

            tol_yy= 1d-2

c     ipix= int((0.7-yy)*1000)
c     ipix= int(((0.7 -yy)/delyy *2-1) *1000 +tol_yy)  

C     Y value
c     ipix= int((0.7 -yy)/delyy *2 *4*n_res2 + tol_yy)
c     ipix= int( (0.7 -yy + tol_yy )/delyy) *2 *4*n_res2  ! wrong


C     X value
c     ipix= mod( int( xx/delyy +tol_yy ) -1 +4*n_res2, 4*n_res2) +1

C     TODO: endpoints



C     ipix= ifrac + n_res2*jfrac

c     ipix= int( (0.7-yy)/delyy * (dble(5*n_res2+1)))! + xx/delyy )

            ii= int(((0.7 -yy)/delyy *2-1) + tol_yy)
            
c            if(yy.gt.0.6-tol_yy)then
            mid_done = 0
            if(yy.gt.0.6)then
               ipix= ((ii)*(ii+1))/2 * 4

c     delxx= dble(max(ii+1,1))/dble(n_res2) *delyy

c            elseif(yy.gt.0.4-tol_yy)then
            elseif(yy.ge.0.4)then
               ipix= ( ((n_res2)*(n_res2+1))/2  +
     _              (ii-n_res2)* n_res2 )  *4

               ipix=ipix+ mod( int( xx/delyy +tol_yy ) 
     _              - n_res/2 +4*n_res2, 4*n_res2) 
               mid_done = 1

            else                ! yy.lt.0.4-tol_yy
               ii= int(((yy-0.3)/delyy *2-1) + tol_yy)
               ipix= npix - ( ((ii+1)*(ii+2))/2 * 4 )
            endif
            

            
c            if((yy.gt.0.6-tol_yy) .or.
c     _           (yy.lt.0.4-tol_yy))then
            if(mid_done.eq.0)then  ! if the middle part has not been done
               if(xx.lt.0.3)then
                  x0=0.2
                  ipix=ipix+ int( (xx-x0)/delyy +tol_yy  + (ii+1)/2 )
               elseif(xx.lt.0.5)then
                  x0=0.4
                  ipix=ipix+ int( (xx-x0)/delyy +tol_yy  +(ii+1)/2 )+
     _                 ii+1
               elseif(xx.lt.0.7)then
                  x0=0.6
                  ipix=ipix+ int( (xx-x0)/delyy +tol_yy  +(ii+1)/2 )+
     _                 2*(ii+1)
               else
                  x0=0.8
                  ipix=ipix+ int( (xx-x0)/delyy +tol_yy  +(ii+1)/2 )+
     _                 3*(ii+1)
               endif
            endif

            ipix= ipix 
     _           + ifpixel      ! convention for first pixel value  0 or 1 

         endif                  !      if(nlevel.gt.1)then

      endif ! if(j_nest.ne.1)then       ! 0.1.18

      return
      end

