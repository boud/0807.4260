C   isolat - test a library for an isolatitude, equi-solid-angle, hierarchical pixel-coordinate system
C
C   Copyright (C) 2004 Boud Roukema
C
C   This program is free software; you can redistribute it and/or modify
C   it under the terms of the GNU General Public License as published by
C   the Free Software Foundation; either version 2, or (at your option)
C   any later version.
C
C   This program is distributed in the hope that it will be useful,
C   but WITHOUT ANY WARRANTY; without even the implied warranty of
C   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C   GNU General Public License for more details.
C
C   You should have received a copy of the GNU General Public License
C   along with this program; if not, write to the Free Software Foundation,
C   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

c     -- converts pixel number to a angular coordinates on the sphere --
      subroutine getang(n_res, inorth, ifpixel, j_warn, ipix_in,  ! inputs
     _     phi,theta,n_res2)           ! outputs

      implicit real*8 (a-h,o-z)

c     INPUTS:
      integer    n_res          ! number of pixels per sq. unit interval side
      integer    inorth         ! multiple of pi/2 of North pole = -1,0,1 or 2 
      integer    ifpixel  ! first pixel number 
      integer    ipix_in           ! pixel number input
      integer    j_warn           ! turn off warnings?

C     OUTPUTS:
      real*8     phi            ! longitude (0 to 2pi)
      real*8     theta          ! latitude (-pi/2 to +pi/2)
      integer     n_res2         ! value of n_res actually used


C     INTERNAL variables:
      integer    ipix           ! internal value of ipix
      logical*4    q_warn         ! turn off warnings?


      parameter(pi=3.141592653589793d0, pi_180=pi/180.0d0, 
     _     pi_4=pi/4.0d0, pi_2= pi/2.0d0)

c     --- tolerance for inverse sin calculation ---
      parameter(toldef=1e-8) ! double precision


c     8192 is the maximum value of n_res for integer*4 (signed 32-bit integers) 
      parameter(nrmax=8192) 

      parameter(mxlevel=13) ! maximum number of levels = log(nrmax)/log(2.0)
      integer    iprime(mxlevel)  ! prime factor at each level
      integer    nlevel           ! number of levels


c     --- how many square unit intervals (regions) ----
      parameter(nsquare=12)

c     --- division between equatorial and polar regions ---
      real*8     thstar         ! asin(2.0/3.0) = "theta star"
      real*8     sthstar         ! sin(asin(2.0/3.0)) = 2.0/3.0
      
      integer*4  insquare,itwo,itwo2  ! for binary operations

      logical    north          ! are we in the northern hemisphere?

      logical    test

      data test /.false./ !.true./ !
      save test



      if(test)print*,'begin getang:',
     _     'n_res, inorth, ifpixel, j_warn, ipix_in, phi, theta=',
     _     n_res, inorth, ifpixel, j_warn, ipix_in, phi, theta


c     --- logical variables are nice internally ---
      q_warn=.false.
      if(j_warn.eq.1)q_warn=.true.


      ipix= ipix_in - ifpixel

c     ---- border between equatorial and polar regions ---
      thstar= asin(2.0/3.0)
      sthstar= 2.0/3.0



c     ==== check that inputs are reasonable ====
c     --- resolution ---
      if(n_res.lt.1 .or. n_res.gt.nrmax)then
         print*,'ERROR: getang: n_res=',n_res,' is out of range '
         stop
      endif

c     --- number of levels in hierarchy ---
c      nlevel=nint(log(dble(n_res))/log(2.0d0)) +1

      call getprimes(n_res,j_warn, iprime,nlevel)
      n_res2=1
      do i=1,nlevel
         n_res2=n_res2*iprime(i)
      enddo

      nlevel= nlevel +1  ! add one level for number of unit intervals
      if(test)print*,'getang: nlevel=',nlevel

c     --- total number of pixels ---
      npix=nsquare *n_res2*n_res2
      n_ressq= n_res2*n_res2
      if(ipix.lt.0 .or. ipix.gt.npix-1)then
         print*,'ERROR: getang: ipix out of range. ipix,npix= ',
     _        ipix,npix
         stop
      endif
     


c     ==== find region number ====
      ir= ipix/n_ressq +1

c     ==== find fractional interpolation between 1st and 2nd corner,
c     and between 1st and 3rd corner  ====
      
      insquare= mod(ipix, n_ressq)
      if(test)print*,'ir,insquare=',ir,insquare

c     ---- double check (if above lines are correctly coded, and the
c     compiler and processor do things correctly, then there should not
c     be any error) ----
      ii= n_ressq * (ir-1) + insquare
      if(ii.ne.ipix)then
         print*,'ERROR: getang: ii.ne.ipix, ii, ipix=',ii,ipix
         stop
      endif

      if(nlevel.eq.1)then
         frac_i=0.5  ! pixel centre
         frac_j=0.5 ! pixel centre
      elseif(nlevel.gt.1)then
c     -- pixel centre --
         frac_i=0.5             ! direction frac_i
         frac_j=0.5             ! direction frac_j

c     --- the label "two" really means any prime ---
         itwo=1                 ! powers of "two" in two dimensions
         itwo1=1                 ! powers of "two" in one dimension

         do kk=1,nlevel-1  
c            itwo2=2*itwo
c            itwo4=4*itwo
            kprime= iprime(kk) 
            itwo2= kprime *itwo
            itwo4= kprime*kprime *itwo 
            
            if(test.and.ir.eq.nsquare)then
               print*,'test C1 kk,frac_i,frac_j=',kk,frac_i,frac_j
            endif

            if(kprime.gt.2 .or. test)then
               jremainder=  insquare/itwo - (insquare/itwo2)*kprime
               frac_i = frac_i + dble(jremainder * itwo1)
            else ! retain the faster version for binary division
               if((insquare/itwo2)*kprime.eq.(insquare/itwo)) then ! in dirn frac_i
                  continue
               else
                  frac_i= frac_i+ dble(itwo1)
               endif
            endif


            if(kprime.gt.2 .or. test)then
               jremainder=  insquare/itwo2 - (insquare/itwo4)*kprime
               frac_j = frac_j + dble(jremainder * itwo1)
            else ! retain the faster version for binary division
               if((insquare/itwo4)*kprime.eq.(insquare/itwo2))then ! in dirn frac_j
                  continue
               else
                  frac_j= frac_j+ dble(itwo1)
               endif
            endif
            
            if(test)print*,'insquare,itwo,itwo2=',insquare,itwo,itwo2

            if(test.and.ir.eq.nsquare)then
               print*,'test C2 kk,frac_i,frac_j=',kk,frac_i,frac_j
            endif
            
            itwo=itwo* kprime*kprime
            itwo1=itwo1 *kprime
         enddo
         if(test)print*,'frac_i,frac_j=',frac_i,frac_j

         frac_i= frac_i/dble(itwo1) ! make it a fraction in (0;1)
         frac_j= frac_j/dble(itwo1) ! make it a fraction in (0;1)
         if(test)print*,'normalised: frac_i,frac_j=',frac_i,frac_j
      endif !      if(nlevel.ge.1)then


C     !! WARNING: 5,8 hard-wired for nsquare=12 case !!
      if(5.le.ir.and.ir.le.8)then ! equatorial case
c     ---- 
         if(test)print*,'eq algorithm test'

c     --- See Eq.~(19) of astro-ph/0409533-v1, which is for (wlog) eq. region 5
c     
c     phi_pi = (f_i-f_j)/4
c     s = 2/3 ((f_i+f_j) -1) 
c     

         pp=  0.5*dble(ir-5) + 0.25*(frac_i - frac_j)
         ss= sthstar*  (frac_i + frac_j -1.0d0)  

         if(test)print*,'pp,ss=',pp,ss
         
c     --- standardize phi domain ---
c         phi= mod(pp*pi ,2.0d0*pi)  

         phi = pp*pi
c         if(phi.lt.0.0)phi = phi + 2.0d0*pi
         if(phi.lt.toldef)phi = phi + 2.0d0*pi



c     --- invert  sth  to angle theta ---
         sth=max(-1.0+toldef,(min(1.0-toldef,ss)))

         if(inorth.eq.1)then
            theta= asin(sth)
         elseif(inorth.eq.-1)then
            theta= - asin(sth)
         elseif(inorth.eq.0)then
            theta= pi_2 - asin(sth) 
         else ! if(inorth.eq.2)
            theta = pi_2 + asin(sth)
         endif

         return
      else ! polar case
C     !! WARNING: le.4  hard-wired for nsquare=12 case !!
         if(ir.le.4)then
            north=.true.
         else
            north=.false.
         endif

c     --- the polar conversion is not as simple as for the equatorial region,
c     so it is done in a separate routine, fr2pol ----

         call fr2pol(north, q_warn, frac_i,frac_j, phipi,sth)

c     ---

         if(north)then
            phi= (0.5*dble(ir-1)+phipi)*pi
         else
            phi= (0.5*dble(ir-9)+phipi)*pi
         endif
         
c     --- alternative conventions for theta ---
         if(inorth.eq.1)then
            theta= asin(sth)
         elseif(inorth.eq.-1)then
            theta= - asin(sth)
         elseif(inorth.eq.0)then
            theta= pi_2 - asin(sth) 
         else ! if(inorth.eq.2)
            theta = pi_2 + asin(sth)
         endif

         if(test)print*,'getang test: phi,theta=',phi,theta

         return
      endif

      return
      end


