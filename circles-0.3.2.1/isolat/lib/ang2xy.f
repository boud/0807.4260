C   isolat - test a library for an isolatitude, equi-solid-angle, hierarchical pixel-coordinate system
C
C   Copyright (C) 2004 Boud Roukema
C
C   This program is free software; you can redistribute it and/or modify
C   it under the terms of the GNU General Public License as published by
C   the Free Software Foundation; either version 2, or (at your option)
C   any later version.
C
C   This program is distributed in the hope that it will be useful,
C   but WITHOUT ANY WARRANTY; without even the implied warranty of
C   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C   GNU General Public License for more details.
C
C   You should have received a copy of the GNU General Public License
C   along with this program; if not, write to the Free Software Foundation,
C   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  


      subroutine ang2xy(j_warn,phi,theta,x,y)
c     ==== function which tests Mark Calabretta's algebra of the 
c     full spherical projection which is part of this pixel-coordinate system
c     ====

      implicit real*8 (a-h,o-z)

C     INPUTS:
      integer   j_warn ! turn off warnings?
      real*8    phi,theta

C     OUTPUTS:
      real*8    x,y

c      INTERNAL:
      logical   test

      parameter(pi=3.14159365359)
      parameter(tol=1d-6)

      data test /.false./!.true. /! 

      sth=sin(theta)

      phi_q= mod(phi+ pi, pi/2.0d0)  ! add pi to avoid problem with negatives
      
      if(abs(sth).lt.2.0d0/3.0d0)then
         x = phi
         y = 3.0d0*pi/8.0d0 * sth
      else  ! if(sth.gt.2.0d0/3.0d0)then
         ytmp=  3.0*(1.0 - abs(sth))
         if(test)print*,'ytmp step 1 = ',ytmp             

         ytmp=max(tol,ytmp)     ! sqrt will fail if ytmp < 0.0
         if(test)print*,'ytmp step 2 = ',ytmp            

         y= pi/4.0d0 *
     _        ( 2.0d0 - sqrt( ytmp ))
         if(test)print*,'y step 3 = ',y
         

         x= (phi - phi_q + pi/4.0d0) 
     _        + (pi/2.0d0 - abs(y)) * 
     _        (4.0d0 * phi_q /pi -1.0d0)

         if(sth.lt.-2.0d0/3.0d0) y = -y
      endif
      return
      end


      subroutine xy2ang(j_warn,x,y,phi,theta)
c     ==== function which tests Mark Calabretta's algebra of the 
c     full spherical projection which is part of this pixel-coordinate system
c     ====

      implicit real*8 (a-h,o-z)

C     INPUTS:
      integer   j_warn ! turn off warnings?
      real*8    x,y

C     OUTPUTS:
      real*8    phi,theta

c      INTERNAL:
      logical   test
      logical   found ! found a solution?

      parameter(pi=3.14159365359)
      parameter(tol=1d-6)

      data test /.false./!.true. /! 


      if(abs(y).lt.pi*0.25d0)then
         phi= x
         sth= y *8.0d0/(3.0d0*pi)
         theta=asin(sth)
      else

         ytmp =  2.0d0 - abs( 4.0d0 * y /pi )
         
         th_tmp= 1.0d0 -   ytmp*ytmp /3.0d0
         
         th_tmp= max(-1.0+tol,min(1.0-tol, th_tmp))
         
         if(y.ge.0.0d0)then
            theta= asin(th_tmp)
         else
            theta= -asin(th_tmp)
         endif
         
         ytmp = max(tol,ytmp)   ! avoid zero values 
         
         found=.false.
         phi=-99.9
         do m=-1,3
            phi_q= ( x - abs(y) + pi/4.0d0 * (1.0d0-2.0d0 * real(m)))
     _           / ytmp
            if(0.0d0.le.phi_q .and. phi_q .lt. pi/2.0d0)then
               phinew= real(m)*pi/2.0d0 + phi_q
               if(found.and.(.not.(j_warn.eq.1)))then
                  print*,'xy2ang WARNING: more than 1 solution found.'
                  print*,'previous solution: m=',m_old,' phi=',phi
                  print*,'new solution: m=',m,' phinew=',phinew
               endif
               found=.true.
               m_old=m
               phi_old=phi
               phi= phinew
            endif
         enddo      
      endif

      return
      end


