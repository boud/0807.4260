C   isolat - test a library for an isolatitude, equi-solid-angle, hierarchical pixel-coordinate system
C
C   Copyright (C) 2004 Boud Roukema
C
C   This program is free software; you can redistribute it and/or modify
C   it under the terms of the GNU General Public License as published by
C   the Free Software Foundation; either version 2, or (at your option)
C   any later version.
C
C   This program is distributed in the hope that it will be useful,
C   but WITHOUT ANY WARRANTY; without even the implied warranty of
C   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C   GNU General Public License for more details.
C
C   You should have received a copy of the GNU General Public License
C   along with this program; if not, write to the Free Software Foundation,
C   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

c     -- obtains prime numbers for each level of pixel subdivision --
c     
c     For a given input requested resolution of  n_res,
c     a list of non-decreasing prime numbers
c     
c     iprime(1), iprime(2), ... , iprime(nlevel) 
c
c     is calculated so that
c
c     \Pi_{i=1}^{i=nlevel} iprime(i) 
c
c     either is equal to n_res (if n_res has no prime factors
c     bigger than prime(mxprime)) or else approximately equal to
c     n_res.  Testing for failure to full prime factorise is done
c     externally to this routine.

      subroutine getprimes(n_res, j_warn, ! input
     _     iprime,nlevel) ! outputs
C     INPUTS:
      integer    n_res             ! number of sublevels of 12 initial pixels +1
      integer    j_warn           ! turn off warnings?
      
C     OUTPUTS:
      integer    iprime(*)  ! prime factor at each level
      integer    nlevel     

C     INTERNAL:
      parameter(mxprime=10)
      integer    prime(mxprime)
      
      logical   test

      data  prime /2,3,5,7,11, 13,17,19,23,29/

      data  test /.false./ ! /.true./ !


      ip=1  ! which prime is being tested
      ilevel=0

      mprime = n_res ! integer which will be successively divided

c     --- there is no point guessing n_res value if it is zero or negative ---
      if(n_res.lt.0)then
         print*,'n_res = ',n_res,' is invalid.'
         stop
      endif

c     ---- the goal is to divide mprime down to a value of 1, but
c     if there is a factor which is a big prime, we also have to stop ----

      do while(mprime.gt.1 .and. ip.le.mxprime)
         if(test)then
            print*,'mprime,ip,ilevel=',mprime,ip,ilevel
         endif

         if( prime(ip)* (mprime/prime(ip)).eq.mprime)then ! if we find a factor
            ilevel = ilevel+1   !  we've found a new level
            iprime(ilevel)= prime(ip)
            mprime= mprime/prime(ip)
            if(test)print*,'Found: prime(ip),ip',prime(ip),ip
         else
            ip=ip+1
         endif
      enddo
      if(test)then
         print*,'After loop: mprime,ip,ilevel=',mprime,ip,ilevel
      endif

      nlevel=ilevel

c     --- if prime factorisation is unsuccessful ---
      if(mprime.gt.1)then
         if(.not.(j_warn.eq.1))then
            print*,'getprimes WARNING:',
     _        ' large prime product remaining = ',mprime
c     print*,'j_warn = ',j_warn
         endif
         
c     --- find the closest power of two and use this for additional levels ---
         jlevel=nint( log(dble(mprime))/log(2.0d0) )
         
         do ilevel= nlevel+jlevel,jlevel+1, -1
            iprime(ilevel)= iprime(ilevel-jlevel) ! shift to higher ilevel 
         enddo
         
         do ilevel=1,jlevel
            iprime(ilevel)= 2   ! add extra binary levels
         enddo
         
         nlevel=nlevel + jlevel ! update nlevel

         if(.not.(j_warn.eq.1))then
            print*,'Have added jlevel=',jlevel,' binary levels'
            print*,'getprimes: final nlevel in getprimes =',nlevel
            print*,'(One more level will probably be added',
     _           ' externally to getprimes.)'
         endif
      endif

      return
      end
