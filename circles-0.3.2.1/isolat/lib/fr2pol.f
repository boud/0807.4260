C   isolat - test a library for an isolatitude, equi-solid-angle, hierarchical pixel-coordinate system
C
C   Copyright (C) 2004 Boud Roukema
C
C   This program is free software; you can redistribute it and/or modify
C   it under the terms of the GNU General Public License as published by
C   the Free Software Foundation; either version 2, or (at your option)
C   any later version.
C
C   This program is distributed in the hope that it will be useful,
C   but WITHOUT ANY WARRANTY; without even the implied warranty of
C   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C   GNU General Public License for more details.
C
C   You should have received a copy of the GNU General Public License
C   along with this program; if not, write to the Free Software Foundation,
C   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  


      subroutine fr2pol(north,q_warn,fi_in,fj_in,  phipi,sth)
c     ==== function which maps
c     from "fractional" point in (0,1)x(0,1) to a polar mapping
c     ====

      implicit real*8 (a-h,o-z)

C     INPUTS:
      logical   q_warn ! turn off warnings?
      logical   north ! is this in the "northern hemisphere"? Y/N
      real*8  fi_in,fj_in

C     OUTPUTS:
      real*8  phipi,sth

c      INTERNAL:
      logical   test

      data test /.false./!.true. /! 

      if(north)then
         fi=fi_in
         fj=fj_in
      else                      ! south: want fi,fj increasing towards pole 
         fi=1.0d0-fi_in
         fj=1.0d0-fj_in
      endif

      toldef=1d-8

c     --- See Eq.~(16) of astro-ph/0409533-v1
      xx= fi-fj
      yy= fi+fj
      if(yy.lt.1.0d0-toldef)then
c     ---- linear part in equatorial region ----
c     --- See Eq.~(21) of astro-ph/0409533-v1
         sth= 2.0d0/3.0d0 *yy
         phipi= 0.25* (1.0d0+xx)
      else
c     ---- quadratic part in polar region ----

c     --- See Eq.~(34) of astro-ph/0409533-v1
         sth= 1.0d0 - 1.0d0/3.0d0 * (2.0d0-yy)*(2.0d0-yy)

         if(yy.gt.2.0d0-toldef)then
            if(.not.(q_warn))
     _           print*,'fr2pol WARNING: xx=',xx
            phipi= 0.25
         else

c     --- See Eq.~(28) of astro-ph/0409533-v1
            phipi= 0.25* (2.0d0 -yy +xx)/(2.0d0-yy)
         endif
      endif

      if(.not.(north))then
c     --- above equations are for north; in the south, this is the correction:
         phipi= 0.5d0-phipi
         sth= -sth
      endif

      return
      end


      subroutine pol2fr(north,q_warn, phipi_in,sth_in,  fi,fj) ! inverse of fr2pol
c     ==== function which maps
c     from a polar mapping
c     to a "fractional" point in (0,1)x(0,1)
c     ====

      implicit real*8 (a-h,o-z)

C     INPUTS:
      logical   north ! is this in the "northern hemisphere"? Y/N
      logical   q_warn ! turn off warnings?
      real*8  phipi_in,sth_in

C     OUTPUTS:
      real*8  fi,fj

c      INTERNAL:
      logical  test


      data test /.false./!.true. /! 

      toldef=1d-8

      sth= sth_in

      if(north)then
         phipi= phipi_in
      else
c     --- equations below are for north; in the south, this is the correction:
         phipi= 0.5d0-phipi_in
         sth=-sth
      endif

      twothirds= 2.0d0/3.0d0
c     ---- linear part - equatorial region ----
      if(sth.lt.twothirds-toldef)then

c     --- See Eq.~(21) of astro-ph/0409533-v1
         xx= 4.0d0*phipi -1.0d0
         yy= 1.5d0* sth

c     --- See Eq.~(16) of astro-ph/0409533-v1
         fi= 0.5d0*(xx+yy)
         fj= 0.5d0*(-xx+yy)
      else
c     --- polar region ---

c     --- See Eq.~(34) of astro-ph/0409533-v1
         two_ysq = 3.0d0*(1.0d0-sth)  ! two-ysq = (2-y)^2 
         if(two_ysq.lt.toldef)then
            if(.not.(q_warn))
     _           print*,'pol2fr WARNING: close to pole two_ysq=',
     _           two_ysq
            fi=1.0d0
            fj=1.0d0
         else
            two_y= sqrt(two_ysq)
            yy= 2.0d0- two_y

c     --- See Eq.~(28) of astro-ph/0409533-v1
            xx= (4.0d0*phipi -1.0d0)*two_y 

            fi=0.5d0*(yy + xx)
            fj=0.5d0*(yy - xx)
         endif
      endif

c     -- south: want fi,fj increasing towards pole --
      if(.not.(north))then
         fi=1.0d0-fi
         fj=1.0d0-fj
      endif


      return
      end
