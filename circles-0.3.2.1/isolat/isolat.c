/* 
   isolat - test a library for an isolatitude, equi-solid-angle, hierarchical pixel-coordinate system

   Copyright (C) 2004 Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

#include <termios.h>
#include <grp.h>
#include <pwd.h>
*/

#include <stdio.h>
#include <sys/types.h>

#include <string.h>


#include "system.h"

#ifdef HAVE_ARGP_H
#  include <argp.h>
#endif /* HAVE_ARGP_H */


#define EXIT_FAILURE 1

#if ENABLE_NLS
# include <libintl.h>
# define _(Text) gettext (Text)
#else
# define textdomain(Domain)
# define _(Text) Text
#endif
#define N_(Text) Text


#include "isolat.h"


char *xmalloc ();
char *xrealloc ();
char *xstrdup ();

#ifdef HAVE_ARGP_H
  static error_t parse_opt (int key, char *arg, struct argp_state *state);
  static void show_version (FILE *stream, struct argp_state *state);
#else
  static void show_version (FILE *stream);
#endif /* HAVE_ARGP_H */

/* argp option keys */
enum {DUMMY_KEY=129
      ,NOWARN_KEY
};


#ifdef HAVE_ARGP_H
static struct argp_option options[] =
{
  { "language",      'l',           N_("LANGUAGE_NAME"),
    0,
    N_("test functions in LANGUAGE_NAME library (e.g. fortran or C)"), 0 },
  { "angle-to-pixel",     'a',           NULL,            0,
    N_("convert Angle to pixel (rather than pixel to angle) (default off)"), 0 },
  { "n-resolution",     'n',           N_("N_RESOLUTION"),            0,
    N_("Number of pixels (resolution) per side of square interval"), 0 },
  { "theta-north",      't',           N_("THETA_NORTH"),
    0,
    N_("Theta convention for north pole in units of pi/2 (integer -1,0,1 or 2) "), 
    0 },
  { "first-pixel",      'f',           N_("FIRST_PIXEL"),
    0,
    N_("First pixel number (0 or 1)"), 
    0 },
  { "plot",     'p',    NULL,            0,
    N_("output Plot in plotutils 'meta' format;  e.g. echo '1' | isolat -p | plot -T X"), 0 },
  { "serial-ordering",     's',           NULL,            0,
    N_("Serial ordering of pixels (rather than parallel) (default off)"), 0 },
  { "global-projection",     'g',           NULL,            0,
    N_("test Global spherical projection"), 0 },
  { "verbose",     'v',           NULL,            0,
    N_("print more Verbal information"), 0 },
  { "no-warn",     'w',    NULL,            0,
    N_("disable Warnings"), 0 },
  { "ring",     'r',    NULL,            0,
    N_("Ring ordering (experimental: getpix && 2^n)"), 0 },
  /*  
      TODO: see TODO's below if you want to implement this
  */
  { "wcslib",     'W',    NULL,            0,
    N_("use wcslib instead of libisolat.a"), 0 },
  { NULL, 0, NULL, 0, NULL, 0 }
};

/* The argp functions examine these global variables.  */
const char *argp_program_bug_address = "<boud@astro.uni.torun.pl>";
void (*argp_program_version_hook) (FILE *, struct argp_state *) = show_version;

static struct argp argp =
{
  options, parse_opt, N_(" "),
  N_("test a library for an isolatitude, equi-solid-angle, hierarchical pixel-coordinate system"),
  NULL, NULL, NULL
};
#endif /* HAVE_ARGP_H */

int
main (int argc, char **argv)
{
  /* local variables */
  int  ipix_in,ipix_out;   /* local use in this routine */
  int  npixel ; 
  double phi,theta;

  textdomain(PACKAGE);

  /* Set up default values.  */
  language = "fortran";

#ifdef HAVE_ARGP_H
  want_C = 0;
#else
  want_C = 1;   /* without command line parameters, at least run this test */
#endif /* HAVE_ARGP_H */

  want_angle_to_pixel = 0;
  n_resolution_string = "";  /* f77 default set in testfort.f */
  n_resolution_atoi = 4;     /* C default set here */
  theta_north = "1";     /* "1" means -pi/2 at South Pole and pi/2 at North Pole */
  inorth = 1;
  first_pixel_string="1";  /* pixels start at 1 */
  ifirst_pixel = 1;
  want_serial = 0;
  want_global_projection = 0;

#ifdef HAVE_ARGP_H
  want_verbose = 0;
#else
  want_verbose = 1;
#endif /* HAVE_ARGP_H */

  want_no_warn = 0;
  want_nested = 1;
  want_plot = 0;
  want_wcslib = 0;
  

#if HAVE_ARGP_H
  argp_parse(&argp, argc, argv, 0, NULL, NULL);
#else
  printf("Warning: no command line options since a recent glibc is not installed.\n");
#endif /* HAVE_ARGP_H */


  
  /* DONE: do the work */

  /* redundant - probably */
  /*  nlen_n_resolution_string= strlen(n_resolution_string);  */

  if(!want_global_projection)
    {
      if( want_C  )
	{
	  /* (1) test that getpix is the inverse of getang for given resolution */

	  /* WARNING: hardwired - should be shifted to, e.g. a function */
	  npixel = 12 * n_resolution_atoi *  n_resolution_atoi ; 

	  if ( want_verbose )
	    { 
	      printf("Will test that getpix and getang invert correctly.\n");
	      printf("Will run through pixels from 1 to npixel = %d \n",npixel);
	      printf("Errors will be reported: no news is good news.\n");
	      if( want_no_warn != 1 )
		printf("Warnings will be reported.\n");
	    };

	  for ( ipix_in =ifirst_pixel; 
		ipix_in <= npixel - (1-ifirst_pixel)
		  ; ipix_in++)
	    { 

	      if(!want_wcslib)   /* if not wcslib, then use isolat fortran lib */
		{
		  GETANG( &n_resolution_atoi,
			  &inorth,
			  &ifirst_pixel,
			  &want_no_warn,
			  &ipix_in,
			  &phi,
			  &theta,
			  &n_res2
			  );
		  
		  GETPIX( &n_resolution_atoi,
			  &inorth,
			  &ifirst_pixel,
			  &want_no_warn,
			  &want_nested,
			  &phi,
			  &theta, 
			  &ipix_out,
			  &n_res2
			  );
		}
	      else	      /* (5) test wcslib-4.1 or later */
		{
#ifdef HAVE_LIBWCS
		  test_wcslib(n_resolution_atoi, ipix_in,  /*  inputs */
			      &ipix_out); /* output(s) */
#endif
		};

	  
	      if ( ipix_out != ipix_in )
		{
		  fprintf(stderr,
			  "ERROR isolat: ipix_in = %d != ipix_out = %d \n",
			  ipix_in,ipix_out); 
		};
	    }    /*   for ( ipix_in =1; ipix_in <= npixel; ipix_in++)   */
	}        /*   ( want_C ) */
      else
	/* (2) test routines  getang( ) and getpix( ) depending on flags */
	TESTF(
	      &want_angle_to_pixel,
	      n_resolution_string, 
	      &nlen_n_resolution_string, 
	      &inorth,
	      &ifirst_pixel,
	      &want_serial,
	      &want_verbose,
	      &want_no_warn,
	      &want_nested,
	      &want_plot
	      );

    };  /*   if(!want_global_projection) */

  /* (3) make a test plot */
  if(want_plot)
    {
      /*
	printf("want_plot= %d",want_plot);
	printf("isolat:  n_res... = %d",n_resolution_atoi);
      */
      plot_projection(n_resolution_atoi);
    }

  /* (4) test global spherical projection, which is not used by other routines */
  if(want_global_projection)
    {
      test_global_projection( want_verbose, want_no_warn );
    };


  exit (0);
}

#ifdef HAVE_ARGP_H
/* Parse a single option.  */
static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  switch (key)
    {
    case ARGP_KEY_INIT:
      /* defaults must be set whether or not HAVE_ARGP_H is true */
      break;
    case 'l':				/* --language */
      language = xstrdup (arg);
      nlen_language = strlen(language);
      if ( nlen_language > 0 ) 
	{
	  if( (strcmp(language,"C") == 0 ) || (strcmp(language,"c") == 0 ) )
	    want_C = 1;
	  else if ( (strcmp(language,"fortran") == 0 ) || 
		   (strcmp(language,"Fortran") == 0 ) || 
		   (strcmp(language,"f") == 0 ) || 
		    (strcmp(language,"F") == 0 ) )
	    want_C = 0;
	  else
	    argp_failure (state, EXIT_FAILURE, errno,
			  _("Library not available in language %s - did you mistype it?"), language);
	};
      break;
    case 'a':			/* --angle-to-pixel */
      want_angle_to_pixel = 1;
      break;
    case 'n':			/* --n-resolution */
      n_resolution_string = xstrdup (arg);
      nlen_n_resolution_string = strlen(n_resolution_string);
      n_resolution_atoi = atoi(n_resolution_string);  /* convert string-int */
      break;
    case 't':				/* --theta-north */
      theta_north = xstrdup (arg);
      nlen_theta_north = strlen(theta_north);
      if ( nlen_theta_north > 0 ) 
	{
	  if (strcmp(theta_north,"1") == 0 ) 
	    inorth = 1;
	  else if (strcmp(theta_north,"2") == 0 ) 
	    inorth = 2;
	  else if (strcmp(theta_north,"0") == 0 ) 
	    inorth = 0;
	  else if (strcmp(theta_north,"-1") == 0 ) 
	    inorth = -1;
	  else
	    argp_failure (state, EXIT_FAILURE, errno,
			  _("North Pole convention %s is invalid - did you mistype it?"), theta_north);
	};
      break;
    case 'f':				/* --first-pixel */
      first_pixel_string = xstrdup (arg);
      nlen_first_pixel_string = strlen(first_pixel_string);
      if ( nlen_first_pixel_string > 0 ) 
	{
	  if (strcmp(first_pixel_string,"0") == 0 ) 
	    ifirst_pixel = 0;
	  else if (strcmp(first_pixel_string,"1") == 0 ) 
	    ifirst_pixel = 1;
	  else
	    argp_failure (state, EXIT_FAILURE, errno,
			  _("First pixel number  %s  has not been programmed."), first_pixel_string);
	};
      break;
    case 'p':		/* --plot */
      want_plot = 1;
      break;
    case 's':			/* --serial-ordering */
      want_serial = 1;
      break;
    case 'g':			/* --serial-ordering */
      want_global_projection = 1;
      break;
    case 'r':		/* --ring */
      want_nested = 0;
      break;
    case 'v':			/* --verbose */
      want_verbose = 1;
      break;
    case 'w':		/* --no-warn */
      want_no_warn = 1;
      break;
    case 'W':		/* --wcslib */
#ifdef HAVE_LIBWCS
      want_wcslib = 1;
#else
      printf("libwcs.a was not found on this system when compiling isolat. It cannot be tested. Sorry :(. Maybe try reinstalling wcslib and isolat.\n");
#endif
      break;
    case ARGP_KEY_ARG:		/* [FILE]... */
      /* TODO: Do something with ARG, or remove this case and make
         main give argp_parse a non-NULL fifth argument.  */
      break;
    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}
#endif /* HAVE_ARGP_H */


/* Show the version number and copyright information.  */
static void
#ifdef HAVE_ARGP_H
show_version (FILE *stream, struct argp_state *state)
#else
  show_version (FILE *stream)
#endif /* HAVE_ARGP_H */

{
#ifdef HAVE_ARGP_H
  (void) state;
#endif /* HAVE_ARGP_H */
  /* Print in small parts whose localizations can hopefully be copied
     from other programs.  */
  fputs(PACKAGE" "VERSION"\n", stream);
  fprintf(stream, _("Written by %s.\n\n"), "Boud Roukema");
  fprintf(stream, _("Copyright (C) %s %s\n"), "2004", "Boud Roukema");
  fputs(_("\
This program is free software; you may redistribute it under the terms of\n\
the GNU General Public License.  This program has absolutely no warranty.\n"),
	stream);
}
