C   circles - calculate identified circles statistics from CMB data (cosmic topology)
C
C   Copyright (C) 2004 Boud Roukema
C
C   This program is free software; you can redistribute it and/or modify
C   it under the terms of the GNU General Public License as published by
C   the Free Software Foundation; either version 2, or (at your option)
C   any later version.
C
C   This program is distributed in the hope that it will be useful,
C   but WITHOUT ANY WARRANTY; without even the implied warranty of
C   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C   GNU General Public License for more details.
C
C   You should have received a copy of the GNU General Public License
C   along with this program; if not, write to the Free Software Foundation,
C   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
C


      subroutine circ_f(   ! INPUTS
     _     indir,nindir,
     _     statu,nstatu,
     _     fileraw,nfraw,filesmooth,nfsmooth,
     _     filemask,nfmask,
     _     toyfile, ntoyfile,
     _     bin_format, nbin_format,
     _     iverb,nowarn,
     _     iwring, iwmask,
     _     iwcdiscs,
     _     iwcS3,
     _     chnrun,n_chnrun,
     _     npix,temp,errtemp,gallong,gallat, tempcp, k_ring,
     _     iw_new,iw_unsm,
     _     iw_ran,iransm ! OUTPUTS
     _     )

C     07.12.2004 In the alpha distributions of the package "circles",
C     that is, for versions less than 1.0.0, a lot of work still is likely
C     to be necessary in improving internal documentation (in program files)
C     and external documentation (such as the texinfo file). You are most
C     welcome to help and integrate your improvements with other people
C     working on this GNU GPL protected package.   --boud (B. Roukema)

c     ==== B. Roukema 27/10/98, IUCAA  boud at iucaa.ernet.in
c     (1) work out positions of COBE "pixels" in fundamental polyhedron.
c     (2) calculate statistics of pairs around circles
c     ====

      parameter(pi=3.1415926535898)
      parameter(pi_180=pi/180.0)

C     INPUTS:
      character*(*)  indir     ! directory with input files
      integer        nindir    ! number of characters in  indir

      character*(*)  statu     ! file with status of calculation
      integer        nstatu    ! number of characters in  statu

      character*(*)  fileraw    ! input raw file with cmb data
      integer        nfraw    ! number of characters in  fileraw
      character*(*)  filesmooth    ! input/output smooth file with cmb data
      integer        nfsmooth    ! number of characters in  filesmooth
      character*(*)  filemask ! input mask file
      integer        nfmask    ! number of characters in  filemask
      character*(*)  bin_format  ! binary format of smoothed file
      integer        nbin_format ! number of characters in  bin_format

      character*(*)  toyfile    ! file to write toy simulation parameters
      integer        ntoyfile   ! number of characters in  toy


      integer        iverb,nowarn ! flags: verbose, suppress warnings
      integer        iwring       ! flag: do ring ordering for input file?
      integer        iwmask       ! flag: want mask file?
      integer*4      iwcdiscs  ! do  cdiscs() ?
      integer*4      iwcS3  ! do  corrS3() ?
      character*(*)  chnrun        ! input suggestion of nrun (e.g. random)
      integer*4      n_chnrun      ! no of chars in  chnrun

      logical*4      masked      ! one of the pixels is masked

      real*4   temp(*),errtemp(*) !   Temperature and uncert.
      real*4   gallong(*),gallat(*) ! galactic

      real*4   tempcp(*) ! temporary copy of temp()
      integer*4  k_ring(*)  ! temporary index for ring ordering

C     OUTPUTS:
      integer*4  iw_ran ! want random circle positions ?   yes/no = 1/0
      integer*4  iransm ! smooth out probs from random circle positions? yes/no = 1/0

C     INTERNAL:
      character*256  indir2 ! safe string version of indir
      character*256  statu2 ! safe string version of statu
      character*2    eol ! end-of-line character pair
      character*256  mclog ! safe string version of statu
      character*256  filer2 ! safe string version of fileraw
      character*256  files2 ! safe string version of filesmooth
      character*256  filem2 ! safe string version of filemask
      character*256  binfo2 ! safe string version of bin_format
      character*256  toyf2  ! safe string version of toyfile

      logical*4  restart
      character*256  sta_string ! for reading in from the status file

      logical*4  cmb           ! do COBE data (T) or simulation (F)?

      real*4   fun_eq(3,3)      ! fundamental axes in B1950.0 equatorial coords
      real*4   efund(3,3) ! galactic coords
      parameter(maxfp=30)       ! upper bound to  [2 rSLS/Rfund(i)]
      real*4   Rfund(3)         ! lengths of axes in h^-1 Mpc
c      real*4   xlis(3)          ! temporary storage

c     common with mkmap
c     common /topocomm/ Rx,Ry,Rz,np,g1,g2,efund,einv
      common /topocomm/ Rfund,np,g1,g2,efund,einv
      integer*4  np             ! in sum over k values

      parameter(maxp=10)
      real*4   g1(-maxp:maxp,-maxp:maxp,-maxp:maxp) ! amplitudes
      real*4   g2(-maxp:maxp,-maxp:maxp,-maxp:maxp) ! amplitudes

      real*4     einv(3,3)
c     real*8   enorm8
c      real*8   efund8(3,3),einv_8(3,3), xmat3_8(3,3)
c      real*8   vec_8(3),vec2_8(3)
c      real*8   efund8(4,4),einv_8(4,4), xmat4_8(4,4)
c      real*8   vec_8(4),vec2_8(4)


      real*4    q_0,H_0,cc_0, zSLS, specn
c      parameter(maxcurv=4)

      include 'lib/npar.dec'

      real*4    q(maxcurv),cc(maxcurv)
      real*4    omm(maxcurv)
      real*4    rSLS            ! distance to SLS in h^-1 Mpc
      real*4    drSLS           ! thickness of SLS in h^-1 Mpc
      common /cosmcomm/ q_0,H_0,cc_0, zSLS, specn, rSLS !

c     ==== COBE stuff in rdCOBE.f ====
c      parameter(npix=6144,maxn=8096) !! WARNING declared several times
c      include 'lib/npix.dec'

c      real*4   temp(maxn),errtemp(maxn) !   Temperature and uncert.
c      real*4   gallong(maxn),gallat(maxn) ! galactic

c     real*4   eclong(maxn),eclat(maxn) ! ecliptic
c      real*4   eqlong(maxn),eqlat(maxn) ! ra dec in degrees, degrees

c      real*4   xg(maxn),yg(maxn),zg(maxn) ! galactic x,y,z for unit radius

c      parameter(nxint=180,nyint=90)
      include 'lib/nxint.dec'
      integer*4 inear(nxint,nyint) ! nearest pixel number
      real*4    dinear(nxint,nyint)
      real*4    dxint,dyint
c     -- Eq. Cartesian coords of table --
      real*4    x(nxint,nyint),y(nxint,nyint),z(nxint,nyint)

c      common /cobecomm/  temp,errtemp,gallong,gallat,eqlong,eqlat,
c     _     dsm,gb0,dxint,dyint,inear,dinear,x,y,z
c      common /wmapcomm/  temp,errtemp,gallong,gallat,
c     _     dsm,gb0,dxint,dyint,inear,dinear,x,y,z
      common /wmapcomm/
     _     dsm,gb0,dxint,dyint,inear,dinear,x,y,z, npixcp


      logical*4 measur,noise
c      real*4    ranerr(maxn)
      common /noisecomm/ measur,noise


c     --- function declarations ---
      real*4    pofk,delT,delTgal,cobeDMR,testfn, dTg_nodip,wmaplin
      external  pofk,delT,delTgal,cobeDMR,testfn, dTg_nodip,wmaplin
      complex*16    delTrad8
      external  delTrad8
      real*8    dofz            ! in cosmdist-0.2.1 packge
      external  dofz

c     real*8    red_d           ! local value: double precision redshift
c     ====

c     --- for output of positions in tipsy format ---
c      real*4    xT2(npix,3)     ! coords in T^2 frame
c      real*4    dummy(npix,4)   ! dummies for tipsy, but (1,4)= 1 = mass
c      character*256   flnm
      character*3     ch3

c      real*4          header(128)

c     ---- difference statistics ----
c     parameter(mxsig=30)
c      parameter(mxpar=1)

c      include 'lib/npar.dec'  ! above

c     -- ipar loops through each individual topology hypothesis --
      real*4    sigma(mxpar,maxcurv),drsigma(mxpar),drs2(mxpar)
      real*4    dmean(mxpar,maxcurv)
      real*4    sfreq(mxpar,maxcurv)
      real*4    corr(mxpar,maxcurv),cwght(mxpar,maxcurv)
      real*4    cdopp(mxpar,maxcurv)

      include 'lib/ncopies.dec'  ! shifted higher up here 0.1.69

c     --- temporary copy while looping through theta values (dodec case)
c      parameter(mxtheta= 200)
      parameter(mxtheta= maxtheta)
      real*4    sig_th(mxtheta)
      real*4    dme_th(mxtheta)
      real*4    sfr_th(mxtheta)
      real*4    cor_th(mxtheta),cwg_th(mxtheta)
      real*4    cdo_th(mxtheta)


c     --- especially useful for Poincare dodecahedral hypothesis ---
      real*4    gl_th(mxtheta),gb_th(mxtheta)
      real*4    thdod_th(mxtheta),ang_th(mxtheta)

      real*4    glpar(mxpar) ! g long
      real*4    gbpar(mxpar) ! g lat
      real*4    thpar(mxpar) ! theta of dodecahedron rotation
      real*4    angpar(mxpar) ! ang radius of circle

      integer*4  kstep_type ! since 0.2.5.12 - type of cycling through steps

      logical*4 isco_,iscontrol(mxpar) ! 0.1.35: is it in the control sample?
      logical*4 accepted(mxpar)

      parameter(maxpair=20000)
      parameter(mxdlist=1,idlist=1)
      real*4    dlist(maxpair,mxdlist,maxcurv)
      integer*4 npair(mxpar,maxcurv)

      integer*4 npa_th(mxtheta) ! temporary copy

      real*4    sigbig(maxcurv),sfrbig(maxcurv)

      logical*4 test,test2,test3,rotate,test4,test5

c     ---- galactic cut ----
      logical*4 galcut
      integer   iwgalc          ! integer version of galcut
      real*4    bcut            ! galactic latitude cut (deg)
      real*4    GCmin           ! cut from Galactic Centre (deg)
      real*4    factcutnoise    ! factor mult. Ssigma above which pix ignored
c     set  factcutnoise  to  10.0 or greater to have no effect

c     ---- magellanic stream (including LMC, SMC) optional cut ---
      logical*4 isMage          ! function
      logical*4 antiGC          ! function - in anti GCentre region
      logical*4 SGcut           ! function - arbitrary South hemi cut
      logical*4 inospot         ! function - in Ophiuchus,Orion complex/loop?
      logical*4 inos            ! local value- in Ophiuchus,Orion complex/loop?

c     ---- smoothing ? ----
      logical*4 smooth,new,dosmap,dos2map,dos2mean

      logical*4 cdoppwarn

c     ---
c      parameter(maxrun=1000)
c      real*4    ranlis(2,maxrun),xlong(maxrun)

c     --- save circle data  -- only evaluate for icc=1=irun
c      parameter(iimax=50,jjmax=50, maxtheta=100)
c      parameter(iimax=180,jjmax=180, maxtheta=360) ! 03.04.2003
c      parameter(iimax=50,jjmax=50, maxtheta=720) ! 03.04.2003
c      parameter(iimax=10,jjmax=10, maxtheta=720) ! 18.12.2003

c      include 'lib/ncopies.dec'

      integer*4  imax,jmax
      integer*4    nthc(-iimax:iimax,0:jjmax)
      real*4    glatc(2,-iimax:iimax,0:jjmax), ! 2 centres
     _     glongc(2,-iimax:iimax,0:jjmax), ! 2 centres
     _     wmodc(-iimax:iimax,0:jjmax), ! dtheta * rSLS
     _     rSLSc,
     _     dthMpc(-iimax:iimax,0:jjmax) ! dtheta * rSLS


      real*4    tt1c(maxtheta, -iimax:iimax,0:jjmax)
      real*4    tt2c(maxtheta, -iimax:iimax,0:jjmax)
      real*4    s1c(maxtheta, -iimax:iimax,0:jjmax)
      real*4    s2c(maxtheta, -iimax:iimax,0:jjmax)
c     ---- 24.02.2004 ----
c     -- store galactic long, lat at each pixel for double-checking --
      real*4    glpix1(maxtheta, -iimax:iimax,0:jjmax)
      real*4    gbpix1(maxtheta, -iimax:iimax,0:jjmax)
      real*4    glpix2(maxtheta, -iimax:iimax,0:jjmax)
      real*4    gbpix2(maxtheta, -iimax:iimax,0:jjmax)

c     ---- v0.1.34 ----
c     ---- calculate correlations of individual (iv=iv1,iv2) circle pairs
      real*4    sfr_iv(mxtheta,-iimax:iimax,0:jjmax)
      real*4    cor_iv(mxtheta,-iimax:iimax,0:jjmax)
      real*4    cwg_iv(mxtheta,-iimax:iimax,0:jjmax)
c     ---- store correlations of individual (iv=iv1,iv2) circle pairs
      real*4    co_iv(mxpar,-iimax:iimax,0:jjmax)


c     ---- alternative to the RE97 T2 hypothesis ----
      logical*4 doRE97 ! .true. if doing RE97
      logical*4 doRfund ! .true. if modifying Rfund3 scale
      logical*4 dopolerot ! .true. if rotating about the long axis
      logical*4 donudge ! .true. if nudging around a position by few deg.
      logical*4 donud3 ! .true. if 2nd nudge param is th_d0
      real*4    thnud
c      logical*4 doranlong ! .true. if random long axis
c      logical*4 doranrotate !.true. if random rotations applied
      logical*4 dosmrotate ! .true. for smooth gal long rotation ! 17.04.2003

      logical*4 ISWnoise ! .true. if adding noise at ISW scale
      real*4    xISW ! factor of ISW to multiply by NSW signal
      logical*4 dovaryISW ! .true. if varying  xISW

      logical*4 doCOBEdipole       ! .true. if testing COBE dipole
      real*4    glCdip,gbCdip, xCdip,yCdip,zCdip

      logical*4 do_ivdelshift ! for slightly different iv subset e.g. BASI

      logical*4 dosiginvert ! sigma -> 1/ss2, not ss2

c     ---- 25.12.2003 ----
      logical*4 doonepair ! test one pair only

c     ---- 02.04.2003 ----
      logical*4 doCOBE
      logical*4 doWMAP
      integer*8 j  ! to prevent overflow
      integer*8 npix8


c     ---- 09.02.2004: which type of hypotheses, e.g. T2? ----
      logical*4  doT2           !T2
      logical*4  dododec        ! Poincare dodecahedral
      logical*4  dodran         ! random: ignore most input parameters
      logical*4  write_dodec    ! should dodec data be written?
      logical*4  dowrcircles    ! should circle data be written?
      logical*4  docS3    ! should corrS3 be called for this ipar value?
      logical*4  doonevalue     ! do only one value (e.g. for circle data)

      logical*4  ivjv_valid     ! test on validity of iv,jv

      logical*4  doallth         ! write out all ithdodec values ? 0.1.79


c     ---- 31.05.2007 - 0.1.69: markov chain monte carlo (MCMC)? ----
      logical*4  mcmc           ! do an MCMC search of parameter space?
      logical*4  mc2done        ! is the (MC)^2 done (converged) yet ? 0=N, 1=Y
      logical*4  oneaxis        ! do only one axis instead of 6 ?

c      real*4     pNULL
c      parameter(pNULL=1e-9) ! null value for likelihoods
      parameter(tolpmc=1e-8) ! tolerance for likelihoods ! 0.2.5.8: s/1e-5/1e-8/
      parameter(tolqq=0.01) ! tolerance for likelihoods ! 0.2.5.5. experimental

c      integer    iwverb         ! verbose = 1 for .true.?
c
c      data       iwverb /1/

      data   glCdip,gbCdip/264.26, 48.22/ !Bennett et al. 464:L1L4, 1996

c     --- removed RE97 stuff from here ---

c     data  fun_eq/ -813.0,-446.0,-254.0,  ! e1 (x,y,z)
c     _     -490.0, 533.0, 633.0, ! e2 (x,y,z)
c     _     -153.0, 665.0, -678.0/ ! e3 (x,y,z)

      data  fun_eq/ -813.0,-490.0, -153.0, !  correct ordering
     _     -446.0, 533.0, 665.0, !
     _     -254.0, 633.0, -678.0/ !

      data H_0,zSLS/100.0,1100.0/

c      data flnm/'cobepix.dat'/


C     BEGINNING OF STATEMENTS:

      n256=min(nindir,256)
      print*,'circ_f: nindir,n256 = ',nindir,n256
      print*,'circ_f: nstatu = ',nstatu
      print*,'circ_f: nfraw = ',nfraw
      print*,'circ_f: nfmask = ',nfmask
      print*,'circ_f: nbin_format = ',nbin_format
      
      indir2=indir(1:min(nindir,256))
      statu2=statu(1:min(nstatu,256))
      filer2=fileraw(1:min(nfraw,256))
      files2=filesmooth(1:min(nfsmooth,256))
      filem2=filemask(1:min(nfmask,256))
      binfo2=bin_format(1:min(nbin_format,256))
      toyf2=toyfile(1:min(ntoyfile,256))

      i=1
      do while(i.le.256.and.statu2(i:i).ne.' ')
         i=i+1
      enddo
      mclog = statu2(1:i-1)//'.log'

c     -- This file is frequently opened/closed during the chain if nstatu.gt.0
c     -- Increase  nr_write  to decrease the frequency ("thin the chain").
      i_mcfile= 17
      eol=char(13)//char(10) ! ^M^J (should look ok on mac/win/linux)

      test=   .false.             !.true.              !
      test2= .false.            !.true.              !
      test3= .false.            !.true.              !
      test4= .true.              !.false.            !
      test5= .false.            !.true.              !
      cdoppwarn=.false.
      rotate=.true.             ! .false.            !

      galcut=.false. !  .true.            !
      if(galcut)then
         iwgcut=1
      else
         iwgcut=0
      endif

c      bcut=20.0 !30.0 !  50.0 !              ! degrees
      bcut=10.0 !20.0 !30.0 !  50.0 !              ! degrees
      GCmin= 40.0
c     factcutnoise= 10.0 ! 1.0 !

      dosiginvert=.false.!.true. !

      doonepair= .false.!.true. !

      np= 2                      ! for simulations
      call chooseamps

      cmb=.true.               !.false.
      doCOBE= .false.!.true. !
      doWMAP= .true. !.false. !

      doT2=.true. !.false. !

      dododec=.true.            !.false. !
c     -- dodran overrides most of the parameters and randomises on S^2
c      dodran=.false. !.true.            !
      doallth=.true.            ! write out all ithdodec values ? 0.1.79

      mcmc=  .true. ! .false. !
      mc2done= .false.  ! initialised to .false.
      mc2stop= 13000  ! number of runs after which mc2done is set to .true.
cc      mc2stop = 1000 !!! TEST ONLY !!!
      oneaxis = .false. !.true.                  ! do only one axis instead of 6 ?.false. !

      if(dododec)doT2=.false.
      resdodec= 0.2 ! degrees  ! resolution for (l,b,and offset)
      bmindodec= 64.0          ! minimum latitude angle in degrees (90=NGP)
      bmaxdodec= 66.0           ! maximum latitude angle in degrees (90=NGP)
      glmindodec= 103.0           ! minimum longitude
      glmaxdodec= 113.0           ! maximum longitude
      thmin= 56.0
      thmax= 58.0
      angmax= 9.0! 40.0! 16.0! 14.0!  32.0 ! maximum circle angular radius in degrees
      angmin= 13.0! 1.0  !6.0 !8.0!  minimum circle angular radius in degrees
      nangle= 20! 39 ! 12!
      twistorient= -1.0 ! \pm 1.0 for dodec, different as "control"

c     --- the "32deg" solution for zero twist ---
c      resdodec= 0.5             ! degrees  ! resolution for (l,b,and offset)
c      bmindodec= 61.0          ! minimum latitude angle in degrees (90=NGP)
c      bmaxdodec= 63.0           ! maximum latitude angle in degrees (90=NGP)
c      glmindodec= 343.0           ! minimum longitude
c      glmaxdodec= 353.0           ! maximum longitude
c      thmin= 58.0
c      thmax= 60.0
c      angmax= 40.0! 16.0! 14.0!  32.0 ! maximum circle angular radius in degrees
c      angmin= 1.0  !6.0 !8.0!  minimum circle angular radius in degrees
c      nangle= 39 ! 12!


c     ---- "full" parameter space search  -- version for RBSG08 arXiv:0801.0006
c     -    As of version (at least) 0.1.80 or later, only (angmin,angmax) are
c     -    hard constraints - the other "limits" are only used for selecting the starting
c     -    point.

      resdodec= 10.0 !0.5             ! degrees  ! resolution for (l,b,and offset)
      bmindodec= 0.0          ! minimum latitude angle in degrees (90=NGP)
      bmaxdodec= 60.0           ! maximum latitude angle in degrees (90=NGP)
      glmindodec= 0.0           ! minimum longitude
      glmaxdodec= 360.0           ! maximum longitude
      thmin= 0.0
      thmax= 72.0
      angmax= 60.0! 13.0! 16.0! 14.0!  32.0 ! maximum circle angular radius in degrees
      angmin= 5.0  !6.0 !8.0!  minimum circle angular radius in degrees
      nangle= 7! 2 ! 12!
      twimin = 0.0 !
      twimax = 10.0 !  ! 10.0 => 360.0 deg twist

      a_maxh= 60.0
      a_minh=  5.0


c     ---- "full" parameter space search  -- higher resolution
c      resdodec= 0.5 ! 2.0            ! degrees  ! resolution for (l,b,and offset)
c      bmindodec= 62.0-2.0          ! minimum latitude angle in degrees (90=NGP)
c      bmaxdodec= 62.0+2.0           ! maximum latitude angle in degrees (90=NGP)
c      glmindodec= 184.0-2.0/cos(62.0*pi_180)           ! minimum longitude
c      glmaxdodec= 184.0+2.0/cos(62.0*pi_180)           ! maximum longitude
c      thmin= 34.0-2.0
c      thmax= 34.0+2.0
c      angmax= 45.0! 13.0! 16.0! 14.0!  32.0 ! maximum circle angular radius in degrees
c      angmin= 15.0  !6.0 !8.0!  minimum circle angular radius in degrees
c
c      a_maxh= 60.0
c      a_minh=  5.0
c
c      nangle= 7! 2 ! 12!  ! irrelevant for MCMC
c      twimin = (37.4 - 2.1) / 36.0 ! highest phi + 1 sigma; see Table 2 RBSG08
c      twimax = (39.0 + 2.4) / 36.0 ! lowest phi - 1 sigma; see Table 2 RBSG08

      doonevalue= .false. !.true.!
      if(doonevalue)then
         resdodec= 0.02          ! degrees  ! resolution for (l,b,and offset)
         bmindodec= 62.0 ! 83.5! 64.7        ! minimum latitude angle in degrees (90=NGP)
         bmaxdodec= bmindodec        ! maximum latitude angle in degrees (90=NGP)
         glmindodec= 184.0! 21.405! 252.4 ! minimum longitude
         glmaxdodec= glmindodec ! maximum longitude
c         angmin= 50.0! 10.7! 5.0! 11.5           ! minimum circle angular radius in degrees
c         angmax= angmin + resdodec        !  maximum circle angular radius in degrees
         angmin= 21.0! 17.6 !10.0
         angmax= angmin + resdodec !60.0
         thmin= 34.0! -23.0 !0.0! 57.0
         thmax= thmin
         nangle=  2!  5! 80             !
c         twistorient= -1.0      ! \pm 1.0 for dodec, different as "control"
c         twistorient= +1.0      ! \pm 1.0 for dodec, different as "control"
         twimin = 0.9999           !
         twimax = 1.0001

      endif


c      ithsave= 12 ! interval for saving maximum values !maybe unused

      ISWnoise=.false. !.true. !
      xISW=0.0! 0.3 !0.6 !13.5/14.5 !9.0/11.0 !0.5 ! 0.2 ! 0.8 !
      ivdel=1                   !! default: ivdel=1 for 'all' circles !!
c      ivdel=6
c      ivdel=3

      nrun=2!400!200!35 !50! 1 !  2! 34 !10!  ! 34 = 360/10 * cos(18.5)

c     -- write out status and mclog results when irun is a multiple of nr_write +1
      nr_write = 1


      if(chnrun(1:n_chnrun).eq.'none')then
         nrunin = 1667   ! default value: 1667*6 gives 10002 pairs
         dodran= .false.
         iw_ran= 0              ! external
      else
         dodran= .true.
         iw_ran= 1              ! external
         if(iverb.eq.1)print*,'Will try reading in nrandom...'
         print*,'chnrun=  ',chnrun(1:n_chnrun)
         read(chnrun(1:n_chnrun),*)nrunin
         print*,'iw_ran=',iw_ran
c     --- 0.1.42 ---
         if(nrunin.lt.0)then
            iransm=0
            nrunin=-nrunin
         else
            iransm=1
         endif

      endif
      if(iverb.eq.1)print*,'dodran, number of runs: nrunin=',
     _     dodran,nrunin


c     ---- possibly read in parameters to restart stopped MCMC chain ----
c     -- since 0.2.5.8 - may be overwritten by -t --toy-simulation option --
      restart = .false.
      if(nstatu.gt.0)then
         open(1,file=statu2,access='sequential',status='old',
     _        err=888)
         read(1,'(a)',err=888,end=888)sta_string
         if(sta_string(1:5).ne.'START')then
            read(sta_string,'(5f13.4)',err=888,end=888)
     _           gl_sta,
     _           gb_sta,
     _           th_sta,
     _           angsta,
     _           twista
         endif
         restart = .true.     ! restart is done only if read is successful 
         if(test4)print*,'restart,gl_sta,gb_sta,...=',
     _        restart,gl_sta,gb_sta,th_sta,angsta,twista
 888     continue
         close(1)

      else
         if(test4)print*,'No status file.'
      endif



      if(dododec)then
c     --- flush generator ---
         do i=1,28271! 44444!67342
            xx= rnd(0)
         enddo
         call getnrun(dododec, dodran, resdodec,
     _        bmindodec,bmaxdodec,glmindodec,glmaxdodec,
     _        thmin,thmax,
     _        angmin,angmax,
     _        twimin,twimax,
     _        nangle, nrunin, mcmc, oneaxis, mc2done,
     _        nrun,nthdomax,
     _        gl_pre,gb_pre,th_pre,angpre,twipre)
         print*,'nrun,nthdomax=',nrun,nthdomax
         if(mcmc)then
            pmc_old= -1.0
         endif
      endif


      q_0=0.5

c     -- 12.12.2003 boud
      w_q= -1.0

      cc_0=0.0
c     dcc=0.0025
      cc1=0.70 ! 0.0 !0.73! 0.0                   !0.617 !0.5 !0.0!
      cc2=0.8                   !622 !0.8
      ncc=1                     !                  !360 !60!
      if(ncc.gt.1)then
         dcc=(cc2-cc1)/real(ncc-1)
      else
         dcc=99.9
      endif

      ntheta= 720! 100 ! 36                !

      doRE97=.false. !.true.             !
      doRfund= .true. !.false.           !
      dosmrotate= .false.        ! .true. !
      dopolerot= .false.        !.true. !
c      doranrotate=.false. !.true.
      donudge= .false. !.true. !
      donud3= .false. !.true. !
      thnud=1.0! 0.5 ! 1.0 (great circle) degree

      dovaryISW= .false.        ! .true.          !
      xISW1=0.0
      xISW2=0.6
      doCOBEdipole=.false.!.true. !
      do_ivdelshift=.false. !.true.!

c      glong3= 53.0 !280.0 !279.0             !86.0 !14.0 !75.0 !142.0 !
c      glat3= -82.0 !37.5 ! 22.5               !-30.0 !-27.5 !18.0! 37.5 !

c     ---- for counterargument to "referee 2" ----
c      glong3= randt()*360.0
c      glat3= randt()*90.0-45.0
      print*,'glong3,glat3=',glong3,glat3
c      glong3= 280.0 !280.0 !329.0 !285.0 !100.0             ! 80.0!
c      glat3= 37.5 !33.5 !-42.0 !-80.0 !-20.0              !50.0 !

c     WMAP
      glong3= 186.0
c      glat3= -44.0
      glat3= -41.5

c     WMAP - T3???
c      glong3= 280.5
c      glat3= -5.0

c     WMAP T3 26.12.2003 ???
c      glong3=189.3
c      glat3=48.5
c      glong3=97.5
c      glat3=1.7

c      glong3= 191.0 ! for 'bad' candidate
c      glat3= -57.5 ! for 'bad' candidate

c      glong3= 184.0 !264.0
c      glat3= 8.0 !-51.0

c     -- from symT2 --
c      glong3= 329.0! 282.0 !
c      glat3=  -42.0 ! 32.0 !
c     -- after nudging --
c      glong3= 286.0
c      glat3= 35.0

c      glong3=74.5270    ! independent RE97
c      glat3= 18.46727 ! independent  RE97

c      th_d0=  328.2 !190.6 ! 0.0!   ! 328.0 for the best COBE candidate
      th_d0=   136.0! 0.0
      th_d0=   135.5  ! 121.0! for (186.0, -41.5)
c     _     +80.0

c     WMAP T3 26.12.2003 ???
c     th_d0= 270.0 ! for (189.3,48.5)
c      th_d0= 48.0 ! for (97.5, 1.7)


c     ---- arbitrary shifts !! test only !!----
c      th_d0=th_d0     +10.0
c      glong3= glong3+30.0 + 20.0

c      th_d0= 40.0 ! for (280.5, -5.0)

c     --- test amr candidate from RLAGN ---
c      glong3= 281.7
c      glat3=  77.0 -90.0
c      th_d0 = 358.0

c     --- test amr candidate from RLAGN  11.07.2003---
c      glong3= 228.0
c      glat3=  75.3 -90.0
c      th_d0 = 114.2

c      th_d0= 74.1 ! for 'badsig' candidate
c      th_d0= 42.35
c      th_d0= 64.9 ! for (184,8) as long axis
c      th_d0= 202.0 ! approximate (not precise) value for  independent RE97

c     RE97 size = ( 1139.95    1155.63 ) for cc=0.7, om=1-cc
c     ---       cc=0  -> (540,840), cc=0.73 ->(900,1400) ---
      Rfund3_1= 9000.0 ! 18400.0! 6750.0! 8800.0 !17600.0! 9500.0! 10000.0! 540.0! 900.0 ! 1200.0 !1320.0 !1000.0 ! 600
      Rfund3_2=  19000.0!840.0! 1400.0! 1440.0 !2400.0 !1200.0 !3000.0
c      Rfund3_2= 6000.0  ! -> nrun= 181
      if(nrun.gt.1)then
         dRfund3 = (Rfund3_2 - Rfund3_1)/real(nrun-1)
      else
         dRfund3 = 9e9
      endif
      print*,'dRfund3=',dRfund3
      Rfund3=Rfund3_1

c      print*,'rSLS,drSLS=',rSLS,drSLS

      npar=1                    ! big npar not used at present


      if(doCOBEdipole)then
         xCdip= cos(gbCdip*pi/180.0) *cos(glCdip*pi/180.0)
         yCdip= cos(gbCdip*pi/180.0) *sin(glCdip*pi/180.0)
         zCdip= sin(gbCdip*pi/180.0)
      endif

      write_dodec=.false.
      if(dododec)write_dodec=.true. ! to initialise sig_th( ) etc

c      do irun=1,nrun !6,6 ! ! 6,6 = test only !!
      irun=0
      if(test2)then
         print*,'test2: irun+1, nrun, (.not.(mcmc)) ='
         print*,irun+1, nrun, (.not.(mcmc))
         print*,'mcmc, .not.(mc2done) = '
         print*,mcmc, .not.(mc2done)
         print*,'correlation in S3? iwcS3 = ',iwcS3
      endif
      do while(irun+1.le.nrun .and.
     _     ( (.not.(mcmc)) .or. (mcmc.and.(.not.(mc2done))) )
     _     )
         irun= irun+1

         if(irun.eq.1)then
            rotate=.false.
         else
            rotate=.true.
         endif

         if(test2.and. 10*((irun-1)/10).eq.irun-1)then
            print*,'irun, nrun, ipar=',irun,nrun,ipar
         endif


c     ---- read in COBE/WMAP/Planck file ----

         if(irun.eq.1)then !modif 01/01/00 -> checked OK

            smooth= .true.      !.false.     !
            new=  .false.        ! .true.        !
            if(iw_new.eq.1)new= .true. !        starts version  0.1.47
            if(iw_unsm.eq.1)smooth= .false. !   starts version  0.1.47
c     -- WMAP --
            sigdeg= 1.0
            resdeg= 0.2
c     -- COBE --
            angledeg=5.0        ! 10.0 !-99.9         !
c     -- in principle any data set, though not recently debugged --
            dosmap=.false.
            dos2map=.false.
            dos2mean=.false.


            print*,'circ_f: iwring=',iwring

            call rddata(doCOBE,doWMAP,smooth,new,sigdeg,resdeg,
     _           dosmap,dos2map,dos2mean,
     _           indir2,nindir,
     _           filer2,nfraw,
     _           files2,nfsmooth,
     _           toyf2,ntoyfile,
     _           filem2,nfmask,iwmask,
     _           binfo2,nbin_format,iwring,
     _           galcut,bcut,
     _           Ssigma,
     _           npix,temp,errtemp,gallong,gallat,tempcp,k_ring)


            kstep_type = 0     ! cycle randomly over parameters when stepping

c     ----- stability test - normally should be turned off -----
c            kstep_type = 5     ! step only in twist
c            twista= rnd(0)*10.0 ! randomise the initial twist


c     ---- If in toy simulation, override both random and restart
c     initial states, forcing the chain to start at the correct
c     solution.  This tests whether a solution is something like a
c     stable maximum or not. ----

            if(1+1.eq.2)then    ! ENabled
c     restart = .false.

               if(ntoyfile.gt.0)then
c               kstep_type = 45  ! only cycle in 4 and 5 (alpha and twist)
                  kstep_type = 5 ! only cycle in 5 (twist)

                  open(17,file=toyf2(1:ntoyfile),access='sequential',
     _                 form='unformatted',status='old')
                  read(17)gl_sta,gb_sta,th_sta,angsta,twista

                  restart = .true. ! restart is done only if read is successful 
                  if(test4)print*,'restart,gl_sta,gb_sta,...=',
     _                 restart,gl_sta,gb_sta,th_sta,angsta,twista
                  close(17)

c     --- if varying only a few dimensions, then start them randomly ---
                  if(kstep_type.eq.45)then
                     angsta=10.0+ rnd(0)*40.0
                     twista= rnd(0)*10.0
                  elseif(kstep_type.eq.5)then
                     twista= rnd(0)*10.0
                     if(test5)twista= 2.48
                  endif

               else
                  if(test4)print*,'-t option not selected: ',
     _                 'will not force start.'
               endif
            endif


c     --- free up memory for gallong, gallat, tempcp(*) ----
c            i= pxfr_2(iwverb)
c            if(i.ne.-1)then     ! -1 means memory freed up or no allocation attempt
c               print*,'circles_f77 ',
c     _              ' WARNING: after rddata etc pxal_2 returned ',i,
c     _              ' - there was a problem freeing up memory :( '
c            endif

c            call newdat(npix,temp,errtemp)  ! make some arbitrary change to data

         endif  !          if(irun.eq.1)then !modif 01/01/00 -> checked OK


c     ---- SLS thickness fixed, though in principle should vary with Lambda

         drSLS= 15.0            !100.0 !
         drbig= 100.0
         drbig2= drbig*drbig

c     11.02.2004  sig being replace by par - drsigma no longer used
c         ddrsigma= 3.0*drSLS/real(nsig)
c         do i=1,nsig
c            drsigma(i)= real(i)*ddrsigma
c            drs2(i)=drsigma(i)*drsigma(i)
c         enddo
c         do icc=1,ncc
c            do i=1,mxpar
c               sigma(i,icc)=0.0
c               dmean(i,icc)=0.0
c               sfreq(i,icc)=0.0
c               corr(i,icc)=0.0
c               cdopp(i,icc)=0.0
c               cwght(i,icc)=0.0
c               if(i.eq.idlist)then
c                  do ipair=1,maxpair
c                     dlist(ipair,i,icc)=0.0
c                  enddo
c               endif
c            enddo
c         enddo


c     --- version 0.1.31 07.07.2006 - cdiscs  - correlate discs
         if(irun.eq.1.and.iwcdiscs.eq.1)then
            delang= (angmax-angmin)/real(max(1,nangle-1))
            do icdisc=1,nangle
               ang_in = angmin + real(icdisc-1)*delang
               call cdiscs(
     _              (glmindodec+glmaxdodec)*0.5,
     _              (bmindodec+bmaxdodec)*0.5,
c     _              (angmin+angmax)*0.5,
     _              ang_in,  ! from version 0.1.46
     _              (thmin+thmax)*0.5,
     _              iwgcut,bcut,GCmin,
     _              iverb,nowarn,
     _              temp,errtemp,gallong,gallat)
            enddo               ! do icdisc=1,nangle
         endif




         if(dododec.and.write_dodec)then
            do ithdodec=1,mxtheta
               sig_th(ithdodec)=0.0
               dme_th(ithdodec)=0.0
               sfr_th(ithdodec)=0.0
               cor_th(ithdodec)=0.0
               cdo_th(ithdodec)=0.0
               cwg_th(ithdodec)=0.0
c     ---- v0.1.34 ----
               do jv=0,jjmax
                  do iv=-iimax,iimax
                     sfr_iv(ithdodec,iv,jv)=0.0
                     cor_iv(ithdodec,iv,jv)=0.0
                     cwg_iv(ithdodec,iv,jv)=0.0
                  enddo
               enddo

            enddo
            write_dodec=.false.  ! .true. first time or when writing out
         endif

         do icc=1,ncc
            cc(icc)=cc1+(real(icc-1))*dcc
            q(icc)= 0.5-1.5*cc(icc)

c     rSLS= pdWein(q_0,H_0,cc_0, zSLS)
c            rSLS= pdWein(q(icc),H_0,cc(icc), zSLS)

c     -- 12.12.2003 boud
            if(test)print*,'before dofz: q(icc),cc(icc)',q(icc),cc(icc)

            omm(icc)= 2.0*(q(icc) + cc(icc))
c            rSLS= DEpdWeiq(omm(icc),H_0,cc(icc),w_q, zSLS)

c     --- test only ---  !! test only !!
c            omm(icc)= 0.22  ! 0.81 !
c            cc(icc)=  0.80  ! 0.20 !

            rSLS= real( dofz(dble(H_0), dble(omm(icc)),
     _           dble(cc(icc)), dble(w_q), dble(zSLS)) )

            if(test)print*,'q(icc),cc(icc),rSLS=',q(icc),cc(icc),rSLS
            if(irun.eq.1.and.icc.eq.1)rSLSc=rSLS


            call hypoth(doRE97, omm(icc),H_0,cc(icc),w_q,
     _           glong3,glat3,Rfund3,
     _           irun,nrun,
     _           dopolerot,
     _           donudge,donud3,thnud,
     _           th_d0,
     _           dovaryISW,xISW1,xISW2,
     _           ISWnoise,Ssigma,
     _           fun_eq,
     _           gl1_3,gb1_3,gl2_3,gb2_3,
     _           xISW,
     _           bISW2)


c     ---- random rotation ----
c     - disabled by default since not really that useful - a sphere
c     with identified circles can have many equally valid choices
c     of orientations, so a genuine detection might be dismissed as
c     just one of many random events -
c            if(doranrotate)then
            if(1+1.eq.3)then
               call ranrotate(nrun,irun,fun_eq)
            endif               ! if(1+1.eq.3)then


            call hypot2(
     _           fun_eq,irun,nrun,Rfund3_1,dRfund3,rSLS,
     _           rotate,doRE97,doRfund,dosmrotate,
     _           efund,Rfund,glong,galb)


c     ---- maybe should be moved earlier if it is needed that
c     hypoth or hypot2
c     have an effect in the poincare dodecahedral case ----
            if(dododec)then
c               ipar=irun

c     --- shifted here 0.1.71
               if(irun.eq.1)ipar=0 ! initialise ipar = 0

               if(ipar.ge.3)then  ! 0.1.69
                  gl_pre= glpar(ipar)
                  gb_pre= gbpar(ipar)
                  th_pre= thpar(ipar)
                  angpre= angpar(ipar)
               endif

c               kstep_type = 0 ! default = random{1, 2, 3, 4, 5}
c               kstep_type = 45 ! only cycle in 4 and 5 (alpha and twist)

               if(irun.gt.1)restart=.false.
               call getirun(dododec,
     _              dodran,
     _              resdodec,
c     _              bmindodec,bmaxdodec,glmindodec,glmaxdodec,
     _              thmin,
     _              angmin,angmax,a_minh,a_maxh,
     _              twimin,twimax,
     _              irun,  mcmc, mc2done,
     _              restart,gl_sta,gb_sta,th_sta,angsta,twista,
     _              gl_pre,gb_pre,th_pre,angpre,twipre, kstep_type,
     _              glong,galb,thdodec,ang, twist,
     _              ilb,ithdodec,iang,
     _              isco_)

               twistorient = twist

               if(test)then
                  print*,'getirun: ',
     _                 glong,galb,thdodec,ang, ilb,ithdodec,iang
               endif

               gl_th(ithdodec)= glong
               gb_th(ithdodec)= galb
               thdod_th(ithdodec)= thdodec
               ang_th(ithdodec)= ang

c               if(irun.eq.1)ipar=0 ! initialise ipar = 0
            else
               ithdodec= 1
            endif

            if(ithdodec.gt.mxtheta)then
               print*,'circleT2 ERROR: ithdodec, mxtheta=',
     _               ithdodec, mxtheta
               ithdodec=mxtheta
            endif


c     ---- this would probably only be needed for doing simulations ----
c            call geteinv(efund,einv) ! invert the eq fundamental matrix


c            call checkiov

c     ----- statistics ----
            if(test)print*,'starting pair list'


            j=0
c     sigma=0.0
c     drs2= drsigma*drsigma
            ttmean =0.0
            ttvar= 0.0

c     --- calculate the monopole, so that later it can be subtracted ---
c     --- *only* used for simulations ---
            if(.not.(cmb))then
               call monopole(cmb,doCOBE,doWMAP,
     _              ttm1,ttv1,ttsd1,
     _              temp,errtemp,gallong,gallat)
            endif

c            ttm1= 0.0001 !! test only !!

c     could set ipar to some parameter to separate diff circles sets
c     default:  ipar = 1
c            ipar=1
c            npair(ipar,icc)= 0
            npa_th(ithdodec)= 0

            itd=0

            call ivjvlimits(
     _           doT2,dododec,Rfund,rSLS,
     _           do_ivdelshift,doonepair,
     _           imax,jmax,iv1,iv2)

            mv=0
            nv=0

            gbhigh = -99.9
            do iv=iv1,iv2,ivdel ! modified 19/12/99 for BASI article
c     do iv=-imax,imax,ivdel
               do jv=0,jmax,ivdel

                  nthc(iv,jv)= 0 ! added BFR 29.01.2004

c                  if(iv.gt.0.or.jv.gt.0)then
                  call chkivjv(doT2,iv,jv, ivjv_valid)

                  if(ivjv_valid)then
                     call getvectors(
     _                    iv,jv,mv,nv,
     _                    doT2,dododec,
     _                    efund,Rfund, rSLS, ntheta,
     _                    gl_th(ithdodec),
     _                    gb_th(ithdodec),
     _                    thdod_th(ithdodec),
     _                    ang_th(ithdodec),
     _                    vx1,vy1,vz1, ax1,ay1,az1,bx1,by1,bz1,
     _                    vx2,vy2,vz2, ax2,ay2,az2,bx2,by2,bz2,
     _                    wmod,
     _                    ivjv_valid, nth,dtheta)
                  endif

                  if(ivjv_valid)then

                     if(ipar.eq.2)then
                        dowrcircles=.true.
                     else
                        dowrcircles=.false.
                     endif
c     ---- 0.1.58 for corrS3.f ---
c                     if(iwcS3.eq.1.and.ipar.ge.2.and.
c     _                    2*(ipar/2).eq.ipar)then
c     --- 0.1.71 - ipar is evaluated later on, not here, so it's irrelevant
                     if(iwcS3.eq.1)then
                        docS3=.true.
                     else
                        docS3=.false.
                     endif

c     --- store centres (in 3d),etc. of ID'd circles ---
c                        if(irun.eq.1.and.icc.eq.1)then
                        if(dowrcircles.or.docS3)then
c     print*,'iv,jv=',iv,jv
                           call xyz_rda(vx1,vy1,vz1,
     _                          rr,del,alph)
                           glatc(1,iv,jv)= del
                           glongc(1,iv,jv)= alph*15.0
c                           call xyz_rda(-vx,-vy,-vz,
                           call xyz_rda(vx2,vy2,vz2,
     _                          rr,del,alph)
                           glatc(2,iv,jv)= del
                           glongc(2,iv,jv)= alph*15.0
                           wmodc(iv,jv)=  wmod
                           dthMpc(iv,jv)= dtheta*wmod

                           nthc(iv,jv)= nth

c     0.1.73: find highest latitude pair of the 12:  glhi,gbhi
                           do i=1,2 ! both elements of a pari
                              if(glatc(i,iv,jv).gt.gbhigh)then
                                 glhi = glongc(i,iv,jv)
                                 gbhi = glatc(i,iv,jv)
                                 gbhigh = gbhi
                              endif
                           enddo
                        endif

                        do ith=1,nth

c                           if(test)print*,'ith, 111: ',ith
                           th= real(ith)*dtheta
                           cth= cos(th)
                           sth= sin(th)

                           x1x= vx1 + wmod*(cth*ax1 + sth*bx1)
                           x1y= vy1 + wmod*(cth*ay1 + sth*by1)
                           x1z= vz1 + wmod*(cth*az1 + sth*bz1)

                           if(test.and.ith.le.10)then
                              print*,'ith,x1x,x1y,x1z=',
     _                             ith,x1x,x1y,x1z
                           endif

                           call xyz_rda(x1x,x1y,x1z,rr,del,alph)
                           gall1= alph*15.0
                           galb1= del
                           GC1= angle(x1x,x1y,x1z, 1.0,0.0,0.0)
c     GC1= angle(x1x,x1y,x1z, 0.0,1.0,0.0) !NOT GC


                           inos=inospot(x1x,x1y,x1z)


                           if(dododec)then
c                              th=th + (1.2*pi)
                              th=th + pi + twistorient *0.2*pi
                              cth= cos(th)
                              sth= sin(th)
                           endif

                           x2x= vx2 + wmod*(cth*ax2 + sth*bx2)
                           x2y= vy2 + wmod*(cth*ay2 + sth*by2)
                           x2z= vz2 + wmod*(cth*az2 + sth*bz2)

                           if(test.and.ith.le.10)then
                              print*,'ith,x2x,x2y,x2z=',
     _                             ith,x2x,x2y,x2z
                           endif

c                           if(test)print*,'222: '
                           call xyz_rda(x2x,x2y,x2z,rr,del,alph)
                           gall2= alph*15.0
                           galb2= del

c                           if(test)print*,'333: '
                           GC2= angle(x2x,x2y,x2z, 1.0,0.0,0.0)
c     GC2= angle(x2x,x2y,x2z, 0.0,1.0,0.0) !NOT GC
c                           if(test)print*,'444: '


                           inos=inos.or.inospot(x2x,x2y,x2z)

                           masked = .false.
                           if(iwmask.eq.1)then
                              if(doWMAP)then
                                 tt1= wmaplin(gall1,galb1,
     _                                temp,errtemp,gallong,gallat)
                                 tt2= wmaplin(gall2,galb2,
     _                                temp,errtemp,gallong,gallat)
                                 if(  tt1.lt.tNULLup .or.
     _                                tt2.lt.tNULLup )then
                                    masked= .true.
                                 endif
                              else
                                 print*,'masks not implemented ',
     _                                'for this option'
                              endif
                           endif

c                           if(test)print*,'555: '

                           if(.not.(galcut).or.
     _                          (abs(galb1).gt.bcut
     _                          .and.abs(galb2).gt.bcut
c     _                             .and.galb1.gt.0.0 ! SG hemisph EXcluded
c     _                             .and.galb2.gt.0.0 ! SG hemisph EXcluded
c     _                     .and.(GC1.gt.pi/3.0.or.galb1.gt.0.0) !+90 EXcluded
c     _                     .and.(GC2.gt.pi/3.0.or.galb2.gt.0.0) !+90 EXcluded
     _                          .and.GC1.gt.pi/9.0 ! GC EXcluded ! GCmin
     _                          .and.GC2.gt.pi/9.0 ! GC EXcluded
c     _                          .and.(GC1.lt.pi/3.0  ! GC included
c     _                              .or.GC2.lt.pi/3.0) ! GC included
c     _                          .and.(.not.(inos)) ! hot spots EXcluded
c     _                          .and.((inos)) ! hot spots included
c     _                          .and.(.false.)  ! test only !
c     _                             .and.(.not.(isMage(gall1,galb1))) !Mag-
c     _                             .and.(.not.(isMage(gall2,galb2))) ! stream
c     _                             .and.(.not.(antiGC(gall1,galb1))) !antiGC
c     _                             .and.(.not.(antiGC(gall2,galb2))) !
c     _                             .and.(.not.(SGcut(gall1,galb1))) !arbitr.
c     _                             .and.(.not.(SGcut(gall2,galb2))) ! SG cut
     _                          .and. (.not.(masked))  ! 0.1.70
     _                          )
     _                          ) then


c     if(1.lt.iv.and.iv.lt.5.and.jv.eq.0)then
c     print*,'TEST out'
c     print*,'x1x,x1y,x1z=',x1x,x1y,x1z
c     print*,'x2x,x2y,x2z=',x2x,x2y,x2z
c     endif
c     !! test only !!
c     gall1=gall1 + 23.0
c     gall2=gall2 + 58.0
c     galb1=galb1 +38.0
c     galb2=galb2 -67.0

                              noise=.false.
                              if(cmb)then
                                 if(doCOBE)then
                                    tt1= cobeDMR(gall1,galb1)
                                    tt2= cobeDMR(gall2,galb2)
                                 elseif(doWMAP)then
                                    tt1= wmaplin(gall1,galb1,
     _                                   temp,errtemp,gallong,gallat)
                                    tt2= wmaplin(gall2,galb2,
     _                                   temp,errtemp,gallong,gallat)
                                 endif
                              else
                                 tt1= delTgal(gall1,galb1)
                                 tt2= delTgal(gall2,galb2)

c     -- following two lines needed to remove monopole
c     should affect  corr  and  cdopp  but not  dmean nor sigma --
                                 tt1=tt1-ttm1
                                 tt2=tt2-ttm1

                              endif


                              noise=.true.
                              if(doCOBE)then
                                 s1= cobeDMR(gall1,galb1)
                                 s2= cobeDMR(gall2,galb2)
                              elseif(doWMAP)then
                                 s1= wmaplin(gall1,galb1,
     _                                temp,errtemp,gallong,gallat)
                                 s2= wmaplin(gall2,galb2,
     _                                temp,errtemp,gallong,gallat)
                              endif

c     ---- add ISW noise in quadrature 20/08/99 ----
                              if(ISWnoise)then
                                 s1= sqrt(bISW2+ s1*s1)
                                 s2= sqrt(bISW2+ s2*s2)
                              endif

c                              if(irun.eq.1.and.icc.eq.1)then
                              if(dowrcircles.or.docS3)then
                                 tt1c(ith,iv,jv)= tt1
                                 tt2c(ith,iv,jv)= tt2
                                 s1c(ith,iv,jv)= s1
                                 s2c(ith,iv,jv)= s2
                                 glpix1(ith,iv,jv)= gall1
                                 gbpix1(ith,iv,jv)= galb1
                                 glpix2(ith,iv,jv)= gall2
                                 gbpix2(ith,iv,jv)= galb2
                              endif
c     s1=1.0
c     s2=1.0

                              xx= tt1-tt2
                              yy= max(s1*s1+s2*s2,1e-20) !
                              if(test.and.yy.lt.2e-20)print*,
     _                             'WARNING s1,s2=',s1,s2
                              ss2= xx*xx /yy

                              if(dosiginvert)then
                                 if(ss2.ge.1e-4)then ! hard limit
                                    ss2=1.0/ss2
                                 else
                                    ss2=0.0
                                 endif
                              endif

c     wt= 1.0/sqrt(yy)
                              ss= xx /sqrt(yy)

                              if(.not.(doCOBEdipole))then
                                 dd= dot(x1x,x1y,x1z,
     _                                x2x,x2y,x2z)
     _                                /(rSLS*rSLS)
                              else
                                 dd= dot( (x1x)/rSLS,
     _                                (x1y)/rSLS,
     _                                (x1z)/rSLS,
     _                                xCdip,yCdip,zCdip) *
     _                                dot( (x2x)/rSLS,
     _                                (x2y)/rSLS,
     _                                (x2z)/rSLS,
     _                                xCdip,yCdip,zCdip)
                              endif


c     if(1.lt.iv.and.iv.lt.5.and.jv.eq.0)then
c     print*,'TEST out'
c     print*,'x1x,x1y,x1z=',x1x,x1y,x1z
c     print*,'x2x,x2y,x2z=',x2x,x2y,x2z
c     print*,'dd,rSLS*rSLS=',dd,rSLS*rSLS
c     endif

c                              sigma(ipar,icc)= sigma(ipar,icc)+ ss2
c                              dmean(ipar,icc)= dmean(ipar,icc)+ ss
c                              sfreq(ipar,icc)= sfreq(ipar,icc) +1.0

                              sig_th(ithdodec)= sig_th(ithdodec)+ ss2
                              dme_th(ithdodec)= dme_th(ithdodec)+ ss
                              sfr_th(ithdodec)= sfr_th(ithdodec) +1.0

                              if(abs(dd).gt.1.0)print*,'WARNING,',
     _                             'dd = ',dd,' > 1.0 '

c                              corr(ipar,icc)=corr(ipar,icc)
cc     _                             + wt*wt*tt1*tt2
c     _                             + 2.0* tt1*tt2 ! CSS98b
c                              cdopp(ipar,icc)=cdopp(ipar,icc)
cc     _                             + wt*wt*tt1*tt2 *dd
c     _                             + 2.0*tt1*tt2 *dd !*dd !  cf. CSS98b
cc     cwght(ipar,icc)=cwght(ipar,icc)+ yy
cc     CSS98b version:
c                              cwght(ipar,icc)=cwght(ipar,icc)+
c     _                             tt1*tt1+tt2*tt2



                              cor_th(ithdodec)=cor_th(ithdodec)
c     _                             + wt*wt*tt1*tt2
     _                             + 2.0* tt1*tt2 ! CSS98b
                              cdo_th(ithdodec)=cdo_th(ithdodec)
c     _                             + wt*wt*tt1*tt2 *dd
     _                             + 2.0*tt1*tt2 *dd !*dd !  cf. CSS98b
c     cwg_th(ithdodec)=cwg_th(ithdodec)+ yy
c     CSS98b version:
                              cwg_th(ithdodec)=cwg_th(ithdodec)+
     _                             tt1*tt1+tt2*tt2


c     ---- v0.1.34 ----
                              sfr_iv(ithdodec,iv,jv)=
     _                             sfr_iv(ithdodec,iv,jv)+ 1.0
                              cor_iv(ithdodec,iv,jv)=
     _                             cor_iv(ithdodec,iv,jv)+
     _                             2.0* tt1*tt2 ! CSS98b
                              cwg_iv(ithdodec,iv,jv)=
     _                             cwg_iv(ithdodec,iv,jv)+
     _                             tt1*tt1+tt2*tt2



c                              if(test)print*,'666: '

c     ---- pair list for KS test ----
c                              npair(ipar,icc)=npair(ipar,icc)+1
                              npa_th(ithdodec)=npa_th(ithdodec)+1
                              itd=itd+1
c                              dlist(npair(ipar,icc),idlist,icc)= ss
                              dlist(npa_th(ithdodec),idlist,icc)= ss

c                              if(cdoppwarn.and.(abs(cdopp(ipar,icc))
c     _                             .gt.abs(corr(ipar,icc))))
c     _                             print*,'WARNING,',
c     _                             'cdopp(ipar,icc),corr(ipar,icc)='
c     _                             ,cdopp(ipar,icc),corr(ipar,icc)
                              if(cdoppwarn.and.(abs(cdo_th(ithdodec))
     _                             .gt.abs(cor_th(ithdodec))))
     _                             print*,'WARNING,',
     _                             'cdo_th(ithdodec),cor_th(ithdodec)='
     _                             ,cdo_th(ithdodec),cor_th(ithdodec)


                              j=j+1
                              ttmean=ttmean+tt1+tt2
                              ttvar=ttvar + tt1*tt1 +tt2*tt2

                           else
c                              if(irun.eq.1.and.icc.eq.1)then
                              if(dowrcircles.or.docS3)then
c     ---- 0.1.58  25.12.2006 - reset null value to -9.99e9 (old: -9.99)
                                 tt1c(ith,iv,jv)= -9.99e9
                                 tt2c(ith,iv,jv)= -9.99e9
                                 s1c(ith,iv,jv)= -9.99e9
                                 s2c(ith,iv,jv)= -9.99e9
                              endif
                           endif !  if( (.not.(galcut).or. ...

c                           if(test)print*,'777: '
                        enddo   ! do ith=1,nth

c                        print*,'iv,jv,nth,npair(ipar,icc)=',
c     _                       iv,jv,nth,npair(ipar,icc)
                        if(test)
     _                       print*,'iv,jv,nth,npa_th(ithdodec)=',
     _                       iv,jv,nth,npa_th(ithdodec)


ccc   endif      ! if(nth.ge.1)then

                     endif      ! if(ivjv_valid)then

cc                     endif      !                     if(vmod.lt.rSLS/2.0)then
cc                  endif         ! if(iv.gt.0.or.jv.gt.0)then

               enddo         ! do jv=0,jmax
            enddo               ! do iv=-imax,imax

c            print*,':: irun,nth,npair(ipar,icc)=',
c     _           irun,nth,npair(ipar,icc)

            if(10000*((irun-1)/10000).eq.irun)then
               print*,':: irun,nth,npa_th(ithdodec)=',
     _              irun,nth,npa_th(ithdodec)
            endif

            ttmean=ttmean/2.0/real(j)

            ttvar= ttvar/2.0/real(j-1) - ttmean*ttmean
            ttsd=-99.9
            if(ttvar.gt.0.0)ttsd=sqrt(ttvar)

            if(test)then
               print*,'j=',j,' npix*(npix+1)/2=',npix*(npix-1)/2
               npix8=npix ! npix is the default integer type; may be 32-bit
               print*,'j=',j,' npix8*(npix8+1)/2=',npix8*(npix8-1)/2
               print*,'zero point stats: ttmean,ttvar,ttsd=',
     _              ttmean,ttvar,ttsd

               print*,'unnormalised: dmean(.,icc)=',
     _              (dme_th(ithdodec))
               print*,'unnormalised: sigma(.,icc)=',
     _              (sig_th(ithdodec))
               print*,'unnormalised: corr(.,icc)=',
     _              (cor_th(ithdodec))
               print*,'unnormalised: cdopp(.,icc)=',
     _              (cdo_th(ithdodec))
               print*,'sfreq=',(sfr_th(ithdodec))
               print*,'cwght=',(cwg_th(ithdodec))
               print*,' '
            endif

c            do ipar=1,npar

c            if(sfreq(ipar,icc).gt.0.001)then
c               dmean(ipar,icc)=dmean(ipar,icc)/sfreq(ipar,icc)
c               corr(ipar,icc)=corr(ipar,icc)/cwght(ipar,icc)
c               cdopp(ipar,icc)=cdopp(ipar,icc)/cwght(ipar,icc)
c
c               sigma(ipar,icc)=sigma(ipar,icc)/sfreq(ipar,icc)
c            endif
            if(sfr_th(ithdodec).gt.0.001)then
               dme_th(ithdodec)=dme_th(ithdodec)/sfr_th(ithdodec)
               cor_th(ithdodec)=cor_th(ithdodec)/cwg_th(ithdodec)
               cdo_th(ithdodec)=cdo_th(ithdodec)/cwg_th(ithdodec)

               sig_th(ithdodec)=sig_th(ithdodec)/sfr_th(ithdodec)
            endif
            do iv=iv1,iv2,ivdel ! modified 19/12/99 for BASI article
               do jv=0,jmax,ivdel
                  if(sfr_iv(ithdodec,iv,jv).gt.0.001)then
                     cor_iv(ithdodec,iv,jv)=cor_iv(ithdodec,iv,jv)/
     _                    cwg_iv(ithdodec,iv,jv)
                  endif
               enddo
            enddo


c            tol=1e-4
c            if(abs(sigma(ipar,icc)-sig_th(ithdodec)).gt.tol)then
c               print*,'## ',irun,sigma(ipar,icc),sig_th(ithdodec)
c            endif


c     if(ttsd.gt.0.0)then
c     cdopp(ipar,icc)=cdopp(ipar,icc)/ttmean/ttmean -1.0
c     corr(ipar,icc)=corr(ipar,icc)/ttmean/ttmean -1.0
c     endif

c            enddo

c     ---- if (do dodec) then write out only if it's the last ithdodec value ----
            write_dodec= dododec.and.
     _           (doallth.or.(ithdodec.eq.nthdomax))

            if(test.and.irun.le.8)
     _           print*,'A-- sigma::',
     _           irun,ithdodec,sig_th(1),sig_th(2)

c            if(2+2.eq.5.and.(.not.(dododec)))then
            if(.not.(dododec))then
               ipar=irun
               dmean(ipar,icc)=dme_th(ithdodec)
               sigma(ipar,icc)=sig_th(ithdodec)
               corr(ipar,icc)= cor_th(ithdodec)
               cdopp(ipar,icc)=cdo_th(ithdodec)

               sfreq(ipar,icc)=sfr_th(ithdodec)
               cwght(ipar,icc)=cwg_th(ithdodec)

               npair(ipar,icc)=npa_th(ithdodec)
            elseif(write_dodec)then
               ithcmax=1 ! maximise corr
               ithsmin=1 ! minimise sigma
               if(.not.(doallth))then
                  cmax= -9.9e9
                  smin= +9.9e9
                  do ith=1,nthdomax
                     if(cor_th(ith).gt.cmax)then
                        ithcmax=ith
                        cmax=cor_th(ith)
                     endif
                     if(sig_th(ith).lt.smin)then
                        ithsmin=ith
                        smin=sig_th(ith)
                     endif
                  enddo
               else
                  ithcmax=ithdodec     ! maximise corr
                  ithsmin=ithdodec     ! minimise sigma
               endif   !if(.not.(doallth))then


               if(test.and.irun.le.8)then
                  print*,'write_dodec: ipar,ithcmax,ithsmin',
     _                 ipar,ithcmax,ithsmin
               endif
c     ---- sigma min ----
               if(test2.and. 10*((irun-1)/10).eq.irun-1)then
                  print*,'smin irun, nrun, ipar=',irun,nrun,ipar
               endif

               ipar=ipar+1

               if(test.and.irun.le.8)then
                  print*,'A- sigma::',sig_th(1),sig_th(2)
                  print*,'AA sigma::',
     _                 ipar,icc,ithsmin,sig_th(ithsmin),
     _                 sigma(ipar,icc)
               endif

               dmean(ipar,icc)=dme_th(ithsmin)
               sigma(ipar,icc)=sig_th(ithsmin)
               corr(ipar,icc)= cor_th(ithsmin)
               cdopp(ipar,icc)=cdo_th(ithsmin)

               sfreq(ipar,icc)=sfr_th(ithsmin)
               cwght(ipar,icc)=cwg_th(ithsmin)

               npair(ipar,icc)=npa_th(ithsmin)

               do iv=iv1,iv2,ivdel !  ---- v0.1.34  ---
                  do jv=0,jmax,ivdel
                     co_iv(ipar,iv,jv)=cor_iv(ithsmin,iv,jv)
                  enddo
               enddo

               glpar(ipar)= gl_th(ithsmin)
               gbpar(ipar)= gb_th(ithsmin)
               thpar(ipar)= thdod_th(ithsmin)
               angpar(ipar)= ang_th(ithsmin)

               if(isco_)then
                  iscontrol(ipar)=.true.
               else
                  iscontrol(ipar)=.false.
               endif
               if(test2.and. 10*((irun-1)/10).eq.irun-1)then
                  print*,'ipar,isco_ =',irun,isco_
               endif

               if(test.and.irun.le.8)then
                  print*,'AAA sigma::',
     _                 ipar,icc,ithsmin,sig_th(ithsmin),
     _                 sigma(ipar,icc)
               endif
               if(test.and.irun.le.8)then
                  print*,'A sigma: ',ipar,
     _                 ((sigma(j,k),j=1,6),k=1,1)
               endif

c     ---- corr max ----
               if(test2.and. 10*((irun-1)/10).eq.irun-1)then
                  print*,'cmax irun, nrun, ipar=',irun,nrun,ipar
               endif
               ipar=ipar+1
               dmean(ipar,icc)=dme_th(ithcmax)
               sigma(ipar,icc)=sig_th(ithcmax)
               corr(ipar,icc)= cor_th(ithcmax)
               cdopp(ipar,icc)=cdo_th(ithcmax)

               sfreq(ipar,icc)=sfr_th(ithcmax)
               cwght(ipar,icc)=cwg_th(ithcmax)

               npair(ipar,icc)=npa_th(ithcmax)


               do iv=iv1,iv2,ivdel ! ---- v0.1.34  ---
                  do jv=0,jmax,ivdel
                     co_iv(ipar,iv,jv)=cor_iv(ithcmax,iv,jv)
                  enddo
               enddo

               glpar(ipar)= gl_th(ithcmax)
               gbpar(ipar)= gb_th(ithcmax)
               thpar(ipar)= thdod_th(ithcmax)
               angpar(ipar)= ang_th(ithcmax)
               if(test.and.irun.le.8)print*,'ipar = ',ipar
               if(test.and.irun.le.8)then
                  print*,'B sigma: ',ipar,
     _                 ((sigma(j,k),j=1,6),k=1,1)
               endif

               if(isco_)then
                  iscontrol(ipar)=.true.
               else
                  iscontrol(ipar)=.false.
               endif
               if(test2.and. 10*((irun-1)/10).eq.irun-1)then
                  print*,'ipar,isco_ =',irun,isco_
               endif

            endif



            if(sfrbig(icc).gt.1.001)then
c     sigbig(icc)= sqrt( sigbig(icc)/(sfrbig(icc)-1.0) )
               sigbig(icc)= sqrt( sigbig(icc)/(sfrbig(icc)-0.0) )
            endif
            if(test)print*,' '
            if(test)print*,'icc,sigbig(icc)=',icc,sigbig(icc)

            if(test)then
               print*,'NORMALISED: dmean(.,icc)='
     _              ,(dme_th(ithdodec))
               print*,'NORMALISED: sigma(.,icc)='
     _              ,(sig_th(ithdodec))
               print*,'NORMALISED: corr(.,icc)='
     _              ,(cor_th(ithdodec))
               print*,'NORMALISED: cdopp(.,icc)='
     _              ,(cdo_th(ithdodec))

            endif


         enddo                  !             do icc=1,ncc

         if(ipar.eq.1)then
            ch3='_RE'
         else
            write(ch3,'(i3)')ipar-1
            if(ch3(1:1).eq.' ')ch3(1:1)='_'
            if(ch3(2:2).eq.' ')ch3(2:2)='_'
         endif


         if(ipar.le.mxwrite.and.
     _        ( (.not.(dododec)).or. write_dodec)
     _        )then
c     ---- write sigma's to file ----
            open(1,file='sigma'//ch3//'.dat',access='sequential',
     _           form='unformatted',status='unknown')
            write(1)glong,galb,thetadeg,Rfund(1),Rfund(2),Rfund(3),
     _           gl1_3,gb1_3,gl2_3,gb2_3,xISW,itd
c     --- WARNING: potential conflict in size of  drsigma   with plotsig.f --
c         write(1)ncc,cc1,cc2,H_0,drSLS,npar,drsigma,maxpair
c         write(1)dmean
c         write(1)sigma
c         write(1)sfreq
c         write(1)corr
c         write(1)cdopp
c         write(1)cwght
c         write(1)npair

            ipar1=ipar
            ipar2=ipar
            write(1)ncc,cc1,cc2,H_0,drSLS,ipar1,ipar2,maxpair

            write(1)((dmean(i,icc),i=ipar1,ipar2),icc=1,maxcurv)
            write(1)((sigma(i,icc),i=ipar1,ipar2),icc=1,maxcurv)
            write(1)((sfreq(i,icc),i=ipar1,ipar2),icc=1,maxcurv)
            write(1)(( corr(i,icc),i=ipar1,ipar2),icc=1,maxcurv)
            write(1)((cdopp(i,icc),i=ipar1,ipar2),icc=1,maxcurv)
            write(1)((cwght(i,icc),i=ipar1,ipar2),icc=1,maxcurv)
            write(1)((npair(i,icc),i=ipar1,ipar2),icc=1,maxcurv)

            write(1)(((dlist(ip,i,icc),ip=1,maxpair),
     _           i=1,mxdlist),icc=1,ncc)

c     --- v0.1.34 ---
            write(1)iv1,iv2,ivdel,jmax,ivdel,nangle
c            print*,'circles_f77: iv1,iv2,ivdel,jmax,ivdel=',
c     _           iv1,iv2,ivdel,jmax,ivdel
            write(1)(((co_iv(i,iv,jv),i=ipar1,ipar2),
     _           iv=iv1,iv2,ivdel),
     _           jv=0,jmax,ivdel)
c            print*,'circles_f77: (co_iv(1,iv,0),iv=1,6)=',
c     _           (co_iv(1,iv,0),iv=1,6)

            close(1)


            if(test.and.irun.le.8)then
               print*,'C sigma: ',ipar,
     _              ((sigma(j,k),j=1,6),k=1,1)
            endif

         endif

         if(test)then
            print*,'TEST B nthc(1,0),glatc(1,1,0),glongc(1,1,0)='
            print*,nthc(1,0),glatc(1,1,0),glongc(1,1,0)
         endif



c     ---- write circle data to file ----
c         if(ipar.le.2.and.ipar.le.mxwrite.and.
c     _        ( (.not.(dododec)).or. write_dodec)
c     _        )then
c         if(irun.eq.1)then
         if(dowrcircles)then
            nparcircles= 1
            if(test3)print*,'Will write file: ',
     _           'circleinfo'//ch3//'.dat'
            open(1,file='circleinfo'//ch3//'.dat',
     _           access='sequential',
     _           form='unformatted',status='unknown')
c            write(1)ncc,cc1,cc2,H_0,drSLS,npar,drsigma
c            write(1)ncc,cc1,cc2,H_0,drSLS,nparcircles,drsigma !24.02.2004
            write(1)ncc,cc1,cc2,H_0,drSLS,nparcircles
            write(1)iimax,jjmax,maxtheta, imax,jmax
            write(1)nthc,glatc,glongc,wmodc,rSLSc,dthMpc
            write(1)tt1c,tt2c,s1c,s2c
c     ---- 24.02.2004 ----
            write(1)glpix1,gbpix1,glpix2,gbpix2
            close(1)
         endif

c     TODO: corrS3 should not need to read in from a file, it
c     should be able to use memory directly.
c
c     --- version 0.1.58  - 3D correlation in S^3
c         if(iwcS3.eq.1.and.dowrcircles)then
         if(test4)print*,'irun,iwcS3,docS3=',irun,iwcS3,docS3

         if(docS3)then
            print*,' '
            print*,'circles_f77: iwcS3, angpar(ipar),ang_th(1)=',
     _           iwcS3, angpar(ipar),ang_th(1)

            if(test3)then
               print*,'imax,jmax=',imax,jmax
               print*,'ncc,cc1,cc2,nsig=',ncc,cc1,cc2,nsig

               print*,'nthc(1,2),glatc(1,1,0),glongc(1,1,0)='
               print*,nthc(1,2),glatc(1,1,0),glongc(1,1,0)
               print*,'nthc(1,0),glatc(1,1,0),glongc(1,1,0)='
               print*,nthc(1,0),glatc(1,1,0),glongc(1,1,0)
               print*,'nthc(2,0),glatc(1,1,0),glongc(1,1,0)='
               print*,nthc(2,0),glatc(1,1,0),glongc(1,1,0)
            endif
            print*,'  calling corrS3...'

            call corrS3(indir,nindir,
     _           angpar(ipar),
     _           iverb,nowarn,
c     _           ang_th(1),
     _           galcut,bcut,GCmin,
     _           ncc,cc1,cc2,H_0,drSLS, ipar,
     _           iimax,jjmax,maxtheta, imax,jmax,
     _           nthc,glatc,glongc,wmodc,rSLSc,dthMpc,
     _           tt1c,tt2c,s1c,s2c,
     _           glpix1,gbpix1,glpix2,gbpix2,
     _           temp,errtemp,gallong,gallat,
     _           twistorient,   !  since 0.1.75
     _           oneaxis,       ! since 0.2.19
     _           pmcnew)
         endif

c     ---- if mcmc then update parameters and decide whether to
c     move or not ----
         if(mcmc)then
            if(test4)print*,'mcmc: irun,pmcold,pmcnew= ',
     _           irun,pmcold,pmcnew

            if(irun.eq.1)pmcold= pNULL

c     ---- REVERT CONDITION: revert to the old point in parameter space if
c     -        the new point is at lower likelihood and
c     -        the relative likelihood  qq  fails a random check
            p=rnd(0)
c            qq= pmcnew/(max(tolpmc, pmcold))
            qq= max(tolqq, max(tolpmc,pmcnew)/(max(tolpmc, pmcold)) )

            if(test4)print*,'mcmc: irun,nrun,p,qq=',irun,nrun,p,qq

c     -- if proposal( ) is symmetric then proposal is not needed
c     q= q* proposal(gl_pre,gb_pre,th_pre,angpre,
c     _        glong,galb,thdodec,ang)

c     --- decision to move is rejected if new likelihood is lower than
c     the present .and. a random sample point says it is unlikely ---
            if(pmcnew.lt.pmcold .and. p.gt.qq)then
               accepted(ipar)= .false.
               glpar(ipar)= glpar(ipar-2)
               glpar(ipar-1)= glpar(ipar-3)
               gbpar(ipar)= gbpar(ipar-2)
               gbpar(ipar-1)= gbpar(ipar-3)
               thpar(ipar)= thpar(ipar-2)
               thpar(ipar-1)= thpar(ipar-3)
               angpar(ipar)= angpar(ipar-2)
               angpar(ipar-1)= angpar(ipar-3)
               if(test4)print*,'mcmc: no move - irun ',irun
            else                ! if the new point in parameter space is accepted
               accepted(ipar)= .true.
               pmcold=pmcnew
               glhiold=glhi
               gbhiold=gbhi
               twipre=twistorient
            endif

            if(ipar.ge.20)then
               aa=0.0
               n= 0
               do ip=ipar-18,ipar,2
                  if(accepted(ip))aa= aa+ 1.0
                  n=n+1
               enddo
               if(n.gt.0)aa= aa/real(n)
               if(test4)
     _              write(6,'(a,f8.3)')
     _              'mcmc: acceptance rate: ',aa
            endif

            if(test4)write(6,'(a,f13.3,f9.3,2f8.3,f7.1,2f7.2,g11.3)')
     _           'mcmc: pars= ',glpar(ipar),gbpar(ipar),
     _           glhiold,gbhiold,
     _           thpar(ipar),
     _           angpar(ipar),
     _           mod(twipre+20.0,10.0),pmcold

            if(nstatu.gt.0 .and.
     _           mod(irun,nr_write)+1.eq.1
     _           )then
c               open(i_mcfile,file=mclog,access='sequential',
c     _              status='unknown')
               open(i_mcfile,file=mclog,access='direct',
     _              form='formatted',recl=84,
     _              status='unknown')

c     echo "12 18+ 16+ 21+ 11+ 2+ p" |dc
c     --- warning: any previous content of file is NOT erased ---
               write(i_mcfile,
     _              '(a12,f13.3,f9.3,2f8.3,f7.1,2f7.2,g11.3,a2)',
     _              rec=irun)   !  (ipar leaves blank lines, doubles file size)
     _              'mcmc: pars= ',glpar(ipar),gbpar(ipar),
     _              glhiold,gbhiold,
     _              thpar(ipar),
     _              angpar(ipar),
     _              mod(twipre+20.0,10.0),pmcold, eol
               close(i_mcfile)

c     ---- one line status for restarts ----
               open(1,file=statu2,access='sequential',
     _              status='unknown',
     _              err=8881)
c     --- old content of status file is ignored here ---
               write(1,'(5f13.4)',err=8881)
     _              glpar(ipar),
     _              gbpar(ipar),
     _              thpar(ipar),
     _              angpar(ipar),
     _              twipre
               close(1)
 8881          continue
            endif

c     TODO: burn-in


c     STOP CONDITION
c     if(irun.ge.20)mc2done=.true.  !! test only !!
c     if(irun.ge.6)mc2done=.true.  !! test only !!
            if(irun.eq.mc2stop)mc2done= .true.  ! as of version 0.2.0

            if(test4)print*,'mcmc: end of iteration - mc2done= ',
     _           mc2done

         endif   ! if(mcmc)then

      enddo            !     do while(i+1.le.nrun .and.  !  do irun=1,nrun


      if(nstatu.gt.0)close(i_mcfile)

      if(test2)print*,'after enddo do irun loop: nrun,ipar=',
     _     nrun,ipar

      npar=ipar

c     ---- final file ----
      ch3='all'
      open(1,file='sigma'//ch3//'.dat',access='sequential',
     _     form='unformatted',status='unknown')
      write(1)glong,galb,thetadeg,Rfund(1),Rfund(2),Rfund(3),
     _     gl1_3,gb1_3,gl2_3,gb2_3,xISW,itd

      ipar1=1
      ipar2=npar
      write(1)ncc,cc1,cc2,H_0,drSLS,ipar1,ipar2,maxpair

      write(1)((dmean(i,icc),i=ipar1,ipar2),icc=1,maxcurv)
      write(1)((sigma(i,icc),i=ipar1,ipar2),icc=1,maxcurv)
      write(1)((sfreq(i,icc),i=ipar1,ipar2),icc=1,maxcurv)
      write(1)(( corr(i,icc),i=ipar1,ipar2),icc=1,maxcurv)
      write(1)((cdopp(i,icc),i=ipar1,ipar2),icc=1,maxcurv)
      write(1)((cwght(i,icc),i=ipar1,ipar2),icc=1,maxcurv)
      write(1)((npair(i,icc),i=ipar1,ipar2),icc=1,maxcurv)

      write(1)(glpar(i),i=ipar1,ipar2)
      write(1)(gbpar(i),i=ipar1,ipar2)
      write(1)(thpar(i),i=ipar1,ipar2)
      write(1)(angpar(i),i=ipar1,ipar2)
      write(1)(iscontrol(i),i=ipar1,ipar2)

      if(test)then
         print*,'dme',((dmean(i,icc),i=ipar1,ipar2),icc=1,maxcurv)
         print*,'sig',((sigma(i,icc),i=ipar1,ipar2),icc=1,maxcurv)
         print*,'sfr',((sfreq(i,icc),i=ipar1,ipar2),icc=1,maxcurv)
         print*,'cor',(( corr(i,icc),i=ipar1,ipar2),icc=1,maxcurv)
         print*,'cdo',((cdopp(i,icc),i=ipar1,ipar2),icc=1,maxcurv)
         print*,'cwg',((cwght(i,icc),i=ipar1,ipar2),icc=1,maxcurv)
         print*,'npa',((npair(i,icc),i=ipar1,ipar2),icc=1,maxcurv)
      endif

      write(1)(((dlist(ip,i,icc),ip=1,maxpair),
     _     i=1,mxdlist),icc=1,ncc)

c     --- v0.1.34 ---
      write(1)iv1,iv2,ivdel,jmax,ivdel,nangle
      write(1)(((co_iv(i,iv,jv),i=ipar1,ipar2),
     _     iv=iv1,iv2,ivdel),
     _     jv=0,jmax,ivdel)

      close(1)


      end

c     !! dot( ) moved to rdCOBE.f
c     !! dotv, crossv, orth_axes to orthaxes.f
